#!/bin/bash

configFile=${1:-}
nbRuns=${2:-1}
executionMode=${3:-"normal"}
memoryLimit=32G
#resultsPath=$(pwd)/results
resultsPathInContainer=/home/user/results
finalresultsPath=$(pwd)/results
finalresultsPathInContainer=/home/user/finalresults
#imageName=${4:-"localhost:5000/daccadqd"} #daccadqd
imageName=${4:-"daccadqd"} #daccadqd
uid=$(id -u)
confPath=$(pwd)/conf
confPathInContainer=/home/user/daccadqd/conf
priorityParam="-c 128"

if [ ! -d $finalresultsPath ]; then
    mkdir -p $finalresultsPath
fi

inDockerGroup=`id -Gn | grep docker`
if [ -z "$inDockerGroup" ]; then
    sudoCMD="sudo"
else
    sudoCMD=""
fi
dockerCMD="$sudoCMD docker"

if [ -d "$confPath" ]; then
    confVolParam="-v $confPath:$confPathInContainer"
else
    confVolParam=""
fi

if [ "$executionMode" = "normal" ]; then
    #exec $dockerCMD run -i -m $memoryLimit --rm $priorityParam -v $resultsPath:$resultsPathInContainer $confVolParam $imageName  "$uid" "normal" "-c $configFile --runs $nbRuns"
    exec $dockerCMD run -i -m $memoryLimit --rm $priorityParam --mount type=tmpfs,tmpfs-size=8589934592,target=$resultsPathInContainer --mount type=bind,source=$finalresultsPath,target=$finalresultsPathInContainer  $confVolParam $imageName  "$uid" "normal" "-c $configFile --runs $nbRuns"
elif [ "$executionMode" = "cluster" ]; then
    # Create host file
    hostsNames=${5:-"shoal1,shoal2,shoal3,shoal4,shoal5,shoal6,shoal7,shoal8"}
    hostsFilename=$resultsPath/hosts
    declare -a hosts
    IFS="," read -r -a hosts <<< "$hostsNames"
    echo -n > $hostsFilename
    for h in "${hosts[@]}"; do
        echo "daccadqd-$h 8" >> $hostsFilename # Only one thread per host, to launch daccad
    done

    # Create network
    $dockerCMD network create --driver=overlay --attachable net-daccadqd
    sleep 1

    # Launch a client on each node
    for h in "${hosts[@]}"; do
        name=daccadqd-$h
        rm $resultsPath/stop-$h
        #ssh $h "$dockerCMD run -di --name $name --network net-daccadqd -m $memoryLimit --rm -v $resultsPath:$resultsPathInContainer $confVolParam  $imageName $uid client $resultsPathInContainer/stop-$h"
        ssh $h "$dockerCMD run -di --name $name --network net-daccadqd -m $memoryLimit --rm --mount type=tmpfs,tmpfs-size=8589934592,target=$resultsPathInContainer --mount type=bind,source=$finalresultsPath,target=$finalresultsPathInContainer $confVolParam  $imageName $uid client $resultsPathInContainer/stop-$h"
    done
    sleep 1

    # Launch a server node
    #$dockerCMD run -i --name daccadqd-main --network net-daccadqd -m $memoryLimit --rm -v $resultsPath:$resultsPathInContainer $confVolParam  $imageName "$uid" "server" $resultsPathInContainer/hosts "-c $configFile -p scoop --runs $nbRuns"
    $dockerCMD run -i --name daccadqd-main --network net-daccadqd -m $memoryLimit --rm --mount type=tmpfs,tmpfs-size=8589934592,target=$resultsPathInContainer --mount type=bind,source=$finalresultsPath,target=$finalresultsPathInContainer $confVolParam  $imageName "$uid" "server" $resultsPathInContainer/hosts "-c $configFile -p scoop"

    # Quit all client nodes
    for h in "${hosts[@]}"; do
        echo -n > $resultsPath/stop-$h
    done

    # Remove network
    $dockerCMD network rm net-daccadqd
fi

# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
