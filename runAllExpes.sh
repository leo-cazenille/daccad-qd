#!/bin/bash

configurationDir=conf/
logsDir=logs/

mkdir -p $configurationDir
mkdir -p $logsDir

function runExpeScoop {
    local conf
    local nbRuns
    conf=$1
    nbRuns=$2
    shift; shift;

    mkdir -p $logsDir/$conf
    #for i in $(seq 1 $nbRuns); do
    #    ./runDockerExpe.sh $configurationDir/$conf.yaml cluster localhost:5000/daccadqd "shoal1,shoal2,shoal3,shoal4,shoal5,shoal6,shoal7,shoal8" | tee $logsDir/$conf/$i.log
    #    sleep 4
    #done

    ./runDockerExpe.sh $configurationDir/$conf.yaml $nbRuns cluster localhost:5000/daccadqd "shoal1,shoal2,shoal3,shoal4,shoal5,shoal6,shoal7,shoal8" | tee $logsDir/$conf/$i.log
    sleep 2
}


function runExpe {
    local conf
    local nbRuns
    conf=$1
    nbRuns=$2
    shift; shift;

    mkdir -p $logsDir/$conf

    ./runDockerExpe.sh $configurationDir/$conf.yaml $nbRuns normal localhost:5000/daccadqd | tee $logsDir/$conf/`hostname`$i.log
    sleep 2
}

#runExpe testDACCAD 1
#runExpe daccad5bottom1000-nTemplate1-20_ratioIn-ME19x20-500x50x50-DaccadUniformStabilitiesSobolWithVaryingSparsityConnections0.92x1x5x500-DaccadPolynomialMutationWithPolynomialSparsityBudget2_0.70x0.10x0.05x0.10x0.05x19x19x20 1
#runExpe daccad5right1000-nTemplate1-20_ratioIn-ME19x20-500x50x50-DaccadUniformStabilitiesSobolWithVaryingSparsityConnections0.92x1x5x500-DaccadPolynomialMutationWithPolynomialSparsityBudget2_0.70x0.10x0.05x0.10x0.05x19x19x20 1
#runExpe daccad5top1000-nTemplate1-20_ratioIn-ME19x20-500x50x50-DaccadUniformStabilitiesSobolWithVaryingSparsityConnections0.92x1x5x500-DaccadPolynomialMutationWithPolynomialSparsityBudget2_0.70x0.10x0.05x0.10x0.05x19x19x20 1
#runExpe daccad5center4000-nTemplate1-20_ratioIn-ME19x20-500x50x50-DaccadUniformStabilitiesSobolWithVaryingSparsityConnections0.92x1x5x500-DaccadPolynomialMutationWithPolynomialSparsityBudget2_0.70x0.10x0.05x0.10x0.05x19x19x20 1

#runExpe daccad5bottom1000-nTemplate1-20_ratioIn-ME19x20-6000x480x50-DaccadUniformStabilitiesSobolWithVaryingSparsityConnections0.92x1x5x6000-DaccadPolynomialMutationWithPolynomialSparsityBudget2_0.70x0.10x0.05x0.10x0.05x19x19x20 1
#runExpe daccad5right1000-nTemplate1-20_ratioIn-ME19x20-6000x480x50-DaccadUniformStabilitiesSobolWithVaryingSparsityConnections0.92x1x5x6000-DaccadPolynomialMutationWithPolynomialSparsityBudget2_0.70x0.10x0.05x0.10x0.05x19x19x20 1
#runExpe daccad5top1000-nTemplate1-20_ratioIn-ME19x20-6000x480x50-DaccadUniformStabilitiesSobolWithVaryingSparsityConnections0.92x1x5x6000-DaccadPolynomialMutationWithPolynomialSparsityBudget2_0.70x0.10x0.05x0.10x0.05x19x19x20 1
#runExpe daccad5center4000-nTemplate1-20_ratioIn-ME19x20-18000x1440x50-DaccadUniformStabilitiesSobolWithVaryingSparsityConnections0.92x1x5x18000-DaccadPolynomialMutationWithPolynomialSparsityBudget2_0.70x0.10x0.05x0.10x0.05x19x19x20 1



#runExpe daccad5top1000-nTemplate10-20_ratioIn-ME10x30-2500x50-DaccadSparseInitAndMut500x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x19x19x20x20.x0.4-onlyAcceptValid 1
#runExpe daccad5right1000-nTemplate10-20_ratioIn-ME10x30-2500x50-DaccadSparseInitAndMut500x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x19x19x20x20.x0.4-onlyAcceptValid 1
#runExpe daccad5bottom1000-nTemplate10-20_ratioIn-ME10x30-2500x50-DaccadSparseInitAndMut500x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x19x19x20x20.x0.4-onlyAcceptValid 1
#runExpe daccad5center4000-nTemplate10-20_ratioIn-ME10x30-7500x50-DaccadSparseInitAndMut500x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x19x19x20x20.x0.4-onlyAcceptValid 1


#runExpe daccad5bottom1000-nTemplate5-25_ratioIn-ME20x25-2500x50-DaccadSparseInitAndMut500x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x20x20x25x25.x0.4-onlyAcceptValid 1
#runExpe daccad5top1000-nTemplate5-25_ratioIn-ME20x25-2500x50-DaccadSparseInitAndMut500x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x20x20x25x25.x0.4-onlyAcceptValid 1
#runExpe daccad5right1000-nTemplate5-25_ratioIn-ME20x25-2500x50-DaccadSparseInitAndMut500x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x20x20x25x25.x0.4-onlyAcceptValid 1
#runExpe daccad5center4000-nTemplate5-25_ratioIn-ME20x25-7500x50-DaccadSparseInitAndMut500x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x20x20x25x25.x0.4-onlyAcceptValid 1

#runExpe daccad5bottom1000-nTemplate5-25_ratioIn-ME20x25-2500x50-DaccadSparseInitAndMut500x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x20x20x25x25.x0.4 1
#runExpe daccad5top1000-nTemplate5-25_ratioIn-ME20x25-2500x50-DaccadSparseInitAndMut500x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x20x20x25x25.x0.4 1
#runExpe daccad5right1000-nTemplate5-25_ratioIn-ME20x25-2500x50-DaccadSparseInitAndMut500x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x20x20x25x25.x0.4 1
#runExpe daccad5center4000-nTemplate5-25_ratioIn-ME20x25-7500x50-DaccadSparseInitAndMut500x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x20x20x25x25.x0.4 1

#runExpe daccad5bottom1000-nTemplate15-30_ratioIn-ME15x25-2500x50-DaccadSparseInitAndMut2_500x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x20x20x30x20.x0.4 1
#runExpe daccad5top1000-nTemplate15-30_ratioIn-ME15x25-2500x50-DaccadSparseInitAndMut2_500x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x20x20x30x20.x0.4 1
#runExpe daccad5right1000-nTemplate15-30_ratioIn-ME15x25-2500x50-DaccadSparseInitAndMut2_500x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x20x20x30x20.x0.4 1
#runExpe daccad5center4000-nTemplate15-30_ratioIn-ME15x25-7500x50-DaccadSparseInitAndMut2_500x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x20x20x30x20.x0.4 1

#runExpe daccad5bottom1000-nTemplate15-30_ratioIn-ME15x25-30000x50-DaccadSparseInitAndMut2_6000x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x20x20x30x20.x0.4 1
#runExpe daccad5top1000-nTemplate15-30_ratioIn-ME15x25-30000x50-DaccadSparseInitAndMut2_6000x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x20x20x30x20.x0.4 1
#runExpe daccad5right1000-nTemplate15-30_ratioIn-ME15x25-30000x50-DaccadSparseInitAndMut2_6000x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x20x20x30x20.x0.4 1
#runExpe daccad5center4000-nTemplate15-30_ratioIn-ME15x25-90000x50-DaccadSparseInitAndMut2_12000x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x20x20x30x20.x0.4 1

#runExpe daccad10center4000-nTemplate15-30_ratioIn-ME15x25-7500x50-DaccadSparseInitAndMut2_500x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x20x20x30x20.x0.4 1
#runExpe daccad5center4000_2trials-nTemplate15-30_ratioIn-ME15x25-7500x50-DaccadSparseInitAndMut2_500x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x20x20x30x20.x0.4 1

#runExpe daccad10bottom1000medianScore_2trials-nTemplate5-20_ratioIn-ME15x25-2500x50-DaccadSparseInitAndMut2_500x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x10x10x20x20.x0.4 1
#runExpe daccad10top1000medianScore_2trials-nTemplate5-20_ratioIn-ME15x25-2500x50-DaccadSparseInitAndMut2_500x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x10x10x20x20.x0.4 1
#runExpe daccad10right1000medianScore_2trials-nTemplate5-20_ratioIn-ME15x25-2500x50-DaccadSparseInitAndMut2_500x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x10x10x20x20.x0.4 1
#runExpe daccad10center4000medianScore_2trials-nTemplate5-20_ratioIn-ME15x25-7500x50-DaccadSparseInitAndMut2_500x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x10x10x20x20.x0.4 1

#runExpe daccad7center4000medianScore_2trials-nTemplate5-20_ratioIn-ME15x25-7500x50-DaccadSparseInitAndMut2_500x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x10x10x20x20.x0.4 1

#runExpe daccad4-7center1000medianScore_2trials-nbNodes_nTemplate5-15_ratioIn-ME4x10x10-7500x50-DaccadSparseInitAndMut3_500x1x0.0x1.0x0.55x0.10x0.05x0.10x0.05x0.10x0.05x10x10x15x20.x0.4 1
#runExpe daccad1-7center1000medianScore_2trials-nbNodes_nTemplate1-15_ratioIn-ME7x15x10-7500x50-DaccadSparseInitAndMut4_1x1x0.0x1.0x0.70x0.05x0.05x0.05x0.05x0.10x0.00x10x10x15x0.2x2.0 1

#runExpe daccad2-7center4000medianScore_2trials-nbNodes_nTemplate1-15_ratioIn-ME6x15x10-7500x50-DaccadSparseInitAndMut4_500x1x0.0x1.0x0.70x0.05x0.05x0.05x0.05x0.10x0.00x10x10x15x20.x0.4 1

#runExpe daccad3-7center1000medianScore_2trials-nbNodes_nbActivations2-8_nbInhibitions0-6-ME5x7x7-7500x50-DaccadTopologicalInit_500x1x0.0x1.0x0.75x0.05x0.05x0.05x0.05x0.05x0.00x8x6x14x0.2.x2.0x0-20 1
#runExpe daccad3-7center1000medianScore_2trials-nbActivations2-8_nbInhibitions0-6_nbNodes_ratioIn-ME6x6x5x5-7500x50-DaccadTopologicalInit_500x1x0.0x1.0x0.75x0.05x0.05x0.05x0.05x0.05x0.00x7x5x12x20.x0.4x0-20 1
#runExpe daccad3-7center1000medianScore_2trials-nbNodes_nbActivations1-7_nbInhibitions0-6-ME5x7x7-7500x50-DaccadSparseInitAndMut5_1x1x0.0x1.0x0.95x0.01x0.01x0.01x0.01x0.01x0.00x7x6x13x20.x0.4 1
#runExpe daccad3-6center4000medianScore_2trials-nbNodes_nbActivations1-6_nbInhibitions0-5-ME4x6x6-7500x50-DaccadBioneatMut_1x1x0.0x1.0x0.96x0.01x0.01x0.01x0.01x6x5x11x0.2.x2.0 1
#runExpe daccad3-6center4000medianScore_2trials-nbNodes_nbActivations1-6_nbInhibitions0-5-ME4x6x6-7500x50-DaccadBioneatMut_1x1x0.0x1.0x0.96x0.01x0.01x0.01x0.01x6x5x11x0.2.x2.0x0.8 1
#runExpe daccad3-6center4000medianScore_2trials-nbNodes_nbActivations1-6_nbInhibitions0-5-ME4x6x6-7500x50-DaccadSparseInitAndMut5_1x1x0.0x1.0x0.95x0.01x0.01x0.01x0.01x0.01x0.00x6x5x11x20.x0.8 1
#runExpe daccad3-6center4000medianScore_2trials-nTemplate1-11-ME11x10-7500x50-DaccadSparseInitAndMut5_1x1x0.0x1.0x0.95x0.01x0.01x0.01x0.01x0.01x0.00x6x5x11x20.x0.8 1
#runExpe daccad3-6center4000medianScore_2trials-nTemplate1-11-ME11x10-7500x50-DaccadBioneatMut2_1x1x0.0x1.0x0.96x0.01x0.01x0.01x0.01x6x5x11x0.2.x2.0x0.8x20 1
#runExpe daccad3-7center4000medianScore_2trials-nTemplate2-12_ratioIn-ME11x10-7500x50-DaccadTopologicalInit2_500x1x0.0x1.0x0.75x0.05x0.05x0.05x0.05x0.05x0.00x7x5x12x20.x0.8x0-20x20 1

#runExpe daccad7center4000medianScore_2trials-nTemplate5-15_ratioIn-ME10x25-7500x50-DaccadSparseInitAndMut2_500x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x10x10x20x20.x0.8 1
#runExpe daccad3-3center4000medianScore_2trials-nTemplate1-7-ME7x25-7500x50-DaccadSparseInitAndMut5_1x1x0.0x1.0x0.96x0.01x0.01x0.01x0.01x0.00x0.00x4x3x7x20.x0.8 1
#runExpe daccad3-3center4000medianScore_2trials-nTemplate1-7-ME7x25-7500x50-DaccadSparseInitAndMut5_1x1x0.0x1.0x0.96x0.01x0.01x0.01x0.01x0.00x0.00x4x3x7x20.x0.8-GD 1
#runExpe daccad3-6center4000medianScore_2trials-nTemplate1-11-ME11x10-7500x50-DaccadSparseInitAndMut5_1x1x0.0x1.0x0.95x0.01x0.01x0.01x0.01x0.01x0.00x6x5x11x20.x0.8-GD 1
#runExpe daccad3-6center4000medianScore_2trials-nTemplate1-11-ME11x10-7500x50-DaccadSparseInitAndMut5_1x1x0.0x1.0x0.95x0.01x0.01x0.01x0.01x0.01x0.00x6x5x11x20.x0.8 1
#runExpe daccad3-7center4000medianScore_2trials-nTemplate1-12_diffInOut-ME12x10-7500x50-DaccadSparseInitAndMut5_1x1x0.0x1.0x0.95x0.01x0.01x0.01x0.01x0.01x0.00x7x5x12x20.x0.8 1

#runExpe daccad3-7center4000medianScore_2trials-nTemplate1-13_ratioIn-ME13x10_10-7500x50-DaccadTopologicalSpecies_1x1x0.0x1.0x0.96x0.01x0.01x0.01x0.00x0.01x0.00x7x6x13x20.x0.8-GD 1
#runExpe daccad3-7center4000medianScore_2trials-ratioIn-ME5_10-7500x50-DaccadTopologicalSpecies_1x1x0.0x1.0x0.96x0.01x0.01x0.01x0.00x0.01x0.00x7x6x13x0.2x2.0x0.8 1
#runExpe daccad3-7center4000medianScore_2trials-nTemplate1-13_ratioIn-ME13x5-7500x50-DaccadSearchMutWithBatchSuggestions_1x1x0.0x1.0x0.96x0.01x0.01x0.01x0.00x0.01x0.00x7x6x13x20.x0.8-GD 1
#runExpe daccad3-7center4000medianScore_2trials-nTemplate1-13_ratioIn-ME13x5-7500x50-DaccadSearchMutWithBatchSuggestions_1x1x0.0x1.0x0.96x0.01x0.01x0.01x0.00x0.01x0.00x7x6x13x20.x0.8 1
#runExpe daccad3-7center4000medianScore_2trials-nTemplate1-13_ratioIn-ME13x5-7500x50-DaccadSearchMutWithSobolBatchSuggestions_1x1x0.0x1.0x0.60x0.10x0.10x0.10x0.10x7x6x13x0.2x2.0x0.8 1

#runExpe daccad3-7center4000medianScore0.0000001_2trials-nTemplate1-13_ratioIn-ME13x10-7500x50-DaccadSearchMutWithBatchSuggestions2_1x1x0.0x1.0x0.60x0.10x0.10x0.10x0.10x7x6x13x0.2x2.0x0.8 1
#runExpe daccad3-7center4000medianScore0.0000001_2trials-nTemplate1-13_ratioIn-ME13x10-7500x50-DaccadSearchMutWithTopologicalSobolBatchSuggestions_1x1x0.3x0.7x0.92x0.02x0.02x0.02x0.02x7x6x13x0.2x2.0x0.8x1-5 1


#runExpe daccad3-7center4000medianScore_5trials-nTemplate1-13-ME13x10-7500x50-DaccadSparseInitAndMut5_1x1x0.0x1.0x0.95x0.01x0.01x0.01x0.01x0.01x0.00x7x6x13x20.x0.8 1

#runExpe daccad3-7center4000medianScore_2trials-nTemplate1-13_ratioIn-ME13x10-7500x50-DaccadBioneatMut3_1x1x0.0x1.0x0.96x0.01x0.01x0.01x0.01x7x6x13x0.2x2.0x0.8 1
#runExpe daccad3-7center4000medianScore_5trials-nTemplate1-13_ratioIn-ME13x10-7500x50-DaccadBioneatMut3_1x1x0.0x1.0x0.96x0.01x0.01x0.01x0.01x7x6x13x0.2x2.0x0.8 1
#runExpe daccad3-7center4000medianScore_5trials-nTemplate1-13_ratioIn-ME13x10-1000x50-DaccadBioneatMut3_1x1x0.0x1.0x0.80x0.05x0.05x0.05x0.05x7x6x13x0.2x2.0x0.8 1
#runExpe daccad3-7center1000medianScore_5trials-nTemplate1-13_ratioIn-ME13x10-2500x50-DaccadBioneatMut3_1x1x0.0x1.0x0.96x0.01x0.01x0.01x0.01x7x6x13x0.2x2.0x0.8 1
#runExpe daccad3-7center1000medianScore_5trials-nTemplate1-13_ratioIn-ME13x10-2500x50-DaccadTopologicalInitBioneat3_1x1x0.1x0.9x0.96x0.01x0.01x0.01x0.01x7x6x13x0.2x2.0x0.8x1-7 1
#runExpe daccad3-7center1000medianScore_5trials-nTemplate1-13_ratioIn-ME13x10-2500x50-DaccadBioneatMut3_1x1x0.0x1.0x0.96x0.01x0.01x0.01x0.01x7x6x13x0.2x2.0x0.8-GD 1
#runExpe daccad3-7center4000medianScore_5trials-nTemplate1-13_ratioIn-ME13x10-2500x50-DaccadTopologicalInitBioneat3_1x1x0.1x0.9x0.96x0.01x0.01x0.01x0.01x7x6x13x0.2x2.0x0.8x1-7-GD 1
#runExpe daccad3-7center4000medianScore_5trials-nTemplate1-13_ratioIn-ME13x10-2500x50-DaccadBioneatMutWithTopologicalBatchSuggestions_1x1x0.1x0.9x0.96x0.01x0.01x0.01x0.01x7x6x13x0.2x2.0x0.8x1-5 1
#runExpe daccad3-7center4000medianScore_5trials-nTemplate1-13_ratioIn-ME13x10-500x50-DaccadBioneatMut3_1x1x0.0x1.0x0.80x0.05x0.05x0.05x0.05x7x6x13x0.2x2.0x0.8-GD 1
#runExpe daccad3-5center4000medianScore_5trials-nTemplate1-8_ratioIn-ME8x5-500x50-DaccadBioneatMut3_1x1x0.0x1.0x0.80x0.05x0.05x0.05x0.05x4x4x8x0.2x2.0x0.8-GD 1
#runExpe daccad3-5center4000medianScore_5trials-nTemplate1-8_ratioIn-ME8x2-250x50-DaccadBioneatMut3_1x1x0.0x1.0x0.96x0.01x0.01x0.01x0.01x4x4x8x0.2x2.0x0.8 1

#runExpe daccad3-7center4000penalty0.08medianScore_5trials-nTemplate1-13_ratioIn-ME13x10-2500x50-DaccadBioneatMut3_1x1x0.0x1.0x0.96x0.01x0.01x0.01x0.01x7x6x13x0.2x2.0x0.8 1
#runExpe daccad3-7center4000medianScore_5trials-nTemplate1-13_ratioIn-ME13x10-2500x50-DaccadBioneatMut3_1x1x0.0x1.0x0.96x0.01x0.01x0.01x0.01x7x6x13x0.2x2.0x0.8 1
#runExpe daccad3-7center4000penalty0.08medianScore_5trials-nTemplate1-8_ratioIn-ME8x3-2500x50-DaccadBioneatMut3_1x1x0.0x1.0x0.96x0.01x0.01x0.01x0.01x4x4x8x0.2x2.0x0.8 1
#runExpe daccad3-7center4000medianScore_5trials-nTemplate1-8_ratioIn-ME8x3-2500x50-DaccadBioneatMut3_1x1x0.0x1.0x0.52x0.12x0.12x0.12x0.12x4x4x8x0.2x2.0x0.8 1
#runExpe daccad3-7center4000medianScore_5trials-nTemplate1-4_ratioIn-ME4x3-2500x50-DaccadBioneatMut3_1x1x0.0x1.0x0.00x0.00x1.00x0.00x0.00x4x4x4x0.2x2.0x0.8-GD 1
#runExpe daccad3-7center4000medianScore_5trials-nTemplate1-8_ratioIn-ME8x3-2500x50-DaccadBioneatMut3_1x1x0.0x1.0x0.52x0.12x0.12x0.12x0.12x8x8x8x0.2x2.0x0.8-GD 1

#runExpe daccad3-7center4000medianScore_5trials-nTemplate1-8_ratioIn-ME8x3-2500x50-DaccadBioneatMut3_1x1x0.0x1.0x0.80x0.05x0.05x0.05x0.05x8x8x8x0.2x2.0x0.8-GD 1
#runExpe daccad3-5center4000medianScore_2trials-nTemplate1-5_ratioIn-ME5x3-2500x50-DaccadBioneatMut3_1x1x0.0x1.0x0.80x0.05x0.05x0.05x0.05x5x4x5x0.2x2.0x0.8-GD0.3 1

#runExpe daccad3-5center4000medianScore_2trials-nTemplate1-5_ratioIn-ME5x10-2500x50-DaccadBioneatMut3_1x1x0.0x1.0x0.80x0.05x0.05x0.05x0.05x5x4x5x0.2x2.0x0.8-GD0.5 1
#runExpe daccad3-7center4000medianScore_2trials-nTemplate1-13_ratioIn-ME13x10-7500x50-DaccadBioneatMut3_1x1x0.0x1.0x0.96x0.01x0.01x0.01x0.01x7x6x13x0.2x2.0x0.8-GD0.3 1
#runExpe daccad3-7center4000medianScore_2trials-nTemplate1-13_ratioIn-ME13x10-30000x50-DaccadBioneatMut3_1x1x0.0x1.0x0.96x0.01x0.01x0.01x0.01x7x6x13x0.2x2.0x0.8-GD0.3 1
#runExpe daccad3-7center4000medianScore_2trials-nTemplate1-8_ratioIn-ME8x3-30000x50-DaccadBioneatMut3_1x1x0.0x1.0x0.80x0.05x0.05x0.05x0.05x8x8x8x0.2x2.0x0.8-GD0.5 1
#runExpe daccad3-7center4000medianScore_2trials-nTemplate1-8_ratioIn-ME8x3-30000x50-DaccadBioneatMut3_1x1x0.0x1.0x0.96.01x0.01x0.01x0.01x8x8x8x0.2x2.0x0.8-GD0.1 1

#runExpe daccad3-7center4000penalty0.05medianScore_2trials-nTemplate1-16_ratioIn-ME16x4-7500x50-DaccadBioneatMut3_1x1x0.0x1.0x0.96.01x0.01x0.01x0.01x8x8x16x0.2x2.0x0.8-GD0.1 1
#runExpe daccad3-7center4000penalty0.05medianScore_2trials-nTemplate1-13_ratioIn-ME13x3-7500x50-DaccadSparseInitAndMut6_1x1x0.0x1.0x0.95x0.01x0.01x0.01x0.01x0.01x0.00x7x6x13x20.x0.8-GD0.3 1
#runExpe daccad3-7center4000medianScore_2trials-nTemplate1-8_ratioIn-ME8x3-500x50-DaccadBioneatMut3_1x1x0.0x1.0x0.96.01x0.01x0.01x0.01x8x8x8x0.2x2.0x0.8-GD0.3 1
#runExpe daccad3-7center4000medianScore_2trials-nTemplate2-8_ratioIn-ME7x3-500x50-DaccadTopologicalInitBioneat3_1x0.9x0.1x1.0x0.96.01x0.01x0.01x0.01x8x8x8x0.2x2.0x0.8x1-3-GD0.5 1
#runExpe daccad3-7center4000medianScore_2trials-nTemplate2-8_ratioIn-ME7x3-3000x50-DaccadTopologicalInitBioneat3_1x0.9x0.1x1.0x0.96.01x0.01x0.01x0.01x8x8x8x0.2x2.0x0.8x1-3-GD0.5 1
#runExpe daccad3-7center4000medianScore_2trials-nTemplate2-4_ratioIn-ME3x6-500x50-DaccadTopologicalInitBioneat3_1x0.9x0.1x1.0x0.96.01x0.01x0.01x0.01x4x4x4x0.2x2.0x0.8x1-3-GD0.5 1
#runExpe daccad3-7center4000medianScore_2trials-nTemplate2-4_ratioIn-ME3x3-500x50-DaccadTopologicalInitBioneat3_1x0.9x0.1x1.0x0.96.01x0.01x0.01x0.01x4x4x4x0.2x2.0x0.8x1-1-GD0.5 1
#runExpe daccad3-7center4000medianScore_10trials-nTemplate1-4_ratioIn-ME4x3-500x50-DaccadTopologicalInitBioneat3_1x0.9x0.1x1.0x0.96.01x0.01x0.01x0.01x4x4x4x0.2x2.0x0.8x1-1-GD0.5 1
#runExpe daccad3-7center4000medianScore_2trials-ratioIn-ME5-500x50-DaccadOnlyThreeNodesTwoTemplates_1x0.9x0.1x0.2x2.0x0.8-GD0.5 1

#runExpe daccad3-7center4000medianScore_2trials-nTemplate2-8_ratioIn-ME7x3-3000x50-DaccadTopologicalInitBioneat3_1x0.9x0.1x1.0x0.96.01x0.01x0.01x0.01x8x8x8x0.2x2.0x0.8x1-8-GD0.5 1
#runExpe daccad3-7center4000medianScore_2trials-nTemplate5-20_ratioIn-ME15x3-3000x50-DaccadTopologicalInitBioneat3_1x0.7x0.3x1.0x0.96.01x0.01x0.01x0.01x20x10x10x0.2x2.0x0.8x5-15-GD0.5 1

#runExpe daccad3-5center4000medianScore_2trials-nTemplate2-4_ratioIn-ME3x3-500x50-DaccadTopologicalInitBioneat3_1x0.7x0.3x1.0x0.96.01x0.01x0.01x0.01x4x4x4x0.2x2.0x0.8x1-1-GD0.5 1
#runExpe daccad3-7center4000medianScore_2trials-nTemplate2-8_ratioIn-ME7x3-500x50-DaccadTopologicalInitBioneat3_1x0.9x0.1x1.0x0.96.01x0.01x0.01x0.01x8x8x8x0.2x2.0x0.8x1-8-GD0.5 1

#runExpe daccad3-7center4000medianScore_2trials-nTemplate1-20_ratioIn-ME20x4-30000x50-DaccadBioneatMut3_1x1x0.0x1.0x0.80x0.05x0.05x0.05x0.05x12x12x20x0.2x2.0x0.8-GD0.5 1
#runExpe daccad3-7center4000medianScore_2trials-nTemplate1-10_ratioIn-ME10x5-3000x50-DaccadBioneatMut4_1x1x0.0x1.0x0.80x0.05x0.05x0.05x0.05x10x10x10x0.2x2.0x0.8-GD0.5 1
#runExpe daccad3-7center4000medianScore_2trials-nTemplate1-20_ratioIn-ME20x5-7500x50-DaccadBioneatMut4_1x1x0.0x1.0x0.80x0.05x0.05x0.05x0.05x20x12x12x0.2x2.0x0.8-GD0.5 1
#runExpe daccad3-7center4000medianScore_2trials-nTemplate1-32_ratioIn-ME32x4-7500x50-DaccadBioneatMut4_1x1x0.0x1.0x0.80x0.05x0.05x0.05x0.05x32x20x20x0.2x2.0x0.8-GD0.5 1
#runExpe daccad3-7center4000medianScore_2trials-nTemplate1-20_ratioIn-ME20x6-7500x50-DaccadBioneatMut4_1x1x0.0x1.0x0.80x0.05x0.05x0.05x0.05x20x12x12x0.2x2.0x0.8-GD0.5 1
#runExpe daccad3-7center4000medianScore_2trials-nTemplate1-20_ratioIn-ME20x4-7500x50-DaccadBioneatMutWithoutElitism_1x1x0.0x1.0x0.80x0.05x0.05x0.05x0.05x20x12x12x0.2x2.0x0.8x0.05x0.70x0.70 1
#runExpe daccad3-7center4000medianScore_2trials-nTemplate1-12_ratioIn-ME12x4-7500x50-DaccadBioneatMutWithoutElitism_1x1x0.0x1.0x0.80x0.05x0.05x0.05x0.05x12x12x12x0.2x2.0x0.8x0.05x0.70x0.70-GD1.0 1
#runExpe daccad3-7center4000medianScore_2trials-nTemplate1-12_ratioIn-ME12x4-7500x50-DaccadBioneatMutWithoutElitism_1x1x0.0x1.0x0.80x0.05x0.05x0.05x0.05x12x12x12x0.2x2.0x0.8x0.05x0.70x0.70x0-GD1.0 1
#runExpe daccad3-7center4000medianScore_2trials-nTemplate1-12_ratioIn-ME12x4-7500x50-DaccadBioneatMutWithoutElitism_1x1x0.0x1.0x0.80x0.05x0.05x0.05x0.05x12x12x12x0.2x2.0x0.8x0.05x0.10x0.70x0-GD1.0 1
#runExpe daccad3-7center4000medianScore_1trials-nTemplate1-12_ratioIn-ME12x4-7500x50-DaccadBioneatMutWithoutElitism_1x1x0.0x1.0x0.80x0.05x0.05x0.05x0.05x12x12x12x0.2x2.0x0.8x0.05x0.10x0.70x0-GD1.0 1
#runExpe daccad3-7center400medianScore_1trials-nTemplate1-12_ratioIn-ME12x4-7500x50-DaccadBioneatMutWithoutElitism_1x1x0.0x1.0x0.80x0.05x0.05x0.05x0.05x12x12x12x0.2x2.0x0.8x0.05x0.10x0.70x0-GD1.0 1
#runExpe daccad3-7center400medianScore_1trials-nTemplate1-12_ratioIn-ME12x4-7500x50-DaccadBioneatMutWithoutElitism_1x1x0.0x1.0x0.80x0.05x0.05x0.05x0.05x12x12x12x0.2x2.0x0.8x0.05x0.00x0.00x0 1

#runExpe daccad3-7center4000medianScore_2trials-nTemplate1-12_ratioIn-ME12x4-7500x50-DaccadBioneatMutWithoutElitism_1x1x0.0x1.0x0.80x0.05x0.05x0.05x0.05x12x12x12x0.2x2.0x0.8x0.05x0.70x0.70x5-GD1.0 1
#runExpe daccad3-7center4000meanScore_2trials-nTemplate1-12_ratioIn-ME12x4-7500x50-DaccadBioneatMutWithoutElitism_1x1x0.0x1.0x0.80x0.05x0.05x0.05x0.05x12x12x12x0.2x2.0x0.8x0.05x0.10x0.70x0-GD1.0 1
#runExpe daccad3-7center4000medianScore_2trials-nTemplate1-12_ratioIn-ME12x4-7500x150-DaccadBioneatMutWithoutElitism_1x1x0.0x1.0x0.80x0.05x0.05x0.05x0.05x12x12x12x0.2x2.0x0.8x0.05x0.70x0.70x0-GD1.0 1
#runExpe daccad3-7center4000medianScore_2trials-nTemplate1-12_ratioIn-ME12x4-7500x75-DaccadBioneatMutWithoutElitism_1x1x0.0x1.0x0.80x0.05x0.05x0.05x0.05x12x12x12x0.2x2.0x0.8x0.05x0.70x0.70x0-GD1.0 1

#runExpe daccad3-7center4000medianScore_2trials-nTemplate2-12_ratioIn-ME11x4-7500x50-DaccadTopologicalInitBioneat3_1x0.9x0.1x1.0x0.96.01x0.01x0.01x0.01x12x8x8x0.2x2.0x0.8x1-8-GD0.5 1
#runExpe daccad3-7center4000medianScore_5trials-nTemplate1-16_ratioIn-ME16x5-10000x50-DaccadBioneatMut3_1x1x0.0x1.0x0.80.05x0.05x0.05x0.05x16x12x12x0.2x2.0x0.8-GD0.5 1
#runExpe daccad3-9center4000medianScore_5trials-nTemplate1-20_ratioIn-ME20x5-10000x50-DaccadBioneatMut3_1x1x0.0x1.0x0.80.05x0.05x0.05x0.05x20x16x16x0.2x2.0x0.8-GD0.5 1
#runExpe daccad3-5center4000medianScore_2trials-nTemplate1-7_ratioIn-ME7x5-2500x50-DaccadBioneatMut3_1x1x0.0x1.0x0.80.05x0.05x0.05x0.05x7x7x7x0.2x2.0x0.8-GD0.5 1
#runExpe daccad3-5center4000medianScore_10trials-nTemplate1-7_max_mad-ME7x3x3-2500x50-DaccadBioneatMut3_1x1x0.0x1.0x0.80.05x0.05x0.05x0.05x7x7x7x0.2x2.0x0.8-GD0.5 1
#runExpe daccad3-5center4000x0.3medianScore_2trials-nTemplate1-5_ratioIn-ME5x5-2500x50-DaccadBioneatMut3_1x1x0.0x1.0x0.80.05x0.05x0.05x0.05x5x5x5x0.2x2.0x0.8-GD1.0 1

#runExpe daccad3-5center4000x0.3medianScore_2trials-nTemplate1-12_ratioIn-ME12x4-2500x50-DaccadBioneatMut4_1x1x0.0x1.0x0.80x0.05x0.05x0.05x0.05x12x6x6x0.2x2.0x0.8-GD1.0 1
#runExpe daccad3-5center4000x0.3medianScore_2trials-nTemplate6-12_ratioIn-ME7x4-2500x50-DaccadTopologicalInitBioneat3_1x0.9x0.1x1.0x0.80.05x0.05x0.05x0.05x12x6x6x0.2x2.0x0.8x6-12-GD1.0 1
#runExpe daccad3-5center4000x0.3medianScore_10trials-nTemplate1-12_ratioIn-ME12x4-2500x50-DaccadBioneatMut3_1x1x0.0x1.0x0.80.05x0.05x0.05x0.05x12x6x6x0.2x2.0x0.8-GD1.0 1

#runExpe daccad3-5center4000x0.3medianScore_50trials-nTemplate1-12-Container-1000x100-DaccadTopologicalInitBioneat3_1x0.5x0.5x1.0x0.80.05x0.05x0.05x0.05x12x6x6x0.2x2.0x0.8x1-16 1
#runExpe daccad3-5center10x0.3medianScore_50trials-nTemplate1-12-Container-1000x100-DaccadTopologicalInitBioneat3_1x0.5x0.5x1.0x0.80.05x0.05x0.05x0.05x12x6x6x0.2x2.0x0.8x1-16 1
#runExpe daccad3-5center4000x0.3medianScore_10trials-nTemplate1-12_ratioIn-ME6x4-2500x50-DaccadBioneatMut4_1x1x0.0x1.0x0.80x0.05x0.05x0.05x0.05x12x6x6x0.2x2.0x0.8-GD1.0 1
#runExpe daccad3-5center4000x0.3medianScore_10trials-nTemplate6-12_ratioIn-ME7x4-2500x50-DaccadTopologicalInitBioneat3_1x0.9x0.1x1.0x0.80.05x0.05x0.05x0.05x12x6x6x0.2x2.0x0.8x6-12-GD1.0 1

#runExpe daccad3-5center4000x0.3medianScore_5trials-nTemplate7-12_ratioIn-ME6x4-2500x50-DaccadTopologicalInitBioneat4_1x0.7x0.3x1.0x0.80.05x0.05x0.05x0.05x12x6x6x0.2x2.0x0.8x1-3-GD1.0 1
#runExpe daccad3-5center4000x0.3medianScore_5trials-nTemplate7-12_ratioIn-ME6x4-2500x50-DaccadTopologicalInitBioneat5_1x0.9x0.1x1.0x0.80.05x0.05x0.05x0.05x12x6x6x0.2x2.0x0.8x1-3x0.05-0.70_GD1.0 1
#runExpe daccad3-5center4000x0.3medianScore_5trials-nTemplate1-12_ratioIn-ME6x4-2500x50-DaccadBioneatMutEpsilon_1x1x0.0x1.0x0.80x0.05x0.05x0.05x0.05x12x6x6x0.2x2.0x0.8x0.90x0.30x0.75-GD1.0 1
#runExpe daccad3-5center4000x0.3medianScore_2trials-nTemplate7-12_meanHellinger-ME6x4-2500x50-DaccadTopologicalInitBioneat5_1x0.9x0.1x1.0x0.80.05x0.05x0.05x0.05x12x6x6x0.2x2.0x0.8x1-3x0.05-0.70_GD1.0 1
#runExpe daccad3-5center4000x0.3medianScore_2trials-nTemplate7-12_normalisedMeanHellinger-ME6x4-2500x50-DaccadTopologicalInitBioneat5_1x0.9x0.1x1.0x0.80.05x0.05x0.05x0.05x12x6x6x0.2x2.0x0.8x1-3x0.20-0.35_GD1.0 1
#runExpe daccad3-5center4000x0.3medianScore_2trials-nTemplate7-12_normalisedMeanHellinger-ME6x4-2500x50-DaccadTopologicalInitBioneat6_1x0.9x0.1x1.0x0.80.05x0.05x0.05x0.05x12x6x6x0.2x2.0x0.8x1-3x0.05-0.95_GD1.0 1
#runExpe daccad3-5center4000x0.3medianScore_2trials-nTemplate7-12_normalisedMeanHellinger-ME6x4-2500x50-DaccadTopologicalInitBioneat6_1x0.9x0.1x1.0x0.80.05x0.05x0.05x0.05x12x6x6x0.2x2.0x0.8x1-3x0.10-0.90_GD1.0 1
#runExpe daccad3-5center4000x0.3medianScore_2trials-nTemplate7-12-ME6-2500x50-DaccadTopologicalInitBioneat6_1x0.9x0.1x1.0x0.80.05x0.05x0.05x0.05x12x6x6x0.2x2.0x0.8x1-3x0.10-0.90_GD1.0 1
#runExpe daccad3-5disk4000x0.3medianScore_2trials-nTemplate7-12-ME6-2500x50-DaccadTopologicalInitBioneat6_1x0.9x0.1x1.0x0.80.05x0.05x0.05x0.05x12x6x6x0.2x2.0x0.8x1-3x0.10-0.90_GD1.0 1
#runExpe daccad3-5disk4000x0.6medianScore_2trials-nTemplate7-12-ME6-2500x50-DaccadTopologicalInitBioneat6_1x0.9x0.1x1.0x0.80.05x0.05x0.05x0.05x12x6x6x0.2x2.0x0.8x1-3x0.10-0.90_GD1.0 1
#runExpe daccad3-5disk4000x0.6medianScore_2trials-nTemplate7-15-ME9-5000x50-DaccadTopologicalInitBioneat6_1x0.9x0.1x1.0x0.80.05x0.05x0.05x0.05x15x15x15x0.2x2.0x0.8x1-5x0.10-0.90_GD1.0 1
#runExpe daccad3-7disk4000x0.5medianScore_2trials-nTemplate7-15-ME9-10000x50-DaccadTopologicalInitBioneat6_1x0.9x0.1x1.0x0.80.05x0.05x0.05x0.05x15x15x15x0.2x2.0x0.8x1-5x0.10-0.90_GD1.0 1
#runExpe daccad3-5T4000x0.3medianScore_2trials-nTemplate7-12-ME6-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.80.05x0.05x0.05x0.05x12x12x12x0.2x2.0x0.8x1-3x0.10-0.90_GD1.0 1
#runExpe daccad3-5T4000x0.3x-0.15medianScore_2trials-nTemplate7-12-ME6-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.80.05x0.05x0.05x0.05x12x12x12x0.2x2.0x0.8x1-3x0.10-0.90_GD1.0 1

#runExpe daccad3-5T4000x0.3x-0.15_FROMCENTER4000x0.3x-0.10_medianScore_2trials-nTemplate7-12-ME6-2500x50-DaccadCollectionInitBioneat_1x0.80.05x0.05x0.05x0.05x12x6x6x0.2x2.0x0.8x0.10-0.90_GD1.0 1
#runExpe daccad3-5T_center_top4000x0.3x-0.15medianScore_medianScore1_medianScore2_2trials-nTemplate7-12-ME5x5-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.80.05x0.05x0.05x0.05x12x12x12x0.2x2.0x0.8x1-3x0.30-0.30_GD1.0 1
#runExpe daccad3-5T_center_top4000x0.3x-0.10medianScore_medianScore1_medianScore2_2trials-nTemplate7-12-ME5x5-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.80.05x0.05x0.05x0.05x12x12x12x0.2x2.0x0.8x1-3x0.30-0.30_GD1.0 1
#runExpe daccad3-6T_center_top4000x0.3x-0.10medianScore_medianScore1_medianScore2_2trials-nTemplate5-15-ME5x5-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.80.05x0.05x0.05x0.05x15x8x8x0.2x2.0x0.8x1-3x0.90-0.90_GD1.0 1
#runExpe daccad3-6T_center_top4000x0.3x-0.10medianScore_ratioIn1_ratioIn2_2trials-nTemplate7-15-ME5x5-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.80.05x0.05x0.05x0.05x15x8x8x0.2x2.0x0.8x1-3x0.90-0.90_GD1.0 1
#runExpe daccad3-6ReversedT_center_bottom4000pre400x0.3x-0.10medianScore_ratioIn1_ratioIn2_2trials-nTemplate7-15-ME5x5-5000x50-DaccadTopologicalInitBionet6_1x0.7x0.3x1.0x0.80.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x0.90-0.90_GD1.0 1
#runExpe daccad3-6T_center_bottom4000pre400x0.3x-0.10medianScore_ratioIn1_ratioIn2_2trials-nTemplate7-15-ME5x5-5000x50-DaccadTopologicalInitBionet6_1x0.7x0.3x1.0x0.80.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x0.90-0.90_GD1.0 1
#runExpe daccad3-6ReversedT4000pre400x0.3x-0.10medianScore_2trials-nTemplate7-13-ME7-10000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.80.05x0.05x0.05x0.05x13x8x8x0.2x2.0x0.8x1-3x0.90-0.90_GD1.0 1
#runExpe daccad3-6ReversedT_center_bottom4000pre400x0.3x-0.10medianScore_2trials-nTemplate7-13_ratioIn1_rationIn2-ME7x5x5-15000x50-DaccadTopologicalInitBionet6_1x0.7x0.3x1.0x0.80.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x0.90-0.90_GD1.0 1
#runExpe daccad3-6T_center_top4000pre400x0.3x-0.10medianScore_2trials-nTemplate7-13_ratioIn1_rationIn2-ME7x5x5-30000x150-DaccadTopologicalInitBionet7_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x0.90-0.90_GD1.0 1
#runExpe daccad3-6T_center_top4000pre400x0.3x-0.20medianScore_5trials-nTemplate7-13_ratioIn1_rationIn2-ME7x5x5-10000x50-DaccadTopologicalInitBionet7cx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x0.90-0.90_GD1.0 1

#runExpe daccad3-6T_center_top4000pre400x0.2x-0.10medianScore_5trials-nTemplate7-13_ratioIn1_rationIn2-ME7x5x5-10000x50-DaccadTopologicalInitBionet7cx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x0.90-0.90_GD1.0 1
#runExpe daccad3-6T4000pre400x0.2x-0.10medianScore_5trials-nTemplate7-13-ME7-10000x50-DaccadTopologicalInitBionet7cx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x0.90-0.90_GD1.0 1

#runExpe daccad3-6T4000x0.2x-0.10medianScore_5trials-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet7cx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x0.90-0.90_GD1.0 1
#runExpe daccad3-6T_center_top4000x0.2x-0.10medianScore_5trials-ratioIn1_rationIn2-ME5x5-3000x50-DaccadTopologicalInitBionet7cx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x0.90-0.90_GD1.0 1
#runExpe daccad3-6T_center_top4000x0.2x-0.10medianScore_5trials-nTemplate7-13_ratioIn1_rationIn2-ME7x5x5-3000x50-DaccadTopologicalInitBionet7cx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x0.90-0.90_GD1.0 1
#runExpe daccad3-6T4000x0.2x-0.10medianScore_5trials-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x0.90-0.90_GD1.0 1
#runExpe daccad3-6T_center_top4000x0.2x-0.10medianScore_5trials-nTemplate7-13_ratioIn1_rationIn2-ME7x5x5-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x0.90-0.90_GD1.0 1

#runExpe daccadApprox8_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-10000x50-DaccadTopologicalInitBionet7cx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x0.90-0.90_GD1.0 1
#runExpe daccadApprox8_3-6T_center_top4000x0.2x-0.20medianScore-ratioIn1_rationIn2-ME5x5-3000x50-DaccadTopologicalInitBionet7cx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x0.90-0.90_GD1.0 1

#runExpe daccadApprox8_3-6T_center_top4000x0.2x-0.20medianScore-ratioIn1_rationIn2-ME5x5-10000x50-DaccadTopologicalInitBionet7cx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x0.90-0.90_GD1.0 1
#runExpe daccadApprox8_3-6T_center_top4000x0.2x-0.20medianScore-nTemplate7-13_ratioIn1_rationIn2-ME7x5x5-10000x50-DaccadTopologicalInitBionet7cx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x0.90-0.90_GD1.0 1
#runExpe daccadApprox8_3-6T_center_top4000x0.2x-0.80medianScore-nTemplate7-15_ratioIn1_rationIn2-ME9x5x5-20000x50-DaccadTopologicalInitBionet7cx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x15x9x9x0.2x2.0x0.8x0.3x1-3x0.10-0.90_GD1.0 1
#runExpe daccadApprox8_3-7T_center_top4000x0.2x-0.80medianScore-nTemplate7-16_ratioIn1_rationIn2-ME10x5x5-50000x100-DaccadTopologicalInitBionet7cx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x16x9x9x0.2x2.0x0.8x0.3x1-3x0.10-0.90_GD1.0 1

#runExpe daccadApprox8_3-6T_center_top4000x0.2x-0.50medianScore-nTemplate7-13_ratioIn1_rationIn2-ME7x5x5-10000x50-DaccadTopologicalInitBionet7cx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x0.90-0.90_GD1.0 1
#runExpe daccadApprox8_3-6T_center_top4000x0.2x-0.50medianScore-nTemplate7-13_ratioIn1_rationIn2-ME7x5x5-20000x50-DaccadTopologicalInitBionet7cx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x0.90-0.90_GD1.0 1
#runExpe daccadApprox8_3-6T4000x0.2x-0.50medianScore-nTemplate7-13-ME7-20000x50-DaccadTopologicalInitBionet7cx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x0.90-0.90_GD1.0 1

#runExpe daccad3-6T4000x0.2x-0.50medianScore-nTemplate2-13-Container-5000x500-DaccadTopologicalInitBionet7cx2_1x0.5x0.5x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x0.90-0.90_GD1.0 1
#runExpe daccadApprox8_3-6T4000x0.2x-0.50medianScore-nTemplate1-13-ME13-5000x50-DaccadBioneatMut3_1x1x0.0x1.0x0.80x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8 1
#runExpe daccad8_3-6T4000x0.2x-0.20medianScore-nTemplate1-13-ME13-5000x50-DaccadBioneatMut3_1x1x0.0x1.0x0.80x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8 1
#runExpe daccadApprox8_3-6T4000x0.2x-0.50medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x1.0-1.0 1
#runExpe daccad3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x1.0-1.0 1
#runExpe daccadApprox8_3-6T_center_top4000x0.2x-0.50medianScore-nTemplate7-13_ratioIn1_rationIn2-ME7x5x5-5000x50-DaccadBioneatMut3Transfer_1x1x0.0x1.0x0.80x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x4000 1


#runExpe daccadApprox8_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6Transfer_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x1.0-1.0x4000 1
#runExpe daccadApprox8_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 16
#runExpe daccad3-6T4000x0.2x-0.20medianScore_5trials-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1

#runExpe daccadApprox8_3-6T_center_top4000x0.2x-0.20medianScore-nTemplate7-13_ratioIn1_rationIn2-ME7x5x5-5000x50-DaccadTopologicalInitBionet6Transfer_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x1.0-1.0x4000 1
#runExpe daccadApprox8_3-6T_center_top4000x0.2x-0.20medianScore-nTemplate7-13_ratioIn1_rationIn2-ME7x5x5-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 16
#runExpe daccad3-6T_center_top4000x0.2x-0.20medianScore_5trials-nTemplate7-13_ratioIn1_rationIn2-ME7x5x5-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1

#runExpe daccadApprox8_3-6top4000x0.2x-0.10medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6Transfer_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x1.0-1.0x4000 1
#runExpe daccadApprox8_3-6top4000x0.2x-0.10medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 16
#runExpe daccad3-6top4000x0.2x-0.10medianScore_5trials-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1

#runExpe daccadApprox8_3-6center4000x0.2x-0.10medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6Transfer_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x1.0-1.0x4000 1
#runExpe daccadApprox8_3-6center4000x0.2x-0.10medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 16
#runExpe daccad3-6center4000x0.2x-0.10medianScore_5trials-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1

#runExpe daccad3-6T_center_top4000x0.2x-0.20medianScore_5trials-nTemplate7-13-Container-10000x500-DaccadTopologicalInitBionet6_1x0.5x0.5x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1



#runExpe daccadApprox2_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6Transfer_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x1.0-1.0x1500 1
#runExpe daccad3-6T4000x0.2x-0.20medianScore_5trials-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1

#runExpe daccadApprox2_3-6T_center_top4000x0.2x-0.20medianScore-nTemplate7-13_ratioIn1_rationIn2-ME7x5x5-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_3-6T_center_top4000x0.2x-0.20medianScore-nTemplate7-13_ratioIn1_rationIn2-ME7x5x5-3000x50-DaccadTopologicalInitBionet6Transfer_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x1.0-1.0x1500 1
#runExpe daccad3-6T_center_top4000x0.2x-0.20medianScore_5trials-nTemplate7-13_ratioIn1_rationIn2-ME7x5x5-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1


#runExpe daccadApprox2_3-6top4000x0.2x-0.10medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_3-6top4000x0.2x-0.10medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6Transfer_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x1.0-1.0x1500 1
#runExpe daccad3-6top4000x0.2x-0.10medianScore_5trials-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1

#runExpe daccadApprox2_3-6center4000x0.3x-0.10medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_3-6center4000x0.3x-0.10medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6Transfer_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x1.0-1.0x1500 1
#runExpe daccad3-6center4000x0.3x-0.10medianScore_5trials-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1


#runExpe daccad_and_daccadApprox2_3-6T_center_top4000x0.2x-0.20medianScore_5trials-nTemplate2-13-Container-5000x500-DaccadTopologicalInitBionet6_1x0.5x0.5x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccad_and_daccadApprox8_3-6T_center_top4000x0.2x-0.50medianScore_5trials-nTemplate7-13-Container-5000x500-DaccadTopologicalInitBionet6_1x0.5x0.5x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccad_and_daccadApprox8_3-6T_center_top4000x0.2x-0.20x-0.50medianScore_5trials-nTemplate7-13-Container-5000x500-DaccadTopologicalInitBionet6_1x0.5x0.5x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccad_and_daccadApprox2_3-6T_center_top4000x0.2x-0.50x-0x50medianScore_5trials-nTemplate2-13-Container-5000x500-DaccadTopologicalInitBionet6_1x0.5x0.5x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccad_and_daccadApprox2_3-6T_center_top4000x0.2x-0.50x-0x50medianScore_5trials-nTemplate7-13-Container-5000x500-DaccadTopologicalInitBionet6_1x0.5x0.5x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccad_and_daccadApprox2_3-6T_center_top4000x0.2x-0.20medianScore_5trials-nTemplate7-13-Container-5000x500-DaccadTopologicalInitBionet6_1x0.5x0.5x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccad_and_daccadApprox2_3-6T_center_top4000x0.2x-0.20medianScore_5trials-nTemplate7-13-ME7andContainer-3000x50-DaccadTopologicalInitBionet6_1x0.5x0.5x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1


#runExpe daccadApprox8_3-6T4000x0.2x-0.50medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1

#runExpe daccad3-6T4000x0.2x-0.50medianScore_5trials-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_3-6T4000x0.2x-0.50medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_3-6T4000x0.2x-0.50medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6Transfer_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x1.0-1.0x1500 1


#runExpe daccadApprox2_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet7cx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x1.0-1.0 1
#runExpe daccadApprox8_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet7cx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x1.0-1.0 1
#runExpe daccad3-6T4000x0.2x-0.20medianScore_5trials-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet7cx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x1.0-1.0 1
#runExpe daccadApprox2_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet7Transfercx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x1.0-1.0x1500 1

#runExpe daccad3-6top1000x0.2x-0.10medianScore_5trials-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1

#runExpe daccadApprox2_3-10smiley10000x0.2x-0.50medianScore-nTemplate7-20-ME14-10000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.6x0.10x0.10x0.10x0.10x20x12x12x0.2x2.0x0.8x1-5x1.0-1.0 1
#runExpe daccadApprox2_3-10smiley4000x0.2x-0.20medianScore-nTemplate7-20-ME14-10000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.6x0.10x0.10x0.10x0.10x20x12x12x0.2x2.0x0.8x1-5x1.0-1.0 1
#runExpe daccadApprox2_3-10smiley4000x0.2x-0.05medianScore-nTemplate7-20-ME14-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.6x0.10x0.10x0.10x0.10x20x12x12x0.2x2.0x0.8x1-5x1.0-1.0 1
#runExpe daccadApprox2_3-10smiley1000x320x320x4000x0.2x-0.05medianScore-nTemplate7-20-ME14-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.6x0.10x0.10x0.10x0.10x20x12x12x0.2x2.0x0.8x1-5x1.0-1.0 1

#runExpe daccadApprox2_3-10T4000x0.2x-0.20medianScore-nTemplate7-20-ME14-10000x50-DaccadTopologicalInitBionetFixedWeights_1x0.9x0.1x1.0x0.0x0.25x0.25x0.25x0.25x20x12x12x0.2x2.0x0.8x1x1.0-1.0xxx0.025 1
#runExpe daccadApprox2_3-10T4000x0.2x-0.20medianScore-nTemplate7-20-ME14-10000x50-DaccadTopologicalInitBionetFixedWeights_1x0.9x0.1x1.0x0.20x0.20x0.20x0.20x0.20x20x12x12x0.2x2.0x0.8x1x1.0-1.0xxx0.025x0.050x0.1x0.2x0.75 1


#runExpe daccadApprox2_3-10smiley4000x0.2x-0.05medianScore-nTemplate7-20-ME14-10000x50-DaccadTopologicalInitBionetFixedWeights_1x0.9x0.1x1.0x0.20x0.20x0.20x0.20x0.20x20x12x12x0.2x2.0x0.8x1x1.0-1.0xxx0.025x0.050x0.1x0.2x0.75 1


#runExpe daccadApprox8_3-6T4000x0.2x-0.05_320x320x200x0.02-medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_3-6T4000x0.2x-0.05_1600x1600x1000x0.004-medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox16_3-6T4000x0.2x-0.05_1600x1600x1000x0.004-medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox8_3-6T4000x0.2x-0.05_640x640x400x0.01-medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox8_3-6T4000x0.2x-0.05_640x640x200x0.006-medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_3-6T4000x0.2x-0.05_640x640x200x0.01-medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_3-6T4000x0.2x-0.20_640x640x200x0.015-medianScore-nTemplate7-13-ME7-1000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_3-6T4000x0.2x-0.20_640x640x200x0.013-medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_3-6center4000x0.3x-0.20_640x640x200x0.013-medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_3-6bottom1000x0.2x-0.20_640x640x200x0.013-medianScore-nTemplate7-13-ME7-1000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_3-6circle4000x0.2x-0.20_640x640x200x0.013-medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6_1x0.8x0.2x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_3-6circle4000x0.2x-0.10_640x640x200x0.013-medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6_1x0.8x0.2x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_3-6circle4000x0.2x-0.10medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6_1x0.8x0.2x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_3-6disk4000x0.2x-0.10medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6_1x0.8x0.2x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_3-6disk4000x0.2x-0.10_640x640x200x0.013-medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6_1x0.8x0.2x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_3-6disk4000x0.2x-0.10-4grad-medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6_1x0.8x0.2x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_3-6disk4000x0.3x-0.10-4grad-medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6_1x0.8x0.2x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_5-8disk4000x0.3x-0.10-4grad-medianScore-nTemplate7-20-ME14-5000x50-DaccadTopologicalInitBionet6Grad4_1x0.8x0.2x1.0x0.8x0.05x0.05x0.05x0.05x20x12x12x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_5-8center4000x0.3x-0.10-4grad-medianScore-nTemplate7-20-ME14-5000x50-DaccadTopologicalInitBionet6Grad4_1x0.8x0.2x1.0x0.8x0.05x0.05x0.05x0.05x20x12x12x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_5-8disk4000x0.5x-0.10-4grad-medianScore-nTemplate7-20-ME14-5000x50-DaccadTopologicalInitBionet6Grad4_1x0.8x0.2x1.0x0.8x0.05x0.05x0.05x0.05x20x12x12x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_5-8disk4000x0.5x-0.05-4grad-medianScore-nTemplate7-20-ME14-5000x50-DaccadTopologicalInitBionet6Grad4_1x0.8x0.2x1.0x0.8x0.05x0.05x0.05x0.05x20x12x12x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_5-8circle4000x0.5x-0.05-4grad-medianScore-nTemplate7-20-ME14-3000x50-DaccadTopologicalInitBionet6Grad4_1x0.8x0.2x1.0x0.8x0.05x0.05x0.05x0.05x20x12x12x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_5-8disk4000x0.3x-0.10-4grad_640x640x200x0.013-medianScore-nTemplate7-20-ME14-5000x50-DaccadTopologicalInitBionet6Grad4_1x0.8x0.2x1.0x0.8x0.05x0.05x0.05x0.05x20x12x12x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_5-8circle4000x0.3x-0.10-4grad_640x640x200x0.013-medianScore-nTemplate7-20-ME14-5000x50-DaccadTopologicalInitBionet6Grad4_1x0.8x0.2x1.0x0.8x0.05x0.05x0.05x0.05x20x12x12x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_5-8smiley4000x0.5x-0.20-4grad_640x640x200x0.013-medianScore-nTemplate7-20-ME14-5000x50-DaccadTopologicalInitBionet6Grad4_1x0.8x0.2x1.0x0.8x0.05x0.05x0.05x0.05x20x12x12x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_5-8smiley_smileyeyes_smileymouth4000x0.5x-0.20-4grad_640x640x200x0.013-medianScore-nTemplate7-20_ratioIn1_ratioIn2-ME14x5x5-5000x50-DaccadTopologicalInitBionet6Grad4_1x0.8x0.2x1.0x0.8x0.05x0.05x0.05x0.05x20x12x12x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_5-8smiley_circle_eyes_mouth4000x0.5x-0.20-4grad_640x640x200x0.013-medianScore-ratioIn1_ratioIn2_ratioIn3-ME5x5x5-5000x50-DaccadTopologicalInitBionet6Grad4_1x0.8x0.2x1.0x0.8x0.05x0.05x0.05x0.05x20x12x12x0.2x2.0x0.8x1-3x1.0-1.0 1



# Concentration-agnostic
#runExpe daccadApprox2_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.0x0.25x0.25x0.25x0.25x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.20x0.20x0.20x0.20x0.20x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionetFixedWeightsFixedStabilities_1x0.9x0.1x1.0x0.0x0.25x0.25x0.25x0.25x13x7x7x0.2x2.0x0.8x1x1.0-1.0xxx0.025xxx0.025 1
#runExpe daccadApprox2_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionetFixedWeightsFixedStabilities_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0xxx0.025x0.2x0.75xxx0.025x0.2x0.75 1
#runExpe daccadApprox2_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionetFixedWeights_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0xxx0.025x0.2x0.75 1
#runExpe daccadApprox2_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionetFixedWeights_1x0.9x0.1x1.0x0.0x0.25x0.25x0.25x0.25x13x7x7x0.2x2.0x0.8x1x1.0-1.0xxx0.025 1
#runExpe daccadApprox2_Noisy10-randind_3-6T4000x0.2x-0.20mean_medianScore-nTemplate7x13-std_medianScore0x0.5-ME7x5-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.0x0.25x0.25x0.25x0.25x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_Noisy10-randtempl_3-6T4000x0.2x-0.20mean_medianScore-nTemplate7x13-std_medianScore0x0.5-ME7x5-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.0x0.25x0.25x0.25x0.25x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_Noisy10-noisyind0.1_3-6T4000x0.2x-0.20mean_medianScore-nTemplate7x13-std_medianScore0x0.5-ME7x5-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.0x0.25x0.25x0.25x0.25x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_Noisy10-noisytempl0.1_3-6T4000x0.2x-0.20mean_medianScore-nTemplate7x13-std_medianScore0x0.5-ME7x5-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.0x0.25x0.25x0.25x0.25x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_Noisy10-noisyind0.05_3-6T4000x0.2x-0.20mean_medianScore-nTemplate7x13-std_medianScore0x0.5-ME7x5-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.0x0.25x0.25x0.25x0.25x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_Noisy10-noisytempl0.05_3-6T4000x0.2x-0.20mean_medianScore-nTemplate7x13-std_medianScore0x0.5-ME7x5-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.0x0.25x0.25x0.25x0.25x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_Noisy10-noisyind0.15_3-6T4000x0.2x-0.20mean_medianScore-nTemplate7x13-std_medianScore0x0.5-ME7x5-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.0x0.25x0.25x0.25x0.25x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_Noisy10-noisytempl0.15_3-6T4000x0.2x-0.20mean_medianScore-nTemplate7x13-std_medianScore0x0.5-ME7x5-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.0x0.25x0.25x0.25x0.25x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_Noisy10-noisyind0.05_3-6T4000x0.2x-0.20mean_medianScore-nTemplate7x13-std_medianScore0x0.5-ME7x5-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.20x0.20x0.20x0.20x0.20x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_Noisy10-noisytempl0.05_3-6T4000x0.2x-0.20mean_medianScore-nTemplate7x13-std_medianScore0x0.5-ME7x5-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.20x0.20x0.20x0.20x0.20x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_Noisy5-noisytempl0.05_3-8T4000x0.2x-0.20mean_medianScore-nTemplate11x20-std_medianScore0x0.25-ME10x5-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.20x0.20x0.20x0.20x0.20x20x12x12x0.2x2.0x0.8x1-5x1.0-1.0 1
#runExpe daccadApprox2_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionetFixedWeights_1x0.9x0.1x1.0x0.0x0.25x0.25x0.25x0.25x13x7x7x0.2x2.0x0.8x1x1.0-1.0xxx0.25 1


# New concentration-agnostic
#runExpe daccadApprox2_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.20x0.20x0.20x0.20x0.20x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1 # CASE B1
#runExpe daccadApprox2_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionetFixedWeights_1x0.9x0.1x1.0x0.0x0.25x0.25x0.25x0.25x13x7x7x0.2x2.0x0.8x1x1.0-1.0xxx0.025 1 # CASE F2
#runExpe daccadApprox2_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionetFixedWeights_1x0.9x0.1x1.0x0.0x0.25x0.25x0.25x0.25x13x7x7x0.2x2.0x0.8x1x1.0-1.0xxx0.25 1 # CASE F3
#runExpe daccadApprox2_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionetFixedWeights_1x0.9x0.1x1.0x0.0x0.25x0.25x0.25x0.25x13x7x7x0.2x2.0x0.8x1x1.0-1.0xxx0.75 1 # CASE F4
#runExpe daccadApprox2_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionetFixedWeights_1x0.9x0.1x1.0x0.0x0.25x0.25x0.25x0.25x13x7x7x0.2x2.0x0.8x1x1.0-1.0xxx0.025x0.2x0.75 1 # CASE F5
#runExpe daccadApprox2_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6Clones_1x0.9x0.1x1.0x0.16x0.16x0.16x0.16x0.16x0.16x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1 # CASE N2
#runExpe daccadApprox2_Noisy10-noisytempl0.05_3-6T4000x0.2x-0.20mean_medianScore-nTemplate7x13-std_medianScore0x0.25-ME7x5-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.20x0.20x0.20x0.20x0.20x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1 # CASE N3
#runExpe daccadApprox2_Noisy10-noisytempl0.05_3-6T4000x0.2x-0.20mean_medianScore-nTemplate7x13-std_medianScore0x0.25-ME7x5-5000x50-DaccadTopologicalInitBionet6Clones_1x0.9x0.1x1.0x0.16x0.16x0.16x0.16x0.16x0.16x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1 # CASE N4
#runExpe daccadApprox2_Noisy10-randtempl_3-6T4000x0.2x-0.20mean_medianScore-nTemplate7x13-std_medianScore0x0.25-ME7x5-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.0x0.25x0.25x0.25x0.25x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1 # CASE N5

#runExpe daccadApprox2_3-6T4000x0.2x-0.50medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.20x0.20x0.20x0.20x0.20x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_3-6topbottom4000x0.2x-0.50medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.20x0.20x0.20x0.20x0.20x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_3-8T4000x0.2x-0.50medianScore-nTemplate7-16-ME10-5000x50-DaccadTopologicalInitBionet6_1x0.7x0.3x1.0x0.20x0.20x0.20x0.20x0.20x16x10x10x0.2x2.0x0.8x1-5x1.0-1.0 1
#runExpe daccadApprox2_3-6T1000x0.2x-0.75medianScore-nTemplate7-13-ME7-20000x50-DaccadTopologicalInitBionet6_1x0.7x0.3x1.0x0.20x0.20x0.20x0.20x0.20x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_3-6T2000x0.2x-0.75medianScore-nTemplate7-13-ME7-10000x50-DaccadTopologicalInitBionet6_1x0.7x0.3x1.0x0.20x0.20x0.20x0.20x0.20x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_3-8T4000x0.2x-0.50medianScore-nTemplate7-16-ME10-10000x100-DaccadTopologicalInitBionet6_1x0.7x0.3x1.0x0.20x0.20x0.20x0.20x0.20x16x10x10x0.2x2.0x0.8x1-5x1.0-1.0 1
#runExpe daccadApprox2_3-6T4000x0.2x-0.30medianScore-nTemplate7-13-ME7-10000x50-DaccadTopologicalInitBionetFixedWeights_1x0.9x0.1x1.0x0.0x0.25x0.25x0.25x0.25x13x7x7x0.2x2.0x0.8x1x1.0-1.0xxx0.025 1
#runExpe daccadApprox2_3-6T4000x0.2x-0.85medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.20x0.20x0.20x0.20x0.20x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1

#runExpe daccadApprox2_5-8circle4000x0.5x-0.75-4grad-medianScore-nTemplate7-20-ME14-5000x50-DaccadTopologicalInitBionet6Grad4_1x0.8x0.2x1.0x0.20x0.20x0.20x0.20x0.20x20x12x12x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_5-8circle4000x0.3x-0.85-4grad-medianScore-nTemplate7-20-ME14-5000x50-DaccadTopologicalInitBionet6Grad4_1x0.8x0.2x1.0x0.20x0.20x0.20x0.20x0.20x20x12x12x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_5-8disk4000x0.3x-0.85-4grad-medianScore-nTemplate7-20-ME14-5000x50-DaccadTopologicalInitBionet6Grad4_1x0.8x0.2x1.0x0.20x0.20x0.20x0.20x0.20x20x12x12x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_5-8disk4000x0.3x-0.50-4grad-medianScore-nTemplate7-20-ME14-5000x50-DaccadTopologicalInitBionet6Grad4_1x0.8x0.2x1.0x0.20x0.20x0.20x0.20x0.20x20x12x12x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_5-8disk4000x0.3x-0.20-4grad-medianScore-nTemplate7-20-ME14-5000x50-DaccadTopologicalInitBionet6Grad4_1x0.8x0.2x1.0x0.20x0.20x0.20x0.20x0.20x20x12x12x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_5-8disk4000x0.3x-0.35-4grad-medianScore-nTemplate7-20-ME14-5000x50-DaccadTopologicalInitBionet6Grad4_1x0.8x0.2x1.0x0.20x0.20x0.20x0.20x0.20x20x12x12x0.2x2.0x0.8x1-5x1.0-1.0 1
#runExpe daccadApprox2_5-8disk4000x0.4x-0.50-4grad-medianScore-nTemplate7-20-ME14-5000x50-DaccadTopologicalInitBionet6Grad4_1x0.8x0.2x1.0x0.20x0.20x0.20x0.20x0.20x20x12x12x0.2x2.0x0.8x1-5x1.0-1.0 1
#runExpe daccadApprox2_5-8disk4000x0.3x-0.85-4grad-medianScore-nTemplate7-20-ME14-10000x50-DaccadTopologicalInitBionet6Grad4_1x0.8x0.2x1.0x0.20x0.20x0.20x0.20x0.20x20x12x12x0.2x2.0x0.8x1-5x1.0-1.0 1
#runExpe daccadApprox2_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet7cx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x1.0-1.0 1
#runExpe daccadApprox2_3-6T4000x0.2x-0.85medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet7cx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x1.0-1.0 1
#runExpe daccadApprox2_5-8disk4000x0.2x-0.85-4grad-medianScore-nTemplate7-20-ME14-10000x50-DaccadTopologicalInitBionet6Grad4_1x0.8x0.2x1.0x0.20x0.20x0.20x0.20x0.20x20x12x12x0.2x2.0x0.8x1-5x1.0-1.0 1
#runExpe daccadApprox2_5-8disk4000x0.4x-0.85-4grad-medianScore-nTemplate7-20-ME14-10000x50-DaccadTopologicalInitBionet6Grad4_1x0.8x0.2x1.0x0.20x0.20x0.20x0.20x0.20x20x12x12x0.2x2.0x0.8x1-5x1.0-1.0 1
#runExpe daccadApprox2_3-10T4000x0.2x-0.85medianScore-nTemplate7-20-ME14-10000x50-DaccadTopologicalInitBionet7cx2_1x0.8x0.2x1.0x0.5x0.3x0.05x0.05x0.05x0.05x20x12x12x0.2x2.0x0.8x0.3x1-5x1.0-1.0 1
#runExpe daccadApprox2_5-8disk4000x0.3x-0.10-4grad-medianScore-nTemplate7-20-ME14-10000x50-DaccadTopologicalInitBionet6Grad4_1x0.8x0.2x1.0x0.8x0.05x0.05x0.05x0.05x20x12x12x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_5-10disk4000x0.2x-0.85-4grad-medianScore-nTemplate7-26-ME20-10000x50-DaccadTopologicalInitBionet6Grad4_1x0.8x0.2x1.0x0.20x0.20x0.20x0.20x0.20x26x16x16x0.2x2.0x0.8x1-5x1.0-1.0 1
#runExpe daccadApprox2_5-8disk4000x0.25x-0.85-4grad-medianScore-nTemplate7-16-ME10-10000x50-DaccadTopologicalInitBionet6Grad4_1x0.7x0.3x1.0x0.20x0.20x0.20x0.20x0.20x16x10x10x0.2x2.0x0.8x1-5x1.0-1.0 1
#runExpe daccadApprox2_5-8disk4000x0.25x-0.30-4grad-medianScore-nTemplate7-16-ME10-10000x50-DaccadTopologicalInitBionet6Grad4_1x0.7x0.3x1.0x0.20x0.20x0.20x0.20x0.20x16x10x10x0.2x2.0x0.8x1-5x1.0-1.0 1
#runExpe daccadApprox2_5-8disk4000x0.25x-0.20-4grad-medianScore-nTemplate7-16-ME10-10000x50-DaccadTopologicalInitBionet6Grad4_1x0.7x0.3x1.0x0.20x0.20x0.20x0.20x0.20x16x10x10x0.2x2.0x0.8x1-5x1.0-1.0 1

# New concentration-agnostic disk
#runExpe daccadApprox2_5-8disk4000x0.25x-0.20-4grad-medianScore-nTemplate7-20-ME14-10000x50-DaccadTopologicalInitBionet6_1x0.7x0.3x1.0x0.20x0.20x0.20x0.20x0.20x20x12x12x0.2x2.0x0.8x1-3x1.0-1.0 1 # CASE B1
#runExpe daccadApprox2_5-8disk4000x0.25x-0.20-4grad-medianScore-nTemplate7-20-ME14-10000x50-DaccadTopologicalInitBionetFixedWeights_1x0.7x0.3x1.0x0.0x0.25x0.25x0.25x0.25x20x12x12x0.2x2.0x0.8x1-3x1.0-1.0xxx0.025 1 # CASE F2
#runExpe daccadApprox2_5-8disk4000x0.25x-0.20-4grad-medianScore-nTemplate7-20-ME14-10000x50-DaccadTopologicalInitBionetFixedWeights_1x0.7x0.3x1.0x0.0x0.25x0.25x0.25x0.25x20x12x12x0.2x2.0x0.8x1-3x1.0-1.0xxx0.25 1 # CASE F3
#runExpe daccadApprox2_5-8disk4000x0.25x-0.20-4grad-medianScore-nTemplate7-20-ME14-10000x50-DaccadTopologicalInitBionetFixedWeights_1x0.7x0.3x1.0x0.0x0.25x0.25x0.25x0.25x20x12x12x0.2x2.0x0.8x1-3x1.0-1.0xxx0.75 1 # CASE F4
#runExpe daccadApprox2_5-8disk4000x0.25x-0.20-4grad-medianScore-nTemplate7-20-ME14-10000x50-DaccadTopologicalInitBionetFixedWeights_1x0.7x0.3x1.0x0.0x0.25x0.25x0.25x0.25x20x12x12x0.2x2.0x0.8x1-3x1.0-1.0xxx0.025x0.2x0.75 1 # CASE F5
#runExpe daccadApprox2_5-8disk4000x0.25x-0.20-4grad-medianScore-nTemplate7-20-ME14-10000x50-DaccadTopologicalInitBionet6Clones_1x0.7x0.3x1.0x0.16x0.16x0.16x0.16x0.16x0.16x20x12x12x0.2x2.0x0.8x1-3x1.0-1.0 1 # CASE N2
#runExpe daccadApprox2_Noisy10-noisytempl0.05_3-8disk4000x0.25x-0.20mean_medianScore-nTemplate7x20-std_medianScore0x0.25-ME14x5-10000x50-DaccadTopologicalInitBionet6_1x0.7x0.3x1.0x0.20x0.20x0.20x0.20x0.20x20x12x1x0.2x2.0x0.8x1-3x1.0-1.0 1 # CASE N3
#runExpe daccadApprox2_Noisy10-noisytempl0.05_3-8disk4000x0.25x-0.20mean_medianScore-nTemplate7x20-std_medianScore0x0.25-ME14x5-10000x50-DaccadTopologicalInitBionet6Clones_1x0.7x0.3x1.0x0.16x0.16x0.16x0.16x0.16x0.16x20x12x1x0.2x2.0x0.8x1-3x1.0-1.0 1 # CASE N4
#runExpe daccadApprox2_Noisy10-randtempl_3-8disk4000x0.25x-0.20mean_medianScore-nTemplate7x20-std_medianScore0x0.25-ME14x5-10000x50-DaccadTopologicalInitBionet6_1x0.7x0.3x1.0x0.0x0.25x0.25x0.25x0.25x20x12x1x0.2x2.0x0.8x1-3x1.0-1.0 1 # CASE N5
#runExpe daccadApprox2_Noisy10-noisytempl0.05_3-8disk4000x0.25x-0.20mean_medianScore-nTemplate7x20-ME14-10000x50-DaccadTopologicalInitBionet6_1x0.7x0.3x1.0x0.20x0.20x0.20x0.20x0.20x20x12x1x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_Noisy10-noisytempl0.05_3-8disk4000x0.25x-0.20mean_medianScore-nTemplate7x13-ME7-10000x50-DaccadTopologicalInitBionet6_1x0.7x0.3x1.0x0.20x0.20x0.20x0.20x0.20x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccadApprox2_Noisy10-noisytempl0.05_3-8disk4000x0.25x-0.20mean_medianScore-nTemplate7x13-ME7-10000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.80x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1


#runExpe daccad3-6center4000x0.3x-0.30medianScore_5trials-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.20x0.20x0.20x0.20x0.20x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccad3-6bottom1000x0.2x-0.30medianScore_5trials-nTemplate7-13-ME7-1000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.20x0.20x0.20x0.20x0.20x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccad3-6bottom1000x0.2x-0.30medianScore_5trials-nTemplate3-9-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.20x0.20x0.20x0.20x0.20x9x5x5x0.2x2.0x0.8x1-3x1.0-1.0 4
#runExpe daccad3-6bottom1000x0.3x-0.10medianScore_5trials-nTemplate3-7-ME5-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.80x0.05x0.05x0.05x0.05x7x5x5x0.2x2.0x0.8x1-3x1.0-1.0 4
#runExpe daccad3-6center4000x0.3x-0.10medianScore_5trials-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.80x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 2
#runExpe daccad3-6center4000x0.3x-0.30medianScore_5trials-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.80x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 2
#runExpe daccad3-6center4000x0.3x-0.30medianScore_5trials-nTemplate7-13-ME7-7500x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.80x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 2


#runExpe daccad3-6center4000x0.3x-0.30medianScore_5trials-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBioneatWithIntMut_1x0.9x0.1x1.0x0.80x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0x400x10 1
#runExpe daccad3-6center4000x0.3x-0.30medianScore_5trials-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBioneatWithIntMut_1x0.9x0.1x1.0x0.80x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0x40x100 1
#runExpe daccad3-6center2000x0.3x-0.30medianScore_5trials-nTemplate7-13-ME7-7500x50-DaccadTopologicalInitBioneatWithIntMut_1x0.9x0.1x1.0x0.80x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0x400x10 1
#runExpe daccad3-6center2000x0.3x-0.30medianScore_5trials-nTemplate7-13-ME7-7500x50-DaccadTopologicalInitBioneatWithIntMut_1x0.9x0.1x1.0x0.80x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0x40x100 1
#runExpe daccad3-6center2000x0.3x-0.30medianScore_5trials-nTemplate7-13-ME7-7500x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.80x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0 1
#runExpe daccad3-6center2000x0.3x-0.30medianScore_5trials-nTemplate7-13-ME7-100x50-DaccadTopologicalInitBioneatWithIntMut_1x0.9x0.1x1.0x0.80x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0x40x100 1
runExpe daccad3-6center2000x0.3x-0.30medianScore_5trials-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBioneatWithIntMut_1x0.9x0.1x1.0x0.80x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0x40x100 1


# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
