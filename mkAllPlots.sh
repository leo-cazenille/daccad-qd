#!/bin/bash

dataDir=results/ #~/Dropbox/LeoResults/Daccad-QD
plotsDir=plots/ #~/Dropbox/LeoResults/Daccad-QD/plots
finalPlotsDir=figs/plots/
tikzDir=figs/
plotScripts=./scripts/

origDir=$(pwd)

mkdir -p $dataDir
mkdir -p $plotsDir
mkdir -p $finalPlotsDir


declare -a pids


# Plots Approx
mkdir -p $plotsDir/approx020
#$plotScripts/plotApprox.py -T 0 -i /data/prj/daccad-qd/results/daccad3-6T4000x0.2x-0.50medianScore-nTemplate2-13-Container-5000x500-DaccadTopologicalInitBionet7cx2_1x0.5x0.5x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x0.90-0.90_GD1.0/final_20190611054459.p -o $plotsDir/approx &
#$plotScripts/plotApprox.py -T 0 -i results/daccad3-6T_center_top4000x0.2x-0.20medianScore_5trials-nTemplate7-13-Container-10000x500-DaccadTopologicalInitBionet6_1x0.5x0.5x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0/final_20190617074350.p -o $plotsDir/approx &
$plotScripts/plotApprox.py -T 0 -i /data/prj/daccad-qd/results/daccad_and_daccadApprox2_3-6T_center_top4000x0.2x-0.20medianScore_5trials-nTemplate7-13-Container-5000x500-DaccadTopologicalInitBionet6_1x0.5x0.5x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0/final_20190625043635.p -o $plotsDir/approx020 --maxXY 0.18 &
pids+=($!)
mkdir -p $plotsDir/approx050
$plotScripts/plotApprox.py -T 0 -i /data/prj/daccad-qd/results/daccad_and_daccadApprox2_3-6T_center_top4000x0.2x-0.50x-0x50medianScore_5trials-nTemplate2-13-Container-5000x500-DaccadTopologicalInitBionet6_1x0.5x0.5x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0/final_20190625013918.p -o $plotsDir/approx050 --maxXY 0.13 &
pids+=($!)

# Plots retrials
mkdir -p $plotsDir/retrials
$plotScripts/plotStability.py -i results/daccad3-5center4000x0.3medianScore_50trials-nTemplate1-12-Container-1000x100-DaccadTopologicalInitBioneat3_1x0.5x0.5x1.0x0.80.05x0.05x0.05x0.05x12x6x6x0.2x2.0x0.8x1-16/final_20190517052306.p -o $plotsDir/retrials &
pids+=($!)



#
## Plots GECCO2017
#declare -A gecco2017nbIterations
#gecco2017nbIterations[bottom]=50
#gecco2017nbIterations[right]=50
#gecco2017nbIterations[top]=50
#gecco2017nbIterations[center]=150
#mkdir -p $plotsDir/gecco2017
#for name in "${!gecco2017nbIterations[@]}"; do
#    $plotScripts/plotFitnessGECCO2017.py -i $dataDir/gecco2017/results_$name.dat -o $plotsDir/gecco2017/fitness_gecco2017_$name.pdf --nbRuns 12 --nbIterations ${gecco2017nbIterations[$name]} --nbEvalsPerIt 50 &
#    pids+=($!)
#done
#

declare -A fitnessMax[center]=0.6
declare -A fitnessMax[top]=1.0
declare -A fitnessMax[T]=0.75
declare -A fitnessMax[T2]=0.6
declare -A fitnessMax[Tcx]=0.75
#declare -A fitnessMax[TDecomp]=0.7


# Plots *-bioneat
declare -A expeBioneatPath
expeBioneatPath[center]=bioneat-centerBioneat/result.dat
expeBioneatPath[top]=bioneat-topBioneat/result.dat
expeBioneatPath[T]=bioneat-TBioneat/result.dat
expeBioneatPath[T2]=bioneat-TBioneat2/result.dat
declare -A BioneatnbIterations
BioneatnbIterations[center]=60 #100
BioneatnbIterations[top]=60 #100
BioneatnbIterations[T]=60 #100
BioneatnbIterations[T2]=60 #100
mkdir -p $plotsDir/Bioneat
for name in "${!BioneatnbIterations[@]}"; do
    $plotScripts/plotFitnessGECCO2017.py -i $dataDir/"${expeBioneatPath[$name]}"  -o $plotsDir/Bioneat/fitness_Bioneat_$name.pdf --nbRuns 24 --nbIterations ${BioneatnbIterations[$name]} --nbEvalsPerIt 50 --max ${fitnessMax[$name]} &
    #$plotScripts/plotFitnessGECCO2017.py -i $dataDir/"${expeBioneatPath[$name]}"  -o $plotsDir/Bioneat/fitness_Bioneat_$name.pdf --nbRuns 8 --nbIterations ${BioneatnbIterations[$name]} --nbEvalsPerIt 50 --max ${fitnessMax[$name]} &
    pids+=($!)
done



# Plots *-approx-bioneat
declare -A expeApproxBioneatPath
expeApproxBioneatPath[center]=bioneat-RDLineLeoApprox/result.dat
expeApproxBioneatPath[top]=bioneat-RDTopLeoApprox/result.dat
expeApproxBioneatPath[T]=bioneat-TApproxBioneat/result.dat
expeApproxBioneatPath[T2]=bioneat-TApproxBioneat2/result.dat
declare -A ApproxBioneatnbIterations
ApproxBioneatnbIterations[center]=60 #100
ApproxBioneatnbIterations[top]=60 #100
ApproxBioneatnbIterations[T]=60 #100
ApproxBioneatnbIterations[T2]=60 #100
mkdir -p $plotsDir/ApproxBioneat
for name in "${!ApproxBioneatnbIterations[@]}"; do
    $plotScripts/plotFitnessGECCO2017.py -i $dataDir/"${expeApproxBioneatPath[$name]}"  -o $plotsDir/ApproxBioneat/fitness_ApproxBioneat_$name.pdf --nbRuns 24 --nbIterations ${ApproxBioneatnbIterations[$name]} --nbEvalsPerIt 50 --max ${fitnessMax[$name]} &
    pids+=($!)
done



# Plots *-transfer-bioneat
declare -A expeTransferBioneatPath
expeTransferBioneatPath[center]=bioneat-RDLineLeoTransfer/result.dat
expeTransferBioneatPath[top]=bioneat-RDTopLeoTransfer/result.dat
expeTransferBioneatPath[T]=bioneat-TBioneatTransfer/result.dat
expeTransferBioneatPath[T2]=bioneat-TBioneatTransfer2/result.dat
declare -A TransferBioneatnbIterations
TransferBioneatnbIterations[center]=60 #100
TransferBioneatnbIterations[top]=60 #100
TransferBioneatnbIterations[T]=60 #100
TransferBioneatnbIterations[T2]=60 #100
mkdir -p $plotsDir/TransferBioneat
for name in "${!TransferBioneatnbIterations[@]}"; do
    $plotScripts/plotFitnessGECCO2017.py -i $dataDir/"${expeTransferBioneatPath[$name]}"  -o $plotsDir/TransferBioneat/fitness_TransferBioneat_$name.pdf --nbRuns 24 --nbIterations ${TransferBioneatnbIterations[$name]} --nbEvalsPerIt 50 --max ${fitnessMax[$name]} &
    #$plotScripts/plotFitnessGECCO2017.py -i $dataDir/"${expeTransferBioneatPath[$name]}"  -o $plotsDir/TransferBioneat/fitness_TransferBioneat_$name.pdf --nbRuns 8 --nbIterations ${TransferBioneatnbIterations[$name]} --nbEvalsPerIt 50 --max ${fitnessMax[$name]} &
    pids+=($!)
done



# Plots *-ME-TI
declare -A expeMETIPath
expeMETIPath[center]=daccad3-6center4000x0.3x-0.10medianScore_5trials-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0
expeMETIPath[top]=daccad3-6top1000x0.2x-0.10medianScore_5trials-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0
#expeMETIPath[T]=daccad3-6T4000x0.2x-0.20medianScore_5trials-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0
expeMETIPath[T]=daccad3-6T4000x0.2x-0.20medianScore_5trials-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0
#expeMETIPath[TDecomp]=daccad3-6T_center_top4000x0.2x-0.20medianScore_5trials-nTemplate7-13_ratioIn1_rationIn2-ME7x5x5-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0
expeMETIPath[T2]=daccad3-6T4000x0.2x-0.50medianScore_5trials-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0
expeMETIPath[Tcx]=daccad3-6T4000x0.2x-0.20medianScore_5trials-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet7cx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x1.0-1.0
declare -A expeMETInbIterations
expeMETInbIterations[center]=60 #100
expeMETInbIterations[top]=60 #100
expeMETInbIterations[T]=60 #100
expeMETInbIterations[T2]=60 #100
expeMETInbIterations[Tcx]=60 #100
#expeMETInbIterations[TDecomp]=60 #100
mkdir -p $plotsDir/METI
for name in "${!expeMETInbIterations[@]}"; do
    $plotScripts/plotFitness.py -i $dataDir/"${expeMETIPath[$name]}" -o $plotsDir/METI/fitness_METI_$name.pdf --nbRuns 24 --nbIterations ${expeMETInbIterations[$name]} --nbEvalsPerIt 50 --max ${fitnessMax[$name]} &
    #$plotScripts/plotFitness.py -i $dataDir/"${expeMETIPath[$name]}" -o $plotsDir/METI/fitness_METI_$name.pdf --nbRuns 8 --nbIterations ${expeMETInbIterations[$name]} --nbEvalsPerIt 50 --max ${fitnessMax[$name]} &
    pids+=($!)
done



# Plots *-approx-ME-TI
declare -A expeApproxMETIPath
#expeApproxMETIPath[center]=daccadApprox8_3-6center4000x0.2x-0.10medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0
expeApproxMETIPath[center]=daccadApprox2_3-6center4000x0.3x-0.10medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0
#expeApproxMETIPath[top]=daccadApprox8_3-6top4000x0.2x-0.10medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0
expeApproxMETIPath[top]=daccadApprox2_3-6top4000x0.2x-0.10medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0
#expeApproxMETIPath[T]=daccadApprox8_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0
expeApproxMETIPath[T]=daccadApprox2_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0
#expeApproxMETIPath[TDecomp]=daccadApprox8_3-6T_center_top4000x0.2x-0.20medianScore-nTemplate7-13_ratioIn1_rationIn2-ME7x5x5-5000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0
expeApproxMETIPath[T2]=daccadApprox2_3-6T4000x0.2x-0.50medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0
expeApproxMETIPath[Tcx]=daccadApprox2_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet7cx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x1.0-1.0
declare -A expeApproxMETInbIterations
expeApproxMETInbIterations[center]=60 #100
expeApproxMETInbIterations[top]=60 #100
expeApproxMETInbIterations[T]=60 #100
expeApproxMETInbIterations[T2]=60 #100
expeApproxMETInbIterations[Tcx]=60 #100
#expeApproxMETInbIterations[TDecomp]=60 #100
mkdir -p $plotsDir/ApproxMETI
for name in "${!expeApproxMETInbIterations[@]}"; do
    $plotScripts/plotFitness.py -i $dataDir/"${expeApproxMETIPath[$name]}" -o $plotsDir/ApproxMETI/fitness_ApproxMETI_$name.pdf --nbRuns 24 --nbIterations ${expeApproxMETInbIterations[$name]} --nbEvalsPerIt 50 --max ${fitnessMax[$name]} &
    pids+=($!)
done



# Plots *-transfer-ME-TI
declare -A expeTransferMETIPath
expeTransferMETIPath[center]=daccadApprox8_3-6center4000x0.3x-0.10medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6Transfer_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x1.0-1.0x4000
expeTransferMETIPath[top]=daccadApprox8_3-6top4000x0.2x-0.10medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6Transfer_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x1.0-1.0x4000
#expeTransferMETIPath[T]=daccadApprox8_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-5000x50-DaccadTopologicalInitBionet6Transfer_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x1.0-1.0x4000
expeTransferMETIPath[T]=daccadApprox2_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6Transfer_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x1.0-1.0x1500
##expeTransferMETIPath[TDecomp]=daccadApprox8_3-6T_center_top4000x0.2x-0.20medianScore-nTemplate7-13_ratioIn1_rationIn2-ME7x5x5-5000x50-DaccadTopologicalInitBionet6Transfer_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x1.0-1.0x4000
#expeTransferMETIPath[TDecomp]=daccadApprox2_3-6T_center_top4000x0.2x-0.20medianScore-nTemplate7-13_ratioIn1_rationIn2-ME7x5x5-3000x50-DaccadTopologicalInitBionet6Transfer_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x1.0-1.0x1500
expeTransferMETIPath[T2]=daccadApprox2_3-6T4000x0.2x-0.50medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6Transfer_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x1.0-1.0x1500
expeTransferMETIPath[Tcx]=daccadApprox2_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet7Transfercx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x1.0-1.0x1500
declare -A expeTransferMETInbIterations
expeTransferMETInbIterations[center]=60 #100
expeTransferMETInbIterations[top]=60 #100
expeTransferMETInbIterations[T]=60 #100
expeTransferMETInbIterations[T2]=60 #100
expeTransferMETInbIterations[Tcx]=60 #100
#expeTransferMETInbIterations[TDecomp]=60 #100
mkdir -p $plotsDir/TransferMETI
for name in "${!expeTransferMETInbIterations[@]}"; do
    $plotScripts/plotFitness.py -i $dataDir/"${expeTransferMETIPath[$name]}" -o $plotsDir/TransferMETI/fitness_TransferMETI_$name.pdf --nbRuns 24 --nbIterations ${expeTransferMETInbIterations[$name]} --nbEvalsPerIt 50 --max ${fitnessMax[$name]} &
    #$plotScripts/plotFitness.py -i $dataDir/"${expeTransferMETIPath[$name]}" -o $plotsDir/TransferMETI/fitness_TransferMETI_$name.pdf --nbRuns 8 --nbIterations ${expeTransferMETInbIterations[$name]} --nbEvalsPerIt 50 --max ${fitnessMax[$name]} &
    pids+=($!)
done


#
## Plots *-approx-ME-TI-CX
#declare -A expeApproxMETICXPath
##jexpeApproxMETICXPath[center]=daccadApprox2_3-6center4000x0.3x-0.10medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0
##jexpeApproxMETICXPath[top]=daccadApprox2_3-6top4000x0.2x-0.10medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0
#expeApproxMETICXPath[T]=daccadApprox2_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0
##expeApproxMETICXPath[T2]=daccadApprox2_3-6T4000x0.2x-0.50medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet6_1x0.9x0.1x1.0x0.8x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x1-3x1.0-1.0
#declare -A expeApproxMETICXnbIterations
##expeApproxMETICXnbIterations[center]=60 #100
##expeApproxMETICXnbIterations[top]=60 #100
#expeApproxMETICXnbIterations[T]=60 #100
##expeApproxMETICXnbIterations[T2]=60 #100
##expeApproxMETICXnbIterations[TDecomp]=60 #100
#mkdir -p $plotsDir/ApproxMETICX
#for name in "${!expeApproxMETICXnbIterations[@]}"; do
#    $plotScripts/plotFitness.py -i $dataDir/"${expeApproxMETICXPath[$name]}" -o $plotsDir/ApproxMETICX/fitness_ApproxMETICX_$name.pdf --nbRuns 16 --nbIterations ${expeApproxMETICXnbIterations[$name]} --nbEvalsPerIt 50 --max ${fitnessMax[$name]} &
#    pids+=($!)
#done
#








##################################

#function analyseExpe {
#	local configName expeId
#	configName=$1
#	expeId=$2
#	fitnessBounds=$3
#	shift; shift; shift;
#
#	echo Creating plots for config \'$configName\' expe \'$expeId\'
#	mkdir -p $plotsDir/$configName
#	$plotScripts/plotMaps.py -i $dataDir/$configName/final_$expeId.p -o $plotsDir/$configName --suffix $expeId --fitnessBounds $fitnessBounds --drawCbar &
#	pids+=($!)
#}



#
## Plots LongBioneat
#declare -A expeLongBioneatPath
#expeLongBioneatPath[bottom]=bioneat-RDBottomNicolas/fitness-bioneat-RDBottomNicolas.dat
#expeLongBioneatPath[right]=bioneat-RDRightNicolas/fitness-bioneat-RDRightNicolas.dat
#expeLongBioneatPath[top]=bioneat-RDTopNicolas/fitness-bioneat-RDTopNicolas.dat
#expeLongBioneatPath[center]=bioneat-RDLineNicolasGoodParams/fitness-bioneat-RDLineNicolasGoodParams.dat
#declare -A longBioneatnbIterations
#longBioneatnbIterations[bottom]=200
#longBioneatnbIterations[right]=200
#longBioneatnbIterations[top]=200
#longBioneatnbIterations[center]=200
#mkdir -p $plotsDir/longBioneat
#for name in "${!longBioneatnbIterations[@]}"; do
#    $plotScripts/plotFitnessGECCO2017.py -i $dataDir/"${expeLongBioneatPath[$name]}"  -o $plotsDir/longBioneat/fitness_longBioneat_$name.pdf --nbRuns 10 --nbIterations ${longBioneatnbIterations[$name]} --nbEvalsPerIt 50 &
#    pids+=($!)
#done
#
## Plots LongBioneatv2
#declare -A expeLongBioneatv2Path
#expeLongBioneatv2Path[center]=bioneat-RDLineNicolasGoodParams-v2/result.dat
#declare -A longBioneatv2nbIterations
#longBioneatv2nbIterations[center]=200
#mkdir -p $plotsDir/longBioneatv2
#for name in "${!longBioneatv2nbIterations[@]}"; do
#    $plotScripts/plotFitnessGECCO2017.py -i $dataDir/"${expeLongBioneatv2Path[$name]}"  -o $plotsDir/longBioneatv2/fitness_longBioneatv2_$name.pdf --nbRuns 10 --nbIterations ${longBioneatv2nbIterations[$name]} --nbEvalsPerIt 50 &
#    pids+=($!)
#done
#
## Plots SmallNetBioneat
#declare -A expeSmallNetBioneatPath
#expeSmallNetBioneatPath[center]=bioneat-RDLineLeoSmallNetwork200/result.dat
#declare -A smallNetBioneatnbIterations
#smallNetBioneatnbIterations[center]=50
#mkdir -p $plotsDir/smallNetBioneat
#for name in "${!smallNetBioneatnbIterations[@]}"; do
#    $plotScripts/plotFitnessGECCO2017.py -i $dataDir/"${expeSmallNetBioneatPath[$name]}"  -o $plotsDir/smallNetBioneat/fitness_smallNetBioneat_$name.pdf --nbRuns 8 --nbIterations ${smallNetBioneatnbIterations[$name]} --nbEvalsPerIt 50 &
#    pids+=($!)
#done
#
#
## Plots Bioneat T target
#declare -A expeBioneatTPath
#expeBioneatTPath[center]=bioneat-RDTLeoSmallNetwork/result.dat
#declare -A bioneatTnbIterations
#bioneatTnbIterations[center]=100
#mkdir -p $plotsDir/bioneatT
#for name in "${!bioneatTnbIterations[@]}"; do
#    $plotScripts/plotFitnessGECCO2017.py -i $dataDir/"${expeBioneatTPath[$name]}"  -o $plotsDir/bioneatT/fitness_bioneatT_$name.pdf --nbRuns 8 --nbIterations ${bioneatTnbIterations[$name]} --nbEvalsPerIt 50 &
#    pids+=($!)
#done
#
#
#
#
##
### Plots Map-elites
##declare -A expeMapElitesPath
##expeMapElitesPath[bottom]=daccad5bottom1000-nTemplate1-20_ratioIn-ME19x20-500x50x50-DaccadUniformStabilitiesSobolWithVaryingSparsityConnections0.92x1x5x500-DaccadPolynomialMutationWithPolynomialSparsityBudget2_0.70x0.10x0.05x0.10x0.05x19x19x20
##expeMapElitesPath[right]=daccad5right1000-nTemplate15-30_ratioIn-ME15x25-2500x50-DaccadSparseInitAndMut2_500x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x20x20x30x20.x0.4
##expeMapElitesPath[top]=daccad5top1000-nTemplate15-30_ratioIn-ME15x25-2500x50-DaccadSparseInitAndMut2_500x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x20x20x30x20.x0.4
###expeMapElitesPath[center]=daccad5center4000-nTemplate15-30_ratioIn-ME15x25-7500x50-DaccadSparseInitAndMut2_500x1x0.0x1.0x0.70x0.10x0.05x0.10x0.05x20x20x30x20.x0.4
##expeMapElitesPath[center]=daccad3-7center4000medianScore0.0000001_2trials-nTemplate1-13_ratioIn-ME13x10-7500x50-DaccadSearchMutWithBatchSuggestions2_1x1x0.0x1.0x0.60x0.10x0.10x0.10x0.10x7x6x13x0.2x2.0x0.8
##declare -A expeMapElitesnbIterations
##expeMapElitesnbIterations[bottom]=50
##expeMapElitesnbIterations[right]=50
##expeMapElitesnbIterations[top]=50
##expeMapElitesnbIterations[center]=150
##mkdir -p $plotsDir/mapelites
##for name in "${!expeMapElitesnbIterations[@]}"; do
##    $plotScripts/plotFitness.py -i $dataDir/"${expeMapElitesPath[$name]}" -o $plotsDir/mapelites/fitness_mapelites_$name.pdf --nbRuns 7 --nbIterations ${expeMapElitesnbIterations[$name]} --nbEvalsPerIt 50 &
##    pids+=($!)
##done
##
#
## Plots Map-elites with Bioneat mutations
#declare -A expeMEBIONEATPath
#expeMEBIONEATPath[center]=daccad3-7center4000medianScore_5trials-nTemplate1-16_ratioIn-ME16x5-10000x50-DaccadBioneatMut3_1x1x0.0x1.0x0.80.05x0.05x0.05x0.05x16x12x12x0.2x2.0x0.8-GD0.5
#declare -A expeMEBIONEATnbIterations
#expeMEBIONEATnbIterations[center]=200
#mkdir -p $plotsDir/meBioneat
#for name in "${!expeMEBIONEATnbIterations[@]}"; do
#    $plotScripts/plotFitness.py -i $dataDir/"${expeMEBIONEATPath[$name]}" -o $plotsDir/meBioneat/fitness_meBioneat_$name.pdf --nbRuns 8 --nbIterations ${expeMEBIONEATnbIterations[$name]} --nbEvalsPerIt 50 &
#    pids+=($!)
#done
#
#
## Plots Map-elites on small networks with Bioneat mutations
#declare -A expeSMALLMEBIONEATPath
##expeSMALLMEBIONEATPath[center]=daccad3-5center4000medianScore_2trials-nTemplate1-7_ratioIn-ME7x5-2500x50-DaccadBioneatMut3_1x1x0.0x1.0x0.80.05x0.05x0.05x0.05x7x7x7x0.2x2.0x0.8-GD0.5
#expeSMALLMEBIONEATPath[center]=daccad3-5center4000x0.3medianScore_5trials-nTemplate7-12_ratioIn-ME6x4-2500x50-DaccadTopologicalInitBioneat4_1x0.7x0.3x1.0x0.80.05x0.05x0.05x0.05x12x6x6x0.2x2.0x0.8x1-3-GD1.0
#declare -A expeSMALLMEBIONEATnbIterations
#expeSMALLMEBIONEATnbIterations[center]=50
#mkdir -p $plotsDir/smallMEBioneat
#for name in "${!expeSMALLMEBIONEATnbIterations[@]}"; do
#    $plotScripts/plotFitness.py -i $dataDir/"${expeSMALLMEBIONEATPath[$name]}" -o $plotsDir/smallMEBioneat/fitness_smallMEBioneat_$name.pdf --nbRuns 8 --nbIterations ${expeSMALLMEBIONEATnbIterations[$name]} --nbEvalsPerIt 50 &
#    pids+=($!)
#done
#
#
## Plots Map-elites on small networks with Bioneat mutations and meanHellinger as feature
#declare -A expeSMALLMEBIONEATHELLPath
#expeSMALLMEBIONEATHELLPath[center]=daccad3-5center4000x0.3medianScore_2trials-nTemplate7-12_meanHellinger-ME6x4-2500x50-DaccadTopologicalInitBioneat5_1x0.9x0.1x1.0x0.80.05x0.05x0.05x0.05x12x6x6x0.2x2.0x0.8x1-3x0.05-0.70_GD1.0/
#declare -A expeSMALLMEBIONEATHELLnbIterations
#expeSMALLMEBIONEATHELLnbIterations[center]=50
#mkdir -p $plotsDir/smallMEBioneatHELL
#for name in "${!expeSMALLMEBIONEATHELLnbIterations[@]}"; do
#    $plotScripts/plotFitness.py -i $dataDir/"${expeSMALLMEBIONEATHELLPath[$name]}" -o $plotsDir/smallMEBioneatHELL/fitness_smallMEBioneatHELL_$name.pdf --nbRuns 8 --nbIterations ${expeSMALLMEBIONEATHELLnbIterations[$name]} --nbEvalsPerIt 50 &
#    pids+=($!)
#done
#
#
##
### Plots Map-elites with Bioneat mutations
##declare -A expeMEBIONEATPath
##expeMEBIONEATPath[center]=daccad3-7center4000medianScore_2trials-nTemplate1-13_ratioIn-ME13x10-7500x50-DaccadBioneatMut3_1x1x0.0x1.0x0.96x0.01x0.01x0.01x0.01x7x6x13x0.2x2.0x0.8
##declare -A expeMEBIONEATnbIterations
##expeMEBIONEATnbIterations[center]=150
##mkdir -p $plotsDir/meBioneat
##for name in "${!expeMEBIONEATnbIterations[@]}"; do
##    $plotScripts/plotFitness.py -i $dataDir/"${expeMEBIONEATPath[$name]}" -o $plotsDir/meBioneat/fitness_meBioneat_$name.pdf --nbRuns 7 --nbIterations ${expeMEBIONEATnbIterations[$name]} --nbEvalsPerIt 50 &
##    pids+=($!)
##done
##
#
#
### Plots Map-elites long
##declare -A expeMapElitesLongPath
##expeMapElitesLongPath[bottom]=daccad5bottom1000-nTemplate1-20_ratioIn-ME19x20-6000x480x50-DaccadUniformStabilitiesSobolWithVaryingSparsityConnections0.92x1x5x6000-DaccadPolynomialMutationWithPolynomialSparsityBudget2_0.70x0.10x0.05x0.10x0.05x19x19x20
##expeMapElitesLongPath[right]=daccad5right1000-nTemplate1-20_ratioIn-ME19x20-6000x480x50-DaccadUniformStabilitiesSobolWithVaryingSparsityConnections0.92x1x5x6000-DaccadPolynomialMutationWithPolynomialSparsityBudget2_0.70x0.10x0.05x0.10x0.05x19x19x20
##expeMapElitesLongPath[top]=daccad5top1000-nTemplate1-20_ratioIn-ME19x20-6000x480x50-DaccadUniformStabilitiesSobolWithVaryingSparsityConnections0.92x1x5x6000-DaccadPolynomialMutationWithPolynomialSparsityBudget2_0.70x0.10x0.05x0.10x0.05x19x19x20
###expeMapElitesLongPath[center]=daccad5center4000-nTemplate1-20_ratioIn-ME19x20-18000x1440x50-DaccadUniformStabilitiesSobolWithVaryingSparsityConnections0.92x1x5x18000-DaccadPolynomialMutationWithPolynomialSparsityBudget2_0.70x0.10x0.05x0.10x0.05x19x19x20
##declare -A expeMapElitesLongnbIterations
##expeMapElitesLongnbIterations[bottom]=480
##expeMapElitesLongnbIterations[right]=480
##expeMapElitesLongnbIterations[top]=480
###expeMapElitesLongnbIterations[center]=1440
##mkdir -p $plotsDir/mapelitesLong
##for name in "${!expeMapElitesLongnbIterations[@]}"; do
##    $plotScripts/plotFitness.py -i $dataDir/"${expeMapElitesLongPath[$name]}" -o $plotsDir/mapelitesLong/fitness_mapelitesLong_$name.pdf --nbRuns 1 --nbIterations ${expeMapElitesLongnbIterations[$name]} --nbEvalsPerIt 50 &
##    pids+=($!)
##done
#
#
#
#
## Plots Map-elites with Bioneat mutations on T target
#declare -A expeMEBIONEATTPath
#expeMEBIONEATTPath[T]=daccad3-6T4000x0.2x-0.10medianScore_5trials-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet7cx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x0.90-0.90_GD1.0
#declare -A expeMEBIONEATTnbIterations
#expeMEBIONEATTnbIterations[T]=60
#mkdir -p $plotsDir/meBioneatT
#for name in "${!expeMEBIONEATTnbIterations[@]}"; do
#    $plotScripts/plotFitness.py -i $dataDir/"${expeMEBIONEATTPath[$name]}" -o $plotsDir/meBioneatT/fitness_meBioneatT_$name.pdf --nbRuns 8 --nbIterations ${expeMEBIONEATTnbIterations[$name]} --nbEvalsPerIt 50 --min 0.3 --max 0.8 &
#    pids+=($!)
#done
#
## Plots Map-elites with Bioneat mutations on T target, with decomposition
#declare -A expeMEBIONEATTDECOMPPath
#expeMEBIONEATTDECOMPPath[T]=daccad3-6T_center_top4000x0.2x-0.10medianScore_5trials-ratioIn1_rationIn2-ME5x5-3000x50-DaccadTopologicalInitBionet7cx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x0.90-0.90_GD1.0
#declare -A expeMEBIONEATTDECOMPnbIterations
#expeMEBIONEATTDECOMPnbIterations[T]=60
#mkdir -p $plotsDir/meBioneatTDecomp
#for name in "${!expeMEBIONEATTDECOMPnbIterations[@]}"; do
#    $plotScripts/plotFitness.py -i $dataDir/"${expeMEBIONEATTDECOMPPath[$name]}" -o $plotsDir/meBioneatTDecomp/fitness_meBioneatTDecomp_$name.pdf --nbRuns 8 --nbIterations ${expeMEBIONEATTDECOMPnbIterations[$name]} --nbEvalsPerIt 50 --min 0.3 --max 0.8 &
#    pids+=($!)
#done
#
## Plots Map-elites with Bioneat mutations on T target, with decomposition + nbTemplates
#declare -A expeMEBIONEATTDECOMP2Path
#expeMEBIONEATTDECOMP2Path[T]=daccad3-6T_center_top4000pre400x0.2x-0.10medianScore_5trials-nTemplate7-13_ratioIn1_rationIn2-ME7x5x5-10000x50-DaccadTopologicalInitBionet7cx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x0.90-0.90_GD1.0
#declare -A expeMEBIONEATTDECOMP2nbIterations
#expeMEBIONEATTDECOMP2nbIterations[T]=60 #200
#mkdir -p $plotsDir/meBioneatTDecomp2
#for name in "${!expeMEBIONEATTDECOMP2nbIterations[@]}"; do
#    $plotScripts/plotFitness.py -i $dataDir/"${expeMEBIONEATTDECOMP2Path[$name]}" -o $plotsDir/meBioneatTDecomp2/fitness_meBioneatTDecomp2_$name.pdf --nbRuns 8 --nbIterations ${expeMEBIONEATTDECOMP2nbIterations[$name]} --nbEvalsPerIt 50 --min 0.3 --max 0.8 &
#    pids+=($!)
#done
#
#
## Plots Map-elites approx with Bioneat mutations on T target
#declare -A expeAPPROXMEBIONEATTPath
#expeAPPROXMEBIONEATTPath[T]=daccadApprox8_3-6T4000x0.2x-0.20medianScore-nTemplate7-13-ME7-3000x50-DaccadTopologicalInitBionet7cx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x0.90-0.90_GD1.0
#declare -A expeAPPROXMEBIONEATTnbIterations
#expeAPPROXMEBIONEATTnbIterations[T]=60
#mkdir -p $plotsDir/approxmeBioneatT
#for name in "${!expeAPPROXMEBIONEATTnbIterations[@]}"; do
#    $plotScripts/plotFitness.py -i $dataDir/"${expeAPPROXMEBIONEATTPath[$name]}" -o $plotsDir/approxmeBioneatT/fitness_approxmeBioneatT_$name.pdf --nbRuns 16 --nbIterations ${expeAPPROXMEBIONEATTnbIterations[$name]} --nbEvalsPerIt 50 --min 0.6 --max 1.0 &
#    pids+=($!)
#done
#
## Plots Map-elites approx with Bioneat mutations on T target, with decomposition
#declare -A expeAPPROXMEBIONEATTDECOMPPath
#expeAPPROXMEBIONEATTDECOMPPath[T]=daccadApprox8_3-6T_center_top4000x0.2x-0.20medianScore-ratioIn1_rationIn2-ME5x5-3000x50-DaccadTopologicalInitBionet7cx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x0.90-0.90_GD1.0
#declare -A expeAPPROXMEBIONEATTDECOMPnbIterations
#expeAPPROXMEBIONEATTDECOMPnbIterations[T]=60
#mkdir -p $plotsDir/approxmeBioneatTDecomp
#for name in "${!expeAPPROXMEBIONEATTDECOMPnbIterations[@]}"; do
#    $plotScripts/plotFitness.py -i $dataDir/"${expeAPPROXMEBIONEATTDECOMPPath[$name]}" -o $plotsDir/approxmeBioneatTDecomp/fitness_approxmeBioneatTDecomp_$name.pdf --nbRuns 16 --nbIterations ${expeAPPROXMEBIONEATTDECOMPnbIterations[$name]} --nbEvalsPerIt 50 --min 0.6 --max 1.0 &
#    pids+=($!)
#done
#
## Plots Map-elites approx with Bioneat mutations on T target, with decomposition + nbTemplates
#declare -A expeAPPROXMEBIONEATTDECOMP2Path
#expeAPPROXMEBIONEATTDECOMP2Path[T]=daccadApprox8_3-6T_center_top4000x0.2x-0.80medianScore-nTemplate7-15_ratioIn1_rationIn2-ME9x5x5-20000x50-DaccadTopologicalInitBionet7cx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x15x9x9x0.2x2.0x0.8x0.3x1-3x0.10-0.90_GD1.0
#declare -A expeAPPROXMEBIONEATTDECOMP2nbIterations
#expeAPPROXMEBIONEATTDECOMP2nbIterations[T]=400
#mkdir -p $plotsDir/approxmeBioneatTDecomp2
#for name in "${!expeAPPROXMEBIONEATTDECOMP2nbIterations[@]}"; do
#    $plotScripts/plotFitness.py -i $dataDir/"${expeAPPROXMEBIONEATTDECOMP2Path[$name]}" -o $plotsDir/approxmeBioneatTDecomp2/fitness_approxmeBioneatTDecomp2_$name.pdf --nbRuns 7 --nbIterations ${expeAPPROXMEBIONEATTDECOMP2nbIterations[$name]} --nbEvalsPerIt 50 --min 0.1 --max 0.8 &
#    pids+=($!)
#done
#
#
## Plots Map-elites approx with Bioneat mutations on T target, with decomposition + nbTemplates 3
#declare -A expeAPPROXMEBIONEATTDECOMP3Path
#expeAPPROXMEBIONEATTDECOMP3Path[T]=daccadApprox8_3-6T_center_top4000x0.2x-0.50medianScore-nTemplate7-13_ratioIn1_rationIn2-ME7x5x5-10000x50-DaccadTopologicalInitBionet7cx2_1x0.9x0.1x1.0x0.5x0.3x0.05x0.05x0.05x0.05x13x7x7x0.2x2.0x0.8x0.3x1-3x0.90-0.90_GD1.0
#declare -A expeAPPROXMEBIONEATTDECOMP3nbIterations
#expeAPPROXMEBIONEATTDECOMP3nbIterations[T]=200
#mkdir -p $plotsDir/approxmeBioneatTDecomp3
#for name in "${!expeAPPROXMEBIONEATTDECOMP3nbIterations[@]}"; do
#    $plotScripts/plotFitness.py -i $dataDir/"${expeAPPROXMEBIONEATTDECOMP3Path[$name]}" -o $plotsDir/approxmeBioneatTDecomp3/fitness_approxmeBioneatTDecomp3_$name.pdf --nbRuns 7 --nbIterations ${expeAPPROXMEBIONEATTDECOMP3nbIterations[$name]} --nbEvalsPerIt 50 --min 0.1 --max 0.8 &
#    pids+=($!)
#done
#




# Wait for plots to finish
for i in "${pids[@]}"; do
    wait $i
done


# Copy final plots
mkdir -p $finalPlotsDir
rm -fr $finalPlotsDir/*
cp $plotsDir/gecco2017/fitness*pdf $finalPlotsDir/
cp $plotsDir/Bioneat/fitness*pdf $finalPlotsDir/
cp $plotsDir/ApproxBioneat/fitness*pdf $finalPlotsDir/
cp $plotsDir/TransferBioneat/fitness*pdf $finalPlotsDir/
cp $plotsDir/METI/fitness*pdf $finalPlotsDir/
cp $plotsDir/ApproxMETI/fitness*pdf $finalPlotsDir/
cp $plotsDir/TransferMETI/fitness*pdf $finalPlotsDir/


cp $plotsDir/approx/joint-medianScore-approxMedianScore-target0.pdf $finalPlotsDir/
cp $plotsDir/retrials/joint-size_subparts-dist_median_to_subpartsmedian.pdf $finalPlotsDir/


#
## Copy final plots
#mkdir -p $finalPlotsDir
#rm -fr $finalPlotsDir/*
#cp $plotsDir/gecco2017/fitness*pdf $finalPlotsDir/
#cp $plotsDir/mapelites/fitness*pdf $finalPlotsDir/
##cp $plotsDir/mapelitesLong/fitness*pdf $finalPlotsDir/
#cp $plotsDir/longBioneat/fitness*pdf $finalPlotsDir/
#cp $plotsDir/meBioneat/fitness*pdf $finalPlotsDir/
#cp $plotsDir/smallMEBioneat/fitness*pdf $finalPlotsDir/
#


# Compile tikz files
cd $tikzDir
pdflatex fitnessPerEvalsCenter.tex
pdflatex fitnessPerEvalsTop.tex
pdflatex fitnessPerEvalsT.tex


# Return to original directory
cd $origDir


# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
