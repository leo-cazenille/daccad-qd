#!/usr/bin/env python3


########## IMPORTS ########### {{{1

import os
from timeit import default_timer as timer
import copy
import numpy as np
import random
import warnings
import traceback

from qdpy.algorithms import *
from qdpy.containers import *
from qdpy.plots import *
from qdpy.base import *
from qdpy import tools
from qdpy import hdsobol

from typing import Optional, Tuple, List, Iterable, Iterator, Any, TypeVar, Generic, Union, Sequence, MutableSet, MutableSequence, Type, Callable, Generator, Mapping, MutableMapping, overload

from base import *



@registry.register
class DaccadSearchMutWithBatchSuggestions(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [3, 7],
            min_init_budget: int = 1,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            prob_parameter_mut: float = 0.96,
            prob_template_add: float = 0.01,
            prob_template_del: float = 0.01,
            prob_signal_species_add: float = 0.01,
            prob_inhibition_species_add: float = 0.01,
            nbConnActivationsDomain: Sequence[int] = [1, 7],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [1, 13],
            f1: float = 0.2,
            f2: float = 2.0,
            mut_pb: float = 0.8,
            allow_gd: bool = True,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.prob_parameter_mut = prob_parameter_mut
        self.prob_template_add = prob_template_add
        self.prob_template_del = prob_template_del
        self.prob_signal_species_add = prob_signal_species_add
        self.prob_inhibition_species_add = prob_inhibition_species_add
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.f1 = f1
        self.f2 = f2
        self.mut_pb = 0.8
        self.ref_ind = None
        self.ref_usage_budget = 0

        base_ind_gen = gen_daccad_individuals_GD(self.ind_domain) if allow_gd else gen_daccad_individuals(self.ind_domain)
        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=base_ind_gen, **kwargs)


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        if initialise: # Initialisation
            # Create a base individual
            standard_init_ind(base_ind)
            return base_ind, False

        else: # Selection
            try:
                res = tools.non_trivial_sel_random(collection), True
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()
            return res


    def _vary(self, individual):
        for _ in range(500): # XXX
            ind = copy.deepcopy(individual)
            ind.name = str(id(ind))

            active_activations_coords = list(zip(*np.where(ind.activations)))
            nb_active_activations = len(active_activations_coords)
            active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
            nb_active_inhibitions = len(active_inhibitions_coords)

            # Choose the kind of mutation
            choice = np.random.choice(5, p=(self.prob_parameter_mut,
                self.prob_template_add, self.prob_template_del,
                self.prob_signal_species_add, self.prob_inhibition_species_add))

            # Perform the mutation
            if choice == 0: # Parameters mutation
                if self.ref_ind is not None and self.ref_usage_budget > 0:
                    ind = copy.deepcopy(self.ref_ind)
                    ind.name = str(id(ind))
                    self.ref_usage_budget -= 1
                else:
                    self.ref_ind = ind
                    self.ref_usage_budget = np.random.randint(10, 30)
                mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
            elif choice == 1: # Add Activation
                mutation_add_activation(ind)
            elif choice == 2: # Del Activation
                mutation_del_activation(ind)
            elif choice == 3: # Signal species
                mutation_bioneat_signal_species(ind)
            elif choice == 4: # Inhibition species
                mutation_bioneat_inhibition_species(ind)


            ## Verify if there are no orphaned nodes
            #is_orphaned = (not(any(ind.activations[:,i]) or any(ind.activations[i,:]) or any(ind.inhibitions[i,:,:].flatten())) for i in range(ind.nb_nodes))
            #if any(is_orphaned):
            #    continue

            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                active_activations_coords = list(zip(*np.where(ind.activations)))
                nb_active_activations = len(active_activations_coords)
                active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
                nb_active_inhibitions = len(active_inhibitions_coords)
                if nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        if choice > 0:
            self.ref_ind = ind
            self.ref_usage_budget = np.random.randint(20, 50)
        return ind





@registry.register
class DaccadSearchMutWithSobolBatchSuggestions(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [3, 7],
            min_init_budget: int = 1,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            prob_parameter_mut: float = 0.96,
            prob_template_add: float = 0.01,
            prob_template_del: float = 0.01,
            prob_signal_species_add: float = 0.01,
            prob_inhibition_species_add: float = 0.01,
            nbConnActivationsDomain: Sequence[int] = [1, 7],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [1, 13],
            f1: float = 0.2,
            f2: float = 2.0,
            mut_pb: float = 0.8,
            allow_gd: bool = True,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.prob_parameter_mut = prob_parameter_mut
        self.prob_template_add = prob_template_add
        self.prob_template_del = prob_template_del
        self.prob_signal_species_add = prob_signal_species_add
        self.prob_inhibition_species_add = prob_inhibition_species_add
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.f1 = f1
        self.f2 = f2
        self.mut_pb = 0.8
        self.ref_ind = None
        self.ref_usage_budget = 0
        self.ref_sobol_vec = None

        base_ind_gen = gen_daccad_individuals_GD(self.ind_domain) if allow_gd else gen_daccad_individuals(self.ind_domain)
        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=base_ind_gen, **kwargs)


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        if initialise: # Initialisation
            # Create a base individual
            standard_init_ind(base_ind)
            self.ref_ind = base_ind
            dim = len(np.where(base_ind)[0])
            self.ref_usage_budget = dim * 5
            self._init_sobol_vec(self.ref_usage_budget, dim)
            return base_ind, False

        else: # Selection
            try:
                res = tools.non_trivial_sel_random(collection), True
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()
            return res

    def _init_sobol_vec(self, nb, dim):
        self.ref_sobol_vec = list(hdsobol.gen_sobol_vectors(nb, dim))

    def _vary(self, individual):
        if self.ref_ind is not None and self.ref_usage_budget > 0:
            ind = copy.deepcopy(self.ref_ind)
            ind.name = str(id(ind))
            self.ref_usage_budget -= 1
            if self.ref_sobol_vec is not None and len(self.ref_sobol_vec):
                weights = self.ref_sobol_vec.pop(0)
                for iw, iind in enumerate(np.where(ind)[0]):
                    ind[iind] = weights[iw]
                ind.update()
            else:
                mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                ind.assemble()
            return ind

        for _ in range(500): # XXX
            ind = copy.deepcopy(individual)
            ind.name = str(id(ind))

            active_activations_coords = list(zip(*np.where(ind.activations)))
            nb_active_activations = len(active_activations_coords)
            active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
            nb_active_inhibitions = len(active_inhibitions_coords)

            # Choose the kind of mutation
            choice = np.random.choice(5, p=(self.prob_parameter_mut,
                self.prob_template_add, self.prob_template_del,
                self.prob_signal_species_add, self.prob_inhibition_species_add))

            # Perform the mutation
            if choice == 0: # Parameters mutation
                self.ref_ind = ind
                self.ref_usage_budget = np.random.randint(10, 30)
                self.ref_sobol_vec = None
                mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
            elif choice == 1: # Add Activation
                mutation_add_activation(ind)
            elif choice == 2: # Del Activation
                mutation_del_activation(ind)
            elif choice == 3: # Signal species
                mutation_bioneat_signal_species(ind)
            elif choice == 4: # Inhibition species
                mutation_bioneat_inhibition_species(ind)


            ## Verify if there are no orphaned nodes
            #is_orphaned = (not(any(ind.activations[:,i]) or any(ind.activations[i,:]) or any(ind.inhibitions[i,:,:].flatten())) for i in range(ind.nb_nodes))
            #if any(is_orphaned):
            #    continue

            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                active_activations_coords = list(zip(*np.where(ind.activations)))
                nb_active_activations = len(active_activations_coords)
                active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
                nb_active_inhibitions = len(active_inhibitions_coords)
                if nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        if choice > 0:
            self.ref_ind = ind
            dim = len(np.where(ind)[0])
            self.ref_usage_budget = dim * 5
            self._init_sobol_vec(self.ref_usage_budget, dim)
        return ind




@registry.register
class DaccadSearchMutWithBatchSuggestions2(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes: int = 7,
            min_init_budget: int = 1,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            prob_parameter_mut: float = 0.96,
            prob_activation_add: float = 0.01,
            prob_activation_del: float = 0.01,
            prob_inhibition_add: float = 0.01,
            prob_inhibition_del: float = 0.01,
            nbConnActivationsDomain: Sequence[int] = [1, 7],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [1, 13],
            f1: float = 0.2,
            f2: float = 2.0,
            mut_pb: float = 0.8,
            allow_gd: bool = False,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes = nb_nodes
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.prob_parameter_mut = prob_parameter_mut
        self.prob_activation_add = prob_activation_add
        self.prob_activation_del = prob_activation_del
        self.prob_inhibition_add = prob_inhibition_add
        self.prob_inhibition_del = prob_inhibition_del
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.f1 = f1
        self.f2 = f2
        self.mut_pb = 0.8
        self.ref_ind = None
        self.ref_usage_budget = 0
        self.ref_sobol_vec = None

        base_ind_gen = gen_daccad_individuals_GD(self.ind_domain) if allow_gd else gen_daccad_individuals(self.ind_domain)
        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=base_ind_gen, **kwargs)


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        if initialise: # Initialisation
            # Create a base individual
            base_ind.resize(self.nb_nodes)
            base_ind.stabilities = np.array([1.-random_log_scale() for _ in range(base_ind.nb_nodes)])
            base_ind.activations[2,2] = random_log_scale()
            #base_ind.inhibitions[2,2,2] = math.exp(0. + random.random() * math.log(.0002)) / 200.
            #base_ind.activations[2,1] = random_log_scale()
            base_ind.assemble()

            #if self.ref_ind is None or self.ref_sobol_vec is None or len(self.ref_sobol_vec)==0:
            #    self.ref_ind = copy.deepcopy(base_ind)
            #    dim = len(base_ind.values)
            #    self.ref_usage_budget = dim * 5
            #    self._init_sobol_vec(self.budget, dim)
            #else:
            #    base_ind.values = self.ref_sobol_vec.pop(0)
            return base_ind, False

        else: # Selection
            try:
                res = tools.non_trivial_sel_random(collection), True
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()
            return res

    def _init_sobol_vec(self, nb, dim):
        vec = hdsobol.gen_sobol_vectors(nb, dim)
        noise_level = (self.ind_domain[1] - self.ind_domain[0]) / 20.
        vec += np.random.uniform(-noise_level, noise_level, vec.shape)
        vec[vec<self.ind_domain[0]] = self.ind_domain[0] + 0.001
        vec[vec>self.ind_domain[1]] = self.ind_domain[1]
        self.ref_sobol_vec = list(vec)

    def _vary(self, individual):
        #if self.ref_ind is not None and self.ref_usage_budget > 0:
        #    ind = copy.deepcopy(self.ref_ind)
        #    ind.name = str(id(ind))
        #    self.ref_usage_budget -= 1
        #    if self.ref_sobol_vec is not None and len(self.ref_sobol_vec):
        #        ind.values = self.ref_sobol_vec.pop(0)
        #    else:
        #        mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
        #        ind.assemble()
        #    return ind

        for _ in range(500): # XXX
            ind = copy.deepcopy(individual)
            ind.name = str(id(ind))

            active_activations_coords = list(zip(*np.where(ind.activations)))
            nb_active_activations = len(active_activations_coords)
            active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
            nb_active_inhibitions = len(active_inhibitions_coords)

            # Choose the kind of mutation
            choice = np.random.choice(5, p=(self.prob_parameter_mut,
                self.prob_activation_add, self.prob_activation_del,
                self.prob_inhibition_add, self.prob_inhibition_del))

            # Verify that the add and del are allowed
            if choice == 1 and (nb_active_activations >= self.nbConnActivationsDomain[1] or nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[1]):
                choice = 2 # Transform into del
            elif choice == 2 and (nb_active_activations <= self.nbConnActivationsDomain[0] or nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[0]):
                choice = 1 # Transform into add
            elif choice == 3 and (nb_active_inhibitions >= self.nbConnInhibitionsDomain[1] or nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[1]):
                choice = 4 # Transform into del
            elif choice == 4 and (nb_active_inhibitions <= self.nbConnInhibitionsDomain[0] or nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[0]):
                choice = 3 # Transform into add

            # Perform the mutation
            if choice == 0: # Parameters mutation
                if self.ref_ind is not None and self.ref_usage_budget > 0:
                    ind = copy.deepcopy(self.ref_ind)
                    ind.name = str(id(ind))
                    self.ref_usage_budget -= 1
                    if self.ref_sobol_vec is not None and len(self.ref_sobol_vec):
                        ind.values = self.ref_sobol_vec.pop(0)
                    else:
                        mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                else:
                    self.ref_ind = ind
                    self.ref_usage_budget = np.random.randint(10, 30)
                    self.ref_sobol_vec = None
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
            elif choice == 1: # Add Activation
                mutation_add_activation(ind, trivial=True)
            elif choice == 2: # Del Activation
                mutation_del_activation(ind)
            elif choice == 3: # Add Inhibition
                mutation_add_inhibition(ind, trivial=True)
            elif choice == 4: # Del Inhibition
                mutation_del_inhibition(ind)

            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                active_activations_coords = list(zip(*np.where(ind.activations)))
                nb_active_activations = len(active_activations_coords)
                active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
                nb_active_inhibitions = len(active_inhibitions_coords)
                if nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        if choice > 0:
            self.ref_ind = ind
            self.ref_usage_budget = np.random.randint(10, 50)
            self.ref_sobol_vec = None
            #dim = len(ind.values)
            #self.ref_usage_budget = dim * 5
            #self._init_sobol_vec(self.ref_usage_budget, dim)
        return ind




# TODO
@registry.register
class DaccadSearchMutWithTopologicalSobolBatchSuggestions(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [3, 7],
            min_init_budget: int = 1,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            prob_parameter_mut: float = 0.96,
            prob_template_add: float = 0.01,
            prob_template_del: float = 0.01,
            prob_signal_species_add: float = 0.01,
            prob_inhibition_species_add: float = 0.01,
            nbConnActivationsDomain: Sequence[int] = [1, 7],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [1, 13],
            f1: float = 0.2,
            f2: float = 2.0,
            mut_pb: float = 0.8,
            init_nb_muts_domain: Sequence[int] = [1, 5],
            allow_gd: bool = True,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.prob_parameter_mut = prob_parameter_mut
        self.prob_template_add = prob_template_add
        self.prob_template_del = prob_template_del
        self.prob_signal_species_add = prob_signal_species_add
        self.prob_inhibition_species_add = prob_inhibition_species_add
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.f1 = f1
        self.f2 = f2
        self.mut_pb = 0.8
        self.init_nb_muts_domain = init_nb_muts_domain
        self.init_ref_ind = None
        self.init_ref_usage_budget = 0
        self.init_ref_sobol_vec = None
        self.ref_ind = None
        self.ref_usage_budget = 0
        self.ref_sobol_vec = None

        base_ind_gen = gen_daccad_individuals_GD(self.ind_domain) if allow_gd else gen_daccad_individuals(self.ind_domain)
        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=base_ind_gen, **kwargs)


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        if initialise: # Initialisation
            if self.init_ref_ind is not None and self.init_ref_usage_budget>0:
                ind = copy.deepcopy(self.init_ref_ind)
                ind.name = str(id(ind))
                self.init_ref_usage_budget -= 1
                if self.init_ref_sobol_vec is not None and len(self.init_ref_sobol_vec):
                    ind.values = self.init_ref_sobol_vec.pop(0)
                else:
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                    ind.assemble()
                #print("DEBUG1", ind)
                return ind, False

            else:
                prob_parameter_mut = 0.
                prob_template_add = 1./3.
                prob_template_del = 0.
                prob_signal_species_add = 1./3.
                prob_inhibition_species_add = 1./3.

                for _ in range(500): # XXX
                    # Create a base individual
                    standard_init_ind(base_ind)

                    # Apply N topological mutations
                    nb_mutations = np.random.randint(self.init_nb_muts_domain[0], self.init_nb_muts_domain[1])
                    for _ in range(nb_mutations):
                        base_ind, _ = self._vary_with_pb(base_ind, prob_parameter_mut,
                                prob_template_add, prob_template_del,
                                prob_signal_species_add, prob_inhibition_species_add)

                    # Verify if valid
                    if base_ind.is_valid():
                        break
                    else:
                        print("INIT: NOT VALID !")
                        raise RuntimeError("INIT: NOT VALID !")

                self.init_ref_ind = base_ind
                dim = len(np.where(base_ind)[0])
                self.init_ref_usage_budget = dim * 2
                self.init_ref_sobol_vec = self._init_sobol_vec(self.init_ref_usage_budget, dim)
                #print("DEBUG2", base_ind)
                return base_ind, False

        else: # Selection
            try:
                res = tools.non_trivial_sel_random(collection), True
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()
            return res

    def _init_sobol_vec(self, nb, dim):
        vec = hdsobol.gen_sobol_vectors(nb, dim)
        noise_level = (self.ind_domain[1] - self.ind_domain[0]) / 20.
        vec += np.random.uniform(-noise_level, noise_level, vec.shape)
        vec[vec<self.ind_domain[0]] = self.ind_domain[0] + 0.001
        vec[vec>self.ind_domain[1]] = self.ind_domain[1]
        return list(vec)

    def _vary(self, ind):
        res, choice = self._vary_with_pb(ind, self.prob_parameter_mut,
                self.prob_template_add, self.prob_template_del,
                self.prob_signal_species_add, self.prob_inhibition_species_add)
        if choice > 0:
            self.ref_ind = ind
            dim = len(np.where(ind)[0])
            self.ref_usage_budget = dim*5 # np.random.randint(10, 30)
            self.ref_sobol_vec = self._init_sobol_vec(self.ref_usage_budget, dim)
        return res

    def _vary_with_pb(self, ind, prob_parameter_mut, prob_template_add, prob_template_del, prob_signal_species_add, prob_inhibition_species_add):
        for _ in range(500): # XXX
            ind = copy.deepcopy(ind)
            ind.name = str(id(ind))

            active_activations_coords = list(zip(*np.where(ind.activations)))
            nb_active_activations = len(active_activations_coords)
            active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
            nb_active_inhibitions = len(active_inhibitions_coords)

            # Choose the kind of mutation
            choice = np.random.choice(5, p=(prob_parameter_mut,
                prob_template_add, prob_template_del,
                prob_signal_species_add, prob_inhibition_species_add))

            # Perform the mutation
            if choice == 0: # Parameters mutation
                if self.ref_ind is not None and self.ref_usage_budget > 0:
                    ind = copy.deepcopy(self.ref_ind)
                    ind.name = str(id(ind))
                    self.ref_usage_budget -= 1
                    if self.ref_sobol_vec is not None and len(self.ref_sobol_vec):
                        ind.values = self.ref_sobol_vec.pop(0)
                    else:
                        mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                else:
                    self.ref_ind = ind
                    self.ref_usage_budget = np.random.randint(10, 30)
                    self.ref_sobol_vec = None
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
            elif choice == 1: # Add Activation
                mutation_add_activation2(ind)
            elif choice == 2: # Del Activation
                mutation_del_activation(ind)
            elif choice == 3: # Signal species
                mutation_bioneat_signal_species(ind)
            elif choice == 4: # Inhibition species
                mutation_bioneat_inhibition_species(ind)


            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                active_activations_coords = list(zip(*np.where(ind.activations)))
                nb_active_activations = len(active_activations_coords)
                active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
                nb_active_inhibitions = len(active_inhibitions_coords)
                if nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        return ind, choice




@registry.register
class DaccadBioneatMutWithTopologicalBatchSuggestions(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [3, 7],
            min_init_budget: int = 1,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            prob_parameter_mut: float = 0.96,
            prob_disable_template: float = 0.01,
            prob_activation_add: float = 0.01,
            prob_inhibition_add: float = 0.01,
            prob_node_add: float = 0.01,
            nbConnActivationsDomain: Sequence[int] = [1, 7],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [1, 13],
            f1: float = 0.2,
            f2: float = 2.0,
            mut_pb: float = 0.8,
            init_nb_muts_domain: Sequence[int] = [1, 5],
            allow_gd: bool = True,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.prob_parameter_mut = prob_parameter_mut
        self.prob_disable_template = prob_disable_template
        self.prob_activation_add = prob_activation_add
        self.prob_inhibition_add = prob_inhibition_add
        self.prob_node_add = prob_node_add
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.f1 = f1
        self.f2 = f2
        self.mut_pb = 0.8
        self.init_nb_muts_domain = init_nb_muts_domain
        self.init_ref_ind = None
        self.init_ref_usage_budget = 0
        self.ref_ind = None
        self.ref_usage_budget = 0

        base_ind_gen = gen_daccad_individuals_GD(self.ind_domain) if allow_gd else gen_daccad_individuals(self.ind_domain)
        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=base_ind_gen, **kwargs)


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        if initialise: # Initialisation
            if self.init_ref_ind is not None and self.init_ref_usage_budget>0:
                ind = copy.deepcopy(self.init_ref_ind)
                ind.name = str(id(ind))
                self.init_ref_usage_budget -= 1
                mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                ind.assemble()
                #print("DEBUG1", ind)
                return ind, False

            else:
                prob_parameter_mut     = 0.
                prob_disable_template  = 1./4.
                prob_activation_add    = 1./4.
                prob_inhibition_add    = 1./4.
                prob_node_add          = 1./4.

                for _ in range(500): # XXX
                    # Create a base individual
                    standard_init_ind(base_ind)

                    # Apply N topological mutations
                    nb_mutations = np.random.randint(self.init_nb_muts_domain[0], self.init_nb_muts_domain[1])
                    for _ in range(nb_mutations):
                        base_ind, _ = self._vary_with_pb(base_ind, prob_parameter_mut,
                                prob_disable_template, prob_activation_add,
                                prob_inhibition_add, prob_node_add)

                    # Verify if valid
                    if base_ind.is_valid():
                        break
                    else:
                        print("INIT: NOT VALID !")
                        raise RuntimeError("INIT: NOT VALID !")

                self.init_ref_ind = base_ind
                dim = len(np.where(base_ind)[0])
                self.init_ref_usage_budget = dim * 2
                return base_ind, False

        else: # Selection
            try:
                res = tools.non_trivial_sel_random(collection), True
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()
            return res


    def _vary(self, ind):
        res, choice = self._vary_with_pb(ind, self.prob_parameter_mut,
                self.prob_disable_template, self.prob_activation_add,
                self.prob_inhibition_add, self.prob_node_add)
        if choice > 0:
            self.ref_ind = ind
            dim = len(np.where(ind)[0])
            self.ref_usage_budget = dim*2 # np.random.randint(10, 30)
        return res

    def _vary_with_pb(self, ind, prob_parameter_mut, prob_disable_template, prob_activation_add, prob_inhibition_add, prob_node_add):
        for _ in range(500): # XXX
            ind = copy.deepcopy(ind)
            ind.name = str(id(ind))

            active_activations_coords = list(zip(*np.where(ind.activations)))
            nb_active_activations = len(active_activations_coords)
            active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
            nb_active_inhibitions = len(active_inhibitions_coords)

            # Choose the kind of mutation
            choice = np.random.choice(5, p=(prob_parameter_mut,
                prob_disable_template, prob_activation_add,
                prob_inhibition_add, prob_node_add))

            # Perform the mutation
            if choice == 0: # Parameters mutation
                self.ref_ind = ind
                self.ref_usage_budget = np.random.randint(1, 5)
                mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
            elif choice == 1: # Disable template
                disable_template(ind)
            elif choice == 2: # Add Activation
                add_activation_with_gradients(ind)
            elif choice == 3: # Add inhibition
                add_inhibition_with_gradients(ind)
            elif choice == 4: # Add node
                add_node_with_gradients(ind)


            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                active_activations_coords = list(zip(*np.where(ind.activations)))
                nb_active_activations = len(active_activations_coords)
                active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
                nb_active_inhibitions = len(active_inhibitions_coords)
                if nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        return ind, choice




# Create a new grid every gen (like bioneat). If empty grid, or every N gen, take total grid as parent
@registry.register
class DaccadBioneatMutWithoutElitism(QDAlgorithm):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [3, 7],
            min_init_budget: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            prob_parameter_mut: float = 0.96,
            prob_disable_template: float = 0.01,
            prob_activation_add: float = 0.01,
            prob_inhibition_add: float = 0.01,
            prob_node_add: float = 0.01,
            nbConnActivationsDomain: Sequence[int] = [1, 7],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [1, 13],
            f1: float = 0.2,
            f2: float = 2.0,
            mut_pb: float = 0.8,
            allow_gd: bool = True,
            gd_pb: float = 1.0,
            use_entire_container_as_parents_pb: float = 0.05,
            use_entire_container_as_parents_pb2: float = 0.40,
            non_trivial_sel_pb: float = 0.70,
            max_parents: int = 0,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.min_init_budget = min_init_budget
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.prob_parameter_mut = prob_parameter_mut
        self.prob_disable_template = prob_disable_template
        self.prob_activation_add = prob_activation_add
        self.prob_inhibition_add = prob_inhibition_add
        self.prob_node_add = prob_node_add
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.f1 = f1
        self.f2 = f2
        self.mut_pb = mut_pb
        self.use_entire_container_as_parents_pb = use_entire_container_as_parents_pb
        self.use_entire_container_as_parents_pb2 = use_entire_container_as_parents_pb2
        self.non_trivial_sel_pb = non_trivial_sel_pb
        self.max_parents = max_parents

        self._batch_inds = []
        self.internal_container = copy.deepcopy(container)
        self.internal_container.clear()

        base_ind_gen = gen_daccad_individuals_GD(self.ind_domain, gd_pb) if allow_gd else gen_daccad_individuals(self.ind_domain)
        #super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=base_ind_gen, **kwargs)
        super().__init__(container, budget, base_ind_gen=base_ind_gen, **kwargs)


#    def _new_gen(self, base_ind: IndividualLike):
#        choice = np.random.choice(2, p=(0.90, 0.10))
#        # Select individuals to put into new internal container
#        #if self.internal_container.best is None or self.container.best is None:
#        #    print("DEBUG: creating initial batch.")
#        #    ind = copy.deepcopy(base_ind)
#        #    ind.fitness.values = [0.]
#        #    ind.name = str(hash(ind) ** 2)
#        #    self.internal_container.update([ind])
#        #    new_internal_container = copy.deepcopy(self.internal_container)
#        if len(self.internal_container) == 0 or self.internal_container.best is None or self.internal_container.best_fitness[0] <= self.container.fitness_domain[0][0] or choice == 1:
#            print("DEBUG: creating a new batch, using all container elites as parents.")
#            # Internal container only contains trivial individuals, use entire container
#            #new_internal_container = copy.deepcopy(self.container)
#            # Replace internal container
#            self.internal_container = copy.deepcopy(self.container)
#        else:
#            print("DEBUG: creating a new batch, using elites from the previous batch as parents.")
#            # Container contains a number of non-trivial individuals, use previous non-trivial offsprings as parents
#            new_internal_container = copy.deepcopy(self.internal_container)
#            new_internal_container.clear()
#
#            non_trivials = [ind for ind in self.internal_container if ind.fitness[0] > self.container.fitness_domain[0][0]]
#            #print("DEBUGGGGGGG0 : ", len(new_internal_container), len(self.internal_container), len(self.container), len(non_trivials))
#            nb_updated = new_internal_container.update(non_trivials, issue_warning = True, ignore_exceptions = True)
#            if len(new_internal_container) == 0:
#                self.internal_container = copy.deepcopy(self.container)
#                #print("DEBUG2", nb_updated, len(new_internal_container), len(non_trivials), non_trivials)
#                #print("DEBUG2b", new_internal_container)
#            else:
#                self.internal_container = new_internal_container
#                #print("DEBUG3", nb_updated, len(new_internal_container), len(non_trivials), non_trivials)
#                #print("DEBUG3b", new_internal_container)
#        # Create offprings from new internal container
#        #print("DEBUGGGGGGG1 : ", len(self.internal_container), len(self.container))
#        for _ in range(self._batch_size):
#            selected = tools.sel_random(self.internal_container)
#            ind = self._vary(selected)
#            self._batch_inds.append(ind)


    # TODO : limit also the number of trivial parents ?
    #   -> GD on trivial parents ??
    #       --> can be used directly with current GD implementation ?
    def _new_gen(self, base_ind: IndividualLike):
        choice = np.random.choice(2, p=(1. - self.use_entire_container_as_parents_pb, self.use_entire_container_as_parents_pb))

        # Select individuals
        if len(self.internal_container) == 0 or self.internal_container.best is None or self.internal_container.best_fitness[0] <= self.container.fitness_domain[0][0] or choice == 1:
            # Use all container elites as parents of the new batch
            candidates = list(self.container)
            print(f"DEBUG: creating a new batch, using all container elites ({len(candidates)}) as parents (v1).")
            #print(f"DEBUGv1: ", len(self.internal_container))
        else:
            # Use previous (preferably non-trivial) offspring as parents of the new batch
            candidates = [ind for ind in self.internal_container if ind.fitness[0] > self.container.fitness_domain[0][0]]
            if len(candidates) == 0:
                choice2 = np.random.choice(2, p=(1. - self.use_entire_container_as_parents_pb2, self.use_entire_container_as_parents_pb2))
                if choice2 == 0:
                    candidates = list(self.internal_container)
                    print(f"DEBUG: creating a new batch, using {len(candidates)} trivial elites from the previous batch as parents.")
                else:
                    candidates = list(self.container)
                    print(f"DEBUG: creating a new batch, using all container elites ({len(candidates)}) as parents (v2).")
            else:
                candidates_fitnesses = [ind.fitness[0] for ind in candidates]
                print(f"DEBUG: creating a new batch, using {len(candidates)} non-trivial elites from the previous batch as parents (fitnesses: {candidates_fitnesses}).")

        if self.max_parents > 0:
            random.shuffle(candidates)
            candidates = candidates[:self.max_parents]

        # Select from candidates and create offspring
        for _ in range(self._batch_size):
            choice_sel = np.random.choice(2, p=(1. - self.non_trivial_sel_pb, self.non_trivial_sel_pb))
            if choice_sel == 0:
                selected = tools.sel_random(candidates)
            else:
                non_trivials = [ind for ind in candidates if ind.fitness[0] > self.container.fitness_domain[0][0]]
                if len(non_trivials) == 0:
                    selected = tools.sel_random(candidates)
                else:
                    selected = tools.sel_random(non_trivials)
            ind = self._vary(selected)
            self._batch_inds.append(ind)

        # Clear internal container
        self.internal_container.clear()



    def _internal_ask(self, base_ind: IndividualLike) -> IndividualLike:
        # Check if initialisation needed
        #initialise = self._nb_suggestions < self.min_init_budget
        initialise = len(self.container) < self.min_init_budget
        if initialise:
            #print("DEBUGGGGGGG2 : initialise")
            standard_init_ind(base_ind)
            #base_ind.name = str(hash(base_ind) ** 2) + str(id(base_ind))
            return base_ind
        # Check if must generate new batch suggestions
        if len(self._batch_inds) == 0:
            self._new_gen(base_ind)
        # Return suggestion
        res = self._batch_inds.pop()
        return res


    def _internal_tell(self, individual: IndividualLike, added_to_container: bool) -> None:
        #print("DEBUG internal tell", added_to_container, individual.fitness, individual.features)
        ind2 = copy.deepcopy(individual)
        ind2.name = str(id(ind2))
        self.internal_container.update([ind2])


    def _vary(self, individual):
        for _ in range(500): # XXX
            ind = copy.deepcopy(individual)
            ind.name = str(id(ind))

            # Choose the kind of mutation
            choice = np.random.choice(5, p=(self.prob_parameter_mut,
                self.prob_disable_template,
                self.prob_activation_add, self.prob_inhibition_add,
                self.prob_node_add))

            # Perform the mutation
            try:
                if choice == 0: # Parameters mutation
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                elif choice == 1: # Disable template
                    #print("DEBUG disable template")
                    disable_template(ind)
                elif choice == 2: # Add Activation
                    #print("DEBUG add activation")
                    add_activation_with_gradients(ind)
                elif choice == 3: # Add inhibition
                    #print("DEBUG add inhibition")
                    add_inhibition_with_gradients(ind)
                elif choice == 4: # Add node
                    #print("DEBUG add node")
                    add_node_with_gradients(ind)
            except Exception as e:
                print(f"VARY: EXCEPTION (choice={choice}): {e}")
                continue

            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                nb_active_activations = len(activeConnActivationsIndexes)
                activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                # Verify if there is a template from at least one of the gradients
                if nb_active_activations > 1:
                    possess_templates_from_gradients = False
                    gradients_id = [0, 1]
                    for g in gradients_id:
                        possess_templates_from_gradients |= np.any(ind.activations[g,:] >= 0.005)
                else:
                    possess_templates_from_gradients = True

                if possess_templates_from_gradients and nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        #ind.name = str(hash(ind) ** 2) + str(id(ind))
        return ind



# TODO Algo 1: Re-create a new grid every batch, and use only 1 trial per eval
#   -> probability to select and vary, or just to retest one individual
#   -> each individual possess a trial count, and if an individual is re-tried, its fitness (median fitness) and position in the grid can change
#   -> individuals can be replaced if another individual is found that scores higher in the grid
#   -> pb: grid with individuals with different confidence --> fitness comparisons using ucb ?

# TODO Algo 2: use only 1 trial per eval on internal grids individuals, but re-test them with more trials when they are added to the container


# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
