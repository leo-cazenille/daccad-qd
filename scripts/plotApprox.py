#!/usr/bin/env python3


########## IMPORTS ########### {{{1

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

import os
import pathlib
from timeit import default_timer as timer
import copy
import yaml
import numpy as np
import random
import warnings
import traceback

from illuminate import *

import scipy.constants
import pandas as pd
import seaborn as sns
sns.set(font_scale = 2.1)
sns.set_style("ticks")




########## MAIN ########### {{{1
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--inputFile', type=str, default='conf/final.p', help = "Path of final data file")
    parser.add_argument('-o', '--outputDir', type=str, default='./', help = "Path of resulting files")
    parser.add_argument('-T', '--target', type=int, default=0, help = "Number of the target")
    parser.add_argument('--maxXY', type=float, default=0.18, help = "Max values of X and Y")
    parser.add_argument('--depot', action='store_true')
    args = parser.parse_args()

    with open(args.inputFile, "rb") as f:
        data = pickle.load(f)

    targetId = int(args.target)
    container = data['container']
    inds = container.depot if args.depot else list(container.items)
    scores = [ind.scores for ind in inds]
    scoresApprox = [s['approx'] for s in scores]

    #print(scores[-1])

    nEvaluations = scores[0]['nEvaluations']
    median_vals = [s[f"medianScore{targetId}"] for s in scores]
    #approx_median_vals = [s[f"medianScore{targetId}"] for s in scoresApprox]
    approx_median_vals = [s[f"fitness0_{targetId}"] for s in scoresApprox]
    dist_medians_vals = [approx_median_vals[i] - median_vals[i] for i in range(len(scores)) ]

#    nEvaluations = scores[0]['nEvaluations']
#    median_vals = [s[f"medianScore{targetId}"] for s in scores if s[f"medianScore{targetId}"] > 0.05]
#    approx_median_vals = [scoresApprox[i][f"fitness0_{targetId}"] for i in range(len(scoresApprox)) if scores[i][f"medianScore{targetId}"] > 0.05]
#    dist_medians_vals = [approx_median_vals[i] - median_vals[i] for i in range(len(approx_median_vals)) ]

#    nEvaluations = scores[0]['nEvaluations']
#    median_vals = [scores[i][f"medianScore{targetId}"] for i in range(len(scores)) if scoresApprox[i][f"fitness0_{targetId}"] > 0.10]
#    approx_median_vals = [scoresApprox[i][f"fitness0_{targetId}"] for i in range(len(scoresApprox)) if scoresApprox[i][f"fitness0_{targetId}"] > 0.10]
#    dist_medians_vals = [approx_median_vals[i] - median_vals[i] for i in range(len(approx_median_vals)) ]

    for i in range(len(median_vals)):
        print(median_vals[i], approx_median_vals[i], dist_medians_vals[i])

    # Compute RMSE
    rmse = np.sqrt(np.mean(np.array(dist_medians_vals)**2.))
    print(f"RMSE={rmse}")

    xref = np.linspace(0., 0.6, 7)

    #fig = sns.jointplot(median_vals, approx_median_vals, kind="hex")
    #fig = sns.jointplot(median_vals, approx_median_vals)
    #fig = sns.jointplot(median_vals, approx_median_vals, kind="reg", xlim=[0., 0.45], ylim=[0., 0.62])
    #fig = sns.jointplot(median_vals, approx_median_vals, kind="reg", xlim=[0., 0.60], ylim=[0., 0.62])
    #fig = sns.jointplot(median_vals, approx_median_vals, kind="reg", xlim=[0., 0.30], ylim=[0., 0.50])
    fig = sns.jointplot(median_vals, approx_median_vals, kind="reg", xlim=[0., args.maxXY], ylim=[0., args.maxXY], joint_kws={'line_kws':{'color':'red'}})
    #fig = sns.jointplot(median_vals, approx_median_vals, kind="hex", xlim=[0., 0.30], ylim=[0., 0.50])
    #fig = sns.jointplot(median_vals, approx_median_vals, kind="reg")
    #fig = sns.jointplot(median_vals, dist_medians_vals, kind="reg")

    fig.set_axis_labels('Median Score', 'Srg Median Score')
    ax = fig.ax_joint
    plt.sca(ax)
    plt.plot(xref, xref, 'k-')
    fig.savefig(os.path.join(args.outputDir, f"joint-medianScore-approxMedianScore-target{targetId}.pdf"))


# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
