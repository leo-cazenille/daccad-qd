#!/usr/bin/env python3


########## IMPORTS ########### {{{1

import os
from timeit import default_timer as timer
import copy
import numpy as np
import random
import warnings
import traceback

from qdpy.algorithms import *
from qdpy.containers import *
from qdpy.plots import *
from qdpy.base import *
from qdpy import tools
from qdpy import hdsobol

from typing import Optional, Tuple, List, Iterable, Iterator, Any, TypeVar, Generic, Union, Sequence, MutableSet, MutableSequence, Type, Callable, Generator, Mapping, MutableMapping, overload

from base import *


@registry.register
class DaccadTopologicalInitBioneatWithIntMut(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [3, 7],
            min_init_budget: int = 500,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            prob_parameter_mut: float = 0.96,
            prob_disable_template: float = 0.01,
            prob_activation_add: float = 0.01,
            prob_inhibition_add: float = 0.01,
            prob_node_add: float = 0.01,
            nbConnActivationsDomain: Sequence[int] = [2, 8],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [2, 14],
            f1: float = 0.2,
            f2: float = 2.0,
            mut_pb: float = 0.8,
            #init_nb_muts_domain: Sequence[int] = [0, 5],
            init_nb_param_muts_domain: Sequence[int] = [1, 3],
            epsilon_domain: Sequence[float] = [0.05, 0.70],
            allow_gd: bool = False,
            gd_pb: float = 1.0,
            grad4: bool = False,
            intmut_nb_sugg = 10,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.prob_parameter_mut = prob_parameter_mut
        self.prob_disable_template = prob_disable_template
        self.prob_activation_add = prob_activation_add
        self.prob_inhibition_add = prob_inhibition_add
        self.prob_node_add = prob_node_add
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.f1 = f1
        self.f2 = f2
        self.mut_pb = mut_pb
        self.init_nb_param_muts_domain = init_nb_param_muts_domain
        self.epsilon_domain = epsilon_domain
        self.grad4 = grad4
        self.intmut_nb_sugg = intmut_nb_sugg

        self.epsilon = np.random.uniform(self.epsilon_domain[0], self.epsilon_domain[1]) if self.epsilon_domain[0] != self.epsilon_domain[1] else self.epsilon_domain[0]
        print(f"Using epsilon={self.epsilon}")
        self.suggestions_gen_fn = partial(self._suggestions_gen_fn)

        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=gen_daccad_individuals(self.ind_domain), deepcopy_on_selection=False, **kwargs)


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        choice = np.random.choice(2, p=(0.99, 0.01))
        if choice == 1:
            self.epsilon = np.random.uniform(self.epsilon_domain[0], self.epsilon_domain[1]) if self.epsilon_domain[0] != self.epsilon_domain[1] else self.epsilon_domain[0]
            print(f"Using epsilon={self.epsilon}")

        if initialise: # Initialisation
            gradients_id = [0,1,2,3] if self.grad4 else [0,1]
            for _ in range(5000): # XXX
                ind = copy.deepcopy(base_ind)
                #ind.name = str(id(ind))

                # Create a base individual
                if self.grad4:
                    standard_init_ind_grad4(ind)
                else:
                    standard_init_ind(ind)

                # Select a target number of templates
                target_nb_templates = np.random.randint(self.nbConnDomain[0], self.nbConnDomain[1])

                # Apply topological mutations until individual reach chosen number of templates
                for _ in range(500): # XXX
                    nb_active_activations = len(np.where(ind.activations >= 0.005)[0])
                    nb_active_inhibitions = len(np.where(ind.inhibitions >= 0.005)[0])
                    if nb_active_activations + nb_active_inhibitions == target_nb_templates:
                        break
                    elif nb_active_activations + nb_active_inhibitions > target_nb_templates:
                        ind = self._vary_with_pb(ind,
                            0.,
                            1.0,
                            0.,
                            0.,
                            0.,
                            True)
                    else:
                        ind = self._vary_with_pb(ind,
                            0.,
                            1./4.,
                            1./4.,
                            1./4.,
                            1./4.,
                            True)

                # Apply a random number of parameter mutations
                nb_mut_param = np.random.randint(self.init_nb_param_muts_domain[0], self.init_nb_param_muts_domain[1])
                for _ in range(nb_mut_param):
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                ind.assemble()


                # Verify if valid
                if ind.is_valid():
                    # Verify if within bounds
                    activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                    nb_active_activations = len(activeConnActivationsIndexes)
                    activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                    nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                    # Verify if there are templates from gradients
                    possess_templates_from_gradients = False
                    for g in gradients_id:
                        possess_templates_from_gradients |= np.any(ind.activations[g,:] >= 0.005)

                    if possess_templates_from_gradients and ind.nb_nodes >= self.nb_nodes_domain[0] and ind.nb_nodes <= self.nb_nodes_domain[1] and nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                        break
                else:
                    print("INIT: NOT VALID !")
                    raise RuntimeError("INIT: NOT VALID !")
            ind.name = str(hash(ind) ** 2)
            return ind, False

        else: # Selection
            try:
                choice = np.random.random() > self.epsilon
                if choice:
                    res = collection.best, True
                else:
                    choice2 = np.random.choice(2, p=(0.70, 0.30))
                    if choice2 == 0:
                        res = tools.non_trivial_sel_random(collection), True
                    else:
                        res = tools.sel_random(collection), True
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()
            return res



    def _vary_with_pb(self, individual, prob_parameter_mut, prob_disable_template, prob_activation_add, prob_inhibition_add, prob_node_add, weak_validation = False):
        gradients_id = [0,1,2,3] if self.grad4 else [0,1]

        for _ in range(5000): # XXX
            ind = copy.deepcopy(individual)
            #if hasattr(ind, "suggestions"):
            #    print("HAS SUGGESTIONS")
            #    del ind.suggestions

            #ind.name = str(id(ind))

            # Choose the kind of mutation
            choice = np.random.choice(5, p=(prob_parameter_mut, prob_disable_template, prob_activation_add, prob_inhibition_add, prob_node_add))

            # Perform the mutation
            try:
                if choice == 0: # Parameters mutation
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                elif choice == 1: # Disable template
                    disable_template(ind)
                elif choice == 2: # Add Activation
                    add_activation_with_gradients(ind, gradients=gradients_id)
                elif choice == 3: # Add inhibition
                    add_inhibition_with_gradients(ind, gradients=gradients_id)
                elif choice == 4: # Add node
                    add_node_with_gradients(ind, gradients=gradients_id)
            except Exception as e:
                print(f"VARY: EXCEPTION (choice={choice}): {e}")
                continue

            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                nb_active_activations = len(activeConnActivationsIndexes)
                activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                # Verify if there are templates from gradients
                possess_templates_from_gradients = False
                for g in gradients_id:
                    possess_templates_from_gradients |= np.any(ind.activations[g,:] >= 0.005)

                if possess_templates_from_gradients and weak_validation:
                    break
                elif possess_templates_from_gradients and ind.nb_nodes >= self.nb_nodes_domain[0] and ind.nb_nodes <= self.nb_nodes_domain[1] and nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        ind.name = str(hash(ind) ** 2) + str(id(ind))
        return ind


    def _vary(self, individual: Any) -> Any:
        sugg = individual.suggestions if hasattr(individual, "suggestions") else []
        if len(sugg) == 0:
            sugg = self._vary_with_pb(individual, 
                self.prob_parameter_mut,
                self.prob_disable_template,
                self.prob_activation_add,
                self.prob_inhibition_add,
                self.prob_node_add)
            return sugg

        best_idx = 0
        best_sugg = sugg[0]
        #print(f"---- {best_sugg}")
        for i in range(1, len(sugg)):
            s = sugg[i]
            if s.dominates(best_sugg):
                best_sugg = s
                best_idx = i
            #print(f"---- {s}")
        del individual.suggestions[best_idx]
        #print(f"vary: {len(sugg)} {best_sugg} {best_sugg.fitness} {hasattr(best_sugg, 'suggestions')}")
        #del individual.suggestions
        return best_sugg


    def _suggestions_gen_fn(self, ind):
        res = []
        for _ in range(self.intmut_nb_sugg):
            sugg = self._vary_with_pb(ind, 
                self.prob_parameter_mut,
                self.prob_disable_template,
                self.prob_activation_add,
                self.prob_inhibition_add,
                self.prob_node_add)
            res.append(sugg)
            #print(f"---- {sugg}")
        return res



# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
