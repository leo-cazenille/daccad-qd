#!/usr/bin/env python3


########## IMPORTS ########### {{{1

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

import os
import pathlib
from timeit import default_timer as timer
import copy
import yaml
import numpy as np
import random
import warnings
import traceback

from illuminate import *

sys.modules["SCOOP_WORKER"] = sys.modules["__main__"]




########## MAIN ########### {{{1
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--dir', type=str, default='results/testDACCAD', help="Directory of results data files")
    parser.add_argument('--nbRuns', type=int, default=12, help="Number of runs to take into account or 0 for all")
    parser.add_argument('--nbIterations', type=int, default=50, help="Number of iterations to take into account")
    parser.add_argument('-o', '--outputFile', type=str, default="", help="Path of the output file")
    args = parser.parse_args()

    nb_runs = args.nbRuns
    nb_iterations = args.nbIterations

    data_lst = []
    for f_name in os.listdir(args.dir):
        if f_name.startswith("final_") and f_name.endswith(".p"):
            print(f_name)
            with open(os.path.join(args.dir, f_name), "rb") as f:
                d = pickle.load(f)
            data_lst.append(d)

    if nb_runs > 0:
        data_lst = data_lst[:args.nbRuns]

    maxs = np.zeros((nb_iterations, nb_runs))
    for i, d in enumerate(data_lst):
        for j in range(min(nb_iterations, len(d['iterations']['max']))):
            m = d['iterations']['max'][j]
            if isinstance(m, Iterable):
                maxs[j, i] = m[0]
            elif np.isnan(m):
                maxs[j, i] = 0.

    sorted_maxs = np.zeros((nb_runs, nb_iterations))
    for j in range(sorted_maxs.shape[1]):
        sorted_maxs[:, j] = sorted(maxs[j, :])

    print(sorted_maxs)
    #if args.outputFile is not None and len(args.outputFile) > 0:
    #    np.savetxt(args.outputFile, maxs)


    import PyGnuplot as gp

    gp.s(sorted_maxs, "result.dat")

    gp.c("reset")

    # wxt
    #set terminal wxt size 350,262 enhanced font 'Verdana,10' persist
    # png
    gp.c("set terminal pngcairo size 350,350 enhanced font 'Verdana,14'")
    gp.c("set output '" + args.outputFile + "'")
    # svg
    #set terminal svg size 350,262 fname 'Verdana, Helvetica, Arial, sans-serif' \
    #fsize '10'
    #set output 'simple_statistics.svg'

    # color definitions
    gp.c("set border linewidth 1.5")
    gp.c("set style line 1 lc rgb 'grey70' ps 0 lt 1 lw 2")
    gp.c("set style line 2 lc rgb 'grey10' lt 1 lw 2")

    gp.c("set style fill solid 1.0 border rgb 'grey70'")

    gp.c("unset key")
    gp.c("set border 3")
    gp.c("set xtics nomirror out scale 0.0 1000")
    gp.c("set ytics nomirror out scale 0.75 0.5")
    gp.c("set yrange [0:1]")

    gp.c("set xlabel 'Evaluations'")
    gp.c("set ylabel 'Fitness'")

    gp.c("set title 'bottom-line'")

    # Plot mean with variance (std^2) as boxes with yerrorbar and use the first row
    # in the data file as xtic labels
    gp.c("plot 'result.dat' using (($0+1)*200):4:1:12:9 every 4::3 with candlestick ls 1, '' u (($0+1)*200):(($6+$7)/2.0) every 4::3 with points ls 2")



# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
