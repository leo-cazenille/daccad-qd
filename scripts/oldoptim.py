#!/usr/bin/env python3

# DEBUGGING
import faulthandler
faulthandler.enable()


########## IMPORTS ########### {{{1

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

import os
import pathlib
import datetime
from timeit import default_timer as timer
import copy
import itertools
from functools import partial
import yaml
import numpy as np
import random
import warnings
import traceback
import math
import socket

import submitDACCAD

from qdpy.algorithms import *
from qdpy.containers import *
#from qdpy.benchmarks import *
from qdpy.plots import *
from qdpy.base import *
from qdpy import tools

from typing import Optional, Tuple, List, Iterable, Iterator, Any, TypeVar, Generic, Union, Sequence, MutableSet, MutableSequence, Type, Callable, Generator, Mapping, MutableMapping, overload
import qdpy.hdsobol as hdsobol
from base import *
from species import *
from batch import *





@registry.register
class DaccadUniformStabilitiesSobolWithVaryingSparsityConnections(RandomUniform):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            dimension: int, ind_domain: DomainLike,
            nb_nodes: int = 5,
            initialisation_sparsities: Sequence[float] = [0.90],
            nb_stability_entries_per_connections_matrix_entry: int = 1,
            nb_tested_connections_values: int = 5,
            nb_sobol_connection_values: int = 400,
            **kwargs):
        super().__init__(container, budget, dimension=dimension, ind_domain=ind_domain, **kwargs)
        self.nb_nodes = nb_nodes
        self.initialisation_sparsities = initialisation_sparsities
        self.nb_stability_entries_per_connections_matrix_entry = nb_stability_entries_per_connections_matrix_entry
        self.nb_tested_connections_values = nb_tested_connections_values
        self.nb_sobol_connection_values = nb_sobol_connection_values

        #self.sobol_vect = np.array(hdsobol.gen_sobol_vectors(self.budget + 1, self.dimension), dtype=float)
        #self.sobol_vect = self.sobol_vect * (ind_domain[1] - ind_domain[0]) + ind_domain[0]

        # Prepare the suggestions
        dimension_connections = self.dimension - self.nb_nodes
        connections = []
        for sparsity in self.initialisation_sparsities:
            connections += list(generateSobolConnectionsWithUniformValues(dimension_connections, self.ind_domain,
                self.nb_sobol_connection_values, sparsity, self.nb_tested_connections_values))
        connections = list(np.unique(np.array(connections), axis=0))
        if len(connections) > budget // self.nb_stability_entries_per_connections_matrix_entry:
            connections = random.sample(connections[1:], budget // self.nb_stability_entries_per_connections_matrix_entry)
        self.suggestions_vect = []
        for cM in connections:
            stabilities = generateUniform(self.nb_nodes, self.ind_domain, self.nb_stability_entries_per_connections_matrix_entry)
            self.suggestions_vect += [np.concatenate((stabilities[i], cM)) for i in range(self.nb_stability_entries_per_connections_matrix_entry)]

        remaining_suggestions_nb = self.budget - len(self.suggestions_vect)
        for i in range(remaining_suggestions_nb):
            cM = random.choice(connections)
            stabilities = generateUniform(self.nb_nodes, self.ind_domain, 1)
            self.suggestions_vect += [np.concatenate((stabilities[0], cM))]

    def _internal_ask(self, base_ind: IndividualLike) -> IndividualLike:
        base_ind[:] = self.suggestions_vect[self._nb_suggestions]
        #base_ind[:] = self.sobol_vect[self._nb_suggestions]
        return base_ind


@registry.register
class DaccadPolynomialMutationWithPolynomialSparsityBudget2(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes: int = 5,
            probParametersMutation: float = 0.70,
            probTemplateActivationAdd: float = 0.10,
            probTemplateActivationDel: float = 0.05,
            probTemplateInhibitionAdd: float = 0.10,
            probTemplateInhibitionDel: float = 0.05,
            nbConnActivationsDomain: Sequence[int] = [1, 19],
            nbConnInhibitionsDomain: Sequence[int] = [1, 19],
            nbConnDomain: Sequence[int] = [2, 20],
            eta: float = 20.,
            mutationPb: float = 0.4,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes = nb_nodes
        self.probParametersMutation = probParametersMutation
        self.probTemplateActivationAdd = probTemplateActivationAdd
        self.probTemplateActivationDel = probTemplateActivationDel
        self.probTemplateInhibitionAdd = probTemplateInhibitionAdd
        self.probTemplateInhibitionDel = probTemplateInhibitionDel
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.eta = eta
        self.mutationPb = mutationPb
        select = partial(tools.sel_random)
        vary = partial(self._vary)
        super().__init__(container, budget, select=select, vary=vary, **kwargs)


    def _vary(self, individual: Any) -> Any:
        ind = np.array(individual)
        connActivations = ind[self.nb_nodes:self.nb_nodes+self.nb_nodes*self.nb_nodes]
        connInhibitions = ind[self.nb_nodes+self.nb_nodes*self.nb_nodes:self.nb_nodes+self.nb_nodes*self.nb_nodes+self.nb_nodes*self.nb_nodes*self.nb_nodes]

        activeConnActivationsIndexes = np.where(connActivations >= 0.000001)
        activeConnActivations = connActivations[activeConnActivationsIndexes]
        activeConnInhibitionsIndexes = np.where(connInhibitions >= 0.000001)
        activeConnInhibitions = connInhibitions[activeConnInhibitionsIndexes]
        inactiveConnActivationsIndexes = np.where(connActivations < 0.000001)
        inactiveConnActivations = connActivations[inactiveConnActivationsIndexes]
        inactiveConnInhibitionsIndexes = np.where(connInhibitions < 0.000001)
        inactiveConnInhibitions = connInhibitions[inactiveConnInhibitionsIndexes]
        currentNbActiveConnActivations = len(activeConnActivations)
        currentNbActiveConnInhibitions = len(activeConnInhibitions)

        # Choose the kind of mutation
        choice = np.random.choice(5, p=(self.probParametersMutation, self.probTemplateActivationAdd, self.probTemplateActivationDel, self.probTemplateInhibitionAdd, self.probTemplateInhibitionDel))
        # Verify that the add and del are allowed
        if choice == 1 and (currentNbActiveConnActivations >= self.nbConnActivationsDomain[1] or currentNbActiveConnActivations + currentNbActiveConnInhibitions >= self.nbConnDomain[1]):
            choice = 2 # Transform into del
        elif choice == 2 and (currentNbActiveConnActivations <= self.nbConnActivationsDomain[0] or currentNbActiveConnActivations + currentNbActiveConnInhibitions <= self.nbConnDomain[0]):
            choice = 1 # Transform into add
        elif choice == 3 and (currentNbActiveConnInhibitions >= self.nbConnInhibitionsDomain[1] or currentNbActiveConnActivations + currentNbActiveConnInhibitions >= self.nbConnDomain[1]):
            choice = 4 # Transform into del
        elif choice == 4 and (currentNbActiveConnInhibitions <= self.nbConnInhibitionsDomain[0] or currentNbActiveConnActivations + currentNbActiveConnInhibitions <= self.nbConnDomain[0]):
            choice = 3 # Transform into add

        # Perform the mutation
        if choice == 0: # Parameters mutation
            #print("DEBUG: Mutation")
            tools.mut_polynomial_bounded(ind[:self.nb_nodes], eta=self.eta, low=self.ind_domain[0], up=self.ind_domain[1], mut_pb=self.mutationPb) #stabilities = list(mutPolynomialBounded(ind[:self.nb_nodes], self.eta, 0., 1., self.mutationPb))
            tools.mut_polynomial_bounded(activeConnActivations, eta=self.eta, low=self.ind_domain[0], up=self.ind_domain[1], mut_pb=self.mutationPb)
            tools.mut_polynomial_bounded(activeConnInhibitions, eta=self.eta, low=self.ind_domain[0], up=self.ind_domain[1], mut_pb=self.mutationPb)
        elif choice == 1: # Add Activation
            #print("DEBUG: Add Activation")
            inactiveConnActivations[np.random.choice(len(inactiveConnActivations))] = 0.0000015
        elif choice == 2: # Del Activation
            #print("DEBUG: Del Activation")
            activeConnActivations[np.random.choice(len(activeConnActivations))] = 0.
        elif choice == 3: # Add Inhibition
            #print("DEBUG: Add Inhibition")
            inactiveConnInhibitions[np.random.choice(len(inactiveConnInhibitions))] = 0.0000015
        elif choice == 4: # Del Inhibition
            #print("DEBUG: Del Inhibition")
            activeConnInhibitions[np.random.choice(len(activeConnInhibitions))] = 0.
        connActivations[activeConnActivationsIndexes] = activeConnActivations
        connActivations[inactiveConnActivationsIndexes] = inactiveConnActivations
        connInhibitions[activeConnInhibitionsIndexes] = activeConnInhibitions
        connInhibitions[inactiveConnInhibitionsIndexes] = inactiveConnInhibitions
        return ind
        #individual[:] = ind
        #return individual





@registry.register
class DaccadSparseInitAndMut(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes: int = 5,
            min_init_budget: int = 500,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            probParametersMutation: float = 0.70,
            probTemplateActivationAdd: float = 0.10,
            probTemplateActivationDel: float = 0.05,
            probTemplateInhibitionAdd: float = 0.10,
            probTemplateInhibitionDel: float = 0.05,
            nbConnActivationsDomain: Sequence[int] = [1, 19],
            nbConnInhibitionsDomain: Sequence[int] = [1, 19],
            nbConnDomain: Sequence[int] = [2, 20],
            eta: float = 20.,
            mutationPb: float = 0.4,
            #nb_stability_entries_per_connections_matrix_entry: int = 1,
            #nb_tested_connections_values: int = 5,
            #nb_sobol_connection_values: int = 400,
            only_accept_valid: bool = False,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes = nb_nodes
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.probParametersMutation = probParametersMutation
        self.probTemplateActivationAdd = probTemplateActivationAdd
        self.probTemplateActivationDel = probTemplateActivationDel
        self.probTemplateInhibitionAdd = probTemplateInhibitionAdd
        self.probTemplateInhibitionDel = probTemplateInhibitionDel
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.eta = eta
        self.mutationPb = mutationPb
        #self.nb_stability_entries_per_connections_matrix_entry = nb_stability_entries_per_connections_matrix_entry
        #self.nb_tested_connections_values = nb_tested_connections_values
        #self.nb_sobol_connection_values = nb_sobol_connection_values
        self.only_accept_valid = only_accept_valid

        select_or_initialise = partial(self._select_or_initialise)
        vary = partial(self._vary)
        super().__init__(container, budget, select_or_initialise=select_or_initialise, vary=vary, **kwargs)
        #self._init_sobol()


    #def _init_sobol(self) -> None:
    #    # Prepare the suggestions
    #    dimension_connections = self.dimension - self.nb_nodes
    #    self.connections = []
    #    # XXX
    #    for sparsity in self.initialisation_sparsities:
    #        self.connections += list(generateSobolConnectionsWithUniformValues(dimension_connections, self.ind_domain,
    #            self.nb_sobol_connection_values, sparsity, self.nb_tested_connections_values))
    #    self.connections = list(np.unique(np.array(self.connections), axis=0))
    #    #if len(self.connections) > self.budget // self.nb_stability_entries_per_connections_matrix_entry:
    #    #    self.connections = random.sample(self.connections[1:], self.budget // self.nb_stability_entries_per_connections_matrix_entry)


    def _select_or_initialise(self, collection: Sequence[Any]) -> Tuple[IndividualLike, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        if initialise: # Initialisation
            for _ in range(5000): # XXX
                #cM = random.choice(self.connections)
                cM = generateSparseUniformDomain(self.dimension - self.nb_nodes, self.ind_domain, 1, self.nbConnDomain)
                stabilities = generateUniform(self.nb_nodes, self.ind_domain, 1)
                res_ind = np.concatenate((stabilities[0], cM[0]))
                if not self.only_accept_valid or submitDACCAD.isValid(res_ind):
                    #print("INIT: VALID !") # XXX
                    break
                else: # XXX
                    pass
                    #print("INIT: NOT VALID !") # XXX
            return res_ind, False

        else: # Selection
            return tools.sel_random(collection), True


    def _vary(self, individual: Any) -> Any:
        for _ in range(5000): # XXX
            ind = copy.deepcopy(np.array(individual))
            connActivations = ind[self.nb_nodes:self.nb_nodes+self.nb_nodes*self.nb_nodes]
            connInhibitions = ind[self.nb_nodes+self.nb_nodes*self.nb_nodes:self.nb_nodes+self.nb_nodes*self.nb_nodes+self.nb_nodes*self.nb_nodes*self.nb_nodes]

            activeConnActivationsIndexes = np.where(connActivations >= 0.000001)
            activeConnActivations = connActivations[activeConnActivationsIndexes]
            activeConnInhibitionsIndexes = np.where(connInhibitions >= 0.000001)
            activeConnInhibitions = connInhibitions[activeConnInhibitionsIndexes]
            inactiveConnActivationsIndexes = np.where(connActivations < 0.000001)
            inactiveConnActivations = connActivations[inactiveConnActivationsIndexes]
            inactiveConnInhibitionsIndexes = np.where(connInhibitions < 0.000001)
            inactiveConnInhibitions = connInhibitions[inactiveConnInhibitionsIndexes]
            currentNbActiveConnActivations = len(activeConnActivations)
            currentNbActiveConnInhibitions = len(activeConnInhibitions)

            # Choose the kind of mutation
            choice = np.random.choice(5, p=(self.probParametersMutation, self.probTemplateActivationAdd, self.probTemplateActivationDel, self.probTemplateInhibitionAdd, self.probTemplateInhibitionDel))
            # Verify that the add and del are allowed
            if choice == 1 and (currentNbActiveConnActivations >= self.nbConnActivationsDomain[1] or currentNbActiveConnActivations + currentNbActiveConnInhibitions >= self.nbConnDomain[1]):
                choice = 2 # Transform into del
            elif choice == 2 and (currentNbActiveConnActivations <= self.nbConnActivationsDomain[0] or currentNbActiveConnActivations + currentNbActiveConnInhibitions <= self.nbConnDomain[0]):
                choice = 1 # Transform into add
            elif choice == 3 and (currentNbActiveConnInhibitions >= self.nbConnInhibitionsDomain[1] or currentNbActiveConnActivations + currentNbActiveConnInhibitions >= self.nbConnDomain[1]):
                choice = 4 # Transform into del
            elif choice == 4 and (currentNbActiveConnInhibitions <= self.nbConnInhibitionsDomain[0] or currentNbActiveConnActivations + currentNbActiveConnInhibitions <= self.nbConnDomain[0]):
                choice = 3 # Transform into add

            # Perform the mutation
            if choice == 0: # Parameters mutation
                #print("DEBUG: Mutation")
                tools.mut_polynomial_bounded(ind[:self.nb_nodes], eta=self.eta, low=self.ind_domain[0], up=self.ind_domain[1], mut_pb=self.mutationPb) #stabilities = list(mutPolynomialBounded(ind[:self.nb_nodes], self.eta, 0., 1., self.mutationPb))
                tools.mut_polynomial_bounded(activeConnActivations, eta=self.eta, low=self.ind_domain[0], up=self.ind_domain[1], mut_pb=self.mutationPb)
                tools.mut_polynomial_bounded(activeConnInhibitions, eta=self.eta, low=self.ind_domain[0], up=self.ind_domain[1], mut_pb=self.mutationPb)
            elif choice == 1: # Add Activation
                #print("DEBUG: Add Activation")
                #inactiveConnActivations[np.random.choice(len(inactiveConnActivations))] = 0.0000015
                inactiveConnActivations[np.random.choice(len(inactiveConnActivations))] = np.random.uniform(self.ind_domain[0], self.ind_domain[1])
            elif choice == 2: # Del Activation
                #print("DEBUG: Del Activation")
                activeConnActivations[np.random.choice(len(activeConnActivations))] = 0.
            elif choice == 3: # Add Inhibition
                #print("DEBUG: Add Inhibition")
                #inactiveConnInhibitions[np.random.choice(len(inactiveConnInhibitions))] = 0.0000015
                inactiveConnInhibitions[np.random.choice(len(inactiveConnInhibitions))] = np.random.uniform(self.ind_domain[0], self.ind_domain[1])
            elif choice == 4: # Del Inhibition
                #print("DEBUG: Del Inhibition")
                activeConnInhibitions[np.random.choice(len(activeConnInhibitions))] = 0.
            connActivations[activeConnActivationsIndexes] = activeConnActivations
            connActivations[inactiveConnActivationsIndexes] = inactiveConnActivations
            connInhibitions[activeConnInhibitionsIndexes] = activeConnInhibitions
            connInhibitions[inactiveConnInhibitionsIndexes] = inactiveConnInhibitions

            if not self.only_accept_valid or submitDACCAD.isValid(ind, self.nb_nodes):
                #print("VARY: VALID !") # XXX
                break
            else: # XXX
                pass
                #print("VARY: NOT VALID !") # XXX
        return ind





@registry.register
class DaccadSparseInitAndMut2(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes: int = 5,
            min_init_budget: int = 500,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            probParametersMutation: float = 0.70,
            probTemplateActivationAdd: float = 0.10,
            probTemplateActivationDel: float = 0.05,
            probTemplateInhibitionAdd: float = 0.10,
            probTemplateInhibitionDel: float = 0.05,
            nbConnActivationsDomain: Sequence[int] = [1, 19],
            nbConnInhibitionsDomain: Sequence[int] = [1, 19],
            nbConnDomain: Sequence[int] = [2, 20],
            eta: float = 20.,
            mutationPb: float = 0.4,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes = nb_nodes
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.probParametersMutation = probParametersMutation
        self.probTemplateActivationAdd = probTemplateActivationAdd
        self.probTemplateActivationDel = probTemplateActivationDel
        self.probTemplateInhibitionAdd = probTemplateInhibitionAdd
        self.probTemplateInhibitionDel = probTemplateInhibitionDel
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.eta = eta
        self.mutationPb = mutationPb

        self.nb_activations = self.nb_nodes * self.nb_nodes
        self.nb_inhibitions = self.nb_nodes * self.nb_nodes * self.nb_nodes
        self.start_activations = self.nb_nodes
        self.start_inhibitions = self.start_activations + self.nb_activations

        select_or_initialise = partial(self._select_or_initialise)
        vary = partial(self._vary)
        super().__init__(container, budget, select_or_initialise=select_or_initialise, vary=vary, **kwargs)



    def _select_or_initialise(self, collection: Sequence[Any]) -> Tuple[IndividualLike, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        if initialise: # Initialisation
            for _ in range(500):
                # Generate activations
                activations = generateSparseUniformDomain(self.nb_activations, self.ind_domain, 1, self.nbConnActivationsDomain)[0]
                activations_idx = list(np.where(activations)[0])
                activations_coords = [(x//self.nb_nodes, x%self.nb_nodes) for x in activations_idx]
                # Find allowed inhibitions
                allowed_inhibitions_idx = []
                for coord in activations_coords:
                    for i in range(self.nb_nodes):
                        allowed_inhibitions_idx.append(i*self.nb_nodes*self.nb_nodes + coord[0] * self.nb_nodes + coord[1])
                np.random.shuffle(allowed_inhibitions_idx)
                nb_gen_inhibitions = np.random.randint(self.nbConnInhibitionsDomain[0], self.nbConnInhibitionsDomain[1])
                allowed_inhibitions_idx = allowed_inhibitions_idx[:nb_gen_inhibitions]
                # Generate inhibitions
                inhibitions = np.zeros(self.nb_inhibitions)
                for idx in allowed_inhibitions_idx:
                    inhibitions[idx] = np.random.uniform(self.ind_domain[0], self.ind_domain[1])
                # Verify if the total number of templates is within domain
                if len(activations_idx) + len(allowed_inhibitions_idx) < self.nbConnDomain[0] or \
                        len(activations_idx) + len(allowed_inhibitions_idx) > self.nbConnDomain[1]:
                    continue
                # Generate stabilities
                stabilities = generateUniform(self.nb_nodes, self.ind_domain, 1)[0]
                # Assemble individual
                res_ind = np.concatenate((stabilities, activations, inhibitions))
                # Verify if valid
                if submitDACCAD.isValid(res_ind, self.nb_nodes):
                    #print("INIT: VALID !")
                    break
                else:
                    pass
                    print("INIT: NOT VALID !") # XXX
            return res_ind, False

        else: # Selection
            try:
                res = tools.sel_random(collection), True
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()
            return res


    def _vary(self, individual: Any) -> Any:
        for _ in range(500): # XXX
            ind = copy.deepcopy(np.array(individual))
            connActivations = ind[self.nb_nodes:self.nb_nodes+self.nb_nodes*self.nb_nodes]
            connInhibitions = ind[self.nb_nodes+self.nb_nodes*self.nb_nodes:self.nb_nodes+self.nb_nodes*self.nb_nodes+self.nb_nodes*self.nb_nodes*self.nb_nodes]

            activeConnActivationsIndexes = np.where(connActivations >= 0.000001)[0]
            activeConnActivations = connActivations[activeConnActivationsIndexes]
            activeConnInhibitionsIndexes = np.where(connInhibitions >= 0.000001)[0]
            activeConnInhibitions = connInhibitions[activeConnInhibitionsIndexes]
            inactiveConnActivationsIndexes = np.where(connActivations < 0.000001)[0]
            inactiveConnActivations = connActivations[inactiveConnActivationsIndexes]
            inactiveConnInhibitionsIndexes = np.where(connInhibitions < 0.000001)[0]
            inactiveConnInhibitions = connInhibitions[inactiveConnInhibitionsIndexes]
            currentNbActiveConnActivations = len(activeConnActivations)
            currentNbActiveConnInhibitions = len(activeConnInhibitions)

            # Choose the kind of mutation
            choice = np.random.choice(5, p=(self.probParametersMutation, self.probTemplateActivationAdd, self.probTemplateActivationDel, self.probTemplateInhibitionAdd, self.probTemplateInhibitionDel))
            # Verify that the add and del are allowed
            if choice == 1 and (currentNbActiveConnActivations >= self.nbConnActivationsDomain[1] or currentNbActiveConnActivations + currentNbActiveConnInhibitions >= self.nbConnDomain[1]):
                choice = 2 # Transform into del
            elif choice == 2 and (currentNbActiveConnActivations <= self.nbConnActivationsDomain[0] or currentNbActiveConnActivations + currentNbActiveConnInhibitions <= self.nbConnDomain[0]):
                choice = 1 # Transform into add
            elif choice == 3 and (currentNbActiveConnInhibitions >= self.nbConnInhibitionsDomain[1] or currentNbActiveConnActivations + currentNbActiveConnInhibitions >= self.nbConnDomain[1]):
                choice = 4 # Transform into del
            elif choice == 4 and (currentNbActiveConnInhibitions <= self.nbConnInhibitionsDomain[0] or currentNbActiveConnActivations + currentNbActiveConnInhibitions <= self.nbConnDomain[0]):
                choice = 3 # Transform into add

            # Perform the mutation
            if choice == 0: # Parameters mutation
                #print("DEBUG: Mutation")
                tools.mut_polynomial_bounded(ind[:self.nb_nodes], eta=self.eta, low=self.ind_domain[0], up=self.ind_domain[1], mut_pb=self.mutationPb) #stabilities = list(mutPolynomialBounded(ind[:self.nb_nodes], self.eta, 0., 1., self.mutationPb))
                tools.mut_polynomial_bounded(activeConnActivations, eta=self.eta, low=self.ind_domain[0], up=self.ind_domain[1], mut_pb=self.mutationPb)
                tools.mut_polynomial_bounded(activeConnInhibitions, eta=self.eta, low=self.ind_domain[0], up=self.ind_domain[1], mut_pb=self.mutationPb)
            elif choice == 1: # Add Activation
                #print("DEBUG: Add Activation")
                #inactiveConnActivations[np.random.choice(len(inactiveConnActivations))] = 0.0000015
                inactiveConnActivations[np.random.choice(len(inactiveConnActivations))] = np.random.uniform(self.ind_domain[0], self.ind_domain[1])
            elif choice == 2: # Del Activation
                #print("DEBUG: Del Activation")
                idxidx = np.random.choice(len(activeConnActivations))
                activeConnActivations[idxidx] = 0.
                # Remove invalid inhibitions
                idx_activation = activeConnActivationsIndexes[idxidx] #list(activeConnActivationsIndexes).index(idxidx)
                coord_activation = idx_activation//self.nb_nodes, idx_activation%self.nb_nodes
                for i in range(self.nb_nodes):
                    idx_inhibition = i*self.nb_nodes*self.nb_nodes + coord_activation[0] * self.nb_nodes + coord_activation[1]
                    if idx_inhibition in activeConnInhibitionsIndexes:
                        idx2 = list(activeConnInhibitionsIndexes).index(idx_inhibition)
                        activeConnInhibitions[idx2] = 0.


            elif choice == 3: # Add Inhibition
                #print("DEBUG: Add Inhibition")
                #inactiveConnInhibitions[np.random.choice(len(inactiveConnInhibitions))] = 0.0000015

                activations_idx = list(activeConnActivationsIndexes)
                activations_coords = [(x//self.nb_nodes, x%self.nb_nodes) for x in activations_idx]
                allowed_inhibitions_idx = []
                for coord in activations_coords:
                    for i in range(self.nb_nodes):
                        idx = i*self.nb_nodes*self.nb_nodes + coord[0] * self.nb_nodes + coord[1]
                        if idx in inactiveConnInhibitionsIndexes:
                            idxidx = list(inactiveConnInhibitionsIndexes).index(idx)
                            allowed_inhibitions_idx.append(idxidx)
                #print("DEBUG: Add Inhibition3:", allowed_inhibitions_idx)
                if len(allowed_inhibitions_idx):
                    np.random.shuffle(allowed_inhibitions_idx)
                    #print("DEBUG: Add Inhibition4:", allowed_inhibitions_idx[0])
                    inactiveConnInhibitions[allowed_inhibitions_idx[0]] = np.random.uniform(self.ind_domain[0], self.ind_domain[1])
                    #print("DEBUG: Add Inhibition5:", inactiveConnInhibitions)
            elif choice == 4: # Del Inhibition
                #print("DEBUG: Del Inhibition")
                activeConnInhibitions[np.random.choice(len(activeConnInhibitions))] = 0.
            connActivations[activeConnActivationsIndexes] = activeConnActivations
            connActivations[inactiveConnActivationsIndexes] = inactiveConnActivations
            connInhibitions[activeConnInhibitionsIndexes] = activeConnInhibitions
            connInhibitions[inactiveConnInhibitionsIndexes] = inactiveConnInhibitions

            if submitDACCAD.isValid(ind, self.nb_nodes):
                #print("VARY: VALID !")
                break
            else: # XXX
                pass
                print(f"VARY: NOT VALID ! (choice={choice})")
        return ind




@registry.register
class DaccadSparseInitAndMut3(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [4, 10],
            min_init_budget: int = 500,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            probParametersMutation: float = 0.55,
            probTemplateActivationAdd: float = 0.10,
            probTemplateActivationDel: float = 0.05,
            probTemplateInhibitionAdd: float = 0.10,
            probTemplateInhibitionDel: float = 0.05,
            probNodeAdd: float = 0.10,
            probNodeDel: float = 0.05,
            nbConnActivationsDomain: Sequence[int] = [1, 19],
            nbConnInhibitionsDomain: Sequence[int] = [1, 19],
            nbConnDomain: Sequence[int] = [2, 20],
            eta: float = 20.,
            mutationPb: float = 0.4,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.probParametersMutation = probParametersMutation
        self.probTemplateActivationAdd = probTemplateActivationAdd
        self.probTemplateActivationDel = probTemplateActivationDel
        self.probTemplateInhibitionAdd = probTemplateInhibitionAdd
        self.probTemplateInhibitionDel = probTemplateInhibitionDel
        self.probNodeAdd = probNodeAdd
        self.probNodeDel = probNodeDel
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.eta = eta
        self.mutationPb = mutationPb

        self.nb_nodes_max = self.nb_nodes_domain[1]
        select_or_initialise = partial(self._select_or_initialise)
        vary = partial(self._vary)
        super().__init__(container, budget, select_or_initialise=select_or_initialise, vary=vary, base_ind_gen=gen_daccad_individuals(self.ind_domain), **kwargs)


#    def __getstate__(self):
#        odict = super().__getstate__() # self.__dict__.copy()
#        del odict['base_ind_gen']
#        #del odict['base_ind_gen_fn']
#        return odict

    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        if initialise: # Initialisation
            for _ in range(500): # XXX
                # Define the number of nodes
                nb_nodes = np.random.randint(self.nb_nodes_domain[0], self.nb_nodes_domain[1])
                base_ind.nb_nodes = nb_nodes
                nb_activations = int(nb_nodes * nb_nodes)
                nb_inhibitions = int(nb_nodes * nb_nodes * nb_nodes)
                # Generate activations
                base_ind.activations = np.array(generateSparseUniformDomain(nb_activations, self.ind_domain, 1, self.nbConnActivationsDomain)[0]).reshape((nb_nodes, nb_nodes))
                activations_coords = list(zip(*np.where(base_ind.activations)))
                # Find allowed inhibitions
                allowed_inhibitions_coords = []
                for coord in activations_coords:
                    for i in range(nb_nodes):
                        allowed_inhibitions_coords.append((i,) + coord)
                np.random.shuffle(allowed_inhibitions_coords)
                nb_gen_inhibitions = np.random.randint(self.nbConnInhibitionsDomain[0], self.nbConnInhibitionsDomain[1])
                allowed_inhibitions_coords = allowed_inhibitions_coords[:nb_gen_inhibitions]
                # Generate inhibitions
                base_ind.inhibitions = np.zeros((nb_nodes, nb_nodes, nb_nodes))
                for coord in allowed_inhibitions_coords:
                    base_ind.inhibitions[coord] = np.random.uniform(self.ind_domain[0], self.ind_domain[1])
                # Verify if the total number of templates is within domain
                if len(activations_coords) + len(allowed_inhibitions_coords) < self.nbConnDomain[0] or \
                        len(activations_coords) + len(allowed_inhibitions_coords) > self.nbConnDomain[1]:
                    continue
                # Generate stabilities
                base_ind.stabilities = generateUniform(nb_nodes, self.ind_domain, 1)[0]
                # Assemble individual
                base_ind.assemble()
                # Verify if valid
                if base_ind.is_valid():
                    break
                else:
                    print("INIT: NOT VALID !")
                    raise RuntimeError("INIT: NOT VALID !")
            return base_ind, False

        else: # Selection
            try:
                res = tools.sel_random(collection), True
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()
            return res


    def _vary(self, individual: Any) -> Any:
        for _ in range(500): # XXX
            ind = copy.deepcopy(individual)
            ind.name = str(id(ind))

            activations_coords_set = {(x, y) for x in range(ind.nb_nodes) for y in range(ind.nb_nodes)}
            active_activations_coords = list(zip(*np.where(ind.activations)))
            nb_active_activations = len(active_activations_coords)
            inactive_activations_coords = list(activations_coords_set - set(active_activations_coords))
            inhibitions_coords_set = {(x, y, z) for x in range(ind.nb_nodes) for y in range(ind.nb_nodes) for z in range(ind.nb_nodes)}
            active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
            nb_active_inhibitions = len(active_inhibitions_coords)
            inactive_inhibitions_coords = list(inhibitions_coords_set - set(active_inhibitions_coords))

            # Choose the kind of mutation
            choice = np.random.choice(7, p=(self.probParametersMutation, self.probTemplateActivationAdd, self.probTemplateActivationDel, self.probTemplateInhibitionAdd, self.probTemplateInhibitionDel, self.probNodeAdd, self.probNodeDel))
            # Verify that the add and del are allowed
            if choice == 1 and (nb_active_activations >= self.nbConnActivationsDomain[1] or nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[1]):
                choice = 2 # Transform into del
            elif choice == 2 and (nb_active_activations <= self.nbConnActivationsDomain[0] or nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[0]):
                choice = 1 # Transform into add
            elif choice == 3 and (nb_active_inhibitions >= self.nbConnInhibitionsDomain[1] or nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[1]):
                choice = 4 # Transform into del
            elif choice == 4 and (nb_active_inhibitions <= self.nbConnInhibitionsDomain[0] or nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[0]):
                choice = 3 # Transform into add
            elif choice == 5 and ind.nb_nodes >= self.nb_nodes_domain[1]:
                choice = 6 # Transform into del
            elif choice == 6 and ind.nb_nodes <= self.nb_nodes_domain[0]:
                choice = 5 # Transform into add


            # Perform the mutation
            if choice == 0: # Parameters mutation
                #print("DEBUG: Mutation")
                tools.mut_polynomial_bounded(ind.stabilities, eta=self.eta, low=self.ind_domain[0], up=self.ind_domain[1], mut_pb=self.mutationPb)
                ind.activations[np.where(ind.activations)] = tools.mut_polynomial_bounded(ind.activations[np.where(ind.activations)], eta=self.eta, low=self.ind_domain[0], up=self.ind_domain[1], mut_pb=self.mutationPb)
                ind.inhibitions[np.where(ind.inhibitions)] = tools.mut_polynomial_bounded(ind.inhibitions[np.where(ind.inhibitions)], eta=self.eta, low=self.ind_domain[0], up=self.ind_domain[1], mut_pb=self.mutationPb)

            elif choice == 1: # Add Activation
                #print("DEBUG: Add Activation")
                ind.activations[random.choice(inactive_activations_coords)] = np.random.uniform(self.ind_domain[0], self.ind_domain[1])
            elif choice == 2: # Del Activation
                #print("DEBUG: Del Activation")
                coord = random.choice(active_activations_coords)
                ind.activations[coord] = 0.
                # Remove invalid inhibitions
                for i in range(ind.nb_nodes):
                    ind.inhibitions[(i,) + coord] = 0.

            elif choice == 3: # Add Inhibition
                #print("DEBUG: Add Inhibition")
                allowed_inhibitions_coords = []
                for coord in active_activations_coords:
                    for i in range(ind.nb_nodes):
                        coord2 = (i,) + coord
                        if coord2 not in active_inhibitions_coords:
                            allowed_inhibitions_coords.append(coord2)
                if len(allowed_inhibitions_coords):
                    ind.inhibitions[random.choice(allowed_inhibitions_coords)] = np.random.uniform(self.ind_domain[0], self.ind_domain[1])
            elif choice == 4: # Del Inhibition
                #print("DEBUG: Del Inhibition")
                ind.inhibitions[random.choice(active_inhibitions_coords)] = 0.

            elif choice == 5: # Add node
                ind.nb_nodes += 1
                stabilities = np.empty(ind.nb_nodes)
                stabilities[:ind.nb_nodes-1] = ind.stabilities
                stabilities[-1] = np.random.uniform(self.ind_domain[0], self.ind_domain[1])
                ind.stabilities = stabilities
                activations = np.zeros((ind.nb_nodes, ind.nb_nodes))
                activations[:ind.nb_nodes-1, :ind.nb_nodes-1] = ind.activations
                ind.activations = activations
                inhibitions = np.zeros((ind.nb_nodes, ind.nb_nodes, ind.nb_nodes))
                inhibitions[:ind.nb_nodes-1, :ind.nb_nodes-1, :ind.nb_nodes-1] = ind.inhibitions
                ind.inhibitions = inhibitions

                allowed_inhibitions_coords = []
                for coord in active_activations_coords:
                    coord2 = (ind.nb_nodes-1,) + coord
                    allowed_inhibitions_coords.append(coord2)

                choice2 = np.random.choice(4) if len(allowed_inhibitions_coords) else np.random.choice(2)
                if choice2 == 0:
                    ind.activations[-1, np.random.choice(ind.nb_nodes)] = np.random.uniform(self.ind_domain[0], self.ind_domain[1])
                elif choice2 == 1:
                    ind.activations[np.random.choice(ind.nb_nodes), -1] = np.random.uniform(self.ind_domain[0], self.ind_domain[1])
                elif choice2 == 2 or choice2 == 3:
                    ind.inhibitions[random.choice(allowed_inhibitions_coords)] = np.random.uniform(self.ind_domain[0], self.ind_domain[1])

            elif choice == 6: # Del node
                node_to_del = np.random.choice(ind.nb_nodes)
                ind.nb_nodes -= 1
                mask_activations = np.ones_like(ind.activations, dtype=bool)
                mask_activations[node_to_del, :] = mask_activations[:, node_to_del] = False
                ind.activations = ind.activations[mask_activations].reshape((ind.nb_nodes, ind.nb_nodes))
                mask_inhibitions = np.ones_like(ind.inhibitions, dtype=bool)
                mask_inhibitions[node_to_del, :, :] = mask_inhibitions[:, node_to_del, :] = mask_inhibitions[:, :, node_to_del] = False
                ind.inhibitions = ind.inhibitions[mask_inhibitions].reshape((ind.nb_nodes, ind.nb_nodes, ind.nb_nodes))
                ind.stabilities = np.delete(ind.stabilities, node_to_del)

            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                active_activations_coords = list(zip(*np.where(ind.activations)))
                nb_active_activations = len(active_activations_coords)
                active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
                nb_active_inhibitions = len(active_inhibitions_coords)
                if nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        return ind



@registry.register
class DaccadSparseInitAndMut4(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [1, 7],
            nb_nodes_domain_init: Sequence[int] = [1, 1],
            min_init_budget: int = 1,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            probParametersMutation: float = 0.55,
            probTemplateActivationAdd: float = 0.10,
            probTemplateActivationDel: float = 0.05,
            probTemplateInhibitionAdd: float = 0.10,
            probTemplateInhibitionDel: float = 0.05,
            probNodeAdd: float = 0.10,
            probNodeDel: float = 0.05,
            nbConnActivationsDomain: Sequence[int] = [1, 19],
            nbConnInhibitionsDomain: Sequence[int] = [1, 19],
            nbConnDomain: Sequence[int] = [2, 20],
            eta: float = 20.,
            mutationPb: float = 0.4,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.nb_nodes_domain_init = nb_nodes_domain_init
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.probParametersMutation = probParametersMutation
        self.probTemplateActivationAdd = probTemplateActivationAdd
        self.probTemplateActivationDel = probTemplateActivationDel
        self.probTemplateInhibitionAdd = probTemplateInhibitionAdd
        self.probTemplateInhibitionDel = probTemplateInhibitionDel
        self.probNodeAdd = probNodeAdd
        self.probNodeDel = probNodeDel
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.eta = eta
        self.mutationPb = mutationPb

        select_or_initialise = partial(self._select_or_initialise)
        vary = partial(self._vary)
        super().__init__(container, budget, select_or_initialise=select_or_initialise, vary=vary, base_ind_gen=gen_daccad_individuals(self.ind_domain), **kwargs)


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        if initialise: # Initialisation
            for _ in range(500): # XXX
                # Define the number of nodes
                if self.nb_nodes_domain_init[0] != self.nb_nodes_domain_init[1]:
                    nb_nodes = np.random.randint(self.nb_nodes_domain_init[0], self.nb_nodes_domain_init[1])
                else:
                    nb_nodes = self.nb_nodes_domain_init[0]
                base_ind.nb_nodes = nb_nodes
                nb_activations = int(nb_nodes * nb_nodes)
                nb_inhibitions = int(nb_nodes * nb_nodes * nb_nodes)
                # Generate activations
                nb_activations_domain = self.nbConnActivationsDomain
                nb_activations_domain[1] = min(nb_activations_domain[1], nb_activations)
                #nb_activations_domain[1] -= 1 # Remove 1 for auto-catalysis connection
                if nb_nodes == 1:
                    base_ind.activations = np.array([np.random.uniform(self.ind_domain[0], self.ind_domain[1])]).reshape((nb_nodes, nb_nodes))
                else:
                    base_ind.activations = np.array(generateSparseUniformDomain(nb_activations, self.ind_domain, 1, nb_activations_domain)[0]).reshape((nb_nodes, nb_nodes))
                ## Add auto-catalysis on a random node
                chosen_node_autocatalysis = np.random.choice(nb_nodes)
                base_ind.activations[chosen_node_autocatalysis, chosen_node_autocatalysis] = np.random.uniform(self.ind_domain[0], self.ind_domain[1])
                # Find allowed inhibitions
                activations_coords = list(zip(*np.where(base_ind.activations)))
                allowed_inhibitions_coords = []
                for coord in activations_coords:
                    target_node = np.random.choice(nb_nodes)
                    allowed_inhibitions_coords.append((target_node,) + coord)
                    #for i in range(nb_nodes):
                    #    allowed_inhibitions_coords.append((i,) + coord)
                np.random.shuffle(allowed_inhibitions_coords)
                nb_gen_inhibitions = np.random.randint(self.nbConnInhibitionsDomain[0], self.nbConnInhibitionsDomain[1])
                allowed_inhibitions_coords = allowed_inhibitions_coords[:nb_gen_inhibitions]
                # Generate inhibitions
                base_ind.inhibitions = np.zeros((nb_nodes, nb_nodes, nb_nodes))
                for coord in allowed_inhibitions_coords:
                    base_ind.inhibitions[coord] = np.random.uniform(self.ind_domain[0], self.ind_domain[1])
                # Verify if the total number of templates is within domain
                if len(activations_coords) + len(allowed_inhibitions_coords) < self.nbConnDomain[0] or \
                        len(activations_coords) + len(allowed_inhibitions_coords) > self.nbConnDomain[1]:
                    #print("INIT: OUT OF BOUNDS")
                    continue
                # Generate stabilities
                base_ind.stabilities = generateUniform(nb_nodes, self.ind_domain, 1)[0]
                # Assemble individual
                base_ind.assemble()
                # Verify if valid
                if base_ind.is_valid():
                    #print("INIT: VALID !")
                    break
                else:
                    print("INIT: NOT VALID !")
                    raise RuntimeError("INIT: NOT VALID !")
            return base_ind, False

        else: # Selection
            try:
                res = tools.non_trivial_sel_random(collection), True
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()
            return res


    def _vary(self, individual: Any) -> Any:
        for _ in range(500): # XXX
            ind = copy.deepcopy(individual)
            ind.name = str(id(ind))

            active_activations_coords = list(zip(*np.where(ind.activations)))
            nb_active_activations = len(active_activations_coords)
            active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
            nb_active_inhibitions = len(active_inhibitions_coords)

            # Choose the kind of mutation
            choice = np.random.choice(7, p=(self.probParametersMutation, self.probTemplateActivationAdd, self.probTemplateActivationDel, self.probTemplateInhibitionAdd, self.probTemplateInhibitionDel, self.probNodeAdd, self.probNodeDel))
            # Verify that the add and del are allowed
            if choice == 1 and (nb_active_activations >= self.nbConnActivationsDomain[1] or nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[1]):
                choice = 2 # Transform into del
            elif choice == 2 and (nb_active_activations <= self.nbConnActivationsDomain[0] or nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[0]):
                choice = 1 # Transform into add
            elif choice == 3 and (nb_active_inhibitions >= self.nbConnInhibitionsDomain[1] or nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[1]):
                choice = 4 # Transform into del
            elif choice == 4 and (nb_active_inhibitions <= self.nbConnInhibitionsDomain[0] or nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[0]):
                choice = 3 # Transform into add
            elif choice == 5 and ind.nb_nodes >= self.nb_nodes_domain[1]:
                choice = 6 # Transform into del
            elif choice == 6 and ind.nb_nodes <= self.nb_nodes_domain[0]:
                choice = 5 # Transform into add

            # Perform the mutation
            if choice == 0: # Parameters mutation
                mutation_param_polybounded(ind, self.eta, self.mutationPb)
            elif choice == 1: # Add Activation
                mutation_add_activation(ind)
            elif choice == 2: # Del Activation
                mutation_del_activation(ind)
            elif choice == 3: # Add Inhibition
                mutation_add_inhibition(ind)
            elif choice == 4: # Del Inhibition
                mutation_del_inhibition(ind)
            elif choice == 5: # Add node
                mutation_add_node(ind)
            elif choice == 6: # Del node
                mutation_del_node(ind)

            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                active_activations_coords = list(zip(*np.where(ind.activations)))
                nb_active_activations = len(active_activations_coords)
                active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
                nb_active_inhibitions = len(active_inhibitions_coords)
                if nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        return ind


@registry.register
class DaccadSparseInitAndMut5(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [3, 7],
            min_init_budget: int = 1,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            probParametersMutation: float = 0.95,
            probTemplateActivationAdd: float = 0.01,
            probTemplateActivationDel: float = 0.01,
            probTemplateInhibitionAdd: float = 0.01,
            probTemplateInhibitionDel: float = 0.01,
            probNodeAdd: float = 0.01,
            probNodeDel: float = 0.00,
            nbConnActivationsDomain: Sequence[int] = [1, 7],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [1, 13],
            eta: float = 20.,
            mutationPb: float = 0.4,
            allow_gd: bool = False,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.probParametersMutation = probParametersMutation
        self.probTemplateActivationAdd = probTemplateActivationAdd
        self.probTemplateActivationDel = probTemplateActivationDel
        self.probTemplateInhibitionAdd = probTemplateInhibitionAdd
        self.probTemplateInhibitionDel = probTemplateInhibitionDel
        self.probNodeAdd = probNodeAdd
        self.probNodeDel = probNodeDel
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.eta = eta
        self.mutationPb = mutationPb

        base_ind_gen = gen_daccad_individuals_GD(self.ind_domain) if allow_gd else gen_daccad_individuals(self.ind_domain)
        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=base_ind_gen, **kwargs)


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        if initialise: # Initialisation
            # Create a base individual
            standard_init_ind(base_ind)
            return base_ind, False

        else: # Selection
            try:
                res = tools.non_trivial_sel_random(collection), True
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()
            return res


    def _vary_with_pb(self, individual, param_mut_pb, act_add_mut_pb, act_del_mut_pb, inh_add_mut_pb, inh_del_mut_pb, node_add_mut_pb, node_del_mut_pb):
        for _ in range(500): # XXX
            ind = copy.deepcopy(individual)
            ind.name = str(id(ind))

            active_activations_coords = list(zip(*np.where(ind.activations)))
            nb_active_activations = len(active_activations_coords)
            active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
            nb_active_inhibitions = len(active_inhibitions_coords)

            # Choose the kind of mutation
            choice = np.random.choice(7, p=(param_mut_pb, act_add_mut_pb, act_del_mut_pb, inh_add_mut_pb, inh_del_mut_pb, node_add_mut_pb, node_del_mut_pb))
            # Verify that the add and del are allowed
            if choice == 1 and (nb_active_activations >= self.nbConnActivationsDomain[1] or nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[1]):
                choice = 2 # Transform into del
            elif choice == 2 and (nb_active_activations <= self.nbConnActivationsDomain[0] or nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[0]):
                choice = 1 # Transform into add
            elif choice == 3 and (nb_active_inhibitions >= self.nbConnInhibitionsDomain[1] or nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[1]):
                choice = 4 # Transform into del
            elif choice == 4 and (nb_active_inhibitions <= self.nbConnInhibitionsDomain[0] or nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[0]):
                choice = 3 # Transform into add
            elif choice == 5 and ind.nb_nodes >= self.nb_nodes_domain[1]:
                choice = 6 # Transform into del
            elif choice == 6 and ind.nb_nodes <= self.nb_nodes_domain[0]:
                choice = 5 # Transform into add

            # Perform the mutation
            if choice == 0: # Parameters mutation
                mutation_param_polybounded(ind, self.eta, self.mutationPb)
                #mutation_param_bioneat(ind, self.f1, self.f2, 1)
            elif choice == 1: # Add Activation
                mutation_add_activation(ind)
            elif choice == 2: # Del Activation
                mutation_del_activation(ind)
            elif choice == 3: # Add Inhibition
                mutation_add_inhibition(ind)
            elif choice == 4: # Del Inhibition
                mutation_del_inhibition(ind)
            elif choice == 5: # Add node
                mutation_add_node(ind)
            elif choice == 6: # Del node
                mutation_del_node(ind)


            ## Verify if there are no orphaned nodes
            #is_orphaned = (not(any(ind.activations[:,i]) or any(ind.activations[i,:]) or any(ind.inhibitions[i,:,:].flatten())) for i in range(ind.nb_nodes))
            #if any(is_orphaned):
            #    continue

            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                active_activations_coords = list(zip(*np.where(ind.activations)))
                nb_active_activations = len(active_activations_coords)
                active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
                nb_active_inhibitions = len(active_inhibitions_coords)
                if nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        return ind


    def _vary(self, individual: Any) -> Any:
        return self._vary_with_pb(individual, self.probParametersMutation,
                self.probTemplateActivationAdd, self.probTemplateActivationDel,
                self.probTemplateInhibitionAdd, self.probTemplateInhibitionDel,
                self.probNodeAdd, self.probNodeDel)





@registry.register
class DaccadSparseInitAndMut6(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [3, 7],
            min_init_budget: int = 1,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            probParametersMutation: float = 0.95,
            probTemplateActivationAdd: float = 0.01,
            probTemplateActivationDel: float = 0.01,
            probTemplateInhibitionAdd: float = 0.01,
            probTemplateInhibitionDel: float = 0.01,
            probNodeAdd: float = 0.01,
            probNodeDel: float = 0.00,
            nbConnActivationsDomain: Sequence[int] = [1, 7],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [1, 13],
            eta: float = 20.,
            mutationPb: float = 0.4,
            allow_gd: bool = False,
            gd_pb: float = 1.0,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.probParametersMutation = probParametersMutation
        self.probTemplateActivationAdd = probTemplateActivationAdd
        self.probTemplateActivationDel = probTemplateActivationDel
        self.probTemplateInhibitionAdd = probTemplateInhibitionAdd
        self.probTemplateInhibitionDel = probTemplateInhibitionDel
        self.probNodeAdd = probNodeAdd
        self.probNodeDel = probNodeDel
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.eta = eta
        self.mutationPb = mutationPb

        base_ind_gen = gen_daccad_individuals_GD(self.ind_domain, gd_pb) if allow_gd else gen_daccad_individuals(self.ind_domain)
        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=base_ind_gen, **kwargs)


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        if initialise: # Initialisation
            # Create a base individual
            standard_init_ind(base_ind)
            return base_ind, False

        else: # Selection
            try:
                #res = tools.non_trivial_sel_random(collection), True
                choice = np.random.choice(2, p=(0.70, 0.30))
                if choice == 0:
                    res = tools.non_trivial_sel_random(collection), True
                else:
                    res = tools.sel_random(collection), True
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()
            return res


    def _vary_with_pb(self, individual, param_mut_pb, act_add_mut_pb, act_del_mut_pb, inh_add_mut_pb, inh_del_mut_pb, node_add_mut_pb, node_del_mut_pb):
        for _ in range(500): # XXX
            ind = copy.deepcopy(individual)
            ind.name = str(id(ind))

            active_activations_coords = list(zip(*np.where(ind.activations)))
            nb_active_activations = len(active_activations_coords)
            active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
            nb_active_inhibitions = len(active_inhibitions_coords)

            # Choose the kind of mutation
            choice = np.random.choice(7, p=(param_mut_pb, act_add_mut_pb, act_del_mut_pb, inh_add_mut_pb, inh_del_mut_pb, node_add_mut_pb, node_del_mut_pb))
            # Verify that the add and del are allowed
            if choice == 1 and (nb_active_activations >= self.nbConnActivationsDomain[1] or nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[1]):
                choice = 2 # Transform into del
            elif choice == 2 and (nb_active_activations <= self.nbConnActivationsDomain[0] or nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[0]):
                choice = 1 # Transform into add
            elif choice == 3 and (nb_active_inhibitions >= self.nbConnInhibitionsDomain[1] or nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[1]):
                choice = 4 # Transform into del
            elif choice == 4 and (nb_active_inhibitions <= self.nbConnInhibitionsDomain[0] or nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[0]):
                choice = 3 # Transform into add
            elif choice == 5 and ind.nb_nodes >= self.nb_nodes_domain[1]:
                choice = 6 # Transform into del
            elif choice == 6 and ind.nb_nodes <= self.nb_nodes_domain[0]:
                choice = 5 # Transform into add

            # Perform the mutation
            if choice == 0: # Parameters mutation
                mutation_param_polybounded(ind, self.eta, self.mutationPb)
                #mutation_param_bioneat(ind, self.f1, self.f2, 1)
            elif choice == 1: # Add Activation
                mutation_add_activation_with_gradients(ind)
            elif choice == 2: # Del Activation
                mutation_del_activation(ind)
            elif choice == 3: # Add Inhibition
                mutation_add_inhibition(ind)
            elif choice == 4: # Del Inhibition
                mutation_del_inhibition(ind)
            elif choice == 5: # Add node
                mutation_add_node_with_gradients(ind)
            elif choice == 6: # Del node
                mutation_del_node(ind)


            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                #active_activations_coords = list(zip(*np.where(ind.activations)))
                #nb_active_activations = len(active_activations_coords)
                #active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
                #nb_active_inhibitions = len(active_inhibitions_coords)
                activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                nb_active_activations = len(activeConnActivationsIndexes)
                activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                if nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        return ind


    def _vary(self, individual: Any) -> Any:
        return self._vary_with_pb(individual, self.probParametersMutation,
                self.probTemplateActivationAdd, self.probTemplateActivationDel,
                self.probTemplateInhibitionAdd, self.probTemplateInhibitionDel,
                self.probNodeAdd, self.probNodeDel)



# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
