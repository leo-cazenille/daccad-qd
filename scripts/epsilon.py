#!/usr/bin/env python3

# DEBUGGING
import faulthandler
faulthandler.enable()


########## IMPORTS ########### {{{1

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

import os
import pathlib
import datetime
from timeit import default_timer as timer
import copy
import itertools
from functools import partial
import yaml
import numpy as np
import random
import warnings
import traceback
import math
import socket

import submitDACCAD

from qdpy.algorithms import *
from qdpy.containers import *
#from qdpy.benchmarks import *
from qdpy.plots import *
from qdpy.base import *
from qdpy import tools

from typing import Optional, Tuple, List, Iterable, Iterator, Any, TypeVar, Generic, Union, Sequence, MutableSet, MutableSequence, Type, Callable, Generator, Mapping, MutableMapping, overload
import qdpy.hdsobol as hdsobol
from base import *
from species import *
from batch import *



@registry.register
class DaccadBioneatMutEpsilon(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [3, 7],
            min_init_budget: int = 1,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            prob_parameter_mut: float = 0.96,
            prob_disable_template: float = 0.01,
            prob_activation_add: float = 0.01,
            prob_inhibition_add: float = 0.01,
            prob_node_add: float = 0.01,
            nbConnActivationsDomain: Sequence[int] = [1, 7],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [1, 13],
            f1: float = 0.2,
            f2: float = 2.0,
            mut_pb: float = 0.8,
            epsilon_exploration: float = 0.90,
            epsilon_exploitation: float = 0.30,
            exploitation_threshold: float = 0.75,
            allow_gd: bool = False,
            gd_pb: float = 1.0,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.prob_parameter_mut = prob_parameter_mut
        self.prob_disable_template = prob_disable_template
        self.prob_activation_add = prob_activation_add
        self.prob_inhibition_add = prob_inhibition_add
        self.prob_node_add = prob_node_add
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.f1 = f1
        self.f2 = f2
        self.mut_pb = mut_pb
        self.epsilon_exploration = epsilon_exploration
        self.epsilon_exploitation = epsilon_exploitation
        self.exploitation_threshold = exploitation_threshold

        base_ind_gen = gen_daccad_individuals_GD(self.ind_domain, gd_pb) if allow_gd else gen_daccad_individuals(self.ind_domain)
        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=base_ind_gen, **kwargs)

    def _finished_exploration(self):
        # Check if we filled out most of the grid with non-trivial individuals
        nb_non_trivials = len([ind for ind in self.container if ind.fitness.values[0] > 0.])
        return nb_non_trivials / self.container.capacity >= self.exploitation_threshold


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        if initialise: # Initialisation
            # Create a base individual
            standard_init_ind(base_ind)
            return base_ind, False

        else: # Selection
            try:
                exploration = not self._finished_exploration()
                epsilon = self.epsilon_exploration if exploration else self.epsilon_exploitation

                choice = np.random.random() > epsilon
                if choice:
                    res = collection.best, True
                else:
                    choice2 = np.random.choice(2, p=(0.70, 0.30))
                    if choice2 == 0:
                        res = tools.non_trivial_sel_random(collection), True
                    else:
                        res = tools.sel_random(collection), True

            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()
            return res


    def _vary(self, individual):
        for _ in range(500): # XXX
            ind = copy.deepcopy(individual)
            ind.name = str(id(ind))

            # Choose the kind of mutation
            choice = np.random.choice(5, p=(self.prob_parameter_mut,
                self.prob_disable_template,
                self.prob_activation_add, self.prob_inhibition_add,
                self.prob_node_add))

            # Perform the mutation
            try:
                if choice == 0: # Parameters mutation
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                elif choice == 1: # Disable template
                    disable_template(ind)
                elif choice == 2: # Add Activation
                    add_activation_with_gradients(ind)
                elif choice == 3: # Add inhibition
                    add_inhibition_with_gradients(ind)
                elif choice == 4: # Add node
                    add_node_with_gradients(ind)
            except Exception as e:
                print(f"VARY: EXCEPTION (choice={choice}): {e}")
                continue

            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                nb_active_activations = len(activeConnActivationsIndexes)
                activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                if nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        return ind






# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
