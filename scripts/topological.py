#!/usr/bin/env python3

# DEBUGGING
import faulthandler
faulthandler.enable()


########## IMPORTS ########### {{{1

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

import os
import pathlib
import datetime
from timeit import default_timer as timer
import copy
import itertools
from functools import partial
import yaml
import numpy as np
import random
import warnings
import traceback
import math
import socket

import submitDACCAD

from qdpy.algorithms import *
from qdpy.containers import *
#from qdpy.benchmarks import *
from qdpy.plots import *
from qdpy.base import *
from qdpy import tools

from typing import Optional, Tuple, List, Iterable, Iterator, Any, TypeVar, Generic, Union, Sequence, MutableSet, MutableSequence, Type, Callable, Generator, Mapping, MutableMapping, overload
import qdpy.hdsobol as hdsobol
from base import *
from species import *
from batch import *





@registry.register
class DaccadTopologicalInit(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [3, 7],
            min_init_budget: int = 500,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            probParametersMutation: float = 0.55,
            probTemplateActivationAdd: float = 0.10,
            probTemplateActivationDel: float = 0.05,
            probTemplateInhibitionAdd: float = 0.10,
            probTemplateInhibitionDel: float = 0.05,
            probNodeAdd: float = 0.10,
            probNodeDel: float = 0.05,
            nbConnActivationsDomain: Sequence[int] = [2, 8],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [2, 14],
            eta: float = 20.,
            mutationPb: float = 0.4,
            init_nb_muts_domain: Sequence[int] = [0, 20],
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.probParametersMutation = probParametersMutation
        self.probTemplateActivationAdd = probTemplateActivationAdd
        self.probTemplateActivationDel = probTemplateActivationDel
        self.probTemplateInhibitionAdd = probTemplateInhibitionAdd
        self.probTemplateInhibitionDel = probTemplateInhibitionDel
        self.probNodeAdd = probNodeAdd
        self.probNodeDel = probNodeDel
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.eta = eta
        self.mutationPb = mutationPb
        self.init_nb_muts_domain = init_nb_muts_domain

        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=gen_daccad_individuals(self.ind_domain), **kwargs)


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        if initialise: # Initialisation
            # Define init mutation probabilities
            init_param_mut_pb    = 0.
            init_act_add_mut_pb  = 1./5.
            init_act_del_mut_pb  = 1./5.
            init_inh_add_mut_pb  = 1./5.
            init_inh_del_mut_pb  = 1./5.
            init_node_add_mut_pb = 1./5.
            init_node_del_mut_pb = 0.

            for _ in range(500): # XXX
                # Create a base individual
                standard_init_ind(base_ind)

                # Apply N topological mutations
                nb_mutations = np.random.randint(self.init_nb_muts_domain[0], self.init_nb_muts_domain[1])
                for _ in range(nb_mutations):
                    base_ind = self._vary_with_pb(base_ind, init_param_mut_pb,
                            init_act_add_mut_pb, init_act_del_mut_pb,
                            init_inh_add_mut_pb, init_inh_del_mut_pb,
                            init_node_add_mut_pb, init_node_del_mut_pb)

                # Verify if valid
                if base_ind.is_valid():
                    break
                else:
                    print("INIT: NOT VALID !")
                    raise RuntimeError("INIT: NOT VALID !")
            #print(sum(base_ind.activations.flatten()>0.), sum(base_ind.inhibitions.flatten()>0.))
            return base_ind, False

        else: # Selection
            try:
                choice2 = np.random.choice(2)
                if choice2 == 0:
                    res = tools.non_trivial_sel_random(collection), True
                else:
                    res = tools.sel_random(collection), True
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()
            return res


    def _vary_with_pb(self, individual, param_mut_pb, act_add_mut_pb, act_del_mut_pb, inh_add_mut_pb, inh_del_mut_pb, node_add_mut_pb, node_del_mut_pb):
        for _ in range(500): # XXX
            ind = copy.deepcopy(individual)
            ind.name = str(id(ind))

            active_activations_coords = list(zip(*np.where(ind.activations)))
            nb_active_activations = len(active_activations_coords)
            active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
            nb_active_inhibitions = len(active_inhibitions_coords)

            # Choose the kind of mutation
            choice = np.random.choice(7, p=(param_mut_pb, act_add_mut_pb, act_del_mut_pb, inh_add_mut_pb, inh_del_mut_pb, node_add_mut_pb, node_del_mut_pb))
            # Verify that the add and del are allowed
            if choice == 1 and (nb_active_activations >= self.nbConnActivationsDomain[1] or nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[1]):
                choice = 2 # Transform into del
            elif choice == 2 and (nb_active_activations <= self.nbConnActivationsDomain[0] or nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[0]):
                choice = 1 # Transform into add
            elif choice == 3 and (nb_active_inhibitions >= self.nbConnInhibitionsDomain[1] or nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[1]):
                choice = 4 # Transform into del
            elif choice == 4 and (nb_active_inhibitions <= self.nbConnInhibitionsDomain[0] or nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[0]):
                choice = 3 # Transform into add
            elif choice == 5 and ind.nb_nodes >= self.nb_nodes_domain[1]:
                choice = 6 # Transform into del
            elif choice == 6 and ind.nb_nodes <= self.nb_nodes_domain[0]:
                choice = 5 # Transform into add

            # Perform the mutation
            if choice == 0: # Parameters mutation
                mutation_param_polybounded(ind, self.eta, self.mutationPb)
                #mutation_param_bioneat(ind, self.f1, self.f2, 1)
            elif choice == 1: # Add Activation
                mutation_add_activation(ind)
            elif choice == 2: # Del Activation
                mutation_del_activation(ind)
            elif choice == 3: # Add Inhibition
                mutation_add_inhibition(ind)
            elif choice == 4: # Del Inhibition
                mutation_del_inhibition(ind)
            elif choice == 5: # Add node
                mutation_add_node(ind)
            elif choice == 6: # Del node
                mutation_del_node(ind)


            # Verify if there are no orphaned nodes
            is_orphaned = (not(any(ind.activations[:,i]) or any(ind.activations[i,:]) or any(ind.inhibitions[i,:,:].flatten())) for i in range(ind.nb_nodes))
            if any(is_orphaned):
                continue

            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                active_activations_coords = list(zip(*np.where(ind.activations)))
                nb_active_activations = len(active_activations_coords)
                active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
                nb_active_inhibitions = len(active_inhibitions_coords)
                if nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        return ind


    def _vary(self, individual: Any) -> Any:
        return self._vary_with_pb(individual, self.probParametersMutation,
                self.probTemplateActivationAdd, self.probTemplateActivationDel,
                self.probTemplateInhibitionAdd, self.probTemplateInhibitionDel,
                self.probNodeAdd, self.probNodeDel)



@registry.register
class DaccadTopologicalInit2(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [3, 7],
            min_init_budget: int = 500,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            probParametersMutation: float = 0.75,
            probTemplateActivationAdd: float = 0.05,
            probTemplateActivationDel: float = 0.05,
            probTemplateInhibitionAdd: float = 0.05,
            probTemplateInhibitionDel: float = 0.05,
            probNodeAdd: float = 0.05,
            probNodeDel: float = 0.00,
            nbConnActivationsDomain: Sequence[int] = [2, 8],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [2, 14],
            eta: float = 20.,
            mutationPb: float = 0.8,
            init_nb_muts_domain: Sequence[int] = [0, 20],
            max_bins_before_non_trivial_found: int = 20,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.probParametersMutation = probParametersMutation
        self.probTemplateActivationAdd = probTemplateActivationAdd
        self.probTemplateActivationDel = probTemplateActivationDel
        self.probTemplateInhibitionAdd = probTemplateInhibitionAdd
        self.probTemplateInhibitionDel = probTemplateInhibitionDel
        self.probNodeAdd = probNodeAdd
        self.probNodeDel = probNodeDel
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.eta = eta
        self.mutationPb = mutationPb
        self.init_nb_muts_domain = init_nb_muts_domain
        self.max_bins_before_non_trivial_found = max_bins_before_non_trivial_found

        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=gen_daccad_individuals(self.ind_domain), **kwargs)


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise2 = (self.container.best_fitness is None or self.container.best_fitness[0] <= 0.) and self.container.size > self.max_bins_before_non_trivial_found
        if initialise2:
            initialise = False
        else:
            initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
            if not initialise:
                operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
                initialise = operation == 1

        if initialise: # Initialisation
            # Define init mutation probabilities
            init_param_mut_pb    = 0.
            init_act_add_mut_pb  = 1./5.
            init_act_del_mut_pb  = 1./5.
            init_inh_add_mut_pb  = 1./5.
            init_inh_del_mut_pb  = 1./5.
            init_node_add_mut_pb = 1./5.
            init_node_del_mut_pb = 0.

            for _ in range(500): # XXX
                # Create a base individual
                standard_init_ind(base_ind)

                # Apply N topological mutations
                nb_mutations = np.random.randint(self.init_nb_muts_domain[0], self.init_nb_muts_domain[1])
                for _ in range(nb_mutations):
                    base_ind = self._vary_with_pb(base_ind, init_param_mut_pb,
                            init_act_add_mut_pb, init_act_del_mut_pb,
                            init_inh_add_mut_pb, init_inh_del_mut_pb,
                            init_node_add_mut_pb, init_node_del_mut_pb)

                # Verify if valid
                if base_ind.is_valid():
                    break
                else:
                    print("INIT: NOT VALID !")
                    raise RuntimeError("INIT: NOT VALID !")
            #print(sum(base_ind.activations.flatten()>0.), sum(base_ind.inhibitions.flatten()>0.))
            return base_ind, False

        else: # Selection
            try:
                res = tools.non_trivial_sel_random(collection), True
                #choice2 = np.random.choice(2)
                #if choice2 == 0:
                #    res = tools.non_trivial_sel_random(collection), True
                #else:
                #    res = tools.sel_random(collection), True
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()
            return res


    def _vary_with_pb(self, individual, param_mut_pb, act_add_mut_pb, act_del_mut_pb, inh_add_mut_pb, inh_del_mut_pb, node_add_mut_pb, node_del_mut_pb):
        for _ in range(5000): # XXX
            ind = copy.deepcopy(individual)
            ind.name = str(id(ind))

            active_activations_coords = list(zip(*np.where(ind.activations)))
            nb_active_activations = len(active_activations_coords)
            active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
            nb_active_inhibitions = len(active_inhibitions_coords)

            # Choose the kind of mutation
            choice = np.random.choice(7, p=(param_mut_pb, act_add_mut_pb, act_del_mut_pb, inh_add_mut_pb, inh_del_mut_pb, node_add_mut_pb, node_del_mut_pb))
            # Verify that the add and del are allowed
            if choice == 1 and (nb_active_activations >= self.nbConnActivationsDomain[1] or nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[1]):
                choice = 2 # Transform into del
            elif choice == 2 and (nb_active_activations <= self.nbConnActivationsDomain[0] or nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[0]):
                choice = 1 # Transform into add
            elif choice == 3 and (nb_active_inhibitions >= self.nbConnInhibitionsDomain[1] or nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[1]):
                choice = 4 # Transform into del
            elif choice == 4 and (nb_active_inhibitions <= self.nbConnInhibitionsDomain[0] or nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[0]):
                choice = 3 # Transform into add
            elif choice == 5 and ind.nb_nodes >= self.nb_nodes_domain[1]:
                choice = 6 # Transform into del
            elif choice == 6 and ind.nb_nodes <= self.nb_nodes_domain[0]:
                choice = 5 # Transform into add

            # Perform the mutation
            if choice == 0: # Parameters mutation
                mutation_param_polybounded(ind, self.eta, self.mutationPb)
                #mutation_param_bioneat(ind, self.f1, self.f2, 1)
            elif choice == 1: # Add Activation
                mutation_add_activation(ind)
            elif choice == 2: # Del Activation
                mutation_del_activation(ind)
            elif choice == 3: # Add Inhibition
                mutation_add_inhibition(ind)
            elif choice == 4: # Del Inhibition
                mutation_del_inhibition(ind)
            elif choice == 5: # Add node
                mutation_add_node(ind)
            elif choice == 6: # Del node
                mutation_del_node(ind)


            # Verify if there are no orphaned nodes
            is_orphaned = (not(any(ind.activations[:,i]) or any(ind.activations[i,:]) or any(ind.inhibitions[i,:,:].flatten())) for i in range(ind.nb_nodes))
            if any(is_orphaned):
                continue

            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                active_activations_coords = list(zip(*np.where(ind.activations)))
                nb_active_activations = len(active_activations_coords)
                active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
                nb_active_inhibitions = len(active_inhibitions_coords)
                if nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        return ind


    def _vary(self, individual: Any) -> Any:
        if (self.container.best_fitness is None or self.container.best_fitness[0] <= 0.) and self.container.size > self.max_bins_before_non_trivial_found:
            return self._vary_with_pb(individual, 1.0,
                    0., 0.,
                    0., 0.,
                    0., 0.)
        else:
            return self._vary_with_pb(individual, self.probParametersMutation,
                    self.probTemplateActivationAdd, self.probTemplateActivationDel,
                    self.probTemplateInhibitionAdd, self.probTemplateInhibitionDel,
                    self.probNodeAdd, self.probNodeDel)




@registry.register
class DaccadTopologicalInitBioneat3(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [3, 7],
            min_init_budget: int = 500,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            prob_parameter_mut: float = 0.96,
            prob_disable_template: float = 0.01,
            prob_activation_add: float = 0.01,
            prob_inhibition_add: float = 0.01,
            prob_node_add: float = 0.01,
            nbConnActivationsDomain: Sequence[int] = [2, 8],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [2, 14],
            f1: float = 0.2,
            f2: float = 2.0,
            mut_pb: float = 0.8,
            init_nb_muts_domain: Sequence[int] = [0, 5],
            allow_gd: bool = False,
            gd_pb: float = 1.0,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.prob_parameter_mut = prob_parameter_mut
        self.prob_disable_template = prob_disable_template
        self.prob_activation_add = prob_activation_add
        self.prob_inhibition_add = prob_inhibition_add
        self.prob_node_add = prob_node_add
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.f1 = f1
        self.f2 = f2
        self.mut_pb = mut_pb
        self.init_nb_muts_domain = init_nb_muts_domain

        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=gen_daccad_individuals(self.ind_domain), **kwargs)


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        if initialise: # Initialisation
            # Define init mutation probabilities
            init_prob_parameter_mut     = 0.
            init_prob_disable_template  = 1./4.
            init_prob_activation_add    = 1./4.
            init_prob_inhibition_add    = 1./4.
            init_prob_node_add          = 1./4.

            for _ in range(5000): # XXX
                ind = copy.deepcopy(base_ind)
                #ind.name = str(id(ind))

                # Create a base individual
                standard_init_ind(ind)

                # Apply N topological mutations
                nb_mutations = np.random.randint(self.init_nb_muts_domain[0], self.init_nb_muts_domain[1]) if self.init_nb_muts_domain[0] != self.init_nb_muts_domain[1] else self.init_nb_muts_domain[0]
                for _ in range(nb_mutations):
                    ind = self._vary_with_pb(ind,
                        init_prob_parameter_mut,
                        init_prob_disable_template,
                        init_prob_activation_add,
                        init_prob_inhibition_add,
                        init_prob_node_add,
                        True)

                    # Apply 1 parameter mutation each time
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                    ind.assemble()


                # Verify if valid
                if ind.is_valid():
                    # Verify if within bounds
                    activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                    nb_active_activations = len(activeConnActivationsIndexes)
                    activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                    nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                    # Verify if there are templates from gradients
                    possess_templates_from_gradients = False
                    gradients_id = [0, 1]
                    for g in gradients_id:
                        possess_templates_from_gradients |= np.any(ind.activations[g,:] >= 0.005)

                    if possess_templates_from_gradients and ind.nb_nodes >= self.nb_nodes_domain[0] and ind.nb_nodes <= self.nb_nodes_domain[1] and nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                        break
                else:
                    print("INIT: NOT VALID !")
                    raise RuntimeError("INIT: NOT VALID !")
            ind.name = str(hash(ind) ** 2)
            return ind, False

        else: # Selection
            try:
                choice = np.random.choice(2, p=(0.70, 0.30))
                if choice == 0:
                    res = tools.non_trivial_sel_random(collection), True
                else:
                    res = tools.sel_random(collection), True
                res = tools.non_trivial_sel_random(collection), True
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()
            return res



    def _vary_with_pb(self, individual, prob_parameter_mut, prob_disable_template, prob_activation_add, prob_inhibition_add, prob_node_add, weak_validation = False):
        for _ in range(5000): # XXX
            ind = copy.deepcopy(individual)
            #ind.name = str(id(ind))

            # Choose the kind of mutation
            choice = np.random.choice(5, p=(prob_parameter_mut, prob_disable_template, prob_activation_add, prob_inhibition_add, prob_node_add))

            # Perform the mutation
            try:
                if choice == 0: # Parameters mutation
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                elif choice == 1: # Disable template
                    disable_template(ind)
                elif choice == 2: # Add Activation
                    add_activation_with_gradients(ind)
                elif choice == 3: # Add inhibition
                    add_inhibition_with_gradients(ind)
                elif choice == 4: # Add node
                    add_node_with_gradients(ind)
            except Exception as e:
                print(f"VARY: EXCEPTION (choice={choice}): {e}")
                continue

            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                nb_active_activations = len(activeConnActivationsIndexes)
                activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                # Verify if there are templates from gradients
                possess_templates_from_gradients = False
                gradients_id = [0, 1]
                for g in gradients_id:
                    possess_templates_from_gradients |= np.any(ind.activations[g,:] >= 0.005)

                if possess_templates_from_gradients and weak_validation:
                    break
                elif possess_templates_from_gradients and ind.nb_nodes >= self.nb_nodes_domain[0] and ind.nb_nodes <= self.nb_nodes_domain[1] and nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        ind.name = str(hash(ind) ** 2)
        return ind


    def _vary(self, individual: Any) -> Any:
        return self._vary_with_pb(individual,
                self.prob_parameter_mut,
                self.prob_disable_template,
                self.prob_activation_add,
                self.prob_inhibition_add,
                self.prob_node_add)




@registry.register
class DaccadTopologicalInitBioneat4(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [3, 7],
            min_init_budget: int = 500,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            prob_parameter_mut: float = 0.96,
            prob_disable_template: float = 0.01,
            prob_activation_add: float = 0.01,
            prob_inhibition_add: float = 0.01,
            prob_node_add: float = 0.01,
            nbConnActivationsDomain: Sequence[int] = [2, 8],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [2, 14],
            f1: float = 0.2,
            f2: float = 2.0,
            mut_pb: float = 0.8,
            #init_nb_muts_domain: Sequence[int] = [0, 5],
            init_nb_param_muts_domain: Sequence[int] = [1, 3],
            allow_gd: bool = False,
            gd_pb: float = 1.0,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.prob_parameter_mut = prob_parameter_mut
        self.prob_disable_template = prob_disable_template
        self.prob_activation_add = prob_activation_add
        self.prob_inhibition_add = prob_inhibition_add
        self.prob_node_add = prob_node_add
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.f1 = f1
        self.f2 = f2
        self.mut_pb = mut_pb
        self.init_nb_param_muts_domain = init_nb_param_muts_domain

        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=gen_daccad_individuals(self.ind_domain), **kwargs)


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        if initialise: # Initialisation
            for _ in range(5000): # XXX
                ind = copy.deepcopy(base_ind)
                #ind.name = str(id(ind))

                # Create a base individual
                standard_init_ind(ind)

                # Select a target number of templates
                target_nb_templates = np.random.randint(self.nbConnDomain[0], self.nbConnDomain[1])

                # Apply topological mutations until individual reach chosen number of templates
                for _ in range(500): # XXX
                    nb_active_activations = len(np.where(ind.activations >= 0.005)[0])
                    nb_active_inhibitions = len(np.where(ind.inhibitions >= 0.005)[0])
                    if nb_active_activations + nb_active_inhibitions == target_nb_templates:
                        break
                    elif nb_active_activations + nb_active_inhibitions > target_nb_templates:
                        ind = self._vary_with_pb(ind,
                            0.,
                            1.0,
                            0.,
                            0.,
                            0.,
                            True)
                    else:
                        ind = self._vary_with_pb(ind,
                            0.,
                            0.,
                            1./3.,
                            1./3.,
                            1./3.,
                            True)

                # Apply a random number of parameter mutations
                nb_mut_param = np.random.randint(self.init_nb_param_muts_domain[0], self.init_nb_param_muts_domain[1])
                for _ in range(nb_mut_param):
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                ind.assemble()


                # Verify if valid
                if ind.is_valid():
                    # Verify if within bounds
                    activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                    nb_active_activations = len(activeConnActivationsIndexes)
                    activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                    nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                    # Verify if there are templates from gradients
                    possess_templates_from_gradients = False
                    gradients_id = [0, 1]
                    for g in gradients_id:
                        possess_templates_from_gradients |= np.any(ind.activations[g,:] >= 0.005)

                    if possess_templates_from_gradients and ind.nb_nodes >= self.nb_nodes_domain[0] and ind.nb_nodes <= self.nb_nodes_domain[1] and nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                        break
                else:
                    print("INIT: NOT VALID !")
                    raise RuntimeError("INIT: NOT VALID !")
            ind.name = str(hash(ind) ** 2)
            return ind, False

        else: # Selection
            try:
                choice = np.random.choice(2, p=(0.70, 0.30))
                if choice == 0:
                    res = tools.non_trivial_sel_random(collection), True
                else:
                    res = tools.sel_random(collection), True
                res = tools.non_trivial_sel_random(collection), True
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()
            return res



    def _vary_with_pb(self, individual, prob_parameter_mut, prob_disable_template, prob_activation_add, prob_inhibition_add, prob_node_add, weak_validation = False):
        for _ in range(5000): # XXX
            ind = copy.deepcopy(individual)
            #ind.name = str(id(ind))

            # Choose the kind of mutation
            choice = np.random.choice(5, p=(prob_parameter_mut, prob_disable_template, prob_activation_add, prob_inhibition_add, prob_node_add))

            # Perform the mutation
            try:
                if choice == 0: # Parameters mutation
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                elif choice == 1: # Disable template
                    disable_template(ind)
                elif choice == 2: # Add Activation
                    add_activation_with_gradients(ind)
                elif choice == 3: # Add inhibition
                    add_inhibition_with_gradients(ind)
                elif choice == 4: # Add node
                    add_node_with_gradients(ind)
            except Exception as e:
                print(f"VARY: EXCEPTION (choice={choice}): {e}")
                continue

            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                nb_active_activations = len(activeConnActivationsIndexes)
                activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                # Verify if there are templates from gradients
                possess_templates_from_gradients = False
                gradients_id = [0, 1]
                for g in gradients_id:
                    possess_templates_from_gradients |= np.any(ind.activations[g,:] >= 0.005)

                if possess_templates_from_gradients and weak_validation:
                    break
                elif possess_templates_from_gradients and ind.nb_nodes >= self.nb_nodes_domain[0] and ind.nb_nodes <= self.nb_nodes_domain[1] and nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        ind.name = str(hash(ind) ** 2)
        return ind


    def _vary(self, individual: Any) -> Any:
        return self._vary_with_pb(individual,
                self.prob_parameter_mut,
                self.prob_disable_template,
                self.prob_activation_add,
                self.prob_inhibition_add,
                self.prob_node_add)





@registry.register
class DaccadTopologicalInitBioneat5(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [3, 7],
            min_init_budget: int = 500,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            prob_parameter_mut: float = 0.96,
            prob_disable_template: float = 0.01,
            prob_activation_add: float = 0.01,
            prob_inhibition_add: float = 0.01,
            prob_node_add: float = 0.01,
            nbConnActivationsDomain: Sequence[int] = [2, 8],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [2, 14],
            f1: float = 0.2,
            f2: float = 2.0,
            mut_pb: float = 0.8,
            #init_nb_muts_domain: Sequence[int] = [0, 5],
            init_nb_param_muts_domain: Sequence[int] = [1, 3],
            epsilon_domain: Sequence[float] = [0.05, 0.70],
            allow_gd: bool = False,
            gd_pb: float = 1.0,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.prob_parameter_mut = prob_parameter_mut
        self.prob_disable_template = prob_disable_template
        self.prob_activation_add = prob_activation_add
        self.prob_inhibition_add = prob_inhibition_add
        self.prob_node_add = prob_node_add
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.f1 = f1
        self.f2 = f2
        self.mut_pb = mut_pb
        self.init_nb_param_muts_domain = init_nb_param_muts_domain

        self.epsilon = np.random.uniform(epsilon_domain[0], epsilon_domain[1]) if epsilon_domain[0] != epsilon_domain[1] else epsilon_domain[0]
        print(f"Using epsilon={self.epsilon}")

        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=gen_daccad_individuals(self.ind_domain), **kwargs)


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        if initialise: # Initialisation
            for _ in range(5000): # XXX
                ind = copy.deepcopy(base_ind)
                #ind.name = str(id(ind))

                # Create a base individual
                standard_init_ind(ind)

                # Select a target number of templates
                target_nb_templates = np.random.randint(self.nbConnDomain[0], self.nbConnDomain[1])

                # Apply topological mutations until individual reach chosen number of templates
                for _ in range(500): # XXX
                    nb_active_activations = len(np.where(ind.activations >= 0.005)[0])
                    nb_active_inhibitions = len(np.where(ind.inhibitions >= 0.005)[0])
                    if nb_active_activations + nb_active_inhibitions == target_nb_templates:
                        break
                    elif nb_active_activations + nb_active_inhibitions > target_nb_templates:
                        ind = self._vary_with_pb(ind,
                            0.,
                            1.0,
                            0.,
                            0.,
                            0.,
                            True)
                    else:
                        ind = self._vary_with_pb(ind,
                            0.,
                            1./4.,
                            1./4.,
                            1./4.,
                            1./4.,
                            True)

                # Apply a random number of parameter mutations
                nb_mut_param = np.random.randint(self.init_nb_param_muts_domain[0], self.init_nb_param_muts_domain[1])
                for _ in range(nb_mut_param):
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                ind.assemble()


                # Verify if valid
                if ind.is_valid():
                    # Verify if within bounds
                    activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                    nb_active_activations = len(activeConnActivationsIndexes)
                    activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                    nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                    # Verify if there are templates from gradients
                    possess_templates_from_gradients = False
                    gradients_id = [0, 1]
                    for g in gradients_id:
                        possess_templates_from_gradients |= np.any(ind.activations[g,:] >= 0.005)

                    if possess_templates_from_gradients and ind.nb_nodes >= self.nb_nodes_domain[0] and ind.nb_nodes <= self.nb_nodes_domain[1] and nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                        break
                else:
                    print("INIT: NOT VALID !")
                    raise RuntimeError("INIT: NOT VALID !")
            ind.name = str(hash(ind) ** 2)
            return ind, False

        else: # Selection
            try:
                choice = np.random.random() > self.epsilon
                if choice:
                    res = collection.best, True
                else:
                    choice2 = np.random.choice(2, p=(0.70, 0.30))
                    if choice2 == 0:
                        res = tools.non_trivial_sel_random(collection), True
                    else:
                        res = tools.sel_random(collection), True
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()
            return res



    def _vary_with_pb(self, individual, prob_parameter_mut, prob_disable_template, prob_activation_add, prob_inhibition_add, prob_node_add, weak_validation = False):
        for _ in range(5000): # XXX
            ind = copy.deepcopy(individual)
            #ind.name = str(id(ind))

            # Choose the kind of mutation
            choice = np.random.choice(5, p=(prob_parameter_mut, prob_disable_template, prob_activation_add, prob_inhibition_add, prob_node_add))

            # Perform the mutation
            try:
                if choice == 0: # Parameters mutation
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                elif choice == 1: # Disable template
                    disable_template(ind)
                elif choice == 2: # Add Activation
                    add_activation_with_gradients(ind)
                elif choice == 3: # Add inhibition
                    add_inhibition_with_gradients(ind)
                elif choice == 4: # Add node
                    add_node_with_gradients(ind)
            except Exception as e:
                print(f"VARY: EXCEPTION (choice={choice}): {e}")
                continue

            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                nb_active_activations = len(activeConnActivationsIndexes)
                activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                # Verify if there are templates from gradients
                possess_templates_from_gradients = False
                gradients_id = [0, 1]
                for g in gradients_id:
                    possess_templates_from_gradients |= np.any(ind.activations[g,:] >= 0.005)

                if possess_templates_from_gradients and weak_validation:
                    break
                elif possess_templates_from_gradients and ind.nb_nodes >= self.nb_nodes_domain[0] and ind.nb_nodes <= self.nb_nodes_domain[1] and nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        ind.name = str(hash(ind) ** 2)
        return ind


    def _vary(self, individual: Any) -> Any:
        return self._vary_with_pb(individual,
                self.prob_parameter_mut,
                self.prob_disable_template,
                self.prob_activation_add,
                self.prob_inhibition_add,
                self.prob_node_add)





@registry.register
class DaccadTopologicalInitBioneat6(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [3, 7],
            min_init_budget: int = 500,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            prob_parameter_mut: float = 0.96,
            prob_disable_template: float = 0.01,
            prob_activation_add: float = 0.01,
            prob_inhibition_add: float = 0.01,
            prob_node_add: float = 0.01,
            nbConnActivationsDomain: Sequence[int] = [2, 8],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [2, 14],
            f1: float = 0.2,
            f2: float = 2.0,
            mut_pb: float = 0.8,
            #init_nb_muts_domain: Sequence[int] = [0, 5],
            init_nb_param_muts_domain: Sequence[int] = [1, 3],
            epsilon_domain: Sequence[float] = [0.05, 0.70],
            allow_gd: bool = False,
            gd_pb: float = 1.0,
            grad4: bool = False,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.prob_parameter_mut = prob_parameter_mut
        self.prob_disable_template = prob_disable_template
        self.prob_activation_add = prob_activation_add
        self.prob_inhibition_add = prob_inhibition_add
        self.prob_node_add = prob_node_add
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.f1 = f1
        self.f2 = f2
        self.mut_pb = mut_pb
        self.init_nb_param_muts_domain = init_nb_param_muts_domain
        self.epsilon_domain = epsilon_domain
        self.grad4 = grad4

        self.epsilon = np.random.uniform(self.epsilon_domain[0], self.epsilon_domain[1]) if self.epsilon_domain[0] != self.epsilon_domain[1] else self.epsilon_domain[0]
        print(f"Using epsilon={self.epsilon}")

        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=gen_daccad_individuals(self.ind_domain), **kwargs)


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        choice = np.random.choice(2, p=(0.99, 0.01))
        if choice == 1:
            self.epsilon = np.random.uniform(self.epsilon_domain[0], self.epsilon_domain[1]) if self.epsilon_domain[0] != self.epsilon_domain[1] else self.epsilon_domain[0]
            print(f"Using epsilon={self.epsilon}")

        if initialise: # Initialisation
            gradients_id = [0,1,2,3] if self.grad4 else [0,1]
            for _ in range(5000): # XXX
                ind = copy.deepcopy(base_ind)
                #ind.name = str(id(ind))

                # Create a base individual
                if self.grad4:
                    standard_init_ind_grad4(ind)
                else:
                    standard_init_ind(ind)

                # Select a target number of templates
                target_nb_templates = np.random.randint(self.nbConnDomain[0], self.nbConnDomain[1])

                # Apply topological mutations until individual reach chosen number of templates
                for _ in range(500): # XXX
                    nb_active_activations = len(np.where(ind.activations >= 0.005)[0])
                    nb_active_inhibitions = len(np.where(ind.inhibitions >= 0.005)[0])
                    if nb_active_activations + nb_active_inhibitions == target_nb_templates:
                        break
                    elif nb_active_activations + nb_active_inhibitions > target_nb_templates:
                        ind = self._vary_with_pb(ind,
                            0.,
                            1.0,
                            0.,
                            0.,
                            0.,
                            True)
                    else:
                        ind = self._vary_with_pb(ind,
                            0.,
                            1./4.,
                            1./4.,
                            1./4.,
                            1./4.,
                            True)

                # Apply a random number of parameter mutations
                nb_mut_param = np.random.randint(self.init_nb_param_muts_domain[0], self.init_nb_param_muts_domain[1])
                for _ in range(nb_mut_param):
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                ind.assemble()


                # Verify if valid
                if ind.is_valid():
                    # Verify if within bounds
                    activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                    nb_active_activations = len(activeConnActivationsIndexes)
                    activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                    nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                    # Verify if there are templates from gradients
                    possess_templates_from_gradients = False
                    for g in gradients_id:
                        possess_templates_from_gradients |= np.any(ind.activations[g,:] >= 0.005)

                    if possess_templates_from_gradients and ind.nb_nodes >= self.nb_nodes_domain[0] and ind.nb_nodes <= self.nb_nodes_domain[1] and nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                        break
                else:
                    print("INIT: NOT VALID !")
                    raise RuntimeError("INIT: NOT VALID !")
            ind.name = str(hash(ind) ** 2)
            return ind, False

        else: # Selection
            try:
                choice = np.random.random() > self.epsilon
                if choice:
                    res = collection.best, True
                else:
                    choice2 = np.random.choice(2, p=(0.70, 0.30))
                    if choice2 == 0:
                        res = tools.non_trivial_sel_random(collection), True
                    else:
                        res = tools.sel_random(collection), True
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()
            return res



    def _vary_with_pb(self, individual, prob_parameter_mut, prob_disable_template, prob_activation_add, prob_inhibition_add, prob_node_add, weak_validation = False):
        gradients_id = [0,1,2,3] if self.grad4 else [0,1]

        for _ in range(5000): # XXX
            ind = copy.deepcopy(individual)
            #ind.name = str(id(ind))

            # Choose the kind of mutation
            choice = np.random.choice(5, p=(prob_parameter_mut, prob_disable_template, prob_activation_add, prob_inhibition_add, prob_node_add))

            # Perform the mutation
            try:
                if choice == 0: # Parameters mutation
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                elif choice == 1: # Disable template
                    disable_template(ind)
                elif choice == 2: # Add Activation
                    add_activation_with_gradients(ind, gradients=gradients_id)
                elif choice == 3: # Add inhibition
                    add_inhibition_with_gradients(ind, gradients=gradients_id)
                elif choice == 4: # Add node
                    add_node_with_gradients(ind, gradients=gradients_id)
            except Exception as e:
                print(f"VARY: EXCEPTION (choice={choice}): {e}")
                continue

            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                nb_active_activations = len(activeConnActivationsIndexes)
                activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                # Verify if there are templates from gradients
                possess_templates_from_gradients = False
                for g in gradients_id:
                    possess_templates_from_gradients |= np.any(ind.activations[g,:] >= 0.005)

                if possess_templates_from_gradients and weak_validation:
                    break
                elif possess_templates_from_gradients and ind.nb_nodes >= self.nb_nodes_domain[0] and ind.nb_nodes <= self.nb_nodes_domain[1] and nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        ind.name = str(hash(ind) ** 2)
        return ind


    def _vary(self, individual: Any) -> Any:
        return self._vary_with_pb(individual,
                self.prob_parameter_mut,
                self.prob_disable_template,
                self.prob_activation_add,
                self.prob_inhibition_add,
                self.prob_node_add)




@registry.register
class DaccadTopologicalInitBioneat6Clones(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [3, 7],
            min_init_budget: int = 500,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            prob_parameter_mut: float = 0.1666667,
            prob_disable_template: float = 0.1666667,
            prob_activation_add: float = 0.1666667,
            prob_inhibition_add: float = 0.1666667,
            prob_node_add: float = 0.1666667,
            prob_clone: float = 0.1666667,
            nbConnActivationsDomain: Sequence[int] = [2, 8],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [2, 14],
            f1: float = 0.2,
            f2: float = 2.0,
            mut_pb: float = 0.8,
            #init_nb_muts_domain: Sequence[int] = [0, 5],
            init_nb_param_muts_domain: Sequence[int] = [1, 3],
            epsilon_domain: Sequence[float] = [0.05, 0.70],
            allow_gd: bool = False,
            gd_pb: float = 1.0,
            grad4: bool = False,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.prob_parameter_mut = prob_parameter_mut
        self.prob_disable_template = prob_disable_template
        self.prob_activation_add = prob_activation_add
        self.prob_inhibition_add = prob_inhibition_add
        self.prob_node_add = prob_node_add
        self.prob_clone = prob_clone
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.f1 = f1
        self.f2 = f2
        self.mut_pb = mut_pb
        self.init_nb_param_muts_domain = init_nb_param_muts_domain
        self.epsilon_domain = epsilon_domain
        self.grad4 = grad4

        self.epsilon = np.random.uniform(self.epsilon_domain[0], self.epsilon_domain[1]) if self.epsilon_domain[0] != self.epsilon_domain[1] else self.epsilon_domain[0]
        print(f"Using epsilon={self.epsilon}")

        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=gen_daccad_individuals(self.ind_domain), **kwargs)


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        choice = np.random.choice(2, p=(0.99, 0.01))
        if choice == 1:
            self.epsilon = np.random.uniform(self.epsilon_domain[0], self.epsilon_domain[1]) if self.epsilon_domain[0] != self.epsilon_domain[1] else self.epsilon_domain[0]
            print(f"Using epsilon={self.epsilon}")

        if initialise: # Initialisation
            gradients_id = [0,1,2,3] if self.grad4 else [0,1]
            for _ in range(5000): # XXX
                ind = copy.deepcopy(base_ind)
                #ind.name = str(id(ind))

                # Create a base individual
                if self.grad4:
                    standard_init_ind_grad4(ind)
                else:
                    standard_init_ind(ind)

                # Select a target number of templates
                target_nb_templates = np.random.randint(self.nbConnDomain[0], self.nbConnDomain[1])

                # Apply topological mutations until individual reach chosen number of templates
                for _ in range(500): # XXX
                    nb_active_activations = len(np.where(ind.activations >= 0.005)[0])
                    nb_active_inhibitions = len(np.where(ind.inhibitions >= 0.005)[0])
                    if nb_active_activations + nb_active_inhibitions == target_nb_templates:
                        break
                    elif nb_active_activations + nb_active_inhibitions > target_nb_templates:
                        ind = self._vary_with_pb(ind,
                            0.,
                            1.0,
                            0.,
                            0.,
                            0.,
                            0.,
                            True)
                    else:
                        ind = self._vary_with_pb(ind,
                            0.,
                            1./5.,
                            1./5.,
                            1./5.,
                            1./5.,
                            1./5.,
                            True)

                # Apply a random number of parameter mutations
                nb_mut_param = np.random.randint(self.init_nb_param_muts_domain[0], self.init_nb_param_muts_domain[1])
                for _ in range(nb_mut_param):
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                ind.assemble()


                # Verify if valid
                if ind.is_valid():
                    # Verify if within bounds
                    activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                    nb_active_activations = len(activeConnActivationsIndexes)
                    activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                    nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                    # Verify if there are templates from gradients
                    possess_templates_from_gradients = False
                    for g in gradients_id:
                        possess_templates_from_gradients |= np.any(ind.activations[g,:] >= 0.005)

                    if possess_templates_from_gradients and ind.nb_nodes >= self.nb_nodes_domain[0] and ind.nb_nodes <= self.nb_nodes_domain[1] and nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                        break
                else:
                    print("INIT: NOT VALID !")
                    raise RuntimeError("INIT: NOT VALID !")
            ind.name = str(hash(ind) ** 2)
            return ind, False

        else: # Selection
            try:
                choice = np.random.random() > self.epsilon
                if choice:
                    res = collection.best, True
                else:
                    choice2 = np.random.choice(2, p=(0.70, 0.30))
                    if choice2 == 0:
                        res = tools.non_trivial_sel_random(collection), True
                    else:
                        res = tools.sel_random(collection), True
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()
            return res



    def _vary_with_pb(self, individual, prob_parameter_mut, prob_disable_template, prob_activation_add, prob_inhibition_add, prob_node_add, prob_clone, weak_validation = False):
        gradients_id = [0,1,2,3] if self.grad4 else [0,1]

        for _ in range(5000): # XXX
            ind = copy.deepcopy(individual)
            #ind.name = str(id(ind))

            # Choose the kind of mutation
            choice = np.random.choice(6, p=(prob_parameter_mut, prob_disable_template, prob_activation_add, prob_inhibition_add, prob_node_add, prob_clone))

            # Perform the mutation
            try:
                if choice == 0: # Parameters mutation
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                elif choice == 1: # Disable template
                    disable_template(ind)
                elif choice == 2: # Add Activation
                    add_activation_with_gradients(ind, gradients=gradients_id)
                elif choice == 3: # Add inhibition
                    add_inhibition_with_gradients(ind, gradients=gradients_id)
                elif choice == 4: # Add node
                    add_node_with_gradients(ind, gradients=gradients_id)
                elif choice == 5: # Clone template
                    mutation_clone_template(ind)
            except Exception as e:
                print(f"VARY: EXCEPTION (choice={choice}): {e}")
                continue

            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                nb_active_activations = len(activeConnActivationsIndexes)
                activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                # Verify if there are templates from gradients
                possess_templates_from_gradients = False
                for g in gradients_id:
                    possess_templates_from_gradients |= np.any(ind.activations[g,:] >= 0.005)

                if possess_templates_from_gradients and weak_validation:
                    break
                elif possess_templates_from_gradients and ind.nb_nodes >= self.nb_nodes_domain[0] and ind.nb_nodes <= self.nb_nodes_domain[1] and nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        ind.name = str(hash(ind) ** 2)
        return ind


    def _vary(self, individual: Any) -> Any:
        return self._vary_with_pb(individual,
                self.prob_parameter_mut,
                self.prob_disable_template,
                self.prob_activation_add,
                self.prob_inhibition_add,
                self.prob_node_add,
                self.prob_clone)








@registry.register
class DaccadTopologicalInitBioneat6Grad4(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [3, 7],
            min_init_budget: int = 500,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            prob_parameter_mut: float = 0.96,
            prob_disable_template: float = 0.01,
            prob_activation_add: float = 0.01,
            prob_inhibition_add: float = 0.01,
            prob_node_add: float = 0.01,
            nbConnActivationsDomain: Sequence[int] = [2, 8],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [2, 14],
            f1: float = 0.2,
            f2: float = 2.0,
            mut_pb: float = 0.8,
            #init_nb_muts_domain: Sequence[int] = [0, 5],
            init_nb_param_muts_domain: Sequence[int] = [1, 3],
            epsilon_domain: Sequence[float] = [0.05, 0.70],
            allow_gd: bool = False,
            gd_pb: float = 1.0,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.prob_parameter_mut = prob_parameter_mut
        self.prob_disable_template = prob_disable_template
        self.prob_activation_add = prob_activation_add
        self.prob_inhibition_add = prob_inhibition_add
        self.prob_node_add = prob_node_add
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.f1 = f1
        self.f2 = f2
        self.mut_pb = mut_pb
        self.init_nb_param_muts_domain = init_nb_param_muts_domain
        self.epsilon_domain = epsilon_domain

        self.epsilon = np.random.uniform(self.epsilon_domain[0], self.epsilon_domain[1]) if self.epsilon_domain[0] != self.epsilon_domain[1] else self.epsilon_domain[0]
        print(f"Using epsilon={self.epsilon}")

        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=gen_daccad_individuals(self.ind_domain), **kwargs)


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        choice = np.random.choice(2, p=(0.99, 0.01))
        if choice == 1:
            self.epsilon = np.random.uniform(self.epsilon_domain[0], self.epsilon_domain[1]) if self.epsilon_domain[0] != self.epsilon_domain[1] else self.epsilon_domain[0]
            print(f"Using epsilon={self.epsilon}")

        if initialise: # Initialisation
            for _ in range(5000): # XXX
                ind = copy.deepcopy(base_ind)
                #ind.name = str(id(ind))

                # Create a base individual
                standard_init_ind_grad4(ind)

                # Select a target number of templates
                target_nb_templates = np.random.randint(self.nbConnDomain[0], self.nbConnDomain[1])

                # Apply topological mutations until individual reach chosen number of templates
                for _ in range(500): # XXX
                    nb_active_activations = len(np.where(ind.activations >= 0.005)[0])
                    nb_active_inhibitions = len(np.where(ind.inhibitions >= 0.005)[0])
                    if nb_active_activations + nb_active_inhibitions == target_nb_templates:
                        break
                    elif nb_active_activations + nb_active_inhibitions > target_nb_templates:
                        ind = self._vary_with_pb(ind,
                            0.,
                            1.0,
                            0.,
                            0.,
                            0.,
                            True)
                    else:
                        ind = self._vary_with_pb(ind,
                            0.,
                            1./4.,
                            1./4.,
                            1./4.,
                            1./4.,
                            True)

                # Apply a random number of parameter mutations
                nb_mut_param = np.random.randint(self.init_nb_param_muts_domain[0], self.init_nb_param_muts_domain[1])
                for _ in range(nb_mut_param):
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                ind.assemble()


                # Verify if valid
                if ind.is_valid():
                    # Verify if within bounds
                    activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                    nb_active_activations = len(activeConnActivationsIndexes)
                    activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                    nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                    # Verify if there are templates from gradients
                    possess_templates_from_gradients = False
                    gradients_id = [0, 1, 2, 3]
                    for g in gradients_id:
                        possess_templates_from_gradients |= np.any(ind.activations[g,:] >= 0.005)

                    if possess_templates_from_gradients and ind.nb_nodes >= self.nb_nodes_domain[0] and ind.nb_nodes <= self.nb_nodes_domain[1] and nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                        break
                else:
                    print("INIT: NOT VALID !")
                    raise RuntimeError("INIT: NOT VALID !")
            ind.name = str(hash(ind) ** 2)
            return ind, False

        else: # Selection
            try:
                choice = np.random.random() > self.epsilon
                if choice:
                    res = collection.best, True
                else:
                    choice2 = np.random.choice(2, p=(0.70, 0.30))
                    if choice2 == 0:
                        res = tools.non_trivial_sel_random(collection), True
                    else:
                        res = tools.sel_random(collection), True
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()
            return res



    def _vary_with_pb(self, individual, prob_parameter_mut, prob_disable_template, prob_activation_add, prob_inhibition_add, prob_node_add, weak_validation = False):
        for _ in range(5000): # XXX
            ind = copy.deepcopy(individual)
            #ind.name = str(id(ind))

            # Choose the kind of mutation
            choice = np.random.choice(5, p=(prob_parameter_mut, prob_disable_template, prob_activation_add, prob_inhibition_add, prob_node_add))

            # Perform the mutation
            try:
                if choice == 0: # Parameters mutation
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                elif choice == 1: # Disable template
                    disable_template(ind)
                elif choice == 2: # Add Activation
                    add_activation_with_gradients(ind, [0,1,2,3])
                elif choice == 3: # Add inhibition
                    add_inhibition_with_gradients(ind, [0,1,2,3])
                elif choice == 4: # Add node
                    add_node_with_gradients(ind, [0,1,2,3])
            except Exception as e:
                print(f"VARY: EXCEPTION (choice={choice}): {e}")
                continue

            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                nb_active_activations = len(activeConnActivationsIndexes)
                activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                # Verify if there are templates from gradients
                possess_templates_from_gradients = False
                gradients_id = [0, 1, 2, 3]
                for g in gradients_id:
                    possess_templates_from_gradients |= np.any(ind.activations[g,:] >= 0.005)

                if possess_templates_from_gradients and weak_validation:
                    break
                elif possess_templates_from_gradients and ind.nb_nodes >= self.nb_nodes_domain[0] and ind.nb_nodes <= self.nb_nodes_domain[1] and nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        ind.name = str(hash(ind) ** 2)
        return ind


    def _vary(self, individual: Any) -> Any:
        return self._vary_with_pb(individual,
                self.prob_parameter_mut,
                self.prob_disable_template,
                self.prob_activation_add,
                self.prob_inhibition_add,
                self.prob_node_add)




@registry.register
class DaccadTopologicalInitBioneat6Transfer(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [3, 7],
            min_init_budget: int = 500,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            prob_parameter_mut: float = 0.96,
            prob_disable_template: float = 0.01,
            prob_activation_add: float = 0.01,
            prob_inhibition_add: float = 0.01,
            prob_node_add: float = 0.01,
            nbConnActivationsDomain: Sequence[int] = [2, 8],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [2, 14],
            f1: float = 0.2,
            f2: float = 2.0,
            mut_pb: float = 0.8,
            #init_nb_muts_domain: Sequence[int] = [0, 5],
            init_nb_param_muts_domain: Sequence[int] = [1, 3],
            epsilon_domain: Sequence[float] = [0.05, 0.70],
            allow_gd: bool = False,
            gd_pb: float = 1.0,
            approx_budget: int = 1000,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.prob_parameter_mut = prob_parameter_mut
        self.prob_disable_template = prob_disable_template
        self.prob_activation_add = prob_activation_add
        self.prob_inhibition_add = prob_inhibition_add
        self.prob_node_add = prob_node_add
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.f1 = f1
        self.f2 = f2
        self.mut_pb = mut_pb
        self.init_nb_param_muts_domain = init_nb_param_muts_domain
        self.epsilon_domain = epsilon_domain
        self.approx_budget = approx_budget
        self.resetted_grid_scores = False

        self.epsilon = np.random.uniform(self.epsilon_domain[0], self.epsilon_domain[1]) if self.epsilon_domain[0] != self.epsilon_domain[1] else self.epsilon_domain[0]
        print(f"Using epsilon={self.epsilon}")

        self.inds_to_transfer = []

        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=gen_daccad_individuals(self.ind_domain), **kwargs)


    def _reset_grid_scores(self):
        print("Emptying the grid !")
        for ind in self.container:
            ind.fitness.setValues([0.])
            #ind.fitness.setValues([0.001])
            self.inds_to_transfer.append(ind)
        #self.best_fitness.setValues([0.])


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        choice = np.random.choice(2, p=(0.99, 0.01))
        if choice == 1:
            self.epsilon = np.random.uniform(self.epsilon_domain[0], self.epsilon_domain[1]) if self.epsilon_domain[0] != self.epsilon_domain[1] else self.epsilon_domain[0]
            print(f"Using epsilon={self.epsilon}")

        if len(self.inds_to_transfer):
            res = self.inds_to_transfer.pop()
            res.name = str(hash(res) ** 2)
            initialise = True

        elif initialise: # Initialisation
            for _ in range(5000): # XXX
                ind = copy.deepcopy(base_ind)
                #ind.name = str(id(ind))

                # Create a base individual
                standard_init_ind(ind)

                # Select a target number of templates
                target_nb_templates = np.random.randint(self.nbConnDomain[0], self.nbConnDomain[1])

                # Apply topological mutations until individual reach chosen number of templates
                for _ in range(500): # XXX
                    nb_active_activations = len(np.where(ind.activations >= 0.005)[0])
                    nb_active_inhibitions = len(np.where(ind.inhibitions >= 0.005)[0])
                    if nb_active_activations + nb_active_inhibitions == target_nb_templates:
                        break
                    elif nb_active_activations + nb_active_inhibitions > target_nb_templates:
                        ind = self._vary_with_pb(ind,
                            0.,
                            1.0,
                            0.,
                            0.,
                            0.,
                            True)
                    else:
                        ind = self._vary_with_pb(ind,
                            0.,
                            1./4.,
                            1./4.,
                            1./4.,
                            1./4.,
                            True)

                # Apply a random number of parameter mutations
                nb_mut_param = np.random.randint(self.init_nb_param_muts_domain[0], self.init_nb_param_muts_domain[1])
                for _ in range(nb_mut_param):
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                ind.assemble()


                # Verify if valid
                if ind.is_valid():
                    # Verify if within bounds
                    activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                    nb_active_activations = len(activeConnActivationsIndexes)
                    activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                    nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                    # Verify if there are templates from gradients
                    possess_templates_from_gradients = False
                    gradients_id = [0, 1]
                    for g in gradients_id:
                        possess_templates_from_gradients |= np.any(ind.activations[g,:] >= 0.005)

                    if possess_templates_from_gradients and ind.nb_nodes >= self.nb_nodes_domain[0] and ind.nb_nodes <= self.nb_nodes_domain[1] and nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                        break
                else:
                    print("INIT: NOT VALID !")
                    raise RuntimeError("INIT: NOT VALID !")
            ind.name = str(hash(ind) ** 2)
            res = ind

        else: # Selection
            try:
                choice = np.random.random() > self.epsilon
                if choice:
                    res = collection.best
                else:
                    choice2 = np.random.choice(2, p=(0.70, 0.30))
                    if choice2 == 0:
                        res = tools.non_trivial_sel_random(collection)
                    else:
                        res = tools.sel_random(collection)
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()


        if self.nb_suggestions <= self.approx_budget:
            res.base_configuration_entry = "approx_daccad"
        else:
            res.base_configuration_entry = "daccad"
            if not self.resetted_grid_scores:
                self.resetted_grid_scores = True
                self._reset_grid_scores()

        return res, not initialise



    def _vary_with_pb(self, individual, prob_parameter_mut, prob_disable_template, prob_activation_add, prob_inhibition_add, prob_node_add, weak_validation = False):
        for _ in range(5000): # XXX
            ind = copy.deepcopy(individual)
            #ind.name = str(id(ind))

            # Choose the kind of mutation
            choice = np.random.choice(5, p=(prob_parameter_mut, prob_disable_template, prob_activation_add, prob_inhibition_add, prob_node_add))

            # Perform the mutation
            try:
                if choice == 0: # Parameters mutation
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                elif choice == 1: # Disable template
                    disable_template(ind)
                elif choice == 2: # Add Activation
                    add_activation_with_gradients(ind)
                elif choice == 3: # Add inhibition
                    add_inhibition_with_gradients(ind)
                elif choice == 4: # Add node
                    add_node_with_gradients(ind)
            except Exception as e:
                print(f"VARY: EXCEPTION (choice={choice}): {e}")
                continue

            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                nb_active_activations = len(activeConnActivationsIndexes)
                activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                # Verify if there are templates from gradients
                possess_templates_from_gradients = False
                gradients_id = [0, 1]
                for g in gradients_id:
                    possess_templates_from_gradients |= np.any(ind.activations[g,:] >= 0.005)

                if possess_templates_from_gradients and weak_validation:
                    break
                elif possess_templates_from_gradients and ind.nb_nodes >= self.nb_nodes_domain[0] and ind.nb_nodes <= self.nb_nodes_domain[1] and nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        ind.name = str(hash(ind) ** 2)
        return ind


    def _vary(self, individual: Any) -> Any:
        return self._vary_with_pb(individual,
                self.prob_parameter_mut,
                self.prob_disable_template,
                self.prob_activation_add,
                self.prob_inhibition_add,
                self.prob_node_add)





@registry.register
class DaccadTopologicalInitBioneat7(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [3, 7],
            min_init_budget: int = 500,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            prob_parameter_mut: float = 0.66,
            prob_crossover: float = 0.30,
            prob_disable_template: float = 0.01,
            prob_activation_add: float = 0.01,
            prob_inhibition_add: float = 0.01,
            prob_node_add: float = 0.01,
            nbConnActivationsDomain: Sequence[int] = [2, 8],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [2, 14],
            f1: float = 0.2,
            f2: float = 2.0,
            mut_pb: float = 0.8,
            cx_pb: float = 0.2,
            #init_nb_muts_domain: Sequence[int] = [0, 5],
            init_nb_param_muts_domain: Sequence[int] = [1, 3],
            epsilon_domain: Sequence[float] = [0.05, 0.70],
            allow_gd: bool = False,
            gd_pb: float = 1.0,
            crossover_type: int = 1,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.prob_parameter_mut = prob_parameter_mut
        self.prob_crossover = prob_crossover
        self.prob_disable_template = prob_disable_template
        self.prob_activation_add = prob_activation_add
        self.prob_inhibition_add = prob_inhibition_add
        self.prob_node_add = prob_node_add
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.f1 = f1
        self.f2 = f2
        self.mut_pb = mut_pb
        self.cx_pb = cx_pb
        self.init_nb_param_muts_domain = init_nb_param_muts_domain
        self.epsilon_domain = epsilon_domain
        self.crossover_type = crossover_type

        self.epsilon = np.random.uniform(self.epsilon_domain[0], self.epsilon_domain[1]) if self.epsilon_domain[0] != self.epsilon_domain[1] else self.epsilon_domain[0]
        print(f"Using epsilon={self.epsilon}")

        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=gen_daccad_individuals(self.ind_domain), **kwargs)


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        choice = np.random.choice(2, p=(0.99, 0.01))
        if choice == 1:
            self.epsilon = np.random.uniform(self.epsilon_domain[0], self.epsilon_domain[1]) if self.epsilon_domain[0] != self.epsilon_domain[1] else self.epsilon_domain[0]
            print(f"Using epsilon={self.epsilon}")

        if initialise: # Initialisation
            for _ in range(5000): # XXX
                ind = copy.deepcopy(base_ind)
                #ind.name = str(id(ind))

                # Create a base individual
                standard_init_ind(ind)

                # Select a target number of templates
                target_nb_templates = np.random.randint(self.nbConnDomain[0], self.nbConnDomain[1])

                # Apply topological mutations until individual reach chosen number of templates
                for _ in range(500): # XXX
                    nb_active_activations = len(np.where(ind.activations >= 0.005)[0])
                    nb_active_inhibitions = len(np.where(ind.inhibitions >= 0.005)[0])
                    if nb_active_activations + nb_active_inhibitions == target_nb_templates:
                        break
                    elif nb_active_activations + nb_active_inhibitions > target_nb_templates:
                        ind = self._vary_with_pb(ind,
                            0.,
                            0.,
                            1.0,
                            0.,
                            0.,
                            0.,
                            True)
                    else:
                        ind = self._vary_with_pb(ind,
                            0.,
                            0.,
                            1./4.,
                            1./4.,
                            1./4.,
                            1./4.,
                            True)

                # Apply a random number of parameter mutations
                nb_mut_param = np.random.randint(self.init_nb_param_muts_domain[0], self.init_nb_param_muts_domain[1])
                for _ in range(nb_mut_param):
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                ind.assemble()


                # Verify if valid
                if ind.is_valid():
                    # Verify if within bounds
                    activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                    nb_active_activations = len(activeConnActivationsIndexes)
                    activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                    nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                    # Verify if there are templates from gradients
                    possess_templates_from_gradients = False
                    gradients_id = [0, 1]
                    for g in gradients_id:
                        possess_templates_from_gradients |= np.any(ind.activations[g,:] >= 0.005)

                    if possess_templates_from_gradients and ind.nb_nodes >= self.nb_nodes_domain[0] and ind.nb_nodes <= self.nb_nodes_domain[1] and nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                        break
                else:
                    print("INIT: NOT VALID !")
                    raise RuntimeError("INIT: NOT VALID !")
            ind.name = str(hash(ind) ** 2)
            return ind, False

        else: # Selection
            try:
                choice = np.random.random() > self.epsilon
                if choice:
                    res = collection.best, True
                else:
                    choice2 = np.random.choice(2, p=(0.70, 0.30))
                    if choice2 == 0:
                        res = tools.non_trivial_sel_random(collection), True
                    else:
                        res = tools.sel_random(collection), True
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()
            return res



    def _vary_with_pb(self, individual, prob_parameter_mut, prob_crossover, prob_disable_template, prob_activation_add, prob_inhibition_add, prob_node_add, weak_validation = False):
        for _ in range(5000): # XXX
            ind = copy.deepcopy(individual)
            #ind.name = str(id(ind))

            # Choose the kind of mutation
            choice = np.random.choice(6, p=(prob_parameter_mut, prob_crossover, prob_disable_template, prob_activation_add, prob_inhibition_add, prob_node_add))

            # Perform the mutation
            try:
                if choice == 0: # Parameters mutation
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                elif choice == 1: # Crossover
                    other_ind = tools.non_trivial_sel_random(self.container)
                    if self.crossover_type == 1:
                        ind, other_ind = crossover_uniform(ind, other_ind, self.cx_pb)
                    elif self.crossover_type == 2:
                        ind, other_ind = crossover_uniform2(ind, other_ind, self.cx_pb)
                elif choice == 2: # Disable template
                    disable_template(ind)
                elif choice == 3: # Add Activation
                    add_activation_with_gradients(ind)
                elif choice == 4: # Add inhibition
                    add_inhibition_with_gradients(ind)
                elif choice == 5: # Add node
                    add_node_with_gradients(ind)
            except NotApplicableError as e:
                continue
            except Exception as e:
                print(f"VARY: EXCEPTION (choice={choice}): {e}")
                continue

            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                nb_active_activations = len(activeConnActivationsIndexes)
                activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                # Verify if there are templates from gradients
                possess_templates_from_gradients = False
                gradients_id = [0, 1]
                for g in gradients_id:
                    possess_templates_from_gradients |= np.any(ind.activations[g,:] >= 0.005)

                if possess_templates_from_gradients and weak_validation:
                    break
                elif possess_templates_from_gradients and ind.nb_nodes >= self.nb_nodes_domain[0] and ind.nb_nodes <= self.nb_nodes_domain[1] and nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        ind.name = str(hash(ind) ** 2)
        return ind


    def _vary(self, individual: Any) -> Any:
        return self._vary_with_pb(individual,
                self.prob_parameter_mut,
                self.prob_crossover,
                self.prob_disable_template,
                self.prob_activation_add,
                self.prob_inhibition_add,
                self.prob_node_add)




@registry.register
class DaccadTopologicalInitBioneat7Transfer(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [3, 7],
            min_init_budget: int = 500,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            prob_parameter_mut: float = 0.66,
            prob_crossover: float = 0.30,
            prob_disable_template: float = 0.01,
            prob_activation_add: float = 0.01,
            prob_inhibition_add: float = 0.01,
            prob_node_add: float = 0.01,
            nbConnActivationsDomain: Sequence[int] = [2, 8],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [2, 14],
            f1: float = 0.2,
            f2: float = 2.0,
            mut_pb: float = 0.8,
            cx_pb: float = 0.2,
            #init_nb_muts_domain: Sequence[int] = [0, 5],
            init_nb_param_muts_domain: Sequence[int] = [1, 3],
            epsilon_domain: Sequence[float] = [0.05, 0.70],
            allow_gd: bool = False,
            gd_pb: float = 1.0,
            crossover_type: int = 1,
            approx_budget: int = 1000,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.prob_parameter_mut = prob_parameter_mut
        self.prob_crossover = prob_crossover
        self.prob_disable_template = prob_disable_template
        self.prob_activation_add = prob_activation_add
        self.prob_inhibition_add = prob_inhibition_add
        self.prob_node_add = prob_node_add
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.f1 = f1
        self.f2 = f2
        self.mut_pb = mut_pb
        self.cx_pb = cx_pb
        self.init_nb_param_muts_domain = init_nb_param_muts_domain
        self.epsilon_domain = epsilon_domain
        self.crossover_type = crossover_type
        self.approx_budget = approx_budget
        self.resetted_grid_scores = False

        self.epsilon = np.random.uniform(self.epsilon_domain[0], self.epsilon_domain[1]) if self.epsilon_domain[0] != self.epsilon_domain[1] else self.epsilon_domain[0]
        print(f"Using epsilon={self.epsilon}")

        self.inds_to_transfer = []

        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=gen_daccad_individuals(self.ind_domain), **kwargs)


    def _reset_grid_scores(self):
        print("Emptying the grid !")
        for ind in self.container:
            ind.fitness.setValues([0.])
            #ind.fitness.setValues([0.001])
            self.inds_to_transfer.append(ind)
        #self.best_fitness.setValues([0.])


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        choice = np.random.choice(2, p=(0.99, 0.01))
        if choice == 1:
            self.epsilon = np.random.uniform(self.epsilon_domain[0], self.epsilon_domain[1]) if self.epsilon_domain[0] != self.epsilon_domain[1] else self.epsilon_domain[0]

        if len(self.inds_to_transfer):
            res = self.inds_to_transfer.pop()
            res.name = str(hash(res) ** 2)
            initialise = True
            print(f"Using epsilon={self.epsilon}")

        if initialise: # Initialisation
            for _ in range(5000): # XXX
                ind = copy.deepcopy(base_ind)
                #ind.name = str(id(ind))

                # Create a base individual
                standard_init_ind(ind)

                # Select a target number of templates
                target_nb_templates = np.random.randint(self.nbConnDomain[0], self.nbConnDomain[1])

                # Apply topological mutations until individual reach chosen number of templates
                for _ in range(500): # XXX
                    nb_active_activations = len(np.where(ind.activations >= 0.005)[0])
                    nb_active_inhibitions = len(np.where(ind.inhibitions >= 0.005)[0])
                    if nb_active_activations + nb_active_inhibitions == target_nb_templates:
                        break
                    elif nb_active_activations + nb_active_inhibitions > target_nb_templates:
                        ind = self._vary_with_pb(ind,
                            0.,
                            0.,
                            1.0,
                            0.,
                            0.,
                            0.,
                            True)
                    else:
                        ind = self._vary_with_pb(ind,
                            0.,
                            0.,
                            1./4.,
                            1./4.,
                            1./4.,
                            1./4.,
                            True)

                # Apply a random number of parameter mutations
                nb_mut_param = np.random.randint(self.init_nb_param_muts_domain[0], self.init_nb_param_muts_domain[1])
                for _ in range(nb_mut_param):
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                ind.assemble()


                # Verify if valid
                if ind.is_valid():
                    # Verify if within bounds
                    activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                    nb_active_activations = len(activeConnActivationsIndexes)
                    activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                    nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                    # Verify if there are templates from gradients
                    possess_templates_from_gradients = False
                    gradients_id = [0, 1]
                    for g in gradients_id:
                        possess_templates_from_gradients |= np.any(ind.activations[g,:] >= 0.005)

                    if possess_templates_from_gradients and ind.nb_nodes >= self.nb_nodes_domain[0] and ind.nb_nodes <= self.nb_nodes_domain[1] and nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                        break
                else:
                    print("INIT: NOT VALID !")
                    raise RuntimeError("INIT: NOT VALID !")
            ind.name = str(hash(ind) ** 2)
            res = ind

        else: # Selection
            try:
                choice = np.random.random() > self.epsilon
                if choice:
                    res = collection.best
                else:
                    choice2 = np.random.choice(2, p=(0.70, 0.30))
                    if choice2 == 0:
                        res = tools.non_trivial_sel_random(collection)
                    else:
                        res = tools.sel_random(collection)
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()


        if self.nb_suggestions <= self.approx_budget:
            res.base_configuration_entry = "approx_daccad"
        else:
            res.base_configuration_entry = "daccad"
            if not self.resetted_grid_scores:
                self.resetted_grid_scores = True
                self._reset_grid_scores()

        return res, not initialise



    def _vary_with_pb(self, individual, prob_parameter_mut, prob_crossover, prob_disable_template, prob_activation_add, prob_inhibition_add, prob_node_add, weak_validation = False):
        for _ in range(5000): # XXX
            ind = copy.deepcopy(individual)
            #ind.name = str(id(ind))

            # Choose the kind of mutation
            choice = np.random.choice(6, p=(prob_parameter_mut, prob_crossover, prob_disable_template, prob_activation_add, prob_inhibition_add, prob_node_add))

            # Perform the mutation
            try:
                if choice == 0: # Parameters mutation
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                elif choice == 1: # Crossover
                    other_ind = tools.non_trivial_sel_random(self.container)
                    if self.crossover_type == 1:
                        ind, other_ind = crossover_uniform(ind, other_ind, self.cx_pb)
                    elif self.crossover_type == 2:
                        ind, other_ind = crossover_uniform2(ind, other_ind, self.cx_pb)
                elif choice == 2: # Disable template
                    disable_template(ind)
                elif choice == 3: # Add Activation
                    add_activation_with_gradients(ind)
                elif choice == 4: # Add inhibition
                    add_inhibition_with_gradients(ind)
                elif choice == 5: # Add node
                    add_node_with_gradients(ind)
            except NotApplicableError as e:
                continue
            except Exception as e:
                print(f"VARY: EXCEPTION (choice={choice}): {e}")
                continue

            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                nb_active_activations = len(activeConnActivationsIndexes)
                activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                # Verify if there are templates from gradients
                possess_templates_from_gradients = False
                gradients_id = [0, 1]
                for g in gradients_id:
                    possess_templates_from_gradients |= np.any(ind.activations[g,:] >= 0.005)

                if possess_templates_from_gradients and weak_validation:
                    break
                elif possess_templates_from_gradients and ind.nb_nodes >= self.nb_nodes_domain[0] and ind.nb_nodes <= self.nb_nodes_domain[1] and nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        ind.name = str(hash(ind) ** 2)
        return ind


    def _vary(self, individual: Any) -> Any:
        return self._vary_with_pb(individual,
                self.prob_parameter_mut,
                self.prob_crossover,
                self.prob_disable_template,
                self.prob_activation_add,
                self.prob_inhibition_add,
                self.prob_node_add)






@registry.register
class DaccadTopologicalInitBioneatFixedWeights(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [3, 7],
            min_init_budget: int = 500,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            prob_parameter_mut: float = 0.20,
            prob_disable_template: float = 0.20,
            prob_activation_add: float = 0.20,
            prob_inhibition_add: float = 0.20,
            prob_node_add: float = 0.20,
            nbConnActivationsDomain: Sequence[int] = [2, 8],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [2, 14],
            f1: float = 0.2,
            f2: float = 2.0,
            mut_pb: float = 0.8,
            weights_vals: Sequence[float] = [0.025, 0.2, 0.75],
            #init_nb_muts_domain: Sequence[int] = [0, 5],
            init_nb_param_muts_domain: Sequence[int] = [1, 3],
            epsilon_domain: Sequence[float] = [0.05, 0.70],
            allow_gd: bool = False,
            gd_pb: float = 1.0,
            grad4: bool = False,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.prob_parameter_mut = prob_parameter_mut
        self.prob_disable_template = prob_disable_template
        self.prob_activation_add = prob_activation_add
        self.prob_inhibition_add = prob_inhibition_add
        self.prob_node_add = prob_node_add
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.f1 = f1
        self.f2 = f2
        self.mut_pb = mut_pb
        self.weights_vals = weights_vals
        self.init_nb_param_muts_domain = init_nb_param_muts_domain
        self.epsilon_domain = epsilon_domain
        self.grad4 = grad4

        self.epsilon = np.random.uniform(self.epsilon_domain[0], self.epsilon_domain[1]) if self.epsilon_domain[0] != self.epsilon_domain[1] else self.epsilon_domain[0]
        print(f"Using epsilon={self.epsilon}")

        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=gen_daccad_individuals(self.ind_domain), **kwargs)


    def _adjust_weights(self, ind):
        for i in range(ind.activations.shape[0]):
            for j in range(ind.activations.shape[1]):
                v = ind.activations[i,j]
                if v > 0.000001:
                    ind.activations[i,j] = self.weights_vals[np.argmin([abs(v - w) for w in self.weights_vals])]
        for i in range(ind.inhibitions.shape[0]):
            for j in range(ind.inhibitions.shape[1]):
                for k in range(ind.inhibitions.shape[2]):
                    v = ind.inhibitions[i,j,k]
                    if v > 0.000001:
                        ind.inhibitions[i,j,k] = self.weights_vals[np.argmin([abs(v - w) for w in self.weights_vals])]
        ind.assemble()


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        choice = np.random.choice(2, p=(0.99, 0.01))
        if choice == 1:
            self.epsilon = np.random.uniform(self.epsilon_domain[0], self.epsilon_domain[1]) if self.epsilon_domain[0] != self.epsilon_domain[1] else self.epsilon_domain[0]
            print(f"Using epsilon={self.epsilon}")

        if initialise: # Initialisation
            gradients_id = [0,1,2,3] if self.grad4 else [0,1]
            for _ in range(5000): # XXX
                ind = copy.deepcopy(base_ind)
                #ind.name = str(id(ind))

                # Create a base individual
                if self.grad4:
                    standard_init_ind_grad4(ind)
                else:
                    standard_init_ind(ind)

                # Select a target number of templates
                target_nb_templates = np.random.randint(self.nbConnDomain[0], self.nbConnDomain[1])

                # Apply topological mutations until individual reach chosen number of templates
                for _ in range(500): # XXX
                    nb_active_activations = len(np.where(ind.activations >= 0.005)[0])
                    nb_active_inhibitions = len(np.where(ind.inhibitions >= 0.005)[0])
                    if nb_active_activations + nb_active_inhibitions == target_nb_templates:
                        break
                    elif nb_active_activations + nb_active_inhibitions > target_nb_templates:
                        ind = self._vary_with_pb(ind,
                            0.,
                            1.0,
                            0.,
                            0.,
                            0.,
                            True)
                    else:
                        ind = self._vary_with_pb(ind,
                            0.,
                            1./4.,
                            1./4.,
                            1./4.,
                            1./4.,
                            True)

                # Apply a random number of parameter mutations
                nb_mut_param = np.random.randint(self.init_nb_param_muts_domain[0], self.init_nb_param_muts_domain[1])
                for _ in range(nb_mut_param):
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)

                ind.assemble()
                self._adjust_weights(ind)

                # Verify if valid
                if ind.is_valid():
                    # Verify if within bounds
                    activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                    nb_active_activations = len(activeConnActivationsIndexes)
                    activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                    nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                    # Verify if there are templates from gradients
                    possess_templates_from_gradients = False
                    for g in gradients_id:
                        possess_templates_from_gradients |= np.any(ind.activations[g,:] >= 0.005)

                    if possess_templates_from_gradients and ind.nb_nodes >= self.nb_nodes_domain[0] and ind.nb_nodes <= self.nb_nodes_domain[1] and nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                        break
                else:
                    print("INIT: NOT VALID !")
                    raise RuntimeError("INIT: NOT VALID !")
            ind.name = str(hash(ind) ** 2)
            return ind, False

        else: # Selection
            try:
                choice = np.random.random() > self.epsilon
                if choice:
                    res = collection.best, True
                else:
                    choice2 = np.random.choice(2, p=(0.70, 0.30))
                    if choice2 == 0:
                        res = tools.non_trivial_sel_random(collection), True
                    else:
                        res = tools.sel_random(collection), True
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()
            return res



    def _vary_with_pb(self, individual, prob_parameter_mut, prob_disable_template, prob_activation_add, prob_inhibition_add, prob_node_add, weak_validation = False):
        gradients_id = [0,1,2,3] if self.grad4 else [0,1]

        for _ in range(5000): # XXX
            ind = copy.deepcopy(individual)
            #ind.name = str(id(ind))

            # Choose the kind of mutation
            choice = np.random.choice(5, p=(prob_parameter_mut, prob_disable_template, prob_activation_add, prob_inhibition_add, prob_node_add))

            # Perform the mutation
            try:
                if choice == 0: # Parameters mutation
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                elif choice == 1: # Disable template
                    disable_template(ind)
                elif choice == 2: # Add Activation
                    add_activation_with_gradients(ind, gradients=gradients_id)
                elif choice == 3: # Add inhibition
                    add_inhibition_with_gradients(ind, gradients=gradients_id)
                elif choice == 4: # Add node
                    add_node_with_gradients(ind, gradients=gradients_id)
            except Exception as e:
                print(f"VARY: EXCEPTION (choice={choice}): {e}")
                continue

            # Assemble individual
            ind.assemble()
            self._adjust_weights(ind)
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                nb_active_activations = len(activeConnActivationsIndexes)
                activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                # Verify if there are templates from gradients
                possess_templates_from_gradients = False
                for g in gradients_id:
                    possess_templates_from_gradients |= np.any(ind.activations[g,:] >= 0.005)

                if possess_templates_from_gradients and weak_validation:
                    break
                elif possess_templates_from_gradients and ind.nb_nodes >= self.nb_nodes_domain[0] and ind.nb_nodes <= self.nb_nodes_domain[1] and nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        ind.name = str(hash(ind) ** 2)
        return ind


    def _vary(self, individual: Any) -> Any:
        return self._vary_with_pb(individual,
                self.prob_parameter_mut,
                self.prob_disable_template,
                self.prob_activation_add,
                self.prob_inhibition_add,
                self.prob_node_add)




@registry.register
class DaccadTopologicalInitBioneatFixedWeightsFixedStabilities(DaccadTopologicalInitBioneatFixedWeights):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [3, 7],
            min_init_budget: int = 500,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            prob_parameter_mut: float = 0.20,
            prob_disable_template: float = 0.20,
            prob_activation_add: float = 0.20,
            prob_inhibition_add: float = 0.20,
            prob_node_add: float = 0.20,
            nbConnActivationsDomain: Sequence[int] = [2, 8],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [2, 14],
            f1: float = 0.2,
            f2: float = 2.0,
            mut_pb: float = 0.8,
            weights_vals: Sequence[float] = [0.025, 0.2, 0.75],
            stabilities_vals: Sequence[float] = [0.025, 0.2, 0.75],
            #init_nb_muts_domain: Sequence[int] = [0, 5],
            init_nb_param_muts_domain: Sequence[int] = [1, 3],
            epsilon_domain: Sequence[float] = [0.05, 0.70],
            allow_gd: bool = False,
            gd_pb: float = 1.0,
            grad4: bool = False,
            **kwargs):
        self.stabilities_vals = stabilities_vals

        super().__init__(container, budget, ind_domain,
            nb_nodes_domain = nb_nodes_domain,
            min_init_budget = min_init_budget,
            min_ind_found_in_init = min_ind_found_in_init,
            init_pb = init_pb,
            sel_pb = sel_pb,
            prob_parameter_mut = prob_parameter_mut,
            prob_disable_template = prob_disable_template,
            prob_activation_add = prob_activation_add,
            prob_inhibition_add = prob_inhibition_add,
            prob_node_add = prob_node_add,
            nbConnActivationsDomain = nbConnActivationsDomain,
            nbConnInhibitionsDomain = nbConnInhibitionsDomain,
            nbConnDomain = nbConnDomain,
            f1 = f1,
            f2 = f2,
            mut_pb = mut_pb,
            weights_vals = weights_vals,
            init_nb_param_muts_domain = init_nb_param_muts_domain,
            epsilon_domain = epsilon_domain,
            allow_gd = allow_gd,
            gd_pb = gd_pb,
            grad4 = grad4,
            **kwargs)


    def _adjust_weights(self, ind):
        for i in range(len(ind.stabilities)):
            v = ind.stabilities[i]
            ind.stabilities[i] = self.weights_vals[np.argmin([abs(v - w) for w in self.weights_vals])]
        for i in range(ind.activations.shape[0]):
            for j in range(ind.activations.shape[1]):
                v = ind.activations[i,j]
                if v > 0.000001:
                    ind.activations[i,j] = self.weights_vals[np.argmin([abs(v - w) for w in self.weights_vals])]
        for i in range(ind.inhibitions.shape[0]):
            for j in range(ind.inhibitions.shape[1]):
                for k in range(ind.inhibitions.shape[2]):
                    v = ind.inhibitions[i,j,k]
                    if v > 0.000001:
                        ind.inhibitions[i,j,k] = self.weights_vals[np.argmin([abs(v - w) for w in self.weights_vals])]
        ind.assemble()







# TODO
@registry.register
class DaccadTopologicalInitBioneatFixedWeightsTransfer(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [3, 7],
            min_init_budget: int = 500,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            prob_parameter_mut: float = 0.20,
            prob_disable_template: float = 0.20,
            prob_activation_add: float = 0.20,
            prob_inhibition_add: float = 0.20,
            prob_node_add: float = 0.20,
            transfer_prob_parameter_mut: float = 0.20,
            transfer_prob_disable_template: float = 0.20,
            transfer_prob_activation_add: float = 0.20,
            transfer_prob_inhibition_add: float = 0.20,
            transfer_prob_node_add: float = 0.20,
            nbConnActivationsDomain: Sequence[int] = [2, 8],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [2, 14],
            f1: float = 0.2,
            f2: float = 2.0,
            mut_pb: float = 0.8,
            weights_vals: Sequence[float] = [0.025, 0.2, 0.75],
            #init_nb_muts_domain: Sequence[int] = [0, 5],
            init_nb_param_muts_domain: Sequence[int] = [1, 3],
            epsilon_domain: Sequence[float] = [0.05, 0.70],
            allow_gd: bool = False,
            gd_pb: float = 1.0,
            approx_budget: int = 1000,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.prob_parameter_mut = prob_parameter_mut
        self.prob_disable_template = prob_disable_template
        self.prob_activation_add = prob_activation_add
        self.prob_inhibition_add = prob_inhibition_add
        self.prob_node_add = prob_node_add
        self.transfer_prob_parameter_mut = transfer_prob_parameter_mut
        self.transfer_prob_disable_template = transfer_prob_disable_template
        self.transfer_prob_activation_add = transfer_prob_activation_add
        self.transfer_prob_inhibition_add = transfer_prob_inhibition_add
        self.transfer_prob_node_add = transfer_prob_node_add
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.f1 = f1
        self.f2 = f2
        self.mut_pb = mut_pb
        self.weights_vals = weights_vals
        self.init_nb_param_muts_domain = init_nb_param_muts_domain
        self.epsilon_domain = epsilon_domain
        self.approx_budget = approx_budget
        self.resetted_grid_scores = False

        self.epsilon = np.random.uniform(self.epsilon_domain[0], self.epsilon_domain[1]) if self.epsilon_domain[0] != self.epsilon_domain[1] else self.epsilon_domain[0]
        print(f"Using epsilon={self.epsilon}")

        self.inds_to_transfer = []

        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=gen_daccad_individuals(self.ind_domain), **kwargs)


    def _adjust_weights(self, ind):
        for i in range(ind.activations.shape[0]):
            for j in range(ind.activations.shape[1]):
                v = ind.activations[i,j]
                if v > 0.000001:
                    ind.activations[i,j] = self.weights_vals[np.argmin([abs(v - w) for w in self.weights_vals])]
        for i in range(ind.inhibitions.shape[0]):
            for j in range(ind.inhibitions.shape[1]):
                for k in range(ind.inhibitions.shape[2]):
                    v = ind.inhibitions[i,j,k]
                    if v > 0.000001:
                        ind.inhibitions[i,j,k] = self.weights_vals[np.argmin([abs(v - w) for w in self.weights_vals])]
        ind.assemble()


    def _reset_grid_scores(self):
        print("Emptying the grid !")
        for ind in self.container:
            ind.fitness.setValues([0.])
            #ind.fitness.setValues([0.001])
            self.inds_to_transfer.append(ind)
        #self.best_fitness.setValues([0.])


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        choice = np.random.choice(2, p=(0.99, 0.01))
        if choice == 1:
            self.epsilon = np.random.uniform(self.epsilon_domain[0], self.epsilon_domain[1]) if self.epsilon_domain[0] != self.epsilon_domain[1] else self.epsilon_domain[0]

        if len(self.inds_to_transfer):
            res = self.inds_to_transfer.pop()
            res.name = str(hash(res) ** 2)
            initialise = True
            print(f"Using epsilon={self.epsilon}")

        if initialise: # Initialisation
            for _ in range(5000): # XXX
                ind = copy.deepcopy(base_ind)
                #ind.name = str(id(ind))

                # Create a base individual
                standard_init_ind(ind)

                # Select a target number of templates
                target_nb_templates = np.random.randint(self.nbConnDomain[0], self.nbConnDomain[1])

                # Apply topological mutations until individual reach chosen number of templates
                for _ in range(500): # XXX
                    nb_active_activations = len(np.where(ind.activations >= 0.005)[0])
                    nb_active_inhibitions = len(np.where(ind.inhibitions >= 0.005)[0])
                    if nb_active_activations + nb_active_inhibitions == target_nb_templates:
                        break
                    elif nb_active_activations + nb_active_inhibitions > target_nb_templates:
                        ind = self._vary_with_pb(ind,
                            0.,
                            1.0,
                            0.,
                            0.,
                            0.,
                            True)
                    else:
                        ind = self._vary_with_pb(ind,
                            0.,
                            1./4.,
                            1./4.,
                            1./4.,
                            1./4.,
                            True)

                # Apply a random number of parameter mutations
                nb_mut_param = np.random.randint(self.init_nb_param_muts_domain[0], self.init_nb_param_muts_domain[1])
                for _ in range(nb_mut_param):
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)

                ind.assemble()
                self._adjust_weights(ind)

                # Verify if valid
                if ind.is_valid():
                    # Verify if within bounds
                    activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                    nb_active_activations = len(activeConnActivationsIndexes)
                    activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                    nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                    # Verify if there are templates from gradients
                    possess_templates_from_gradients = False
                    gradients_id = [0, 1]
                    for g in gradients_id:
                        possess_templates_from_gradients |= np.any(ind.activations[g,:] >= 0.005)

                    if possess_templates_from_gradients and ind.nb_nodes >= self.nb_nodes_domain[0] and ind.nb_nodes <= self.nb_nodes_domain[1] and nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                        break
                else:
                    print("INIT: NOT VALID !")
                    raise RuntimeError("INIT: NOT VALID !")
            ind.name = str(hash(ind) ** 2)
            res = ind

        else: # Selection
            try:
                choice = np.random.random() > self.epsilon
                if choice:
                    res = collection.best
                else:
                    choice2 = np.random.choice(2, p=(0.70, 0.30))
                    if choice2 == 0:
                        res = tools.non_trivial_sel_random(collection)
                    else:
                        res = tools.sel_random(collection)
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()


        if self.nb_suggestions <= self.approx_budget:
            res.base_configuration_entry = "approx_daccad"
        else:
            res.base_configuration_entry = "daccad"
            if not self.resetted_grid_scores:
                self.resetted_grid_scores = True
                self._reset_grid_scores()

        return res, not initialise



    def _vary_with_pb(self, individual, prob_parameter_mut, prob_disable_template, prob_activation_add, prob_inhibition_add, prob_node_add, weak_validation = False):
        for _ in range(5000): # XXX
            ind = copy.deepcopy(individual)
            #ind.name = str(id(ind))

            # Choose the kind of mutation
            choice = np.random.choice(5, p=(prob_parameter_mut, prob_disable_template, prob_activation_add, prob_inhibition_add, prob_node_add))

            # Perform the mutation
            try:
                if choice == 0: # Parameters mutation
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                elif choice == 1: # Disable template
                    disable_template(ind)
                elif choice == 2: # Add Activation
                    add_activation_with_gradients(ind)
                elif choice == 3: # Add inhibition
                    add_inhibition_with_gradients(ind)
                elif choice == 4: # Add node
                    add_node_with_gradients(ind)
            except Exception as e:
                print(f"VARY: EXCEPTION (choice={choice}): {e}")
                continue

            # Assemble individual
            ind.assemble()
            self._adjust_weights(ind)
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                nb_active_activations = len(activeConnActivationsIndexes)
                activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                # Verify if there are templates from gradients
                possess_templates_from_gradients = False
                gradients_id = [0, 1]
                for g in gradients_id:
                    possess_templates_from_gradients |= np.any(ind.activations[g,:] >= 0.005)

                if possess_templates_from_gradients and weak_validation:
                    break
                elif possess_templates_from_gradients and ind.nb_nodes >= self.nb_nodes_domain[0] and ind.nb_nodes <= self.nb_nodes_domain[1] and nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        ind.name = str(hash(ind) ** 2)
        return ind


    def _vary(self, individual: Any) -> Any:
        if self.resetted_grid_scores:
            return self._vary_with_pb(individual,
                    self.transfer_prob_parameter_mut,
                    self.transfer_prob_disable_template,
                    self.transfer_prob_activation_add,
                    self.transfer_prob_inhibition_add,
                    self.transfer_prob_node_add)
        else:
            return self._vary_with_pb(individual,
                    self.prob_parameter_mut,
                    self.prob_disable_template,
                    self.prob_activation_add,
                    self.prob_inhibition_add,
                    self.prob_node_add)



# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
