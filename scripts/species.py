#!/usr/bin/env python3


########## IMPORTS ########### {{{1

import os
from timeit import default_timer as timer
import copy
import numpy as np
import random
import warnings
import traceback

from qdpy.algorithms import *
from qdpy.containers import *
from qdpy.plots import *
from qdpy.base import *
from qdpy import tools

from typing import Optional, Tuple, List, Iterable, Iterator, Any, TypeVar, Generic, Union, Sequence, MutableSet, MutableSequence, Type, Callable, Generator, Mapping, MutableMapping, overload

from base import *

@registry.register
class GridWithTopologicalSpecies(Grid):
    """TODO""" # TODO

    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)

    def add(self, individual: IndividualLike, raise_if_not_added_to_depot: bool = False) -> Optional[int]:
        """Add ``individual`` to the grid, and returns its index, if successful, None elsewise. If ``raise_if_not_added_to_depot`` is True, it will raise and exception if it was not possible to add it also to the depot."""
        # Retrieve features and fitness from individual and check if they are not out-of-bounds
        self._check_if_can_be_added(individual)
        # Find corresponding index in the grid
        ig = self.index_grid(individual.features) # Raise exception if features are out of bounds

        # Verify if the same specie is not already present
        assert hasattr(individual, "same_species_as"), f"`individual` must possess method `same_species_as`."
        can_be_added: bool = True
        for i, s in enumerate(self.solutions[ig]):
            if individual.same_species_as(s):
                if individual.fitness.dominates(s.fitness):
                    Container.discard(self, self.solutions[ig][i])
                    self._discard_from_grid(ig, i)
                    can_be_added = True
                else:
                    can_be_added = False
                break
        #can_be_added &= self._nb_items_per_bin[ig] < self.max_items_per_bin

        # Verify if there are enough place in the bin
        if can_be_added:
            if self._nb_items_per_bin[ig] >= self.max_items_per_bin:
                can_be_added = False
                worst_idx = 0
                worst: IndividualLike = self.solutions[ig][worst_idx]
                if self._nb_items_per_bin[ig] > 1:
                    for i in range(1, self._nb_items_per_bin[ig]):
                        s = self.solutions[ig][i]
                        if worst.fitness.dominates(s.fitness):
                            worst = s
                            worst_idx = i
                if individual.fitness.dominates(worst.fitness):
                    Container.discard(self, self.solutions[ig][worst_idx])
                    self._discard_from_grid(ig, worst_idx)
                    can_be_added = True


        # Add individual in grid, if there are enough empty spots
        if can_be_added:
            if self._nb_items_per_bin[ig] == 0:
                self._filled_bins += 1
            # Add individual in container
            old_len: int = self._size
            index: Optional[int] = self._add_internal(individual, raise_if_not_added_to_depot, False)
            if index == old_len: # Individual was not already present in container
                self._solutions[ig].append(individual)
                self._fitness[ig].append(individual.fitness)
                self._features[ig].append(individual.features)
                self.recentness_per_bin[ig].append(self._nb_added)
                self.history_recentness_per_bin[ig].append(self._nb_added)
                self._nb_items_per_bin[ig] += 1
                self.activity_per_bin[ig] += 1
            # Update quality
            self._update_quality(ig)
            return index
        else:
            # Only add to depot
            if self.depot is not None:
                self._add_internal(individual, raise_if_not_added_to_depot, True)
            return None



@registry.register
class DaccadTopologicalSpecies(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [1, 7],
            min_init_budget: int = 1,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            probParametersMutation: float = 0.95,
            probTemplateActivationAdd: float = 0.01,
            probTemplateActivationDel: float = 0.01,
            probTemplateInhibitionAdd: float = 0.01,
            probTemplateInhibitionDel: float = 0.01,
            probNodeAdd: float = 0.01,
            probNodeDel: float = 0.00,
            nbConnActivationsDomain: Sequence[int] = [1, 7],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [1, 13],
            #eta: float = 20.,
            mutationPb: float = 0.4,
            f1: float = 0.2,
            f2: float = 2.0,
            allow_gd: bool = False,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.probParametersMutation = probParametersMutation
        self.probTemplateActivationAdd = probTemplateActivationAdd
        self.probTemplateActivationDel = probTemplateActivationDel
        self.probTemplateInhibitionAdd = probTemplateInhibitionAdd
        self.probTemplateInhibitionDel = probTemplateInhibitionDel
        self.probNodeAdd = probNodeAdd
        self.probNodeDel = probNodeDel
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        #self.eta = eta
        self.mutationPb = mutationPb
        self.f1 = f1
        self.f2 = f2
        self.nb_species = 1

        base_ind_gen = gen_daccad_individuals_GD(self.ind_domain) if allow_gd else gen_daccad_individuals(self.ind_domain)
        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=base_ind_gen, **kwargs)


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        if initialise: # Initialisation
            # Create a base individual
            standard_init_ind(base_ind)
            return base_ind, False

        else: # Selection
            try:
                #res = tools.non_trivial_sel_random(collection), True
                res = self._select(collection), True
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()
            return res


    def _select(self, collection: Sequence[Any]) -> Any:
        # Determine mean fitnesses for each species
        species_fitnesses = [[] for _ in range(self.nb_species)]
        species_inds = [[] for _ in range(self.nb_species)]
        for ind in collection:
            #if all((e > self.ind_domain[0] for e in ind.fitness)):
            #if ind.fitness[0] > self.container.fitness_domain[0][0]:
            #    species_fitnesses[ind.specie].append(ind.fitness[0])
            #    species_inds[ind.specie].append(ind)
            species_fitnesses[ind.specie].append(ind.fitness[0])
            species_inds[ind.specie].append(ind)
        species_mean_fitness = np.array([np.mean(s) if len(s) else 0. for s in species_fitnesses])
        species_prob = np.zeros_like(species_mean_fitness)
        nb_nonempty_species = sum([len(c)>0 for c in species_inds])
        for i in range(len(species_prob)):
            if len(species_inds[i]):
                species_prob[i] = species_mean_fitness[i] * 1. + (self.container.fitness_domain[0][1] / float(nb_nonempty_species)) ** 10.
        #species_prob = species_mean_fitness * 2. + self.container.fitness_domain[0][1] / float(self.nb_species)
        species_normalised_prob = species_prob / np.sum(species_prob)
        selected_specie = np.random.choice(range(self.nb_species), p=species_normalised_prob)

        #res = tools.non_trivial_sel_random(species_inds[selected_specie])
        collection = species_inds[selected_specie]
        fst_ind = collection[0]
        min_fitness = copy.deepcopy(fst_ind.fitness)
        min_fitness.values = tuple([x[0] for x in self.container.fitness_domain])
        candidates = [ind for ind in collection if ind.fitness.dominates(min_fitness)]
        if len(candidates):
            return random.choice(candidates)
        else:
            return random.choice(collection)



    def _vary_with_pb(self, individual, param_mut_pb, act_add_mut_pb, act_del_mut_pb, inh_add_mut_pb, inh_del_mut_pb, node_add_mut_pb, node_del_mut_pb):
        for _ in range(500): # XXX
            ind = copy.deepcopy(individual)
            ind.name = str(id(ind))

            active_activations_coords = list(zip(*np.where(ind.activations)))
            nb_active_activations = len(active_activations_coords)
            active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
            nb_active_inhibitions = len(active_inhibitions_coords)

            # Choose the kind of mutation
            choice = np.random.choice(7, p=(param_mut_pb, act_add_mut_pb, act_del_mut_pb, inh_add_mut_pb, inh_del_mut_pb, node_add_mut_pb, node_del_mut_pb))
            # Verify that the add and del are allowed
            if choice == 1 and (nb_active_activations >= self.nbConnActivationsDomain[1] or nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[1]):
                choice = 2 # Transform into del
            elif choice == 2 and (nb_active_activations <= self.nbConnActivationsDomain[0] or nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[0]):
                choice = 1 # Transform into add
            elif choice == 3 and (nb_active_inhibitions >= self.nbConnInhibitionsDomain[1] or nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[1]):
                choice = 4 # Transform into del
            elif choice == 4 and (nb_active_inhibitions <= self.nbConnInhibitionsDomain[0] or nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[0]):
                choice = 3 # Transform into add
            elif choice == 5 and ind.nb_nodes >= self.nb_nodes_domain[1]:
                choice = 6 # Transform into del
            elif choice == 6 and ind.nb_nodes <= self.nb_nodes_domain[0]:
                choice = 5 # Transform into add

            # Perform the mutation
            if choice == 0: # Parameters mutation
                mutation_param_bioneat(ind, self.f1, self.f2, self.mutationPb)
                #if ind.fitness[0] <= 0.:
                #    mutation_param_reinit(ind)
                #else:
                #    #mutation_param_polybounded(ind, self.eta, self.mutationPb)
                #    mutation_param_bioneat(ind, self.f1, self.f2, self.mutationPb)
            elif choice == 1: # Add Activation
                mutation_add_activation(ind)
            elif choice == 2: # Del Activation
                mutation_del_activation(ind)
            elif choice == 3: # Add Inhibition
                mutation_add_inhibition(ind, False)
            elif choice == 4: # Del Inhibition
                mutation_del_inhibition(ind)
            elif choice == 5: # Add node
                mutation_add_node(ind, False)
            elif choice == 6: # Del node
                mutation_del_node(ind)

            ## Verify if there are no orphaned nodes
            #is_orphaned = (not(any(ind.activations[:,i]) or any(ind.activations[i,:]) or any(ind.inhibitions[i,:,:].flatten())) for i in range(ind.nb_nodes))
            #if any(is_orphaned):
            #    continue

            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                active_activations_coords = list(zip(*np.where(ind.activations)))
                nb_active_activations = len(active_activations_coords)
                active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
                nb_active_inhibitions = len(active_inhibitions_coords)

                if nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        # Update specie id
        if choice >= 3:
            self.nb_species += 1
            ind.specie = self.nb_species - 1
        return ind


    def _vary(self, individual: Any) -> Any:
        return self._vary_with_pb(individual, self.probParametersMutation,
                self.probTemplateActivationAdd, self.probTemplateActivationDel,
                self.probTemplateInhibitionAdd, self.probTemplateInhibitionDel,
                self.probNodeAdd, self.probNodeDel)


#    def _tell(self, individual: Any) -> None:
#        pass # TODO


# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
