#!/usr/bin/env python3

# DEBUGGING
import faulthandler
faulthandler.enable()


########## IMPORTS ########### {{{1

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

import os
import pathlib
import datetime
from timeit import default_timer as timer
import copy
import itertools
from functools import partial
import yaml
import numpy as np
import random
import warnings
import traceback
import math
import socket

import submitDACCAD

from qdpy.algorithms import *
from qdpy.containers import *
#from qdpy.benchmarks import *
from qdpy.plots import *
from qdpy.base import *
from qdpy import tools

from typing import Optional, Tuple, List, Iterable, Iterator, Any, TypeVar, Generic, Union, Sequence, MutableSet, MutableSequence, Type, Callable, Generator, Mapping, MutableMapping, overload
import qdpy.hdsobol as hdsobol
from base import *
from species import *
from batch import *

from oldoptim import *
from epsilon import *
from bioneatmut import *
from topological import *
from sugg import *




@registry.register
class DaccadCollectionInitBioneat(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [3, 7],
            prob_parameter_mut: float = 0.96,
            prob_disable_template: float = 0.01,
            prob_activation_add: float = 0.01,
            prob_inhibition_add: float = 0.01,
            prob_node_add: float = 0.01,
            nbConnActivationsDomain: Sequence[int] = [2, 8],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [2, 14],
            f1: float = 0.2,
            f2: float = 2.0,
            mut_pb: float = 0.8,
            epsilon_domain: Sequence[float] = [0.05, 0.70],
            allow_gd: bool = False,
            gd_pb: float = 1.0,
            reference_container_paths: Sequence[str] = [""],
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.prob_parameter_mut = prob_parameter_mut
        self.prob_disable_template = prob_disable_template
        self.prob_activation_add = prob_activation_add
        self.prob_inhibition_add = prob_inhibition_add
        self.prob_node_add = prob_node_add
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.f1 = f1
        self.f2 = f2
        self.mut_pb = mut_pb
        self.epsilon_domain = epsilon_domain
        self.reference_container_paths = reference_container_paths

        self.epsilon = np.random.uniform(self.epsilon_domain[0], self.epsilon_domain[1]) if self.epsilon_domain[0] != self.epsilon_domain[1] else self.epsilon_domain[0]
        print(f"Using epsilon={self.epsilon}")

        self._load_reference()
        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=gen_daccad_individuals(self.ind_domain), **kwargs)


    def _load_reference(self):
        self.reference_archives = []
        self.reference_containers = []
        self.reference_individuals = []
        for ref_path in self.reference_container_paths:
            with open(ref_path, "rb") as f:
                data = pickle.load(f)
            self.reference_archives.append(data)
            self.reference_containers.append(data['container'])
            self.reference_individuals += list(data['container'])
        if len(self.reference_containers):
            self.reference_combined_container = copy.deepcopy(self.reference_containers[0])
            for i in range(1, len(self.reference_containers)):
                self.reference_combined_container.update(self.reference_containers[i])
        else:
            self.reference_combined_container = None
            raise RuntimeError("NO VALID REFERENCES !")
        self.reference_individuals_to_reeval = copy.deepcopy(self.reference_individuals)


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        if len(self.reference_individuals_to_reeval):
            ind = self.reference_individuals_to_reeval.pop()
            ind.name = str(hash(ind) ** 2)
            #print(f"Using reference individual #{len(self.reference_individuals_to_reeval)}: {ind}")
            print(f"Using reference individual #{len(self.reference_individuals_to_reeval)}: {id(ind)}")
            return ind, False
        elif not len(collection):
            ind = copy.deepcopy(random.choice(self.reference_individuals))
            print(f"Using reference individual: {id(ind)}")
            mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
            ind.assemble()
            ind.name = str(hash(ind) ** 2)
            return ind, False

        choice = np.random.choice(2, p=(0.99, 0.01))
        if choice == 1:
            self.epsilon = np.random.uniform(self.epsilon_domain[0], self.epsilon_domain[1]) if self.epsilon_domain[0] != self.epsilon_domain[1] else self.epsilon_domain[0]
            print(f"Using epsilon={self.epsilon}")

        # Selection
        try:
            choice = np.random.random() > self.epsilon
            if choice:
                res = collection.best, True
            else:
                choice2 = np.random.choice(2, p=(0.70, 0.30))
                if choice2 == 0:
                    res = tools.non_trivial_sel_random(collection), True
                else:
                    res = tools.sel_random(collection), True
        except Exception as e:
            print(f"EXCEPTION ! {e}")
            traceback.print_exc()
        return res



    def _vary_with_pb(self, individual, prob_parameter_mut, prob_disable_template, prob_activation_add, prob_inhibition_add, prob_node_add, weak_validation = False):
        for _ in range(5000): # XXX
            ind = copy.deepcopy(individual)
            #ind.name = str(id(ind))

            # Choose the kind of mutation
            choice = np.random.choice(5, p=(prob_parameter_mut, prob_disable_template, prob_activation_add, prob_inhibition_add, prob_node_add))

            # Perform the mutation
            try:
                if choice == 0: # Parameters mutation
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                elif choice == 1: # Disable template
                    disable_template(ind)
                elif choice == 2: # Add Activation
                    add_activation_with_gradients(ind)
                elif choice == 3: # Add inhibition
                    add_inhibition_with_gradients(ind)
                elif choice == 4: # Add node
                    add_node_with_gradients(ind)
            except Exception as e:
                print(f"VARY: EXCEPTION (choice={choice}): {e}")
                continue

            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                nb_active_activations = len(activeConnActivationsIndexes)
                activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                # Verify if there are templates from gradients
                possess_templates_from_gradients = False
                gradients_id = [0, 1]
                for g in gradients_id:
                    possess_templates_from_gradients |= np.any(ind.activations[g,:] >= 0.005)

                if possess_templates_from_gradients and weak_validation:
                    break
                elif possess_templates_from_gradients and ind.nb_nodes >= self.nb_nodes_domain[0] and ind.nb_nodes <= self.nb_nodes_domain[1] and nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
            #print(f"VARY: NOT GOOD ! (choice={choice})")
        ind.name = str(hash(ind) ** 2)
        return ind


    def _vary(self, individual: Any) -> Any:
        return self._vary_with_pb(individual, 
                self.prob_parameter_mut,
                self.prob_disable_template,
                self.prob_activation_add,
                self.prob_inhibition_add,
                self.prob_node_add)







########## ILLUMINATION DACCAD ########### {{{1

def mad(arr):
    """ Median Absolute Deviation: a "Robust" version of standard deviation.
        Indices variabililty of the sample.
        https://en.wikipedia.org/wiki/Median_absolute_deviation 
    """
    arr = np.ma.array(arr).compressed() # should be faster to not use masked arrays.
    med = np.median(arr)
    return np.median(np.abs(arr - med))


class IlluminateDACCAD(IlluminationExperiment):
    def __init__(self, config_filename, parallelism_type = "concurrent", seed = None, base_config = None):
        super().__init__(config_filename, parallelism_type, seed, base_config)
        self.hostname = socket.gethostname()


    def _launchTrial(self, ind):
        nbNodes = ind.nb_nodes if hasattr(ind, "nb_nodes") else self.config['nbNodes']

        base_configuration_entry = ind.base_configuration_entry if hasattr(ind, "base_configuration_entry") else "daccad"
        #print("base_configuration_entry: ", base_configuration_entry)

        try:
            enable_preevals = self.config.get('enable_preevals') or False
            if enable_preevals:
                preevals_maxTimeEval = self.config['preevals_maxTimeEval']
                preind = copy.deepcopy(ind)
                preScores = self._launchDACCADExpe(preind, base_configuration_entry, {'maxTimeEval': preevals_maxTimeEval})
                if preScores is None:
                    scores = None
                else:
                    nEvaluations = preScores['nEvaluations']
                    preFitnessesList = [preScores["fitness%i_0" % x] for x in range(nEvaluations)]
                    preScores['medianScore'] = sorted(preFitnessesList)[int(len(preFitnessesList)/2 - 1 + len(preFitnessesList) % 2)]
                    #print("DEBUG: preeval: ", preScores['medianScore'])
                    preevals_minMedianScore = self.config['preevals_minMedianScore'] or 0.0
                    if preScores['medianScore'] > preevals_minMedianScore:
                        scores = self._launchDACCADExpe(ind, base_configuration_entry)
                    else:
                        scores = preScores
            else:
                scores = self._launchDACCADExpe(ind, base_configuration_entry)
        except Exception as e:
            warnings.warn(f"Evaluation failed: {str(e)}")
            traceback.print_exc()
            scores = None

        features_list, fitness_from_feature = self._get_features_list()
        if scores is None:
            features = [0.0 for x in features_list]
            fitness = 0.0
        else:
            # Compute means
            nEvaluations = scores['nEvaluations']
            scores['ratioIn'] = np.mean([scores["in%i_0" % x] for x in range(nEvaluations)])
            scores['ratioOut'] = np.mean([scores["out%i_0" % x] for x in range(nEvaluations)])
            scores['score'] = np.mean([scores["fitness%i_0" % x] for x in range(nEvaluations)])
            hellingerScores = [scores["hellinger%i_0" % x] for x in range(nEvaluations)]
            scores['meanHellinger'] = np.mean([scores["hellinger%i_0" % x] for x in range(nEvaluations)])
            normalisedHellingerScores = [1. - (1. / (1. + math.exp(-20.*(x-0.8)))) for x in hellingerScores]
            scores['normalisedMeanHellinger'] = np.mean(normalisedHellingerScores)
            scores['diffInOut'] = scores['ratioIn'] - scores['ratioOut']
            #scores['scoreN'] = min(1., scores['score'] + (0.0 if scores['ratioIn'] == 0. else 0.000001) )
            scores['nTemplateGenome'] = sum(np.array(ind[nbNodes:])>=0.005)
            fitnessesList = [scores["fitness%i_0" % x] for x in range(nEvaluations)]
            scores['medianScore'] = sorted(fitnessesList)[int(len(fitnessesList)/2 - 1 + len(fitnessesList) % 2)]
            scores['maxScore'] = np.max([scores["fitness%i_0" % x] for x in range(nEvaluations)])
            scores['minScore'] = np.min([scores["fitness%i_0" % x] for x in range(nEvaluations)])
            scores['madScore'] = mad([scores["fitness%i_0" % x] for x in range(nEvaluations)])
            scores['nbNodes'] = nbNodes
            scores['nbUsedNodes'] = submitDACCAD.nbUsedNodes(ind, nbNodes)

            otherFitnessIdx = 0
            while True:
                if f"fitness0_{otherFitnessIdx}" not in scores:
                    break
                otherFit = [scores[f"fitness{x}_{otherFitnessIdx}"] for x in range(nEvaluations)]
                scores[f"medianScore{otherFitnessIdx}"] = sorted(otherFit)[int(len(otherFit)/2 - 1 + len(otherFit) % 2)]
                scores[f"ratioIn{otherFitnessIdx}"] = np.mean([scores[f"in{x}_{otherFitnessIdx}"] for x in range(nEvaluations)])
                otherFitnessIdx += 1

            if hasattr(ind, "activations"):
                scores['nbActivations'] = sum(ind.activations.flatten() >= 0.005)
            if hasattr(ind, "inhibitions"):
                scores['nbInhibitions'] = sum(ind.inhibitions.flatten() >= 0.005)

            # Set features and fitness
            features = [scores[x] for x in features_list]
            fitness = scores[fitness_from_feature]

            #print("DEBUGpp", scores)
            #print("DEBUGpp", features_list, features, fitness, scores)
            #print("  {:<20f}".format(fitness), scores)

            # Compute approx if wanted
            also_compute_approx = self.config.get('also_compute_approx') or False
            if also_compute_approx:
                additional_approx_config = self.config.get('additional_approx_config') or {}
                if 'useApprox' not in additional_approx_config:
                    additional_approx_config['useApprox'] = True
                try:
                    scores['approx'] = self._launchDACCADExpe(ind, base_configuration_entry, additional_approx_config)
                    scores['approx']['medianScore'] = scores['approx']["fitness0_0"]
                except Exception as e:
                    warnings.warn(f"Approx failed: {str(e)}")
                    traceback.print_exc()

        ind.scores = scores
        #print(fitness, features, self.config['fitness_domain'], self.config['features_domain'])
        #return [[fitness], features]
        ind.fitness.values = [fitness]
        ind.features = features
        return ind



    def _launchDACCADExpe(self, ind, base_configuration_entry = "daccad", additional_config = {}):
        expeId = ind.name
        nbNodes = ind.nb_nodes if hasattr(ind, "nb_nodes") else self.config['nbNodes']
        bioneatBasePath = self.config.get('bioneatBasePath') or "../../bioneat"
        penSystemLaunchScript = self.config.get('penSystemLaunchScript') or "build/dist/start.sh"
        penSystemLaunchClass = self.config.get('penSystemLaunchClass') or 'use.EvaluateIndividualWithDescriptors'
        penSystemBaseDir = os.path.join(self.config['dataDir'])
        filenameSuffix = f"{self.hostname}_{self.instance_name}_{expeId}"
        jsonFileName = os.path.join(penSystemBaseDir, "generatedGraph_%s.json" % filenameSuffix)
        keepBuggedFiles = self.config.get("keepBuggedFiles") or False
        keepTemporaryFiles = self.config.get("keepTemporaryFiles") or False

        # Verify if daccad configuration file generation is needed
        enableDaccadConfigFileGeneration = "daccad" in self.config
        if enableDaccadConfigFileGeneration:
            penSystemConfigFile = os.path.join(penSystemBaseDir, "daccadConfig_%s.json" % filenameSuffix)
            # Generate a daccad config file
            daccadConfig = {**self.config[base_configuration_entry], **additional_config}
            with open(penSystemConfigFile, "w") as f:
                for k,v in daccadConfig.items():
                    f.write("%s = %s\n" % (k, str(v)))
        else:
            penSystemConfigFile = self.config.get('penSystemConfigFile') or 'config/configEvaluation.config'

        #print("DEBUGnbnodes", nbNodes)
        normalisedInd = np.array(copy.deepcopy(ind))
        # XXX
        normalisedInd[:ind.nb_nodes] *= 1000.
        normalisedInd[ind.nb_nodes:] *= 200.
        #normalisedInd[normalisedInd<=1.] = 0.

        features_list, fitness_from_feature = self._get_features_list()

        if len(np.where(normalisedInd[ind.nb_nodes:]>0.)[0]) == 0:
            # XXX
            print("DEBUG: INDIVIDUAL WITH NO TEMPLATES: ", ind)
            scores = None
        else:
            scores = submitDACCAD.submitPENSystem(normalisedInd, nbNodes, executablePath=bioneatBasePath, launchScript=penSystemLaunchScript, launchClass=penSystemLaunchClass, configFile=penSystemConfigFile, baseDir=penSystemBaseDir, jsonFileName=jsonFileName)

        #print("DEBUG: ", jsonFileName)
        # Remove files
        if not keepBuggedFiles or (scores is not None and keepTemporaryFiles is False):
            self._removeTmpFiles([jsonFileName])
            if enableDaccadConfigFileGeneration:
                self._removeTmpFiles([penSystemConfigFile])
        return scores



def num_or_nan(x):
    if type(x) is float or type(x) is int:
        return x
    else:
        return np.nan

class IlluminateNoisyDACCAD(IlluminateDACCAD):
    def __init__(self, config_filename, parallelism_type = "concurrent", seed = None, base_config = None):
        super().__init__(config_filename, parallelism_type, seed, base_config)
        self.nb_repeats = self.config['nb_repeats']
        noise_type = self.config['noise_type']
        if noise_type == "randomize_ind":
            self.noise_fn = randomize_ind
        elif noise_type == "randomize_templates":
            self.noise_fn = randomize_templates
        elif noise_type == "noisy_ind":
            sigma = self.config['noise_sigma']
            #self.noise_fn = lambda x: noisy_ind(x, sigma)
            self.noise_fn = partial(noisy_ind, sigma=sigma)
        elif noise_type == "noisy_templates":
            sigma = self.config['noise_sigma']
            #self.noise_fn = lambda x: noisy_templates(x, sigma)
            self.noise_fn = partial(noisy_templates, sigma=sigma)
        else:
            raise RuntimeError(f"Unknown 'noise_type' entry: '{noise_type}'")


    def _launchTrial(self, ind):
        nbNodes = ind.nb_nodes if hasattr(ind, "nb_nodes") else self.config['nbNodes']
        base_configuration_entry = ind.base_configuration_entry if hasattr(ind, "base_configuration_entry") else "daccad"
        #print("base_configuration_entry: ", base_configuration_entry)

        all_scores = []
        for i in range(self.nb_repeats):
            try:
                new_ind = self.noise_fn(copy.deepcopy(ind))
                #print("DEBUGind: ", new_ind)
                scores = self._launchDACCADExpe(new_ind, base_configuration_entry)
            except Exception as e:
                warnings.warn(f"Evaluation failed: {str(e)}")
                traceback.print_exc()
                scores = None
            if scores is None:
                all_scores = None
                break

            # Compute means
            nEvaluations = scores['nEvaluations']
            scores['ratioIn'] = np.mean([scores["in%i_0" % x] for x in range(nEvaluations)])
            scores['ratioOut'] = np.mean([scores["out%i_0" % x] for x in range(nEvaluations)])
            scores['score'] = np.mean([scores["fitness%i_0" % x] for x in range(nEvaluations)])
            hellingerScores = [scores["hellinger%i_0" % x] for x in range(nEvaluations)]
            scores['meanHellinger'] = np.mean([scores["hellinger%i_0" % x] for x in range(nEvaluations)])
            normalisedHellingerScores = [1. - (1. / (1. + math.exp(-20.*(x-0.8)))) for x in hellingerScores]
            scores['normalisedMeanHellinger'] = np.mean(normalisedHellingerScores)
            scores['diffInOut'] = scores['ratioIn'] - scores['ratioOut']
            #scores['scoreN'] = min(1., scores['score'] + (0.0 if scores['ratioIn'] == 0. else 0.000001) )
            scores['nTemplateGenome'] = sum(np.array(new_ind[nbNodes:])>=0.005)
            fitnessesList = [scores["fitness%i_0" % x] for x in range(nEvaluations)]
            scores['medianScore'] = sorted(fitnessesList)[int(len(fitnessesList)/2 - 1 + len(fitnessesList) % 2)]
            scores['maxScore'] = np.max([scores["fitness%i_0" % x] for x in range(nEvaluations)])
            scores['minScore'] = np.min([scores["fitness%i_0" % x] for x in range(nEvaluations)])
            scores['madScore'] = mad([scores["fitness%i_0" % x] for x in range(nEvaluations)])
            scores['nbNodes'] = nbNodes
            scores['nbUsedNodes'] = submitDACCAD.nbUsedNodes(new_ind, nbNodes)

            otherFitnessIdx = 0
            while True:
                if f"fitness0_{otherFitnessIdx}" not in scores:
                    break
                otherFit = [scores[f"fitness{x}_{otherFitnessIdx}"] for x in range(nEvaluations)]
                scores[f"medianScore{otherFitnessIdx}"] = sorted(otherFit)[int(len(otherFit)/2 - 1 + len(otherFit) % 2)]
                scores[f"ratioIn{otherFitnessIdx}"] = np.mean([scores[f"in{x}_{otherFitnessIdx}"] for x in range(nEvaluations)])
                otherFitnessIdx += 1

            if hasattr(ind, "activations"):
                scores['nbActivations'] = sum(ind.activations.flatten() >= 0.005)
            if hasattr(ind, "inhibitions"):
                scores['nbInhibitions'] = sum(ind.inhibitions.flatten() >= 0.005)
            all_scores.append(scores)


        # Set features and fitness
        features_list, fitness_from_feature = self._get_features_list()
        if all_scores is None:
            features = [0.0 for x in features_list]
            fitness = 0.0
            global_scores = None
        else:
            # Compute global mean scores over ``nb_repeats`` repeats
            global_scores = {}
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                for k in all_scores[0].keys():
                    #print("DEBUG", k, [x[k] for x in all_scores])
                    global_scores['mean_' + k] = np.nanmean([num_or_nan(x[k]) for x in all_scores])
                    global_scores['std_' + k] = np.nanstd([num_or_nan(x[k]) for x in all_scores])
            global_scores['nbNodes'] = nbNodes
            global_scores['nbUsedNodes'] = submitDACCAD.nbUsedNodes(ind, nbNodes)
            global_scores['nTemplateGenome'] = sum(np.array(ind[nbNodes:])>=0.005)
            global_scores['nTemplate'] = all_scores[0]['nTemplate']
            features = [global_scores[x] for x in features_list]
            fitness = global_scores[fitness_from_feature]

        #print("DEBUGpp", scores)
        #print("DEBUGpp", features_list, features, fitness, scores)
        #print("  {:<20f}".format(fitness), scores)

        ind.scores = global_scores
        ind.all_scores = all_scores
        #print(fitness, features, self.config['fitness_domain'], self.config['features_domain'])
        ind.fitness.values = [fitness]
        ind.features = features
        return ind



class IlluminateDACCADWithSuggestions(IlluminateDACCAD):
    def __init__(self, config_filename, parallelism_type = "concurrent", seed = None, base_config = None):
        super().__init__(config_filename, parallelism_type, seed, base_config)
        self.suggestions_gen_fn = self.algo.suggestions_gen_fn if hasattr(self.algo, "suggestions_gen_fn") else None # XXX self ?

    def _compute_scores(self, ind, scores):
        nbNodes = ind.nb_nodes if hasattr(ind, "nb_nodes") else self.config['nbNodes']
        features_list, fitness_from_feature = self._get_features_list()
        if scores is None:
            features = [0.0 for x in features_list]
            fitness = 0.0
        else:
            # Compute means
            nEvaluations = scores['nEvaluations'] if 'nEvaluations' in scores else 1
            scores['ratioIn'] = np.mean([scores["in%i_0" % x] for x in range(nEvaluations)])
            scores['ratioOut'] = np.mean([scores["out%i_0" % x] for x in range(nEvaluations)])
            scores['score'] = np.mean([scores["fitness%i_0" % x] for x in range(nEvaluations)])
            hellingerScores = [scores["hellinger%i_0" % x] for x in range(nEvaluations)]
            scores['meanHellinger'] = np.mean([scores["hellinger%i_0" % x] for x in range(nEvaluations)])
            normalisedHellingerScores = [1. - (1. / (1. + math.exp(-20.*(x-0.8)))) for x in hellingerScores]
            scores['normalisedMeanHellinger'] = np.mean(normalisedHellingerScores)
            scores['diffInOut'] = scores['ratioIn'] - scores['ratioOut']
            #scores['scoreN'] = min(1., scores['score'] + (0.0 if scores['ratioIn'] == 0. else 0.000001) )
            scores['nTemplateGenome'] = sum(np.array(ind[nbNodes:])>=0.005)
            fitnessesList = [scores["fitness%i_0" % x] for x in range(nEvaluations)]
            scores['medianScore'] = sorted(fitnessesList)[int(len(fitnessesList)/2 - 1 + len(fitnessesList) % 2)]
            scores['maxScore'] = np.max([scores["fitness%i_0" % x] for x in range(nEvaluations)])
            scores['minScore'] = np.min([scores["fitness%i_0" % x] for x in range(nEvaluations)])
            scores['madScore'] = mad([scores["fitness%i_0" % x] for x in range(nEvaluations)])
            scores['nbNodes'] = nbNodes
            scores['nbUsedNodes'] = submitDACCAD.nbUsedNodes(ind, nbNodes)

            otherFitnessIdx = 0
            while True:
                if f"fitness0_{otherFitnessIdx}" not in scores:
                    break
                otherFit = [scores[f"fitness{x}_{otherFitnessIdx}"] for x in range(nEvaluations)]
                scores[f"medianScore{otherFitnessIdx}"] = sorted(otherFit)[int(len(otherFit)/2 - 1 + len(otherFit) % 2)]
                scores[f"ratioIn{otherFitnessIdx}"] = np.mean([scores[f"in{x}_{otherFitnessIdx}"] for x in range(nEvaluations)])
                otherFitnessIdx += 1

            if hasattr(ind, "activations"):
                scores['nbActivations'] = sum(ind.activations.flatten() >= 0.005)
            if hasattr(ind, "inhibitions"):
                scores['nbInhibitions'] = sum(ind.inhibitions.flatten() >= 0.005)

            # Set features and fitness
            features = [scores[x] if x in scores else np.nan for x in features_list]
            fitness = scores[fitness_from_feature] if fitness_from_feature in scores else np.nan
        return fitness, features


    def _launchTrial(self, ind):
        nbNodes = ind.nb_nodes if hasattr(ind, "nb_nodes") else self.config['nbNodes']
        base_configuration_entry = ind.base_configuration_entry if hasattr(ind, "base_configuration_entry") else "daccad"
        #print("base_configuration_entry: ", base_configuration_entry)

        expeId = ind.name
        bioneatBasePath = self.config.get('bioneatBasePath') or "../../bioneat"
        penSystemLaunchScript = self.config.get('penSystemLaunchScript') or "build/dist/start.sh"
        penSystemLaunchClass = self.config.get('penSystemLaunchClass') or 'use.EvaluateIndividualWithMutationSuggestions'
        penSystemBaseDir = os.path.join(self.config['dataDir'])
        filenameSuffix = f"{self.hostname}_{self.instance_name}_{expeId}"
        suggestions_duration = self.config.get('suggestionsDuration') or 10

        # Verify if daccad configuration file generation is needed
        enableDaccadConfigFileGeneration = "daccad" in self.config
        if enableDaccadConfigFileGeneration:
            penSystemConfigFile = os.path.join(penSystemBaseDir, "daccadConfig_%s.json" % filenameSuffix)
            # Generate a daccad config file
            daccadConfig = {**self.config[base_configuration_entry]}
            with open(penSystemConfigFile, "w") as f:
                for k,v in daccadConfig.items():
                    f.write("%s = %s\n" % (k, str(v)))
        else:
            penSystemConfigFile = self.config.get('penSystemConfigFile') or 'config/configEvaluation.config'

        # Create DACCADLauncher
        launcher = submitDACCAD.DACCADLauncher(executablePath=bioneatBasePath, launchScript=penSystemLaunchScript, launchClass=penSystemLaunchClass, configFile=penSystemConfigFile, baseDir=penSystemBaseDir)

        try:
            scores = self._launchDACCADExpe(ind, launcher, base_configuration_entry)
        except Exception as e:
            warnings.warn(f"Evaluation failed: {str(e)}")
            traceback.print_exc()
            scores = None

        fitness, features = self._compute_scores(ind, scores)
        ind.scores = scores

        # Get suggestions and compute them
        if self.suggestions_gen_fn is not None:
            sugg_lst = self.suggestions_gen_fn(ind) # TODO fn also provides the duration of each suggestion
        else:
            sugg_lst = []
        sugg_res = []
        for s in sugg_lst:
            try:
                s_scores = self._launchDACCADExpe(s, launcher, base_configuration_entry, is_suggestion=True, duration=suggestions_duration)
            except Exception as e:
                warnings.warn(f"Evaluation failed: {str(e)}")
                traceback.print_exc()
                s_scores = None
            s_fit, s_feat = self._compute_scores(s, s_scores)
            s.scores = s_scores
            s.fitness.values = [s_fit]
            s.features = s_feat
            sugg_res.append(s)
        # Add suggestions and their scores in ``ind``
        ind.suggestions = sugg_res

        # Stop DACCADLauncher
        launcher.stop()

        keepBuggedFiles = self.config.get("keepBuggedFiles") or False
        keepTemporaryFiles = self.config.get("keepTemporaryFiles") or False
        if not keepBuggedFiles or (scores is not None and keepTemporaryFiles is False):
            if enableDaccadConfigFileGeneration:
                self._removeTmpFiles([penSystemConfigFile])

        #print(fitness, features, self.config['fitness_domain'], self.config['features_domain'])
        #return [[fitness], features]
        ind.fitness.values = [fitness]
        ind.features = features
        return ind


    def _launchDACCADExpe(self, ind, launcher, base_configuration_entry = "daccad", is_suggestion = False, duration = 10):
        expeId = ind.name
        nbNodes = ind.nb_nodes if hasattr(ind, "nb_nodes") else self.config['nbNodes']
        penSystemBaseDir = os.path.join(self.config['dataDir'])
        filenameSuffix = f"{self.hostname}_{self.instance_name}_{expeId}"
        jsonFileName = os.path.join(penSystemBaseDir, "generatedGraph_%s.json" % filenameSuffix)
        keepBuggedFiles = self.config.get("keepBuggedFiles") or False
        keepTemporaryFiles = self.config.get("keepTemporaryFiles") or False

        #print("DEBUGnbnodes", nbNodes)
        normalisedInd = np.array(copy.deepcopy(ind))
        normalisedInd[:ind.nb_nodes] *= 1000.
        normalisedInd[ind.nb_nodes:] *= 200.
        #normalisedInd[normalisedInd<=1.] = 0.

        features_list, fitness_from_feature = self._get_features_list()

        if len(np.where(normalisedInd[ind.nb_nodes:]>0.)[0]) == 0:
            # XXX
            print("DEBUG: INDIVIDUAL WITH NO TEMPLATES: ", ind)
            scores = None
        else:
            if is_suggestion:
                scores = launcher.eval(normalisedInd, nbNodes, jsonFileName, duration)
            else:
                scores = launcher.start(normalisedInd, nbNodes, jsonFileName)

        #print("DEBUG: ", jsonFileName)
        # Remove files
        if not keepBuggedFiles or (scores is not None and keepTemporaryFiles is False):
            self._removeTmpFiles([jsonFileName])
        return scores




########## MAIN ########### {{{1
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--configFilename', type=str, default='conf/test.yaml', help = "Path of configuration file")
    parser.add_argument('--runs', type=int, default=1, help = "Number of runs")
    parser.add_argument('-p', '--parallelismType', type=str, default='concurrent', help = "Type of parallelism to use")
    parser.add_argument('--seed', type=int, default=None, help="Numpy random seed")
    args = parser.parse_args()

    configFilename = args.configFilename
    config = yaml.safe_load(open(configFilename))

    for _ in range(args.runs):
        try:
            if config.get('expeType') == "noisyDaccad":
                ill = IlluminateNoisyDACCAD(args.configFilename, args.parallelismType, seed=args.seed)
            elif config.get('expeType') == "daccadWithSuggestions":
                ill = IlluminateDACCADWithSuggestions(args.configFilename, args.parallelismType, seed=args.seed)
            else:
                ill = IlluminateDACCAD(args.configFilename, args.parallelismType, seed=args.seed)
                #ill = IlluminationExperiment(args.configFilename, args.parallelismType, seed=args.seed)
            print("Using configuration file '%s'. Instance name: '%s'" % (args.configFilename, ill.instance_name))
            ill.run()
        except Exception as e:
            warnings.warn(f"Run failed: {str(e)}")
            traceback.print_exc()


# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
