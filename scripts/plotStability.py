#!/usr/bin/env python3


########## IMPORTS ########### {{{1

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

import os
import pathlib
from timeit import default_timer as timer
import copy
import yaml
import numpy as np
import random
import warnings
import traceback

from illuminate import *

import scipy.constants
import pandas as pd
import seaborn as sns
import matplotlib.ticker as ticker
sns.set(font_scale = 2.1)
sns.set_style("ticks")




########## MAIN ########### {{{1
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--inputFile', type=str, default='conf/final.p', help = "Path of final data file")
    parser.add_argument('-o', '--outputDir', type=str, default='./', help = "Path of resulting files")
    parser.add_argument('-r', '--nbRetrials', type=int, default=50, help = "Number of retrials")
    args = parser.parse_args()

    with open(args.inputFile, "rb") as f:
        data = pickle.load(f)

    container = data['container']
    inds = list(container.items)
    scores = [ind.scores for ind in inds]

    print(scores[-1])
    nb_retrials = int(args.nbRetrials)

    stddevs_vals = [s['standardDeviation'] for s in scores]
    mad_vals = [s['madScore'] for s in scores]
    nTemplate_vals = [s['nTemplate'] for s in scores]
    mean_vals = [s['score'] for s in scores]
    median_vals = [s['medianScore'] for s in scores]
    fitness_vals = [[s[f'fitness{i}'] for i in range(nb_retrials)] for s in scores]

    # Compute stats discarding non-trivial individuals
    nt_scores = [ind.scores for ind in inds if ind.scores['medianScore'] > 0.]
    nt_stddevs_vals = [s['standardDeviation'] for s in nt_scores]
    nt_mad_vals = [s['madScore'] for s in nt_scores]
    nt_nTemplate_vals = [s['nTemplate'] for s in nt_scores]
    nt_mean_vals = [s['score'] for s in nt_scores]
    nt_median_vals = [s['medianScore'] for s in nt_scores]
    nt_fitness_vals = [[s[f'fitness{i}'] for i in range(nb_retrials)] for s in nt_scores]

    # Plot histograms of std-dev
    fig = plt.figure(figsize=(3.*scipy.constants.golden, 3.))
    plt.hist(stddevs_vals, 10, facecolor='k')
    plt.xlabel("std-dev", fontsize=18)
    plt.ylabel("Occurrence", fontsize=18)
    plt.tight_layout()
    fig.savefig(os.path.join(args.outputDir, "hist-stddev.pdf"))
    plt.close(fig)

    # Plot histograms of MAD
    fig = plt.figure(figsize=(3.*scipy.constants.golden, 3.))
    plt.hist(mad_vals, 10, facecolor='k')
    plt.xlabel("MAD", fontsize=18)
    plt.ylabel("Occurrence", fontsize=18)
    plt.tight_layout()
    fig.savefig(os.path.join(args.outputDir, "hist-medianAD.pdf"))
    plt.close(fig)

    # Plot histograms of nTemplates
    fig = plt.figure(figsize=(3.*scipy.constants.golden, 3.))
    plt.hist(nTemplate_vals, 10, facecolor='k')
    plt.xlabel("Number of templates", fontsize=18)
    plt.ylabel("Occurrence", fontsize=18)
    plt.tight_layout()
    fig.savefig(os.path.join(args.outputDir, "hist-nTemplates.pdf"))
    plt.close(fig)

    # Plot histograms of medianScore
    fig = plt.figure(figsize=(3.*scipy.constants.golden, 3.))
    plt.hist(median_vals, 10, facecolor='k')
    plt.xlabel("Median Score", fontsize=18)
    plt.ylabel("Occurrence", fontsize=18)
    plt.tight_layout()
    fig.savefig(os.path.join(args.outputDir, "hist-medianScore.pdf"))
    plt.close(fig)


    # Plots histograms 2D 
    fig = sns.jointplot(nTemplate_vals, median_vals).plot_joint(sns.kdeplot, zorder=0, n_levels=6)
    fig.set_axis_labels('Number of templates', 'Median Score', fontsize=18)
    fig.savefig(os.path.join(args.outputDir, "joint-nTemplate-medianScore.pdf"))
    fig = sns.jointplot(median_vals, stddevs_vals).plot_joint(sns.kdeplot, zorder=0, n_levels=6)
    fig.set_axis_labels('Median Score', 'std-dev', fontsize=18)
    fig.savefig(os.path.join(args.outputDir, "joint-medianScore-stddev.pdf"))
    fig = sns.jointplot(median_vals, mad_vals).plot_joint(sns.kdeplot, zorder=0, n_levels=6)
    fig.set_axis_labels('Median Score', 'MAD', fontsize=18)
    fig.savefig(os.path.join(args.outputDir, "joint-medianScore-medianAD.pdf"))


    # Plots histograms 2D non-trivials
    fig = sns.jointplot(nt_nTemplate_vals, nt_median_vals, kind="hex")
    fig.set_axis_labels('Number of templates', 'Median Score of non-trivial ind.', fontsize=18)
    fig.savefig(os.path.join(args.outputDir, "joint-nTemplate-nonTrivialMedianScore.pdf"))
    fig = sns.jointplot(nt_median_vals, nt_stddevs_vals, kind="hex")
    fig.set_axis_labels('Median Score', 'std-dev of non-trivial ind.', fontsize=18)
    fig.savefig(os.path.join(args.outputDir, "joint-medianScore-nonTrivial-stddev.pdf"))
    fig = sns.jointplot(nt_median_vals, nt_mad_vals, kind="hex")
    fig.set_axis_labels('Median Score', 'MAD of non-trivial ind.', fontsize=18)
    fig.savefig(os.path.join(args.outputDir, "joint-medianScore-nonTrivialMedianAD.pdf"))



    # Generate non-trivial subparts
    subparts = []
    stddevs_subparts = []
    size_subparts = []
    dist_mean_to_subpartsmean = []
    dist_median_to_subpartsmedian = []
    for j in range(len(fitness_vals)):
        if median_vals[j] > 0.0:
            fit = fitness_vals[j]
            for i in range(200): # XXX
                size = np.random.randint(2, nb_retrials)
                subpart = copy.copy(fit)
                np.random.shuffle(subpart)
                subpart = subpart[:size]
                subparts.append(subpart)
                size_subparts.append(len(subpart))
                stddevs_subparts.append(np.std(subpart))
                dist_mean_to_subpartsmean.append(abs(np.mean(subpart) - mean_vals[j]))
                dist_median_to_subpartsmedian.append(abs(np.median(subpart) - median_vals[j]))

#    # Generate subparts
#    subparts = []
#    stddevs_subparts = []
#    size_subparts = []
#    for fit in fitness_vals:
#        for i in range(100): # XXX
#            size = np.random.randint(1, nb_retrials)
#            subpart = copy.copy(fit)
#            np.random.shuffle(subpart)
#            subpart = subpart[:size]
#            subparts.append(subpart)
#            size_subparts.append(len(subpart))
#            stddevs_subparts.append(np.std(subpart))



    # Plot std-dev wrt nb-retrials
    fig = sns.jointplot(size_subparts, stddevs_subparts, kind="hex")
    fig.set_axis_labels('Number of retrials', 'std-dev', fontsize=18)
    fig.savefig(os.path.join(args.outputDir, "joint-size_subparts-stddevs_subparts.pdf"))
    #fig = sns.jointplot(size_subparts, dist_mean_to_subpartsmean, kind="hex", ylim=[0., 0.060])
    fig = sns.jointplot(size_subparts, dist_mean_to_subpartsmean, kind="hex")
    fig.set_axis_labels('Number of retrials', 'Dist. sub-part mean to oracle mean', fontsize=18)
    fig.savefig(os.path.join(args.outputDir, "joint-size_subparts-dist_mean_to_subpartsmean.pdf"))

    #fig = sns.jointplot(size_subparts, dist_median_to_subpartsmedian, kind="hex", ylim=[0., 0.060], joint_kws=dict(gridsize=40), marginal_kws=dict(bins=10, rug=True))
    fig = sns.jointplot(size_subparts, dist_median_to_subpartsmedian, kind="hex", ylim=[0., 0.070], joint_kws=dict(gridsize=40), marginal_kws=dict(bins=10, rug=True))
    fig.set_axis_labels('Number of retrials', "Dist. sub-part median\nto oracle median")
    #plt.sca(fig.ax_joint)
    fig.ax_joint.axvline(5, color='k', lw=1, ls=':')
    fig.ax_joint.xaxis.set_major_locator(ticker.FixedLocator([5, 25, 49]))
    fig.ax_joint.xaxis.set_major_formatter(ticker.ScalarFormatter())
    fig.savefig(os.path.join(args.outputDir, "joint-size_subparts-dist_median_to_subpartsmedian.pdf"))
    #fig = plt.figure(figsize=(3.*scipy.constants.golden, 3.))
    #print(size_subparts)
    #print(stddevs_subparts)
    #plt.plot(size_subparts, stddevs_subparts, color='k')
    #plt.tight_layout()
    #fig.savefig(os.path.join(args.outputDir, "plot-size_subparts-stddevs_subparts.pdf"))
    #plt.close(fig)



# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
