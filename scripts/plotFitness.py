#!/usr/bin/env python3


########## IMPORTS ########### {{{1

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

import os
import pathlib
from timeit import default_timer as timer
import copy
import yaml
import numpy as np
import random
import warnings
import traceback

from illuminate import *

sys.modules["SCOOP_WORKER"] = sys.modules["__main__"]

import scipy.constants
import pandas as pd
import seaborn as sns
import matplotlib.ticker as ticker
sns.set_style("ticks")


# https://stackoverflow.com/questions/47581672/replacement-for-deprecated-tsplot
def tsplot(ax, data,**kw):
    x = np.arange(data.shape[1])
    med = np.median(data, axis=0)
    min_ = np.min(data, axis=0)
    max_ = np.max(data, axis=0)
    q25 = np.percentile(data, 25, axis=0)
    q75 = np.percentile(data, 75, axis=0)
    #sd = np.std(data, axis=0)
    #ax.fill_between(x,cis[0],cis[1],alpha=0.4, **kw)
    ax.fill_between(x,min_,max_,alpha=0.2, **kw)
    ax.fill_between(x,q25,q75,alpha=0.4, **kw)
    ax.plot(x,med,**kw)
    ax.margins(x=0)



########## MAIN ########### {{{1
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--inputDir', type=str, default='results/testDACCAD', help="Directory of results data files")
    parser.add_argument('--nbRuns', type=int, default=12, help="Number of runs to take into account or 0 for all")
    parser.add_argument('--nbIterations', type=int, default=50, help="Number of iterations to take into account")
    parser.add_argument('--nbEvalsPerIt', type=int, default=50, help="Number of evaluations per iteration")
    parser.add_argument('-o', '--outputFile', type=str, default="fitness.pdf", help="Path of the output file")
    parser.add_argument('--min', type=float, default=0.0, help="Min fitness value")
    parser.add_argument('--max', type=float, default=1.0, help="Max fitness value")
    args = parser.parse_args()

    nb_runs = args.nbRuns
    nb_iterations = args.nbIterations

    data_lst = []
    for f_name in os.listdir(args.inputDir):
        if f_name.startswith("final_") and f_name.endswith(".p"):
            #print(f_name)
            with open(os.path.join(args.inputDir, f_name), "rb") as f:
                d = pickle.load(f)
            data_lst.append(d)

    if nb_runs > 0:
        data_lst = data_lst[:args.nbRuns]


    #### Fitnesses wrt evals

    # Find length of fitness
    length_fitness = 1 #len(data_lst[0]['iterations']['max'][0])
    for val in data_lst[0]['iterations']['max']:
        if np.isnan(val):
            continue
        else:
            length_fitness = len(val)
            break
    #print(length_fitness)

    maxs = np.zeros((length_fitness, nb_runs, nb_iterations))
    for i, d in enumerate(data_lst):
        for j in range(min(nb_iterations, len(d['iterations']['max']))):
            m = d['iterations']['max'][j]
            if isinstance(m, Iterable):
                maxs[:, i, j] = m
            elif np.isnan(m):
                maxs[:, i, j] = np.zeros(length_fitness)

#    print(maxs[0])
    #if args.outputFile is not None and len(args.outputFile) > 0:
    #    np.savetxt(args.outputFile, maxs)

    fig = plt.figure(figsize=(3.*scipy.constants.golden, 3.))
    ax = fig.add_subplot(111)
    fig.subplots_adjust(bottom=0.3)
    df = pd.DataFrame(maxs[0])
    #print(df)

    x = np.arange(0, nb_iterations+1, 30)
    if len(x) > 4:
        x = x[::2]
    #sns.tsplot(data=df.values, ax=ax, ci="sd")
    tsplot(ax, maxs[0], color='k')
    ax.set_ylim([args.min, args.max])

    plt.xlabel("Evaluations", fontsize=18)
    plt.xticks(fontsize=18)
    plt.xticks(x, fontsize=18)
    ax.set_xticklabels([str(i * args.nbEvalsPerIt) for i in x])
    plt.ylabel("Fitness", fontsize=18)
    plt.yticks(fontsize=18)

    #ax.xaxis.set_major_locator(ticker.MultipleLocator(30))
    #ax.xaxis.set_major_formatter(ticker.ScalarFormatter())
    ax.yaxis.set_major_locator(ticker.MultipleLocator(0.2))
    ax.yaxis.set_major_formatter(ticker.ScalarFormatter())

    sns.despine()
    #plt.tight_layout(rect=[0, 0, 1.0, 0.95])
    plt.tight_layout()
    fig.savefig(args.outputFile)
    plt.close(fig)


# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
