#!/usr/bin/env python3


########## IMPORTS ########### {{{1
from illuminate import *
import pickle
import warnings
import traceback


def reeval_ind(ind, configFilename, outputDir, runs = 1, parallelismType="concurrent", seed=None, bioneatBasePath="/data/prj/dna/bioneat"):
    # Init base config for reevaluations
    reeval_config = {}
    reeval_config['keepBuggedFiles'] = True
    reeval_config['keepTemporaryFiles'] = True
    reeval_config["dataDir"] = os.path.abspath(outputDir)
    reeval_config['resultsBaseDir'] = os.path.abspath(outputDir)
    reeval_config['bioneatBasePath'] = os.path.abspath(bioneatBasePath)

    for _ in range(runs):
        try:
            ill = IlluminateDACCAD(configFilename, parallelismType, seed=seed, base_config = reeval_config)
            print("Using configuration file '%s'. Instance name: '%s'" % (configFilename, ill.instance_name))
            res = ill._launchTrial(ind)
            print(res)
        except Exception as e:
            warnings.warn(f"Run failed: {str(e)}")
            traceback.print_exc()



########## MAIN ########### {{{1
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    #parser.add_argument('-c', '--configFilename', type=str, default='conf/test.yaml', help = "Path of configuration file")
    parser.add_argument('-d', '--dir', type=str, default='results/testDACCAD', help="Directory of results data files")
    parser.add_argument('-o', '--outputFile', type=str, default="", help="Path of the output file")
    parser.add_argument('--performancesPlotFile', type=str, default="", help="Path of the performance plot file")
    #parser.add_argument('--nbRuns', type=int, default=12, help="Number of runs to take into account or 0 for all")
    args = parser.parse_args()

    #with open(args.inputFile, "rb") as f:
    #    data = pickle.load(f)
    #best = data['container'].best
    #reeval_ind(best, args.configFilename, args.outputDir, args.runs, args.parallelismType, args.seed, args.bioneatBasePath)



    data_lst = []
    for f_name in os.listdir(args.dir):
        if f_name.startswith("final_") and f_name.endswith(".p"):
            print(f_name)
            with open(os.path.join(args.dir, f_name), "rb") as f:
                d = pickle.load(f)
            data_lst.append(d)

#    if nb_runs > 0:
#        data_lst = data_lst[:args.nbRuns]

    combined_grid = copy.deepcopy(data_lst[0]['container'])
    for i in range(1, len(data_lst)):
        combined_grid.update(data_lst[i]['container'])

    print(combined_grid.summary())

    # Save in pickle file
    res_dict = {"container": combined_grid}
    with open(args.outputFile, "wb") as f:
        pickle.dump(res_dict, f)


    # Create plot of the performance grid
    if len(args.performancesPlotFile) > 0:
        plot_path = args.performancesPlotFile
        quality = combined_grid.quality_array[(slice(None),) * (len(combined_grid.quality_array.shape) - 1) + (0,)]
        plotGridSubplots(quality, plot_path, plt.get_cmap("nipy_spectral"), combined_grid.features_domain, combined_grid.fitness_domain[0], nbTicks=None)
        print("\nA plot of the performance grid was saved in '%s'." % os.path.abspath(plot_path))

    ## Create plot of the activity grid
    #plot_path = os.path.join(self.log_base_path, f"activityGrid-{self.instance_name}.pdf")
    #plotGridSubplots(grid.activity_per_bin, plot_path, plt.get_cmap("nipy_spectral"), grid.features_domain, [0, np.max(grid.activity_per_bin)], nbTicks=None)
    #print("\nA plot of the activity grid was saved in '%s'." % os.path.abspath(plot_path))



# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
