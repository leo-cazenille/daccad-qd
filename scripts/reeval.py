#!/usr/bin/env python3


########## IMPORTS ########### {{{1
from illuminate import *
import pickle
import warnings
import traceback


def reeval_ind(ind, configFilename, outputDir, runs = 1, parallelismType="concurrent", seed=None, bioneatBasePath="/data/prj/dna/bioneat"):
    # Init base config for reevaluations
    reeval_config = {}
    reeval_config['keepBuggedFiles'] = True
    reeval_config['keepTemporaryFiles'] = True
    reeval_config["dataDir"] = os.path.abspath(outputDir)
    reeval_config['resultsBaseDir'] = os.path.abspath(outputDir)
    reeval_config['bioneatBasePath'] = os.path.abspath(bioneatBasePath)

    for _ in range(runs):
        try:
            ill = IlluminateDACCAD(configFilename, parallelismType, seed=seed, base_config = reeval_config)
            print("Using configuration file '%s'. Instance name: '%s'" % (configFilename, ill.instance_name))
            res = ill._launchTrial(ind)
            print(res)
            print(res.scores['medianScore'])
            #print(res.scores)
        except Exception as e:
            warnings.warn(f"Run failed: {str(e)}")
            traceback.print_exc()
    return res



########## MAIN ########### {{{1
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--configFilename', type=str, default='conf/test.yaml', help = "Path of configuration file")
    parser.add_argument('-i', '--inputFile', type=str, default='conf/final.p', help = "Path of final data file")
    parser.add_argument('-o', '--outputDir', type=str, default='reevals/', help = "Path of resulting files")
    parser.add_argument('--runs', type=int, default=1, help = "Number of runs")
    parser.add_argument('-p', '--parallelismType', type=str, default='concurrent', help = "Type of parallelism to use")
    parser.add_argument('--seed', type=int, default=None, help="Numpy random seed")
    parser.add_argument('--bioneatBasePath', type=str, default='/data/prj/dna/bioneat/', help = "Bioneat Base Path")
    parser.add_argument('--listScores', action='store_true')
    args = parser.parse_args()

    input_files = []
    if os.path.isdir(args.inputFile):
        for filename in os.listdir(args.inputFile):
            if filename.endswith(".p"):
                input_files.append(os.path.join(args.inputFile, filename))
    else:
        input_files.append(args.inputFile)

    if args.listScores:
        allScores = []
        for filename in input_files:
            with open(filename, "rb") as f:
                data = pickle.load(f)
            #print(f"\n# Evaluating individuals from '{filename}'.")
            inds = list(data['container'])
            for ind in inds:
                res = reeval_ind(ind, args.configFilename, args.outputDir, args.runs, args.parallelismType, args.seed, args.bioneatBasePath)
                allScores.append(res.scores['medianScore'])
        print(f"Final scores: {allScores}")

    else:
        for filename in input_files:
            with open(filename, "rb") as f:
                data = pickle.load(f)
            print(f"\n# Evaluating best from '{filename}'.")
            best = data['container'].best
            reeval_ind(best, args.configFilename, args.outputDir, args.runs, args.parallelismType, args.seed, args.bioneatBasePath)


# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
