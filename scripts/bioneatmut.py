#!/usr/bin/env python3

# DEBUGGING
import faulthandler
faulthandler.enable()


########## IMPORTS ########### {{{1

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

import os
import pathlib
import datetime
from timeit import default_timer as timer
import copy
import itertools
from functools import partial
import yaml
import numpy as np
import random
import warnings
import traceback
import math
import socket

import submitDACCAD

from qdpy.algorithms import *
from qdpy.containers import *
#from qdpy.benchmarks import *
from qdpy.plots import *
from qdpy.base import *
from qdpy import tools

from typing import Optional, Tuple, List, Iterable, Iterator, Any, TypeVar, Generic, Union, Sequence, MutableSet, MutableSequence, Type, Callable, Generator, Mapping, MutableMapping, overload
import qdpy.hdsobol as hdsobol
from base import *
from species import *
from batch import *





@registry.register
class DaccadBioneatMut(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [3, 7],
            min_init_budget: int = 1,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            prob_parameter_mut: float = 0.96,
            prob_template_add: float = 0.01,
            prob_template_del: float = 0.01,
            prob_signal_species_add: float = 0.01,
            prob_inhibition_species_add: float = 0.01,
            nbConnActivationsDomain: Sequence[int] = [1, 7],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [1, 13],
            f1: float = 0.2,
            f2: float = 2.0,
            mut_pb: float = 0.8,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.prob_parameter_mut = prob_parameter_mut
        self.prob_template_add = prob_template_add
        self.prob_template_del = prob_template_del
        self.prob_signal_species_add = prob_signal_species_add
        self.prob_inhibition_species_add = prob_inhibition_species_add
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.f1 = f1
        self.f2 = f2
        self.mut_pb = 0.8

        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=gen_daccad_individuals(self.ind_domain), **kwargs)


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        if initialise: # Initialisation
            # Create a base individual
            standard_init_ind(base_ind)
            return base_ind, False

        else: # Selection
            try:
                choice = np.random.choice(2)
                if choice == 0:
                    res = tools.non_trivial_sel_random(collection), True
                else:
                    res = tools.sel_random(collection), True
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()
            return res


    def _vary(self, individual):
        for _ in range(500): # XXX
            ind = copy.deepcopy(individual)
            ind.name = str(id(ind))

            active_activations_coords = list(zip(*np.where(ind.activations)))
            nb_active_activations = len(active_activations_coords)
            active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
            nb_active_inhibitions = len(active_inhibitions_coords)

            # Choose the kind of mutation
            choice = np.random.choice(5, p=(self.prob_parameter_mut,
                self.prob_template_add, self.prob_template_del,
                self.prob_signal_species_add, self.prob_inhibition_species_add))

            # Perform the mutation
            if choice == 0: # Parameters mutation
                #mutation_param_polybounded(ind, self.eta, self.mutationPb)
                #mutation_param_bioneat(ind, self.f1, self.f2, 1)
                mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
            elif choice == 1: # Add Activation
                mutation_add_activation(ind)
            elif choice == 2: # Del Activation
                mutation_del_activation(ind)
            elif choice == 3: # Signal species
                mutation_bioneat_signal_species(ind)
            elif choice == 4: # Inhibition species
                mutation_bioneat_inhibition_species(ind)


            ## Verify if there are no orphaned nodes
            #is_orphaned = (not(any(ind.activations[:,i]) or any(ind.activations[i,:]) or any(ind.inhibitions[i,:,:].flatten())) for i in range(ind.nb_nodes))
            #if any(is_orphaned):
            #    continue

            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                active_activations_coords = list(zip(*np.where(ind.activations)))
                nb_active_activations = len(active_activations_coords)
                active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
                nb_active_inhibitions = len(active_inhibitions_coords)
                if nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        return ind




@registry.register
class DaccadBioneatMut2(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [3, 7],
            min_init_budget: int = 1,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            prob_parameter_mut: float = 0.96,
            prob_template_add: float = 0.01,
            prob_template_del: float = 0.01,
            prob_signal_species_add: float = 0.01,
            prob_inhibition_species_add: float = 0.01,
            nbConnActivationsDomain: Sequence[int] = [1, 7],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [1, 13],
            f1: float = 0.2,
            f2: float = 2.0,
            mut_pb: float = 0.8,
            max_bins_before_non_trivial_found: int = 20,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.prob_parameter_mut = prob_parameter_mut
        self.prob_template_add = prob_template_add
        self.prob_template_del = prob_template_del
        self.prob_signal_species_add = prob_signal_species_add
        self.prob_inhibition_species_add = prob_inhibition_species_add
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.f1 = f1
        self.f2 = f2
        self.mut_pb = mut_pb
        self.max_bins_before_non_trivial_found = max_bins_before_non_trivial_found

        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=gen_daccad_individuals(self.ind_domain), **kwargs)


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        if initialise: # Initialisation
            # Create a base individual
            standard_init_ind(base_ind)
            return base_ind, False

        else: # Selection
            try:
                choice = np.random.choice(2)
                if choice == 0:
                    res = tools.non_trivial_sel_random(collection), True
                else:
                    res = tools.sel_random(collection), True
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()
            return res


    def _vary(self, individual):
        if (self.container.best_fitness is None or self.container.best_fitness[0] <= 0.) and self.container.size > self.max_bins_before_non_trivial_found:
            prob_parameter_mut = 1.0
            prob_template_add = 0.
            prob_template_del = 0.
            prob_signal_species_add = 0.
            prob_inhibition_species_add = 0.
        else:
            prob_parameter_mut = self.prob_parameter_mut
            prob_template_add = self.prob_template_add
            prob_template_del = self.prob_template_del
            prob_signal_species_add = self.prob_signal_species_add
            prob_inhibition_species_add = self.prob_inhibition_species_add


        for _ in range(500): # XXX
            ind = copy.deepcopy(individual)
            ind.name = str(id(ind))

            active_activations_coords = list(zip(*np.where(ind.activations)))
            nb_active_activations = len(active_activations_coords)
            active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
            nb_active_inhibitions = len(active_inhibitions_coords)

            # Choose the kind of mutation
            choice = np.random.choice(5, p=(prob_parameter_mut,
                prob_template_add, prob_template_del,
                prob_signal_species_add, prob_inhibition_species_add))

            # Perform the mutation
            if choice == 0: # Parameters mutation
                #mutation_param_polybounded(ind, self.eta, self.mutationPb)
                #mutation_param_bioneat(ind, self.f1, self.f2, 1)
                mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
            elif choice == 1: # Add Activation
                mutation_add_activation(ind)
            elif choice == 2: # Del Activation
                mutation_del_activation(ind)
            elif choice == 3: # Signal species
                mutation_bioneat_signal_species(ind)
            elif choice == 4: # Inhibition species
                mutation_bioneat_inhibition_species(ind)


            ## Verify if there are no orphaned nodes
            #is_orphaned = (not(any(ind.activations[:,i]) or any(ind.activations[i,:]) or any(ind.inhibitions[i,:,:].flatten())) for i in range(ind.nb_nodes))
            #if any(is_orphaned):
            #    continue

            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                active_activations_coords = list(zip(*np.where(ind.activations)))
                nb_active_activations = len(active_activations_coords)
                active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
                nb_active_inhibitions = len(active_inhibitions_coords)
                if nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        return ind



@registry.register
class DaccadBioneatMut3(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [3, 7],
            min_init_budget: int = 1,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            prob_parameter_mut: float = 0.96,
            prob_disable_template: float = 0.01,
            prob_activation_add: float = 0.01,
            prob_inhibition_add: float = 0.01,
            prob_node_add: float = 0.01,
            nbConnActivationsDomain: Sequence[int] = [1, 7],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [1, 13],
            f1: float = 0.2,
            f2: float = 2.0,
            mut_pb: float = 0.8,
            allow_gd: bool = False,
            gd_pb: float = 1.0,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.prob_parameter_mut = prob_parameter_mut
        self.prob_disable_template = prob_disable_template
        self.prob_activation_add = prob_activation_add
        self.prob_inhibition_add = prob_inhibition_add
        self.prob_node_add = prob_node_add
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.f1 = f1
        self.f2 = f2
        self.mut_pb = mut_pb

        base_ind_gen = gen_daccad_individuals_GD(self.ind_domain, gd_pb) if allow_gd else gen_daccad_individuals(self.ind_domain)
        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=base_ind_gen, **kwargs)


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        if initialise: # Initialisation
            # Create a base individual
            standard_init_ind(base_ind)
            return base_ind, False

        else: # Selection
            try:
                #res = tools.non_trivial_sel_random(collection), True
                choice = np.random.choice(2, p=(0.70, 0.30))
                if choice == 0:
                    res = tools.non_trivial_sel_random(collection), True
                else:
                    res = tools.sel_random(collection), True
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()
            return res


    def _vary(self, individual):
        for _ in range(5000): # XXX
            ind = copy.deepcopy(individual)
            #ind.name = str(id(ind))

            # Choose the kind of mutation
            choice = np.random.choice(5, p=(self.prob_parameter_mut,
                self.prob_disable_template,
                self.prob_activation_add, self.prob_inhibition_add,
                self.prob_node_add))

            # Perform the mutation
            try:
                if choice == 0: # Parameters mutation
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                elif choice == 1: # Disable template
                    disable_template(ind)
                elif choice == 2: # Add Activation
                    add_activation_with_gradients(ind)
                elif choice == 3: # Add inhibition
                    add_inhibition_with_gradients(ind)
                elif choice == 4: # Add node
                    add_node_with_gradients(ind)
            except Exception as e:
                print(f"VARY: EXCEPTION (choice={choice}): {e}")
                continue

            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                nb_active_activations = len(activeConnActivationsIndexes)
                activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                if ind.nb_nodes >= self.nb_nodes_domain[0] and ind.nb_nodes <= self.nb_nodes_domain[1] and nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        ind.name = str(hash(ind) ** 2)
        return ind




@registry.register
class DaccadOnlyThreeNodesTwoTemplates(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            min_init_budget: int = 1,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            f1: float = 0.2,
            f2: float = 2.0,
            mut_pb: float = 0.8,
            allow_gd: bool = False,
            gd_pb: float = 1.0,
            **kwargs):
        self.ind_domain = ind_domain
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.f1 = f1
        self.f2 = f2
        self.mut_pb = mut_pb

        base_ind_gen = gen_daccad_individuals_GD(self.ind_domain, gd_pb) if allow_gd else gen_daccad_individuals(self.ind_domain)
        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=base_ind_gen, **kwargs)


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        if initialise: # Initialisation
            # Create a base individual
            standard_init_ind(base_ind)
            #base_ind.resize(base_ind.nb_nodes+1)
            #base_ind.stabilities[-1] = random_log_scale_1000()
            #base_ind.activations[-1, -1] = random_log_scale()
            #base_ind.activations[-1, to_node] = random_log_scale()
            choice = np.random.choice(2)
            base_ind.activations[choice, -1] = random_log_scale()
            base_ind.assemble()
            return base_ind, False

        else: # Selection
            try:
                #res = tools.non_trivial_sel_random(collection), True
                choice = np.random.choice(2, p=(0.70, 0.30))
                if choice == 0:
                    res = tools.non_trivial_sel_random(collection), True
                else:
                    res = tools.sel_random(collection), True
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()
            return res


    def _vary(self, individual):
        for _ in range(500): # XXX
            ind = copy.deepcopy(individual)
            ind.name = str(id(ind))

            # Perform the mutation
            try:
                mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
            except Exception as e:
                print(f"VARY: EXCEPTION (choice={choice}): {e}")
                continue

            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                break
                # Verify if within bounds
                #activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                #nb_active_activations = len(activeConnActivationsIndexes)
                #activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                #nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                #if nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                #    break
            else:
                print(f"VARY: NOT VALID !")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        return ind




# TODO
# Hypotheses 1:
#   - easier to get non-trivial individuals with larger number of templates because they are more stable wrt evals and mutations
#   - can go back to smaller individuals from a non-nul large individual
#   - so: must focus especially on large non-nul individuals
#   - when selecting a trivial indiv, choose one that is small (?)
# Hypotheses 2:
#   - must focus on individuals that have the higher chance to give non-trivial offsprings
#   - choice (1): allow more selection on individuals with a higher ratio of non-trivial offsprings
#   - choice (2):
#      - grid: 2 features: nb templates, and ratio of non-trivial offsprings (stability vs mutation)
#      --> grid: system to update the feature of an individual in the grid ?
#   - choice (3): create a new grid every gen (like bioneat). Limitation: small grids ? If empty grid, take total grid as parent



@registry.register
class DaccadBioneatMut4(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [3, 7],
            min_init_budget: int = 1,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            prob_parameter_mut: float = 0.96,
            prob_disable_template: float = 0.01,
            prob_activation_add: float = 0.01,
            prob_inhibition_add: float = 0.01,
            prob_node_add: float = 0.01,
            nbConnActivationsDomain: Sequence[int] = [1, 7],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [1, 13],
            f1: float = 0.2,
            f2: float = 2.0,
            mut_pb: float = 0.8,
            allow_gd: bool = False,
            gd_pb: float = 1.0,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.prob_parameter_mut = prob_parameter_mut
        self.prob_disable_template = prob_disable_template
        self.prob_activation_add = prob_activation_add
        self.prob_inhibition_add = prob_inhibition_add
        self.prob_node_add = prob_node_add
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.f1 = f1
        self.f2 = f2
        self.mut_pb = mut_pb

        base_ind_gen = gen_daccad_individuals_GD(self.ind_domain, gd_pb) if allow_gd else gen_daccad_individuals(self.ind_domain)
        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=base_ind_gen, **kwargs)


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        if initialise: # Initialisation
            # Create a base individual
            standard_init_ind(base_ind)
            return base_ind, False

        else: # Selection
            try:
                #choice = np.random.choice(4, p=(0.10, 0.10, 0.60, 0.20))
                choice = np.random.choice(4, p=(0.40, 0.30, 0.20, 0.10)) # Seems better than previous one... ??

                if choice == 0:
                    # Select the highest performing individual, if it is non-trivial
                    # XXX : try just always select the first individual
                    res = collection.best, True
                    #min_fitness = copy.deepcopy(collection.best.fitness)
                    #min_fitness.values = tuple([x[0] for x in collection.fitness_domain])
                    #if not res.fitness.dominates(min_fitness):
                    #    res = tools.sel_random(collection), True
                elif choice == 1:
                    # Select one of the non-trivial individuals with the highest number of templates
                    shuffled_coll = copy.deepcopy(list(collection))
                    random.shuffle(shuffled_coll)
                    fst_ind = shuffled_coll[0]
                    min_fitness = copy.deepcopy(fst_ind.fitness)
                    min_fitness.values = tuple([x[0] for x in collection.fitness_domain])
                    largest = fst_ind
                    largest_nTemplates = 0
                    for e in shuffled_coll[1:]:
                        #if e.fitness.dominates(min_fitness) and e.features[0] > largest_nTemplates:
                        if e.fitness[0] > min_fitness[0] and e.features[0] > largest_nTemplates:
                            largest = e
                            largest_nTemplates = e.features[0]
                    res = largest, True
                elif choice == 2:
                    # Select any non-trivial individual
                    res = tools.non_trivial_sel_random(collection), True
                else:
                    # Select any individual
                    res = tools.sel_random(collection), True

            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()
            return res


    def _vary(self, individual):
        for _ in range(500): # XXX
            ind = copy.deepcopy(individual)
            ind.name = str(id(ind))

            # Choose the kind of mutation
            choice = np.random.choice(5, p=(self.prob_parameter_mut,
                self.prob_disable_template,
                self.prob_activation_add, self.prob_inhibition_add,
                self.prob_node_add))

            # Perform the mutation
            try:
                if choice == 0: # Parameters mutation
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                elif choice == 1: # Disable template
                    disable_template(ind)
                elif choice == 2: # Add Activation
                    add_activation_with_gradients(ind)
                elif choice == 3: # Add inhibition
                    add_inhibition_with_gradients(ind)
                elif choice == 4: # Add node
                    add_node_with_gradients(ind)
            except Exception as e:
                print(f"VARY: EXCEPTION (choice={choice}): {e}")
                continue

            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                nb_active_activations = len(activeConnActivationsIndexes)
                activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                if nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        return ind




@registry.register
class DaccadBioneatMut3Transfer(Evolution):
    """TODO"""

    def __init__(self, container: Container, budget: int,
            ind_domain: DomainLike,
            nb_nodes_domain: Sequence[int] = [3, 7],
            min_init_budget: int = 1,
            min_ind_found_in_init: int = 1,
            init_pb: float = 0.0,
            sel_pb: float = 1.0,
            prob_parameter_mut: float = 0.96,
            prob_disable_template: float = 0.01,
            prob_activation_add: float = 0.01,
            prob_inhibition_add: float = 0.01,
            prob_node_add: float = 0.01,
            nbConnActivationsDomain: Sequence[int] = [1, 7],
            nbConnInhibitionsDomain: Sequence[int] = [0, 6],
            nbConnDomain: Sequence[int] = [1, 13],
            f1: float = 0.2,
            f2: float = 2.0,
            mut_pb: float = 0.8,
            allow_gd: bool = False,
            gd_pb: float = 1.0,
            approx_budget: int = 1000,
            **kwargs):
        self.ind_domain = ind_domain
        self.nb_nodes_domain = nb_nodes_domain
        self.min_init_budget = min_init_budget
        self.min_ind_found_in_init = min_ind_found_in_init
        self.init_pb = init_pb
        self.sel_pb = sel_pb
        self.prob_parameter_mut = prob_parameter_mut
        self.prob_disable_template = prob_disable_template
        self.prob_activation_add = prob_activation_add
        self.prob_inhibition_add = prob_inhibition_add
        self.prob_node_add = prob_node_add
        self.nbConnActivationsDomain = nbConnActivationsDomain
        self.nbConnInhibitionsDomain = nbConnInhibitionsDomain
        self.nbConnDomain = nbConnDomain
        self.f1 = f1
        self.f2 = f2
        self.mut_pb = mut_pb
        self.approx_budget = approx_budget
        self.resetted_grid_scores = False

        base_ind_gen = gen_daccad_individuals_GD(self.ind_domain, gd_pb) if allow_gd else gen_daccad_individuals(self.ind_domain)
        super().__init__(container, budget, select_or_initialise=self._select_or_initialise, vary=self._vary, base_ind_gen=base_ind_gen, **kwargs)


    def _reset_grid_scores(self):
        print("Emptying the grid !")
        for ind in self.container:
            ind.fitness.setValues([0.])
        #self.best_fitness.setValues([0.])


    def _select_or_initialise(self, collection: Sequence[Any], base_ind: IndividualLike) -> Tuple[Any, bool]:
        initialise = self._nb_suggestions < self.min_init_budget or len(collection) < self.min_ind_found_in_init
        if not initialise:
            operation = np.random.choice(range(2), p=[self.sel_pb, self.init_pb])
            initialise = operation == 1

        if initialise: # Initialisation
            # Create a base individual
            standard_init_ind(base_ind)
            res = base_ind
            #return base_ind, False

        else: # Selection
            try:
                #res = tools.non_trivial_sel_random(collection), True
                choice = np.random.choice(2, p=(0.70, 0.30))
                if choice == 0:
                    res = tools.non_trivial_sel_random(collection)
                else:
                    res = tools.sel_random(collection)
            except Exception as e:
                print(f"EXCEPTION ! {e}")
                traceback.print_exc()

        if self.nb_suggestions <= self.approx_budget:
            res.base_configuration_entry = "approx_daccad"
        else:
            res.base_configuration_entry = "daccad"
            if not self.resetted_grid_scores:
                self.resetted_grid_scores = True
                self._reset_grid_scores()

        return res, not initialise


    def _vary(self, individual):
        for _ in range(5000): # XXX
            ind = copy.deepcopy(individual)
            #ind.name = str(id(ind))

            # Choose the kind of mutation
            choice = np.random.choice(5, p=(self.prob_parameter_mut,
                self.prob_disable_template,
                self.prob_activation_add, self.prob_inhibition_add,
                self.prob_node_add))

            # Perform the mutation
            try:
                if choice == 0: # Parameters mutation
                    mutation_param_bioneat(ind, self.f1, self.f2, self.mut_pb)
                elif choice == 1: # Disable template
                    disable_template(ind)
                elif choice == 2: # Add Activation
                    add_activation_with_gradients(ind)
                elif choice == 3: # Add inhibition
                    add_inhibition_with_gradients(ind)
                elif choice == 4: # Add node
                    add_node_with_gradients(ind)
            except Exception as e:
                print(f"VARY: EXCEPTION (choice={choice}): {e}")
                continue

            # Assemble individual
            ind.assemble()
            # Verify if valid
            if ind.is_valid():
                # Verify if within bounds
                activeConnActivationsIndexes = np.where(ind.activations >= 0.005)[0]
                nb_active_activations = len(activeConnActivationsIndexes)
                activeConnInhibitionsIndexes = np.where(ind.inhibitions >= 0.005)[0]
                nb_active_inhibitions = len(activeConnInhibitionsIndexes)

                if ind.nb_nodes >= self.nb_nodes_domain[0] and ind.nb_nodes <= self.nb_nodes_domain[1] and nb_active_activations >= self.nbConnActivationsDomain[0] and nb_active_activations <= self.nbConnActivationsDomain[1] and nb_active_inhibitions >= self.nbConnInhibitionsDomain[0] and nb_active_inhibitions <= self.nbConnInhibitionsDomain[1] and nb_active_activations + nb_active_inhibitions >= self.nbConnDomain[0] and nb_active_activations + nb_active_inhibitions <= self.nbConnDomain[1]:
                    break
            else:
                print(f"VARY: NOT VALID ! (choice={choice})")
                #raise RuntimeError(f"VARY: NOT VALID ! (choice={choice})")
        ind.name = str(hash(ind) ** 2)
        return ind



# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
