#!/usr/bin/env python3


########## IMPORTS ########### {{{1
from illuminate import *
import pickle
import warnings
import traceback



########## MAIN ########### {{{1
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--inputFile', type=str, default='conf/final.p', help = "Path of final data file")
    args = parser.parse_args()

    input_files = []
    if os.path.isdir(args.inputFile):
        for filename in os.listdir(args.inputFile):
            if filename.endswith(".p"):
                input_files.append(os.path.join(args.inputFile, filename))
    else:
        input_files.append(args.inputFile)

    allScores = []
    for filename in input_files:
        with open(filename, "rb") as f:
            data = pickle.load(f)
        #print(f"\n# Evaluating best from '{filename}'.")
        #best = data['container'].best
        allScores += [a.scores['medianScore'] for a in data['container']]

    print(allScores)


# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
