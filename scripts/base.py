#!/usr/bin/env python3


########## IMPORTS ########### {{{1

import os
import pathlib
import datetime
from timeit import default_timer as timer
import copy
import itertools
from functools import partial
import yaml
import numpy as np
import random
import warnings
import traceback
import math

import submitDACCAD

from qdpy.algorithms import *
from qdpy.containers import *
from qdpy.plots import *
from qdpy.phenotype import *
from qdpy.base import *
from qdpy import tools
import qdpy.hdsobol as hdsobol

from typing import Optional, Tuple, List, Iterable, Iterator, Any, TypeVar, Generic, Union, Sequence, MutableSet, MutableSequence, Type, Callable, Generator, Mapping, MutableMapping, overload


########## BASE CLASSES ########### {{{1

class IlluminationExperiment(object):
    def __init__(self, config_filename, parallelism_type = "concurrent", seed = None, base_config = None):
        self._loadConfig(config_filename)
        if base_config is not None:
            self.config = {**self.config, **base_config}
        self.parallelism_type = parallelism_type
        self.config['parallelism_type'] = parallelism_type
        self._init_seed(seed)
        self.reinit()

    def __getstate__(self):
        odict = self.__dict__.copy()
        del odict['algo']
        del odict['container']
        return odict


    def _loadConfig(self, config_filename):
        self.config_filename = config_filename
        self.config = yaml.safe_load(open(config_filename))


    def _get_features_list(self):
        features_list = self.config['features_list']
        fitness_type = self.config['fitness_type']
        return features_list, fitness_type

    def _define_domains(self):
        features_list, fitness_from_feature = self._get_features_list()
        self.config['features_domain'] = []
        for feature_name in features_list:
            val = self.config['%s%s' % (feature_name, "Domain")]
            self.config['features_domain'] += [tuple(val)]
        self.config['fitness_domain'] = tuple(self.config['%s%s' % (fitness_from_feature, "Domain")]),

    def _init_seed(self, rnd_seed = None):
        # Find random seed
        if rnd_seed is not None:
            seed = rnd_seed
        elif "seed" in self.config:
            seed = self.config["seed"]
        else:
            seed = np.random.randint(1000000)

        # Update and print seed
        np.random.seed(seed)
        random.seed(seed)
        print("Seed: %i" % seed)


    def reinit(self):
        # Name of the expe instance based on the current timestamp
        self.instance_name = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
        # Identify and create result data dir
        if not self.config.get('dataDir'):
            resultsBaseDir = self.config.get('resultsBaseDir') or "../results/"
            dataDir = os.path.join(os.path.expanduser(resultsBaseDir), os.path.splitext(os.path.basename(self.config_filename))[0])
            self.config['dataDir'] = dataDir
        pathlib.Path(self.config['dataDir']).mkdir(parents=True, exist_ok=True)

        # Find the domains of the fitness and features
        self._define_domains()
        default_config = {}
        default_config["fitness_domain"] = self.config['fitness_domain']
        default_config["features_domain"] = self.config['features_domain']
        #print(default_config)

        # Create containers and algorithms from configuration
        factory = Factory()
        assert "containers" in self.config, f"Please specify configuration entry 'containers' containing the description of all containers."
        factory.build(self.config["containers"], default_config)
        assert "algorithms" in self.config, f"Please specify configuration entry 'algorithms' containing the description of all algorithms."
        factory.build(self.config["algorithms"])
        assert "main_algorithm_name" in self.config, f"Please specify configuration entry 'main_algorithm' containing the name of the main algorithm."
        self.algo = factory[self.config["main_algorithm_name"]]
        self.container = self.algo.container

        self.batch_mode = self.config.get('batch_mode', False)
        self.log_base_path = self.config['dataDir']

        # Create a logger to pretty-print everything and generate output data files
        self.iteration_filenames = os.path.join(self.log_base_path, "iteration-%i_" + self.instance_name + ".p")
        self.final_filename = os.path.join(self.log_base_path, "final_" + self.instance_name + ".p")
        self.save_period = self.config.get('save_period', 0)
        self.logger = TQDMAlgorithmLogger(self.algo,
                iteration_filenames=self.iteration_filenames, final_filename=self.final_filename, save_period=self.save_period)


        ## OLD STUFFS
        ##self._computeDimension()
        #self.eta = self.config['algorithm']['eta']
        #self.indDomain = self.config['algorithm'].get('indDomain') or [0., 1.]
        #self.mutationPb = self.config['algorithm']['mutationPb']
        ## Create algo
        #factory = AlgorithmFactory()
        #factory.fromConfig(self.config['algorithm'])
        #factory.evalFn = self._evalFunc
        #factory.logBasePath = self.config['dataDir']
        #factory.iterationFilenames = "iteration-%i_" + self.instance_name + ".p"
        #factory.finalFilename = "final_" + self.instance_name + ".p"
        #factory.instance_name = self.instance_name
        #factory.parallelism_type = self.config['parallelism_type']
        #self.algo = factory.build()
        #self.algo.initiate = self._initiateFn
        #self.algo.mutate = self._mutateFn
        #self.algo.addSavingInfo('config', self.config)


    def run(self):
        # Define evaluation function
        #eval_fn = partial(self._evalFunc)
        eval_fn = self._evalFunc
        #eval_fn = partial(illumination_rastrigin_normalised, nb_features = 2)

        # Run illumination process !
        with ParallelismManager(self.parallelism_type) as pMgr:
            best = self.algo.optimise(eval_fn, executor = pMgr.executor, batch_mode=self.batch_mode) # Disable batch_mode (steady-state mode) to ask/tell new individuals without waiting the completion of each batch

        print("\n------------------------\n")
        print(self.algo.summary())

        if isinstance(self.container, Grid):
            grid = self.container
        else:
            # Transform the container into a grid
            print("\n{:70s}".format("Transforming the container into a grid, for visualisation..."), end="", flush=True)
            grid = Grid(self.container, shape=(10,10), max_items_per_bin=1, fitness_domain=self.container.fitness_domain, features_domain=self.container.features_domain, storage_type=list)
            print("\tDone !")
            print(grid.summary())

        # Create plot of the performance grid
        plot_path = os.path.join(self.log_base_path, f"performancesGrid-{self.instance_name}.pdf")
        quality = grid.quality_array[(slice(None),) * (len(grid.quality_array.shape) - 1) + (0,)]
        plotGridSubplots(quality, plot_path, plt.get_cmap("nipy_spectral"), grid.features_domain, grid.fitness_domain[0], nbTicks=None)
        print("\nA plot of the performance grid was saved in '%s'." % os.path.abspath(plot_path))

        # Create plot of the activity grid
        plot_path = os.path.join(self.log_base_path, f"activityGrid-{self.instance_name}.pdf")
        plotGridSubplots(grid.activity_per_bin, plot_path, plt.get_cmap("nipy_spectral"), grid.features_domain, [0, np.max(grid.activity_per_bin)], nbTicks=None)
        print("\nA plot of the activity grid was saved in '%s'." % os.path.abspath(plot_path))

        print("All results are available in the '%s' pickle file." % self.logger.final_filename)


    def _removeTmpFiles(self, fileList):
        keepTemporaryFiles = self.config.get('keepTemporaryFiles')
        if not keepTemporaryFiles:
            for f in fileList:
                try:
                    os.remove(f)
                except:
                    pass

    def _evalFunc(self, ind):
        # Launch and analyse new trial
        try:
            return self._launchTrial(ind)
        except Exception as e:
            warnings.warn(f"Adding individual failed: {str(e)}")
            traceback.print_exc()
            raise e


    def _launchTrial(self, ind):
        print(ind.name)
        fitness = [np.random.uniform(x[0], x[1]) for x in self.config['fitness_domain']]
        features = [np.random.uniform(x[0], x[1]) for x in self.config['features_domain']]
        ind.fitness.values = fitness
        ind.features = features
        return ind




########## INITIALISATIONS AND MUTATIONS ########### {{{1


def generateUniform(dimension, indBounds, nb):
    res = []
    for i in range(nb):
        res.append(np.random.uniform(indBounds[0], indBounds[1], dimension))
    return res

def generateSparseUniform(dimension, indBounds, nb, sparsity):
    res = []
    for i in range(nb):
        base = np.random.uniform(indBounds[0], indBounds[1], dimension)
        mask = np.random.uniform(0., 1., dimension)
        base[mask < sparsity] = 0.
        res.append(base)
    return res


def generateSparseUniformDomain(dimension, indBounds, nb, sparsityDomain):
    res = []
    for i in range(nb):
        ind = np.full(dimension, indBounds[0])
        sparsity = np.random.randint(sparsityDomain[0], sparsityDomain[1])
        while sum(ind > 0.) < sparsity:
            ind[np.random.randint(len(ind))] = np.random.uniform(indBounds[0], indBounds[1])
        res.append(ind)
    return res


def generateSobol(dimension, indBounds, nb):
    res = hdsobol.gen_sobol_vectors(nb+1, dimension)
    res = res * (indBounds[1] - indBounds[0]) + indBounds[0]
    return res

def generateBinarySobol(dimension, indBounds, nb, cutoff = 0.50):
    res = hdsobol.gen_sobol_vectors(nb+1, dimension)
    res = np.unique((res > cutoff).astype(int), axis=0)
    return res

def generateSobolConnectionsWithUniformValues(dimension, indBounds, nb, cutoff = 0.50, nbValueSets = 1):
    base = generateBinarySobol(dimension, (0., 1.), nb // nbValueSets, cutoff)
    mask = base >= 0.5
    res = []
    nbMasked = len(base[mask])
    for i in range(nbValueSets):
        m = base.astype(float)
        m[mask] = np.random.uniform(indBounds[0], indBounds[1], nbMasked)
        res.append(m)
    return np.concatenate(res)



########## INDIVIDUALS AND FITNESSES ########### {{{1

class DaccadIndividual(Individual):
    def __init__(self, ind_domain, **kwargs):
        super().__init__(**kwargs)
        self.nb_nodes = 0
        self.ind_domain = ind_domain
        self.stabilities = []
        self.activations = np.zeros((self.nb_nodes, self.nb_nodes))
        self.inhibitions = np.zeros((self.nb_nodes, self.nb_nodes, self.nb_nodes))
        self.name = str(id(self))
        self.scores = None
        self.specie = 0

    def is_valid(self):
        return submitDACCAD.isValid(self, self.nb_nodes)

    def assemble(self):
        self[:] = list(self.stabilities) + list(self.activations.flatten()) + list(self.inhibitions.flatten())

    def update(self):
        self.stabilities = np.array(self[:self.nb_nodes])
        self.activations = np.array(self[self.nb_nodes: self.nb_nodes + self.nb_nodes*self.nb_nodes]).reshape((self.nb_nodes, self.nb_nodes))
        self.inhibitions = np.array(self[self.nb_nodes + self.nb_nodes*self.nb_nodes:]).reshape((self.nb_nodes, self.nb_nodes, self.nb_nodes))

    def same_species_as(self, other):
        return self.specie == other.specie

    def resize(self, nb_nodes):
        if nb_nodes == self.nb_nodes:
            return
        elif nb_nodes < self.nb_nodes:
            self.stabilities = self.stabilities[:nb_nodes]
            self.activations = self.activations[:nb_nodes, :nb_nodes]
            self.inhibitions = self.inhibitions[:nb_nodes, :nb_nodes, :nb_nodes]
        else:
            stabilities = np.empty(nb_nodes)
            stabilities[:self.nb_nodes] = self.stabilities
            stabilities[self.nb_nodes:] = np.random.uniform(self.ind_domain[0], self.ind_domain[1], nb_nodes - self.nb_nodes)
            self.stabilities = stabilities
            activations = np.zeros((nb_nodes, nb_nodes))
            activations[:self.nb_nodes, :self.nb_nodes] = self.activations
            self.activations = activations
            inhibitions = np.zeros((nb_nodes, nb_nodes, nb_nodes))
            inhibitions[:self.nb_nodes, :self.nb_nodes, :self.nb_nodes] = self.inhibitions
            self.inhibitions = inhibitions
        self.nb_nodes = nb_nodes
        self.assemble()

    def active_nodes(self):
        res = []
        for i in range(self.nb_nodes):
            if sum(self.activations[i,:]>0) or sum(self.activations[:,i]>0) \
                    or sum(self.inhibitions[i,:,:].flatten()>0) \
                    or sum(self.inhibitions[:,i,:].flatten()>0) \
                    or sum(self.inhibitions[:,:,i].flatten()>0):
                res.append(i)
        return res

    def valid_indexes(self):
        self.assemble()
        res = list(np.where(self)[0])
        # Remove stabilities of unconnected nodes
        for i in range(self.nb_nodes):
            if sum(self.activations[i,:]>0) or sum(self.activations[:,i]>0) \
                    or sum(self.inhibitions[i,:,:].flatten()>0) \
                    or sum(self.inhibitions[:,i,:].flatten()>0) \
                    or sum(self.inhibitions[:,:,i].flatten()>0):
                continue
            else:
                res.remove(i)
        return res

    @property
    def values(self):
        return np.array(self)[self.valid_indexes()]

    @values.setter
    def values(self, vals):
        for ivals, iself in enumerate(self.valid_indexes()):
            self[iself] = vals[ivals]
        self.update()


class FitnessGD(Fitness):
    """Fitness with modified `dominates` method to allow genetic drift."""

    def __new__(cls, values: FitnessValuesLike=(), weights: Optional[FitnessValuesLike]=None, gd_pb: float=1.0):
        return super(FitnessGD, cls).__new__(cls)

    def __init__(self, gd_pb=1.0, **kwargs):
        self.gd_pb = gd_pb
        super().__init__(**kwargs)

    def dominates(self, other: Any, obj: Any = slice(None)) -> bool:
        choice = random.random() <= self.gd_pb
        if choice:
            for self_wvalue, other_wvalue in zip(self.wvalues[obj], other.wvalues[obj]):
                if self_wvalue < other_wvalue:
                    return False
            return True
        else:
            return super().dominates(other, obj)


def gen_daccad_individuals(ind_domain):
    while(True):
        yield DaccadIndividual(ind_domain)

def gen_daccad_individuals_GD(ind_domain, gd_pb=1.0):
    while(True):
        yield DaccadIndividual(ind_domain, fitness=FitnessGD(gd_pb=gd_pb))


def random_log_scale_1000():
    while True:
        res = math.exp(0. + random.random() * math.log(1000.)) / 1000.
        if res >= 10. / 1000.:
            return res

def random_log_scale():
    while True:
        res = math.exp(0. + random.random() * math.log(200.)) / 200.
        if res >= 1. / 200.:
            return res

def mutation_param_polybounded(ind, eta, mut_pb):
    tools.mut_polynomial_bounded(ind.stabilities, eta=eta, low=ind.ind_domain[0], up=ind.ind_domain[1], mut_pb=mut_pb)
    ind.activations[np.where(ind.activations)] = tools.mut_polynomial_bounded(ind.activations[np.where(ind.activations)], eta=eta, low=ind.ind_domain[0], up=ind.ind_domain[1], mut_pb=mut_pb)
    ind.inhibitions[np.where(ind.inhibitions)] = tools.mut_polynomial_bounded(ind.inhibitions[np.where(ind.inhibitions)], eta=eta, low=ind.ind_domain[0], up=ind.ind_domain[1], mut_pb=mut_pb)

def mutation_param_reinit(ind):
    ind.stabilities = np.random.uniform(ind.ind_domain[0], ind.ind_domain[1], len(ind.stabilities))
    ind.activations[np.where(ind.activations)] = np.random.uniform(ind.ind_domain[0], ind.ind_domain[1], len(ind.activations[np.where(ind.activations)]))
    ind.inhibitions[np.where(ind.inhibitions)] = np.random.uniform(ind.ind_domain[0], ind.ind_domain[1], len(ind.inhibitions[np.where(ind.inhibitions)]))


def mutation_add_activation(ind, trivial=False):
    activations_coords_set = {(x, y) for x in range(ind.nb_nodes) for y in range(ind.nb_nodes)}
    active_activations_coords = list(zip(*np.where(ind.activations)))
    inactive_activations_coords = list(activations_coords_set - set(active_activations_coords))
    if trivial:
        ind.activations[random.choice(inactive_activations_coords)] = 0.0000015
    else:
        ind.activations[random.choice(inactive_activations_coords)] = np.random.uniform(ind.ind_domain[0], ind.ind_domain[1])


# Make sure that the activation is added to a node with already an activation
def mutation_add_activation2(ind, trivial=False):
    activations_coords_set = {(x, y) for x in range(ind.nb_nodes) for y in range(ind.nb_nodes)}
    active_activations_coords = list(zip(*np.where(ind.activations)))
    inactive_activations_coords = list(activations_coords_set - set(active_activations_coords))
    active_nodes = ind.active_nodes()
    candidates_activations = []
    for c in inactive_activations_coords:
        if c[0] in active_nodes or c[1] in active_nodes:
            candidates_activations.append(c)
    if trivial:
        ind.activations[random.choice(candidates_activations)] = 0.0000015
    else:
        ind.activations[random.choice(candidates_activations)] = random_log_scale()


def mutation_add_activation_with_gradients(ind, trivial=False, gradients=[0,1]):
    activations_coords_set = {(x, y) for x in range(ind.nb_nodes) for y in range(ind.nb_nodes)}
    activations_coords_set -= {(x, y) for x in range(ind.nb_nodes) for y in gradients}
    active_activations_coords = list(zip(*np.where(ind.activations)))
    inactive_activations_coords = list(activations_coords_set - set(active_activations_coords))
    if trivial:
        ind.activations[random.choice(inactive_activations_coords)] = 0.0051
    else:
        ind.activations[random.choice(inactive_activations_coords)] = np.random.uniform(ind.ind_domain[0], ind.ind_domain[1])


def mutation_del_activation(ind):
    active_activations_coords = list(zip(*np.where(ind.activations)))
    coord = random.choice(active_activations_coords)
    ind.activations[coord] = 0.
    # Remove invalid inhibitions
    for i in range(ind.nb_nodes):
        ind.inhibitions[(i,) + coord] = 0.


def mutation_add_inhibition(ind, trivial=False):
    active_activations_coords = list(zip(*np.where(ind.activations)))
    active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
    allowed_inhibitions_coords = []
    for coord in active_activations_coords:
        # Verify if there are not already an inhibition on this template
        already_an_inhibition = np.any(ind.inhibitions[:, coord[0], coord[1]])
        if already_an_inhibition:
            continue
        # Register an inhibition
        target_nodes = list(range(ind.nb_nodes))
        np.random.shuffle(target_nodes)
        for i in target_nodes:
            coord2 = (i,) + coord
            if coord2 not in active_inhibitions_coords:
                allowed_inhibitions_coords.append(coord2)
                break
    if len(allowed_inhibitions_coords):
        if trivial:
            ind.inhibitions[random.choice(allowed_inhibitions_coords)] = 0.0000015
        else:
            ind.inhibitions[random.choice(allowed_inhibitions_coords)] = np.random.uniform(ind.ind_domain[0], ind.ind_domain[1])
    else:
        # No remaining place to put an inhibition, delete one instead
        ind.inhibitions[random.choice(active_inhibitions_coords)] = 0.

def mutation_del_inhibition(ind):
    active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
    if len(active_inhibitions_coords):
        ind.inhibitions[random.choice(active_inhibitions_coords)] = 0.
    else:
        # No inhibition found, add one instead
        mutation_add_inhibition(ind)

def mutation_add_node(ind, trivial=False):
    ind.resize(ind.nb_nodes+1)

    active_activations_coords = list(zip(*np.where(ind.activations)))
    allowed_inhibitions_coords = []
    for coord in active_activations_coords:
        coord2 = (ind.nb_nodes-1,) + coord
        allowed_inhibitions_coords.append(coord2)

    choice2 = np.random.choice(5) if len(allowed_inhibitions_coords) else np.random.choice(3)
    if trivial:
        if choice2 == 0:
            ind.activations[ind.nb_nodes-1, ind.nb_nodes-1] = 0.0000015
        elif choice2 == 1:
            ind.activations[-1, np.random.choice(ind.nb_nodes-1)] = 0.0000015
        elif choice2 == 2:
            ind.activations[np.random.choice(ind.nb_nodes-1), -1] = 0.0000015
        elif choice2 == 3 or choice2 == 4:
            ind.inhibitions[random.choice(allowed_inhibitions_coords)] = 0.0000015
            ind.activations[ind.nb_nodes-1, ind.nb_nodes-1] = 0.0000015
    else:
        if choice2 == 0:
            ind.activations[ind.nb_nodes-1, ind.nb_nodes-1] = np.random.uniform(ind.ind_domain[0], ind.ind_domain[1])
        elif choice2 == 1:
            ind.activations[-1, np.random.choice(ind.nb_nodes-1)] = np.random.uniform(ind.ind_domain[0], ind.ind_domain[1])
        elif choice2 == 2:
            ind.activations[np.random.choice(ind.nb_nodes-1), -1] = np.random.uniform(ind.ind_domain[0], ind.ind_domain[1])
        elif choice2 == 3 or choice2 == 4:
            ind.inhibitions[random.choice(allowed_inhibitions_coords)] = np.random.uniform(ind.ind_domain[0], ind.ind_domain[1])
            ind.activations[ind.nb_nodes-1, ind.nb_nodes-1] = np.random.uniform(ind.ind_domain[0], ind.ind_domain[1])

def mutation_add_node_with_gradients(ind, trivial=False, gradients=[0,1]):
    ind.resize(ind.nb_nodes+1)

    active_activations_coords = list(zip(*np.where(ind.activations)))
    allowed_inhibitions_coords = []
    for coord in active_activations_coords:
        coord2 = (ind.nb_nodes-1,) + coord
        allowed_inhibitions_coords.append(coord2)

    choice2 = np.random.choice(5) if len(allowed_inhibitions_coords) else np.random.choice(3)
    if trivial:
        if choice2 == 0:
            ind.activations[ind.nb_nodes-1, ind.nb_nodes-1] = 0.0051
        elif choice2 == 1:
            to_node = np.random.choice(list(set(range(ind.nb_nodes-1)) - set(gradients)))
            ind.activations[-1, to_node] = 0.0051
        elif choice2 == 2:
            ind.activations[np.random.choice(ind.nb_nodes-1), -1] = 0.0051
        elif choice2 == 3 or choice2 == 4:
            ind.inhibitions[random.choice(allowed_inhibitions_coords)] = 0.0051
            ind.activations[ind.nb_nodes-1, ind.nb_nodes-1] = 0.0051
    else:
        if choice2 == 0:
            ind.activations[ind.nb_nodes-1, ind.nb_nodes-1] = np.random.uniform(ind.ind_domain[0], ind.ind_domain[1])
        elif choice2 == 1:
            to_node = np.random.choice(list(set(range(ind.nb_nodes-1)) - set(gradients)))
            ind.activations[-1, to_node] = np.random.uniform(ind.ind_domain[0], ind.ind_domain[1])
        elif choice2 == 2:
            ind.activations[np.random.choice(ind.nb_nodes-1), -1] = np.random.uniform(ind.ind_domain[0], ind.ind_domain[1])
        elif choice2 == 3 or choice2 == 4:
            ind.inhibitions[random.choice(allowed_inhibitions_coords)] = np.random.uniform(ind.ind_domain[0], ind.ind_domain[1])
            ind.activations[ind.nb_nodes-1, ind.nb_nodes-1] = np.random.uniform(ind.ind_domain[0], ind.ind_domain[1])

def mutation_del_node(ind):
    node_to_del = np.random.choice(ind.nb_nodes)
    ind.nb_nodes -= 1
    mask_activations = np.ones_like(ind.activations, dtype=bool)
    mask_activations[node_to_del, :] = mask_activations[:, node_to_del] = False
    ind.activations = ind.activations[mask_activations].reshape((ind.nb_nodes, ind.nb_nodes))
    mask_inhibitions = np.ones_like(ind.inhibitions, dtype=bool)
    mask_inhibitions[node_to_del, :, :] = mask_inhibitions[:, node_to_del, :] = mask_inhibitions[:, :, node_to_del] = False
    ind.inhibitions = ind.inhibitions[mask_inhibitions].reshape((ind.nb_nodes, ind.nb_nodes, ind.nb_nodes))
    ind.stabilities = np.delete(ind.stabilities, node_to_del)


def standard_init_ind(ind):
    """Create a base individual, with 3 nodes and a "2->2" connection"""
    ind.resize(3)
    ind.stabilities = np.array([random_log_scale_1000() for _ in range(3)])
    ind.stabilities = [10./1000., 10./1000., 100./1000.]
    ind.activations[2,2] = 0.025 #random_log_scale() #np.random.uniform(ind.ind_domain[0], ind.ind_domain[1])
    ind.assemble()
    return ind


def standard_init_ind_grad4(ind):
    """Create a base individual in a setup with 4 gradients, with 5 nodes and a "4->4" connection"""
    ind.resize(5)
    ind.stabilities = np.array([random_log_scale_1000() for _ in range(5)])
    ind.stabilities = [10./1000., 10./1000., 10./1000., 10./1000, 100./1000.,]
    ind.activations[4,4] = 0.025 #random_log_scale() #np.random.uniform(ind.ind_domain[0], ind.ind_domain[1])
    ind.assemble()
    return ind


def standard_init_ind0(ind):
    """Create a base individual, with 1 node and a "0->0" connection"""
    ind.resize(1)
    ind.activations[0,0] = np.random.uniform(ind.ind_domain[0], ind.ind_domain[1])
    ind.assemble()
    return ind




def mutation_bioneat_signal_species(ind, trivial=False):
    choice = np.random.choice(2)
    if choice == 0: # a->b becomes a->c->b
        ind.resize(ind.nb_nodes+1)
        active_activations_coords = list(zip(*np.where(ind.activations)))
        old_activation_coord = random.choice(active_activations_coords)
        ind.activations[old_activation_coord[0], -1] = ind.activations[old_activation_coord]
        ind.activations[old_activation_coord] = 0.
        ind.activations[-1, old_activation_coord[1]] = ind.activations[old_activation_coord]
        ind.inhibitions[:,old_activation_coord[0], -1] = ind.inhibitions[:,old_activation_coord[0],old_activation_coord[1]]
        ind.inhibitions[:,old_activation_coord[0],old_activation_coord[1]] = 0.
        ind.stabilities[-1] = random_log_scale_1000()

    else: # Add node x->x and x->y with y a random signal
        signal_id = np.random.choice(ind.nb_nodes)
        ind.resize(ind.nb_nodes+1)
        if trivial:
            ind.activations[-1, -1] = 0.0000015
            ind.activations[-1, signal_id] = 0.0000015
            ind.stabilities[-1] = random_log_scale_1000()
        else:
            ind.activations[-1, -1] = random_log_scale() #np.random.uniform(ind.ind_domain[0], ind.ind_domain[1])
            ind.activations[-1, signal_id] = random_log_scale() #np.random.uniform(ind.ind_domain[0], ind.ind_domain[1])
            ind.stabilities[-1] = random_log_scale_1000()


def mutation_bioneat_inhibition_species(ind, trivial=False):
    activations_coords_set = {(x, y) for x in range(ind.nb_nodes) for y in range(ind.nb_nodes)}
    active_activations_coords = list(zip(*np.where(ind.activations)))
    inactive_activations_coords = list(activations_coords_set - set(active_activations_coords))

    choice = np.random.choice(2)
    if choice == 0 or len(inactive_activations_coords) == 0:
        # Inhibition add
        mutation_add_inhibition(ind, trivial)
        return

    # Add a template + inhibition of this template
    added_activation_coord = random.choice(inactive_activations_coords)
    if trivial:
        ind.activations[added_activation_coord] = 0.0000015
    else:
        ind.activations[added_activation_coord] = random_log_scale() #np.random.uniform(ind.ind_domain[0], ind.ind_domain[1])
    active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
    list_nodes = list(range(ind.nb_nodes))
    list_nodes.remove(added_activation_coord[0])
    if added_activation_coord[1] in list_nodes:
        list_nodes.remove(added_activation_coord[1])
    chosen_inhibition_coord = (random.choice(list_nodes),) + added_activation_coord
    if trivial:
        ind.inhibitions[chosen_inhibition_coord] = 0.0000015
    else:
        ind.inhibitions[chosen_inhibition_coord] = random_log_scale() #np.random.uniform(ind.ind_domain[0], ind.ind_domain[1])



# Only applicable if there is at least one template
def disable_template(ind, mut_pb=0.8, disabling_pb=0.1):
    active_activations_coords = list(zip(*np.where(ind.activations)))
    for coord in active_activations_coords:
        if random.random() < mut_pb:
            #if random.random() < min(1., max(0., disabling_pb - math.log(ind.activations[coord]))):
            #print("DEBUGDISABLE1: ", coord, ind.activations[coord])
            if random.random() < min(1., max(0., disabling_pb - math.log10(ind.activations[coord] / 10.))):
                ind.activations[coord] = 0.
                for i in range(ind.nb_nodes): # Remove invalid inhibitions
                    ind.inhibitions[(i,) + coord] = 0.
    active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
    for coord in active_inhibitions_coords:
        if random.random() < mut_pb:
            #print("DEBUGDISABLE2: ", coord, ind.inhibitions[coord])
            #if random.random() < min(1., max(0., disabling_pb - math.log(ind.inhibitions[coord]))):
            if random.random() < min(1., max(0., disabling_pb - math.log10(ind.inhibitions[coord] / 10.))):
                ind.inhibitions[coord] = 0.


#double mutatedParam = Math.exp(Math.log(oldParam) * (1 + randGaussian * 0.2)) + randGaussian * 2;
def mutate_one_param_bioneat(old, ind_domain, f1=0.2, f2=2., max_bound=200.):
    gauss = random.gauss(0., 1.)
    res = (math.exp(math.log(old * max_bound) * (1. + gauss * f1)) + gauss * f2) / max_bound
    if res < ind_domain[0]+0.0051:
        return ind_domain[0]+0.0051
    if res > ind_domain[1]:
        return ind_domain[1]
    return res

def mutation_param_bioneat(ind, f1=0.2, f2=2.0, mut_pb=0.8):
    active_activations_coords = list(zip(*np.where(ind.activations)))
    active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
    for i, old in enumerate(ind.stabilities):
        if random.random() < mut_pb:
            ind.stabilities[i] = mutate_one_param_bioneat(old, ind.ind_domain, f1, f2, 1000.)
    for coord in active_activations_coords:
        if random.random() < mut_pb:
            ind.activations[coord] = mutate_one_param_bioneat(ind.activations[coord], ind.ind_domain, f1, f2, 200.)
    for coord in active_inhibitions_coords:
        if random.random() < mut_pb:
            ind.inhibitions[coord] = mutate_one_param_bioneat(ind.inhibitions[coord], ind.ind_domain, f1, f2, 200.)



# Add node de base avec 2 nodes speciaux 
# --> gradients : virer les connections to.
    # Partie 1 : proba < 1.0 / (nombre d'activations + 1)
        # Version 1a:
            # Rajoute une nouvelle node
            # Stabilities : random log scale
            # Connections : autocatalyst (random log scale) + template activation vers une node existante (random log scale)
        # Version 1b:
            # Il faut deja avoir une inhibition
            # Si yen a un, cree une nouvelle node (autocatalyse) avec template (random log scale) inhibition vers l'inhibiteur
    # Partie 2 (split ?) : si au moins un template
        # Selectionne au pif un template d'activation
            # GetCorrectionConnection: revient le plus possible dans les connections precedentes
            # vire cette connection
            # nouvelle node ajoute (random log scale)
            # rajoute une activation de la node d'orgine vers nouvelle node (random log scale si inhibition, ancienne valeurs si activation)
            # rajoute une activation de la nouvelle node vers la zone target (ancienne valeurs)
def add_node_with_gradients(ind, gradients=[0,1]):
    active_activations_coords = list(zip(*np.where(ind.activations)))
    active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
    if random.random() < 1. / (len(active_activations_coords) + 1.) or len(active_activations_coords) == 0:
        if np.random.choice(2) or len(active_activations_coords) == 0:
            ind.resize(ind.nb_nodes+1)
            ind.stabilities[-1] = random_log_scale_1000()
            ind.activations[-1, -1] = random_log_scale()
            to_node = np.random.choice(list(set(range(ind.nb_nodes)) - set(gradients)))
            ind.activations[-1, to_node] = random_log_scale()
        else:
            selected_activation = random.choice(active_activations_coords)
            ind.resize(ind.nb_nodes+1)
            ind.stabilities[-1] = random_log_scale_1000()
            ind.activations[-1, -1] = random_log_scale()
            ind.inhibitions[-1, selected_activation[0],selected_activation[1]] = random_log_scale()
    else: # XXX Handle inhibitions ?
        selected_activation = random.choice(active_activations_coords)
        def get_back_template(templace_coord):
            #active_out_coords = list(np.where(ind.activations[templace_coord[0],:])[0])
            active_out_coords = [(templace_coord[0], x) for x in np.where(ind.activations[templace_coord[0],:])[0]]
            count_out = len(active_out_coords)
            #active_in_coords = list(np.where(ind.activations[:,templace_coord[0]])[0])
            active_in_coords = [(x, templace_coord[0]) for x in np.where(ind.activations[:,templace_coord[0]])[0]]
            if templace_coord in active_in_coords:
                active_in_coords.remove(templace_coord)
            count_in = len(active_in_coords)
            return active_in_coords[0] if count_in == 1 and count_out == 1 else None
        def get_correct_template(templace_coord):
            coord = templace_coord
            while(True):
                next_coord = get_back_template(coord)
                if next_coord == None or next_coord == templace_coord:
                    break
                coord = next_coord
            return coord
        activation = get_correct_template(selected_activation)
        old_activation_val = ind.activations[activation]
        ind.activations[activation] = 0.
        for i in range(ind.nb_nodes): # Remove invalid inhibitions
            ind.inhibitions[(i,) + activation] = 0.
        ind.resize(ind.nb_nodes+1)
        ind.stabilities[-1] = random_log_scale_1000()
        ind.activations[activation[0], -1] = old_activation_val # random_log_scale() # XXX Handle inhibitions
        ind.activations[-1, activation[1]] = old_activation_val


class NotApplicableError(Exception):
    pass


# Only applicable si toutes les activations n'existent pas deja
# Ajoute une connection qui n'existe pas encore (attention aux gradients qui n'ont pas de connections to) et l'ajoute (random log scale)
def add_activation_with_gradients(ind, gradients=[0,1]):
    activations_coords_set = {(x, y) for x in range(ind.nb_nodes) for y in range(ind.nb_nodes)}
    activations_coords_set -= {(x, y) for x in range(ind.nb_nodes) for y in gradients}
    active_activations_coords = list(zip(*np.where(ind.activations)))
    inactive_activations_coords = list(activations_coords_set - set(active_activations_coords))
    if len(inactive_activations_coords) == 0:
        #return
        raise NotApplicableError("Not applicable")
    ind.activations[random.choice(inactive_activations_coords)] = random_log_scale()


# Only applicable si toutes les inhibitions n'existent pas deja
# 50% chance de flip
# Prend un inhibition qui n'existe pas au pif (destination ne peut pas etre gradient), et la rajoute
# Si toutes les activations auto-catalytique sur la node "To" de l'inhibition
# Rajoute une inhibitions sur une activation qui existe deja (mais pas vers gradient).
def add_inhibition_with_gradients(ind, gradients=[0,1]):
    def add_inhibition(node_from, node_to):
        possibles_templates_from = np.where(ind.activations[:, node_to])[0]
        if len(possibles_templates_from) == 0:
            selected_template = (node_to, node_to)
            ind.activations[selected_template] = random_log_scale()
        else:
            selected_template = (random.choice(possibles_templates_from), node_to)
        ind.inhibitions[node_from, selected_template[0], selected_template[1]] = random_log_scale()

    active_activations_coords = list(zip(*np.where(ind.activations)))
    choice = np.random.choice(2)
    if choice == 0 or len(active_activations_coords) == 0: # Add simple inhibition
        node_from = np.random.choice(ind.nb_nodes)
        node_to = np.random.choice(list(set(range(ind.nb_nodes)) - set(gradients)))
        add_inhibition(node_from, node_to)
        #print("DEBUG: added simple inhibition")
    else: # Replace an activation with a double inhibition
        selected_activation = random.choice(active_activations_coords)
        # Remove selected activation
        ind.activations[selected_activation] = 0.
        # Remove invalid inhibitions
        for i in range(ind.nb_nodes):
            ind.inhibitions[(i,) + selected_activation] = 0.
        # Add double inhibition
        ind.resize(ind.nb_nodes+1)
        new_node = ind.nb_nodes-1
        ind.stabilities[new_node] = random_log_scale_1000()
        add_inhibition(selected_activation[0], new_node)
        add_inhibition(new_node, selected_activation[1])
        #print("DEBUG: double inhibition")




def clone_activation(ind, base_activation):
    base_activation_conc = ind.activations[base_activation]
    ind.resize(ind.nb_nodes+1)
    ind.stabilities[-1] = random_log_scale_1000()
    ind.activations[base_activation[0], -1] = base_activation_conc
    ind.activations[-1, base_activation[1]] = base_activation_conc

def clone_inhibition(ind, base_inhibition):
    base_inhibition_conc = ind.inhibitions[base_inhibition]
    node_from, templ0, templ1 = base_inhibition
    ind.resize(ind.nb_nodes+1)
    ind.stabilities[-1] = random_log_scale_1000()
    ind.activations[node_from, -1] = base_inhibition_conc
    ind.inhibitions[-1, templ0, templ1] = base_inhibition_conc

def mutation_clone_activation(ind):
    active_activations_coords = list(zip(*np.where(ind.activations)))
    if len(active_activations_coords) == 0:
        raise NotApplicableError("Not applicable")
    base_activation = random.choice(active_activations_coords)
    clone_activation(ind, base_activation)

def mutation_clone_inhibition(ind):
    active_inhibitions_coords = list(zip(*np.where(ind.inhibitions)))
    if len(active_inhibitions_coords) == 0:
        raise NotApplicableError("Not applicable")
    base_inhibition = random.choice(active_inhibitions_coords)
    clone_inhibition(ind, base_inhibition)

def mutation_clone_template(ind):
    if np.random.choice(2):
        mutation_clone_activation(ind)
    else:
        mutation_clone_inhibition(ind)


def crossover_uniform(ind1, ind2, prob):
    min_nb_nodes = min(ind1.nb_nodes, ind2.nb_nodes)
    ind1_active_activations_coords = list(zip(*np.where(ind1.activations)))
    ind1_active_inhibitions_coords = list(zip(*np.where(ind1.inhibitions)))
    ind2_active_activations_coords = list(zip(*np.where(ind2.activations)))
    ind2_active_inhibitions_coords = list(zip(*np.where(ind2.inhibitions)))
    new_ind1 = copy.deepcopy(ind1)
    new_ind2 = copy.deepcopy(ind2)

    for x, y in ind1_active_activations_coords:
        if random.random() < prob and x < min_nb_nodes and y < min_nb_nodes:
            new_ind1.activations[x, y], new_ind2.activations[x, y] = ind2.activations[x, y], ind1.activations[x, y]
    for x, y in ind2_active_activations_coords:
        if random.random() < prob and x < min_nb_nodes and y < min_nb_nodes:
            new_ind1.activations[x, y], new_ind2.activations[x, y] = ind2.activations[x, y], ind1.activations[x, y]

    for x, y, z in ind1_active_inhibitions_coords:
        if random.random() < prob and x < min_nb_nodes and y < min_nb_nodes and z < min_nb_nodes:
            new_ind1.inhibitions[x, y, z], new_ind2.inhibitions[x, y, z] = ind2.inhibitions[x, y, z], ind1.inhibitions[x, y, z]
    for x, y, z in ind2_active_inhibitions_coords:
        if random.random() < prob and x < min_nb_nodes and y < min_nb_nodes and z < min_nb_nodes:
            new_ind1.inhibitions[x, y, z], new_ind2.inhibitions[x, y, z] = ind2.inhibitions[x, y, z], ind1.inhibitions[x, y, z]
    return new_ind1, new_ind2



def crossover_uniform2(ind1, ind2, prob):
    min_nb_nodes = min(ind1.nb_nodes, ind2.nb_nodes)
    ind1_active_activations_coords = list(zip(*np.where(ind1.activations)))
    ind1_active_inhibitions_coords = list(zip(*np.where(ind1.inhibitions)))
    ind2_active_activations_coords = list(zip(*np.where(ind2.activations)))
    ind2_active_inhibitions_coords = list(zip(*np.where(ind2.inhibitions)))
    new_ind1 = copy.deepcopy(ind1)
    new_ind2 = copy.deepcopy(ind2)

    for x, y in ind1_active_activations_coords:
        if random.random() < prob and x < min_nb_nodes and y < min_nb_nodes:
            new_ind1.activations[x, y], new_ind2.activations[x, y] = ind2.activations[x, y], ind1.activations[x, y]
            for z in range(min_nb_nodes):
                new_ind1.inhibitions[z, x, y], new_ind2.inhibitions[z, x, y] = ind2.inhibitions[z, x, y], ind1.inhibitions[z, x, y]
    for x, y in ind2_active_activations_coords:
        if random.random() < prob and x < min_nb_nodes and y < min_nb_nodes:
            new_ind1.activations[x, y], new_ind2.activations[x, y] = ind2.activations[x, y], ind1.activations[x, y]
            for z in range(min_nb_nodes):
                new_ind1.inhibitions[z, x, y], new_ind2.inhibitions[z, x, y] = ind2.inhibitions[z, x, y], ind1.inhibitions[z, x, y]

    return new_ind1, new_ind2



def randomize_templates(ind):
    for i in range(ind.activations.shape[0]):
        for j in range(ind.activations.shape[1]):
            v = ind.activations[i,j]
            if v > 0.000001:
                ind.activations[i,j] = random.uniform(0.000002, 1.)
    for i in range(ind.inhibitions.shape[0]):
        for j in range(ind.inhibitions.shape[1]):
            for k in range(ind.inhibitions.shape[2]):
                v = ind.inhibitions[i,j,k]
                if v > 0.000001:
                    ind.inhibitions[i,j,k] = random.uniform(0.000002, 1.)
    ind.assemble()
    return ind

def randomize_ind(ind):
    for i in range(len(ind.stabilities)):
        v = ind.stabilities[i]
        ind.stabilities[i] = random.uniform(0.000002, 1.)
    return randomize_templates(ind)



def noisy_templates(ind, sigma):
    for i in range(ind.activations.shape[0]):
        for j in range(ind.activations.shape[1]):
            v = ind.activations[i,j]
            if v > 0.000001:
                ind.activations[i,j] = max(0.000002, min(1., v+random.gauss(0., sigma)))
    for i in range(ind.inhibitions.shape[0]):
        for j in range(ind.inhibitions.shape[1]):
            for k in range(ind.inhibitions.shape[2]):
                v = ind.inhibitions[i,j,k]
                if v > 0.000001:
                    ind.inhibitions[i,j,k] = max(0.000002, min(1., v+random.gauss(0., sigma)))
    ind.assemble()
    return ind

def noisy_ind(ind, sigma):
    for i in range(len(ind.stabilities)):
        v = ind.stabilities[i]
        ind.stabilities[i] = max(0.000002, min(1., v+random.gauss(0., sigma)))
    return noisy_templates(ind, sigma)

# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
