#First we need to turn an array into a JSON

import os
import subprocess
from threading import Thread
from queue import Queue, Empty
import warnings
import yaml
from timeit import default_timer as timer
import numpy as np
import traceback

#Format array: [ all stabilities (nNodes), all activations from i to j (nNodes * nNodes), all inhibitions created by i to the template j to k (nNodes * nNodes * nNodes)]

def findAllInhibitions(array, nNodes = 5):
    beginInhibitionTemplates = nNodes * (nNodes+1)
    inhibitions = {}
    for i in range(nNodes): #For each possible activator
        index = beginInhibitionTemplates + i * nNodes * nNodes
        for fromId in range(nNodes):
            for toId in range(nNodes):
                if array[index + fromId*nNodes + toId] > 0.0:
                    if not (fromId,toId) in inhibitions:
                        inhibitions[(fromId,toId)] = []
                    inhibitions[(fromId,toId)].append(i)
    return inhibitions

#Kept for testing purpose; correct behavior
def findAllInhibitorsAndConcsLegacy(array,nNodes = 5): 
    beginInhibitionTemplates = nNodes * (nNodes+1)
    inhibitingTemplatesConcs = {}
    for i in range(nNodes): #For each possible activator
        index = beginInhibitionTemplates + i * nNodes * nNodes
        for fromId in range(nNodes):
            for toId in range(nNodes):
                if array[index + fromId*nNodes + toId] > 0.0:
                    name = 'I'+str(fromId)+'T'+str(toId)
                    if not name in inhibitingTemplatesConcs:
                        inhibitingTemplatesConcs[name] = []
                    inhibitingTemplatesConcs[name].append((str(i),array[index + fromId*nNodes + toId]))
    return inhibitingTemplatesConcs

#New version, relies on findAllInhibitions (i.e. more modular).
def findAllInhibitorsAndConcs(array,nNodes = 5):
    allInhibitions = findAllInhibitions(array,nNodes = nNodes)
    beginInhibitionTemplates = nNodes * (nNodes+1)
    inhibitingTemplatesConcs = {}
    for fromId,toId in allInhibitions:
        name = 'I'+str(fromId)+'T'+str(toId)
        indexes = allInhibitions[(fromId,toId)]
        base = fromId*nNodes + toId + beginInhibitionTemplates
        inhibitingTemplatesConcs[name] = [(str(i), array[base+i*nNodes*nNodes]) for i in indexes]
    return inhibitingTemplatesConcs

def invalidInhibitions(array, nNodes = 5):
    inhibTemps = findAllInhibitions(array,nNodes = nNodes)
    beginTemplates = nNodes
    res = []
    for fromId,toId in inhibTemps:
        if array[beginTemplates+fromId*nNodes + toId] <= 0.0:
            res.append(((fromId,toId),inhibTemps[(fromId,toId)]))
    return res

def isValid(array, nNodes = 5):
    return not invalidInhibitions(array, nNodes = nNodes)


def nbUsedNodes(array, nbNodes = 5):
    activations = np.array(array[nbNodes: nbNodes+nbNodes*nbNodes]).reshape((nbNodes, nbNodes))
    inhibitions = np.array(array[nbNodes+nbNodes*nbNodes:]).reshape((nbNodes, nbNodes, nbNodes))
    notUsed = [np.all(activations[:,i] <= 0.) and np.all(activations[i,:] <= 0.) and np.all(inhibitions[i,:,:] <= 0.) and np.all(inhibitions[:,i,:] <= 0.) and np.all(inhibitions[:,:,i] <= 0.) for i in range(nbNodes)]
    lastUsed = list(reversed(notUsed)).index(False)
    return nbNodes - lastUsed

def generateNode(name, stability, activator, tab = 1, tabChar = "  "):
    json = (tabChar*tab)+"{\n"
    json += (tabChar*(tab+1))+'"name": "'+str(name)+'",\n'
    json += (tabChar*(tab+1))+'"parameter": '+str(stability)+',\n'
    json += (tabChar*(tab+1))+'"initialConcentration": '+('1.0' if activator else '0.0')+',\n'
    json += (tabChar*(tab+1))+'"type": '+('1' if activator else '2')+',\n'
    json += (tabChar*(tab+1))+'"protectedSequence": false,\n'
    json += (tabChar*(tab+1))+'"DNAString": "",\n'
    json += (tabChar*(tab+1))+'"reporter": false,\n'
    json += (tabChar*(tab+1))+'"hasPseudoTemplate": false,\n'
    json += (tabChar*(tab+1))+'"pseudoTemplateConcentration": 0.0\n'
    json += (tabChar*tab)+"}"
    return json

def generateConnection(innov, concentration, fromNode, toNode, tab = 1, tabChar = "  "):
    json = (tabChar*tab)+"{\n"
    json += (tabChar*(tab+1))+'"innovation": '+str(innov)+',\n'
    json += (tabChar*(tab+1))+'"enabled": true,\n'
    json += (tabChar*(tab+1))+'"parameter": '+str(concentration)+',\n'
    json += (tabChar*(tab+1))+'"from": "'+str(fromNode)+'",\n'
    json += (tabChar*(tab+1))+'"to": "'+str(toNode)+'"\n'
    json += (tabChar*tab)+"}"
    return json

def generateAllNodes(array, inhibitingSequences, nNodes = 5, tab = 1, tabChar = "  "):
    json = (tabChar*tab)+'"nodes": [\n'
    for i in range(nNodes):
        json += generateNode(i,array[i], True, tab = tab + 1, tabChar = tabChar)+',\n'
    for val in inhibitingSequences:
        json += generateNode(val,0.0, False, tab = tab + 1, tabChar = tabChar)+',\n'
    json = json[:-2]+"\n"
    json += (tabChar*tab)+']'
    return json

def generateAllConnections(array,inhibitions,nNodes = 5, tab = 1, tabChar = "  "):
    innovation = 0
    offset = nNodes
    json = (tabChar*tab)+'"connections": [\n'
    for fromNode in range(nNodes):
        for toNode in range(nNodes):
            index = offset + nNodes * fromNode + toNode
            if array[index] > 0.0:
                json += generateConnection(innovation, array[index], fromNode, toNode, tab = tab+1, tabChar = tabChar)+',\n'
                innovation += 1
    for val in inhibitions:
        for fromNode, concentration in inhibitions[val]:
            json += generateConnection(innovation, concentration, fromNode, val, tab = tab+1, tabChar = tabChar)+',\n'
            innovation += 1
    json = json[:-2]+"\n"
    json += (tabChar*tab)+']'
    return json

def generateLegacyParameters(pol = 10.0, nick = 10.0, exo = 10.0, tab = 1, tabChar = "  "):
    json = (tabChar*tab)+'"parameters": {\n'
    json += (tabChar*(tab+1))+'"nick": '+str(nick)+',\n'
    json += (tabChar*(tab+1))+'"pol": '+str(pol)+',\n'
    json += (tabChar*(tab+1))+'"exo": '+str(exo)+'\n'
    json += (tabChar*tab)+"}"
    return json

def generateFullJson(array, nNodes = 5, tab = 0, tabChar = "  "):
    json = "{\n"
    itc = findAllInhibitorsAndConcs(array,nNodes = nNodes)
    json += generateAllNodes(array, itc, nNodes = nNodes, tab = tab + 1, tabChar = tabChar)
    json += ",\n"
    json += generateAllConnections(array,itc,nNodes = nNodes, tab = tab+1, tabChar = tabChar)
    json += ",\n"
    json += generateLegacyParameters(tab = tab+1, tabChar = tabChar)
    json += "\n}"
    return json

def submitPENSystem(array, nNodes = 5, executablePath = '../../bioneat', launchScript = 'build/dist/start.sh', 
        launchClass = 'use.EvaluateIndividualWithDescriptors', configFile = 'config/configEvaluation.config', baseDir = ".", jsonFileName = 'generatedGraph0_0_0.json'):
    starTime = timer()
    
    json = generateFullJson(array, nNodes)
    with open(jsonFileName,'w') as f:
        f.write(json)
        f.flush()
        os.fsync(f)
    #in case of the default jsonFileName
    if not os.sep in jsonFileName:
        #we are using the default file name
        jsonFileName = os.path.join(os.getcwd(),jsonFileName)
    exect = os.path.join(executablePath,launchScript)
    config = os.path.join(executablePath,configFile)
    command = [exect, launchClass, config , jsonFileName]
    try:
        result = subprocess.check_output(command, stderr=subprocess.STDOUT)
    #except subprocess.CalledProcessError as e:
    except Exception as e:
        warnings.warn("ERROR during DACCAD execution with command: %s" % str(command), RuntimeWarning)
        print(f"ERROR during DACCAD execution with command: {command}")
        traceback.print_exc()
        result = None
    end = timer()
    if result is not None:
        try:
            result2 = yaml.load(result, Loader=yaml.FullLoader)
            #print(result)
        except Exception as e:
            warnings.warn("ERROR parsing DACCAD result with command: %s" % str(command), RuntimeWarning)
            print(f"ERROR parsing DACCAD result with command: {command}")
            print(f" and with result: {result}")
            traceback.print_exc()
            result2 = None
        return result2
    else:
        return None


class DACCADLauncher:
    def __init__(self, executablePath = '../../bioneat', launchScript = 'build/dist/start.sh', 
            launchClass = 'use.EvaluateIndividualWithMutationSuggestions', configFile = 'config/configEvaluation.config',
            baseDir = "."):
        self.executablePath = executablePath
        self.launchScript = launchScript
        self.launchClass = launchClass
        self.configFile = configFile
        self.baseDir = baseDir
        self._q = Queue()
        self._stop_t = False

    def _enqueue_output(self, out):
        for line in iter(out.readline, b''):
            if self._stop_t:
                break
            self._q.put(line)
        out.close()

    def _send_cmd(self, cmd):
        self.popen.stdin.write(cmd + "\n")
        self.popen.stdin.flush()

    def start(self, ind, nNodes, jsonFileName):
        self._create_json(ind, nNodes, jsonFileName)
        exect = os.path.join(self.executablePath, self.launchScript)
        config = os.path.join(self.executablePath, self.configFile)
        cmd = [exect, self.launchClass, config, jsonFileName]
        self.popen = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True, bufsize=1, close_fds=True)
        self._t = Thread(target=self._enqueue_output, args=(self.popen.stdout,))
        self._t.daemon = True
        self._t.start()
        return self._get_yaml_from_str()

    def stop(self):
        self._send_cmd("exit")
        self._stop_t = True
        #self._t.stop()
        #self.popen.wait() # XXX Timeout ?

    def _create_json(self, ind, nNodes, jsonFileName):
        json = generateFullJson(ind, nNodes)
        with open(jsonFileName, 'w') as f:
            f.write(json)
            f.flush()
            os.fsync(f)
        # In case of the default jsonFileName
        if not os.sep in jsonFileName:
            # We are using the default file name
            jsonFileName = os.path.join(os.getcwd(),jsonFileName)

    def _wait_for_answer(self):
        res = ""
        while True: # XXX exception if process is finished
            #line = self.popen.stdout.readline()
            line = self._q.get()
            if "DONE" in line:
                break
            else:
                res += line + "\n"
        return res

    def _get_yaml_from_str(self):
        res = self._wait_for_answer()
        if len(res) == 0:
            return None
        else:
            try:
                res_yaml = yaml.load(res, Loader=yaml.FullLoader)
            except Exception as e:
                warnings.warn(f"ERROR parsing DACCAD result: {res}", RuntimeWarning)
                traceback.print_exc()
                res_yaml = None
            return res_yaml

    def eval(self, ind, nNodes, jsonFileName, duration = 10):
        self._create_json(ind, nNodes, jsonFileName)
        self._send_cmd(f"{jsonFileName} {duration}")
        #return self._get_yaml_from_str()

        res = self._get_yaml_from_str()
        #print(f"SUGG ! '{jsonFileName} {duration}': {res}")
        return res



# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
