#!/bin/sh

imageName=${1:-daccadqd:latest}
#sshKeyFile=${1:-~/.ssh/docker_deploy_id_rsa}
#imageName=${2:-daccadqd:latest}
#if [ ! -f $sshKeyFile ]; then
#	echo Please specify the private SSH key file used to access bitbucket git repositories
#	read sshKeyFile
#fi
#echo Using SSH key \'$sshKeyFile\'...

inDockerGroup=`id -Gn | grep docker`
if [ -z "$inDockerGroup" ]; then
	sudoCMD="sudo"
else
	sudoCMD=""
fi

#$sudoCMD docker build --no-cache -t $imageName . --build-arg SSH_PRIVATE_KEY="`cat $sshKeyFile`"
# Parameter "--network host" necessary on some hosts computers (cf https://github.com/gliderlabs/docker-alpine/issues/307)
$sudoCMD docker build --network host --no-cache -t $imageName .

